# frag.jetzt Backend

Nomen est omen: The app's name says it all: it stands for both the app's main purpose and the web address [frag.jetzt](https://frag.jetzt/).

frag.jetzt consists of a variety of services. Starting them all individually or installing them on the computer is complex, which is why there is a docker compose solution for this.

This repository contains only the Backend for the application. The other components can be found here:
- [frag.jetzt frontend](https://git.thm.de/arsnova/frag.jetzt)
- [frag.jetzt websocket gateway](https://git.thm.de/arsnova/arsnova-ws-gateway)
- [frag.jetzt docker orchestration](https://git.thm.de/arsnova/frag.jetzt-docker-orchestration)

## Setup

### Prerequisite

The following software has to be installed on your computer:

1. GNU/Linux compliant operating system, for example:
    1. Debian based: Debian, Ubuntu, Mint, ...
    2. Arch based: Arch, Manjaro, ...
    3. Red Hat based: Red Hat, RHEL, Fedora, CentOS, ...
    4. ...
2. Docker
    1. [Installation Instructions](https://docs.docker.com/engine/install/)
    2. [Docker Reference](https://docs.docker.com/reference/)
3. Docker Compose
    1. [Installation Instructions](https://docs.docker.com/compose/install/)
    2. [Docker Compose Reference](https://docs.docker.com/compose/reference/)

### Get the code base

Clone the frag.jetzt-backend repository and the Docker Orchestration repository:
* [frag.jetzt-backend](https://git.thm.de/arsnova/frag.jetzt-backend)
* [Docker Orchestration](https://git.thm.de/arsnova/frag.jetzt-docker-orchestration)

### Start the Backend services

Follow the steps described in the [Docker Orchestration repository](https://git.thm.de/arsnova/frag.jetzt-docker-orchestration).

Use the `--no-backend` option so that the backup does not spin up in Docker Orchestration.

### Start the Backend

The frag.jetzt backend ships with an easy setup script: `.docker/setup.sh`. To run the setup script, make sure it is executable. If it is not, make it executable:

```bash
chmod u+x .docker/setup.sh
```

Now, run the setup script:

```bash
./.docker/setup.sh
```

You may now start the backend. Use following commands:

```bash
# Start the app in foreground (not recommended)
docker-compose up

# Start the app in background
docker-compose up -d

# Show and follow the logs
docker-compose logs -f

# Shutdown app
docker-compose down

# Shutdown app and remove volumes
docker-compose down -v
```

## Configuration

You can find the current default application.yml inside the folder `/src/main/resources/application.yml`.

Inside this file you can find every supported configuration parameter of the current version. This information is important to create your custom setup. If you want to override properties you have to take care of the indentation of the attribute. Be careful when changing attributes under the spring namespace, this could crash the whole application.

You may override these values with corresponding environment variables in your Docker Compose setup.

## Code style analysis
For a local code style analysis with docker-compose you'll need docker and docker-compose installed.
To run a local code style check with sonarqube, follow these steps:

1. switch into the analysis folder  
  `cd analysis`
2. start the sonarqube server  
  `docker-compose up -d sonarqube`
3. when sonarqube has started, you may run analysis whenever you want with  
  `docker-compose run --rm analysis`

## Docker cheat sheet
Make sure you are logged into the registry (`docker login`)

* Build image: `docker build -t fragjetzt/backend .`
* Tag image: `docker tag fragjetzt/backend latest`
* Push image: `docker push fragjetzt/backend:latest`
