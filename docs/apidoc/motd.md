# Motd Routes

## Find current active Motd or old Motds by date
```http
POST /motds/find
```

**Needed rights:** valid JWT for authorization.

**Request body**:

| Field     | Type               | Description                                |
|-----------|--------------------|--------------------------------------------|
| findQuery | FindQuery<MOT> | Query information for specification  |

FindQuery Object:

| Field           | Type                | Description                                           |
|-----------------|---------------------|-------------------------------------------------------|
| externalFilters | Map<String, Object> | Here are 2 keys used for the map. `before` to query old Motds and `activeAt` to query current active Motds. Both have a UNIX Timestamp as value. |


**Success 200**:

The result is an array of user objects with the following attributes:

| Field          | Type      | Description                                         |
|----------------|-----------|-----------------------------------------------------|
| id             | UUID      | ID of the user                                      |
| msgGerman      | String    | Motd in german                                      |
| msgEnglish     | String    | Motd in english                                     |
| startTimestamp | Timestamp | Timestamp of the start, where the Motd comes active |
| endTimestamp   | Timestamp | Timestamp of the end, where the Motd comes inactive |

**Error 4xx**:

| Field        | Description                          |
|--------------|--------------------------------------|
| Unauthorized | Access denied when no JWT is present |
