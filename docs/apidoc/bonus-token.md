# BonusToken Routes

These routes are only for interacting with the tokens. The tokens are created and also deleted when a comment is updated and the favorite attribute changes.

## Delete BonusToken by room or single tokens
```http
DELETE /bonustoken/deleteby
```

**Needed rights:** valid JWT for authorization and needs creator or moderator rights

**Request params**:

| Field     | Type | Description                                                     |
|-----------|------|-----------------------------------------------------------------|
| roomId    | UUID | If commentId and userId are set, delete tokens with this roomId |
| commentId | UUID | commentId of the comment, the token relates to                  |
| userId    | UUID | userId of token owner                                           |

**Success 200**:
Empty Response


**Error 4xx**:

| Field        | Description                                  |
|--------------|----------------------------------------------|
| Unauthorized | Access denied when no JWT is present         |
| Forbidden    | User is not moderator or creator of the room |
| NotFound     | Comment not found                            |


---
## Find BonusToken by roomId or accountId
```http
POST /bonustoken/find
```

**Needed rights:** valid JWT for authorization.

**Request body**:

| Field     | Type                  | Description                                              |
|-----------|-----------------------|----------------------------------------------------------|
| findQuery | FindQuery<BonusToken> | Query information like the provided roomId or accountId  |

FindQuery Object:

| Field      | Type         | Description                                                             |
|------------|--------------|-------------------------------------------------------------------------|
| properties | BonusToken   | BonusToken details to query for. Here this is the roomId and accountId  |


**Success 200**:

The result is an array of BonusToken objects with the following attributes:

| Field     | Type      | Description                    |
|-----------|-----------|--------------------------------|
| id        | UUID      | ID of the token                |
| roomId    | UUID      | ID of the related room         |
| accountId | UUID      | ID of the related account      |
| token     | String    | The token key                  |
| timestamp | Timestamp | Timestamp of the creation time |

**Error 4xx**:

| Field        | Description                          |
|--------------|--------------------------------------|
| Unauthorized | Access denied when no JWT is present |
