# List of all Triggers and Functions

Triggers should only be created when a performance or ACID trade off is important.
These trade offs can be for example the possibility to calculate a comment number based on all existing comment numbers.

For example when creating objects based on other objects, these should be handled programmatically to allow more dynamic behaviour and context.

An example for ACID is the created_at and updated_at in each object. To ensure, that created_at is not changed and updated_at will get set each time when something updates.

To specify the usage and possible side effects, all functions and triggers are listed here.

With function
```sql
SELECT
    routine_name
FROM 
    information_schema.routines
WHERE 
    routine_type = 'FUNCTION'
AND
    routine_schema = 'public';
```

this results in:

```sql
 trigger_comment_upvotes -- for comment, calculate upvotes
 trigger_comment_downvotes -- for comment, calculate downvotes
 trigger_brainstorming_vote_func -- for brainstorming_word, calculate upvotes & downvotes
 trigger_timestamp_create_update_func -- for nearly all relations, created_at and updated_at
 trigger_ensure_unique_days -- for comment notification, per email (max 7 days)
 trigger_calculate_comment_depth_func -- for comment, calculate comment_depth 
 increase_comment_number -- for comment, calculate number
```