package de.thm.arsnova.frag.jetzt.backend;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import de.thm.arsnova.frag.jetzt.backend.handler.CommentCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.Settings;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateComment;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateCommentPayload;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.BonusTokenService;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.notification.handler.CommentChangeHandler;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import reactor.core.publisher.Mono;

@DirtiesContext
@SpringBootTest
public class CommentCommandHandlerTest {

  @MockBean
  private AmqpTemplate messagingTemplate;

  @MockBean
  private CommentService commentService;

  @MockBean
  private BonusTokenService bonusTokenService;

  @MockBean
  private RoomService roomService;

  @MockBean
  private CommentChangeHandler commentChangeHandler;

  @MockBean
  private AuthorizationHelper authorizationHelper;

  private CommentCommandHandler commandHandler;

  @BeforeAll
  public void setup() {
    commandHandler =
      new CommentCommandHandler(
        authorizationHelper,
        messagingTemplate,
        commentService,
        bonusTokenService,
        roomService,
        commentChangeHandler
      );
  }

  @Test
  public void testShouldHandleCreateComment() {
    // Arrange
    UUID roomId = UUID.fromString("032f8beb-e089-46b0-811c-533c8d3e72fd");
    UUID creatorId = UUID.fromString("2d9782fd-2cc8-4bec-b85e-dfd3828668b6");
    Comment newComment = new Comment();
    newComment.setCreatorId(creatorId);
    newComment.setRoomId(roomId);
    newComment.setBody("body");
    CreateCommentPayload payload = new CreateCommentPayload(newComment);
    CreateComment command = new CreateComment(payload);

    ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<CreateComment> messageCaptor = ArgumentCaptor.forClass(
      CreateComment.class
    );
    ArgumentCaptor<Comment> commentCaptor = ArgumentCaptor.forClass(
      Comment.class
    );
    ArgumentCaptor<UUID> roomIdCaptor = ArgumentCaptor.forClass(UUID.class);

    Settings settings = new Settings();
    settings.setRoomId(roomId);
    settings.setDirectSend(true);
    //when(roomService.get(roomId)).thenReturn(Mono.empty());
    //when(commentService.create(any())).thenReturn(Mono.just(newComment));

    // Act
    // Mono<Comment> savedComment = commandHandler.handle(command);

    //Assert
    /*savedComment.subscribe(result -> {
            verify(roomService, times(1)).get(roomIdCaptor.capture());
            verify(commentService, times(1)).create(commentCaptor.capture());
            verify(messagingTemplate, times(1)).convertAndSend(topicCaptor.capture(), topicCaptor.capture(), messageCaptor.capture());
            assertThat(roomIdCaptor.getValue()).isEqualTo(roomId);
            assertThat(topicCaptor.getValue()).isEqualTo(roomId + ".comment.stream");
        });*/
  }

  @Test
  public void testShouldHandleDeleteComment() {
    /*UUID id = UUID.fromString("032f8beb-e089-46b0-811c-533c8d3e72fd");
        UUID roomId = UUID.fromString("2d9782fd-2cc8-4bec-b85e-dfd3828668b6");
        Comment c = new Comment();
        c.setId(id);
        c.setRoomId(roomId);
        when(commentService.get(id)).thenReturn(Mono.just(c));
        DeleteCommentPayload payload = new DeleteCommentPayload(id);
        DeleteComment command = new DeleteComment(payload);

        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<CommentDeleted> eventCaptor =
                ArgumentCaptor.forClass(CommentDeleted.class);

        when(commentService.get(id)).thenReturn(Mono.just(c));

        Mono<Comment> deletedComment = commandHandler.handle(command);

        CommentDeletedPayload p = new CommentDeletedPayload();
        p.setId(id);
        CommentDeleted expectedEvent = new CommentDeleted(p, roomId);

        deletedComment.subscribe(result -> {
            verify(commentService, times(1)).get(id);
            verify(commentService, times(1)).delete(id);
            verify(messagingTemplate, times(1)).convertAndSend(
                    topicCaptor.capture(),
                    keyCaptor.capture(),
                    eventCaptor.capture()
            );

            assertThat(keyCaptor.getValue()).isEqualTo(roomId + ".comment.stream");
            assertThat(eventCaptor.getValue()).isEqualTo(expectedEvent);
        });*/
  }
}
