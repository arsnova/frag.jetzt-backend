package de.thm.arsnova.frag.jetzt.gpt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomKey;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomModel;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomModelRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomSettingRepository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

@Service
public class GPTRoomSettingService {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private final GPTRoomSettingRepository repository;
  private final GPTRoomKeyService keyService;
  private final GPTRoomModelRepository modelRepository;
  private final RoomService roomService;
  private final AuthorizationHelper authHelper;
  private final QuotaManageService quotaManageService;

  @Autowired
  public GPTRoomSettingService(
    GPTRoomSettingRepository repository,
    GPTRoomKeyService keyService,
    GPTRoomModelRepository modelRepository,
    RoomService roomService,
    AuthorizationHelper authHelper,
    QuotaManageService quotaManageService
  ) {
    this.repository = repository;
    this.keyService = keyService;
    this.modelRepository = modelRepository;
    this.roomService = roomService;
    this.authHelper = authHelper;
    this.quotaManageService = quotaManageService;
  }

  public Mono<GPTRoomSetting> getByRoomId(UUID roomId) {
    return Mono
      .zip(authHelper.getCurrentUser(), roomService.get(roomId))
      .switchIfEmpty(Mono.error(new BadRequestException("Room not found")))
      .map(tuple ->
        Tuples.of(
          authHelper.checkCreatorOfRoom(tuple.getT1(), tuple.getT2()),
          authHelper.checkOnlyModeratorOfRoom(tuple.getT1(), tuple.getT2())
        )
      )
      .filter(tuple -> tuple.getT1() || tuple.getT2())
      .switchIfEmpty(Mono.error(new ForbiddenException("Not allowed")))
      .flatMap(e -> get(roomId, true));
  }

  public Mono<GPTRoomSetting> get(UUID roomId, boolean internal) {
    return repository
      .findByRoomId(roomId)
      .flatMap(setting -> this.applyTransient(setting, internal));
  }

  public Mono<Quota> patchParticipantQuota(
    UUID roomId,
    UUID quotaId,
    Map<String, Object> map
  ) {
    return ensureParticipantQuota(roomId, quotaId)
      .flatMap(setting -> quotaManageService.patch(quotaId, map));
  }

  public Mono<Quota> patchModeratorQuota(
    UUID roomId,
    UUID quotaId,
    Map<String, Object> map
  ) {
    return ensureModeratorQuota(roomId, quotaId)
      .flatMap(setting -> quotaManageService.patch(quotaId, map));
  }

  public Mono<Quota> patchRoomQuota(
    UUID roomId,
    UUID quotaId,
    Map<String, Object> map
  ) {
    return ensureRoomQuota(roomId, quotaId)
      .flatMap(setting -> quotaManageService.patch(quotaId, map));
  }

  public Mono<GPTRoomSetting> patch(UUID roomId, Map<String, Object> patch) {
    return repository
      .findByRoomId(roomId)
      .switchIfEmpty(Mono.error(new BadRequestException("Setting not found")))
      .flatMap(setting -> patch(setting, patch));
  }

  public Mono<GPTRoomSetting> patch(
    GPTRoomSetting roomSetting,
    Map<String, Object> patch
  ) {
    return Mono
      .zip(
        authHelper.getCurrentUser(),
        roomService.get(roomSetting.getRoomId())
      )
      .switchIfEmpty(Mono.error(new BadRequestException("Room not found")))
      .map(tuple ->
        Tuples.of(
          authHelper.checkCreatorOfRoom(tuple.getT1(), tuple.getT2()),
          authHelper.checkOnlyModeratorOfRoom(tuple.getT1(), tuple.getT2())
        )
      )
      .filter(tuple -> tuple.getT1() || tuple.getT2())
      .switchIfEmpty(Mono.error(new ForbiddenException("Not allowed")))
      .map(tuple -> parseChanges(roomSetting, patch, tuple.getT1()))
      .flatMap(setting -> {
        Mono<?> start = Mono.just(true);
        if (patch.containsKey("apiKeys")) {
          start =
            start.then(
              keyService
                .patchAllBySettingId(
                  roomSetting.getId(),
                  (List<Map<String, ?>>) patch.get("apiKeys")
                )
                .then()
            );
        }
        if (patch.containsKey("apiModels")) {
          start =
            start.then(
              this.patchModels(
                  roomSetting.getId(),
                  (List<Map<String, ?>>) patch.get("apiModels")
                )
                .then()
            );
        }
        return start.then(repository.save(setting));
      })
      .flatMap(setting -> this.applyTransient(setting, true));
  }

  private Mono<GPTRoomSetting> ensureParticipantQuota(
    UUID roomId,
    UUID quotaId
  ) {
    return ensureQuota(
      roomId,
      quotaId,
      GPTRoomSettingRight.CHANGE_PARTICIPANT_QUOTA,
      GPTRoomSetting::getParticipantQuotaId
    );
  }

  private Mono<GPTRoomSetting> ensureModeratorQuota(UUID roomId, UUID quotaId) {
    return ensureQuota(
      roomId,
      quotaId,
      GPTRoomSettingRight.CHANGE_MODERATOR_QUOTA,
      GPTRoomSetting::getModeratorQuotaId
    );
  }

  private Mono<GPTRoomSetting> ensureRoomQuota(UUID roomId, UUID quotaId) {
    return ensureQuota(
      roomId,
      quotaId,
      GPTRoomSettingRight.CHANGE_ROOM_QUOTA,
      GPTRoomSetting::getRoomQuotaId
    );
  }

  private Mono<GPTRoomSetting> ensureQuota(
    UUID roomId,
    UUID quotaId,
    GPTRoomSettingRight right,
    Function<GPTRoomSetting, UUID> quotaMapper
  ) {
    return Mono
      .zip(
        authHelper.getCurrentUser(),
        repository.findByRoomId(roomId),
        roomService.get(roomId)
      )
      .filter(tuple ->
        authHelper.checkCreatorOfRoom(tuple.getT1(), tuple.getT3()) ||
        (
          authHelper.checkOnlyModeratorOfRoom(tuple.getT1(), tuple.getT3()) &&
          right.isRightSet(tuple.getT2().getRightsBitset())
        )
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .filter(tuple -> Objects.equals(quotaMapper.apply(tuple.getT2()), quotaId)
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Quota does not match id"))
      )
      .map(tuple -> tuple.getT2());
  }

  private Flux<GPTRoomModel> patchModels(
    UUID settingId,
    List<Map<String, ?>> models
  ) {
    return modelRepository
      .deleteAllBySettingId(settingId)
      .thenMany(Flux.fromIterable(models))
      .map(model -> MAPPER.convertValue(model, GPTRoomModel.class))
      .map(e -> {
        e.setSettingId(settingId);
        return e;
      })
      .flatMap(model -> this.modelRepository.save(model));
  }

  private GPTRoomSetting parseChanges(
    GPTRoomSetting s,
    Map<String, Object> patch,
    boolean isOwner
  ) {
    final int rights = s.getRightsBitset();
    patch.forEach((key, value) -> {
      switch (key) {
        case "rightsBitset":
          if (!isOwner) {
            throw new ForbiddenException(
              "You are not allowed to change the rightsBitset"
            );
          }
          s.setRightsBitset((Integer) value);
          break;
        case "presetContext":
          s.setPresetContext((String) value);
          break;
        case "presetLength":
          s.setPresetLength((String) value);
          break;
        case "roleInstruction":
          s.setRoleInstruction((String) value);
          break;
        case "defaultModel":
          s.setDefaultModel((String) value);
          break;
        case "apiKeys":
          if (
            !isOwner && !GPTRoomSettingRight.CHANGE_API_KEY.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the apiKey"
            );
          }
          break;
        case "apiModels":
          s.setApiModels((List<GPTRoomModel>) value);
          break;
        case "id":
        case "roomId":
        case "paymentCounter":
        case "createdAt":
        case "updatedAt":
          throw new ForbiddenException(
            "You are not allowed to change the " + key
          );
        case "participantQuotaId":
        case "moderatorQuotaId":
        case "roomQuotaId":
          throw new BadRequestException("Wrong Endpoint");
        default:
          throw new BadRequestException(
            "Invalid ChangeAttribute provided (" + key + ")"
          );
      }
    });
    return s;
  }

  private Mono<GPTRoomSetting> applyTransient(
    GPTRoomSetting s,
    boolean internal
  ) {
    Mono<Void> start = Mono
      .zip(
        keyService.getAllBySettingId(s.getId(), internal).collectList(),
        modelRepository.findAllBySettingId(s.getId()).collectList()
      )
      .flatMap(tuple -> {
        tuple
          .getT1()
          .sort(
            new Comparator<GPTRoomKey>() {
              @Override
              public int compare(GPTRoomKey a, GPTRoomKey b) {
                return Integer.compare(a.getIndex(), b.getIndex());
              }
            }
          );
        s.setApiKeys(tuple.getT1());
        s.setApiModels(tuple.getT2());
        s.setGlobalQuota(
          GPTConfig.getInstance().getRestrictions().getGlobalQuota()
        );
        return Mono.empty();
      });
    return start.thenReturn(s);
  }
}
