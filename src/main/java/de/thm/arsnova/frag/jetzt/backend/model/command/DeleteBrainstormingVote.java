package de.thm.arsnova.frag.jetzt.backend.model.command;

public class DeleteBrainstormingVote extends WebSocketCommand<DeleteBrainstormingVotePayload> {

    public DeleteBrainstormingVote() {
        super(DeleteBrainstormingVote.class.getSimpleName());
    }

    public DeleteBrainstormingVote(DeleteBrainstormingVotePayload payload) {
        super(DeleteBrainstormingVote.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteBrainstormingVote that = (DeleteBrainstormingVote) o;
        return this.getPayload().equals(that.getPayload());
    }
}
