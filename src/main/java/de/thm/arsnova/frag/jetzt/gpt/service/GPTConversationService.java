package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversation;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversationEntry;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTConversationEntryRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTConversationRepository;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class GPTConversationService {

  private final AuthorizationHelper helper;
  private final GPTConversationRepository repository;
  private final GPTConversationEntryRepository entryRepository;

  @Autowired
  public GPTConversationService(
    AuthorizationHelper helper,
    GPTConversationRepository repository,
    GPTConversationEntryRepository entryRepository
  ) {
    this.helper = helper;
    this.repository = repository;
    this.entryRepository = entryRepository;
  }

  public Mono<Void> deleteEntries(UUID conversationId, int index) {
    return ensureOwnConversation(conversationId)
      .flatMap(tuple ->
        entryRepository
          .deleteAllByConversationIdAndIndexGreaterThanEqual(
            tuple.getT1().getId(),
            index
          )
          .then()
      );
  }

  public Mono<GPTConversationEntry> addEntry(GPTConversationEntry entry) {
    return ensureOwnConversation(entry.getConversationId())
      .flatMap(tuple ->
        entryRepository
          .countByConversationId(entry.getConversationId())
          .flatMap(l -> {
            entry.setId(null);
            entry.setIndex(l.intValue());
            return entryRepository.save(entry);
          })
      );
  }

  public Mono<GPTConversation> patch(
    UUID conversationId,
    Map<String, Object> changes
  ) {
    return ensureOwnConversation(conversationId)
      .map(tuple -> this.parseChanges(tuple.getT1(), changes))
      .flatMap(repository::save)
      .flatMap(this::applyTransient);
  }

  public Mono<GPTConversation> update(GPTConversation c) {
    if (c.getId() != null) {
      c
        .getMessages()
        .forEach(m -> {
          m.setConversationId(c.getId());
        });
      return ensureOwnConversation(c.getId())
        .flatMap(tuple -> {
          return deleteEntries(c.getId(), 0)
            .then(
              entryRepository.saveAll(c.getMessages()).then(repository.save(c))
            );
        })
        .flatMap(this::applyTransient)
        .onErrorResume(
          NotFoundException.class,
          a -> {
            c.setId(null);
            return update(c);
          }
        );
    }
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        c.setAccountId(user.getAccountId());
        return repository.save(c);
      })
      .flatMap(conv -> {
        c
          .getMessages()
          .forEach(m -> {
            m.setConversationId(conv.getId());
          });
        return entryRepository.saveAll(c.getMessages()).then(Mono.just(conv));
      })
      .flatMap(this::applyTransient);
  }

  public Mono<Void> delete(UUID conversationId) {
    return ensureOwnConversation(conversationId)
      .flatMap(tuple -> repository.deleteById(conversationId));
  }

  public Flux<GPTConversation> getAll() {
    return helper
      .getCurrentUser()
      .flatMapMany(user -> repository.findAllByAccountId(user.getAccountId()))
      .flatMap(this::applyTransient);
  }

  private Mono<Tuple2<GPTConversation, AuthenticatedUser>> ensureOwnConversation(
    UUID conversationId
  ) {
    return Mono
      .zip(repository.findById(conversationId), helper.getCurrentUser())
      .switchIfEmpty(
        Mono.error(new NotFoundException("Conversation does not exist!"))
      )
      .filter(tuple ->
        tuple.getT2().getAccount().hasRole(AccountRole.ADMIN_ALL_ROOMS_OWNER) ||
        tuple.getT1().getAccountId().equals(tuple.getT2().getAccountId())
      )
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("You can not mutate others conversation!")
        )
      );
  }

  private GPTConversation parseChanges(
    GPTConversation conversation,
    Map<String, Object> changes
  ) throws ClassCastException {
    changes.forEach((key, value) -> {
      switch (key) {
        case "model":
          conversation.setModel((String) value);
          break;
        case "temperature":
          conversation.setTemperature((Float) value);
          break;
        case "topP":
          conversation.setTopP((Float) value);
          break;
        case "presencePenalty":
          conversation.setPresencePenalty((Float) value);
          break;
        case "frequencyPenalty":
          conversation.setFrequencyPenalty((Float) value);
          break;
        case "id":
        case "accountId":
        case "roomId":
        case "createdAt":
        case "updatedAt":
          throw new ForbiddenException(
            "You are not allowed to change these values"
          );
        default:
          throw new BadRequestException("Invalid ChangeAttribute provided");
      }
    });
    return conversation;
  }

  private Mono<GPTConversation> applyTransient(GPTConversation c) {
    return entryRepository
      .findAllByConversationId(c.getId())
      .collectSortedList((a, b) -> Integer.compare(a.getIndex(), b.getIndex()))
      .map(list -> {
        c.setMessages(list);
        return c;
      });
  }
}
