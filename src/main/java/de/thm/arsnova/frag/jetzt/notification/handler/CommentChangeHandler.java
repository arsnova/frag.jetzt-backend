package de.thm.arsnova.frag.jetzt.notification.handler;

import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import de.thm.arsnova.frag.jetzt.notification.service.CommentChangeService;
import de.thm.arsnova.frag.jetzt.notification.service.WebNotificationService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CommentChangeHandler {

  private final AuthorizationHelper authorizationHelper;
  private final CommentChangeService commentChangeService;
  private final RoomService roomService;
  private final WebNotificationService notificationService;
  private final CommentService commentService;

  @Autowired
  public CommentChangeHandler(
    AuthorizationHelper authorizationHelper,
    CommentChangeService commentChangeService,
    RoomService roomService,
    WebNotificationService notificationService,
    CommentService commentService
  ) {
    this.authorizationHelper = authorizationHelper;
    this.commentChangeService = commentChangeService;
    this.roomService = roomService;
    this.notificationService = notificationService;
    this.commentService = commentService;
  }

  public Mono<Void> deleteAndEmit(
    Comment current,
    AuthenticatedUser initiator
  ) {
    return roomService
      .get(current.getRoomId())
      .flatMap(room ->
        commentChangeService
          .create(
            new CommentChange(
              current.getId(),
              current.getRoomId(),
              CommentInterestBit.DELETED,
              null,
              null,
              initiator.getAccountId(),
              getUserInformation(room, initiator),
              current.getCreatorId()
            )
          )
          .map(change -> {
            change.setComment(current);
            change.setRoom(room);
            return change;
          })
          .flatMap(notificationService::sendNotification)
      );
  }

  public Mono<Void> createAndEmit(
    Comment current,
    AuthenticatedUser initiator
  ) {
    return roomService
      .get(current.getRoomId())
      .flatMap(room ->
        createAndEmit(
          current,
          room,
          initiator.getAccountId(),
          getUserInformation(room, initiator)
        )
      );
  }

  public Mono<Void> createAndEmit(
    Comment current,
    Room room,
    UUID initiatorId,
    CommentChange.UserRole role
  ) {
    return commentChangeService
      .create(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.CREATED,
          null,
          null,
          initiatorId,
          role,
          current.getCreatorId()
        )
      )
      .map(change -> {
        change.setComment(current);
        change.setRoom(room);
        return change;
      })
      .flatMap(change -> {
        UUID reference = change.getComment().getCommentReference();
        if (reference != null) {
          return notificationService
            .sendNotification(change)
            .then(
              commentService
                .get(reference)
                .flatMap(comment ->
                  answerAndEmit(
                    change.getRoom(),
                    comment,
                    change.getCommentId(),
                    initiatorId,
                    role
                  )
                )
            );
        }
        return notificationService.sendNotification(change);
      })
      .then();
  }

  public Mono<Void> patchAndEmit(
    Comment previous,
    Comment current,
    AuthenticatedUser initiator
  ) {
    List<CommentChange> changes = new ArrayList<>();
    if (previous.isAck() != current.isAck()) {
      changes.add(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.CHANGE_ACK,
          previous.isAck() ? "1" : "0",
          current.isAck() ? "1" : "0",
          initiator.getAccountId(),
          null,
          current.getCreatorId()
        )
      );
    }
    if (previous.isFavorite() != current.isFavorite()) {
      changes.add(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.CHANGE_FAVORITE,
          previous.isFavorite() ? "1" : "0",
          current.isFavorite() ? "1" : "0",
          initiator.getAccountId(),
          null,
          current.getCreatorId()
        )
      );
    }
    if (previous.getCorrect() != current.getCorrect()) {
      changes.add(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.CHANGE_CORRECT,
          String.valueOf(previous.getCorrect()),
          String.valueOf(current.getCorrect()),
          initiator.getAccountId(),
          null,
          current.getCreatorId()
        )
      );
    }
    if (!Objects.equals(previous.getTag(), current.getTag())) {
      changes.add(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.CHANGE_TAG,
          previous.getTag(),
          current.getTag(),
          initiator.getAccountId(),
          null,
          current.getCreatorId()
        )
      );
    }
    if (changes.size() == 0) {
      return Mono.empty();
    }
    return roomService
      .get(current.getRoomId())
      .flatMap(room -> {
        CommentChange.UserRole role = getUserInformation(room, initiator);
        changes.forEach(change -> change.setInitiatorRole(role));
        return commentChangeService
          .createAll(changes)
          .map(change -> {
            change.setComment(current);
            change.setRoom(room);
            return change;
          })
          .flatMap(notificationService::sendNotification)
          .then();
      });
  }

  public Mono<Void> changeScoreAndEmit(
    Comment previous,
    AuthenticatedUser initiator
  ) {
    return Mono
      .zip(
        roomService.get(previous.getRoomId()),
        commentService.get(previous.getId())
      )
      .flatMap(tuple -> {
        Room room = tuple.getT1();
        Comment current = tuple.getT2();
        return commentChangeService
          .create(
            new CommentChange(
              current.getId(),
              current.getRoomId(),
              CommentInterestBit.CHANGE_SCORE,
              previous.getScore() +
              "/" +
              previous.getUpvotes() +
              "/" +
              previous.getDownvotes(),
              current.getScore() +
              "/" +
              current.getUpvotes() +
              "/" +
              current.getDownvotes(),
              initiator.getAccountId(),
              getUserInformation(room, initiator),
              current.getCreatorId()
            )
          )
          .map(change -> {
            change.setComment(current);
            change.setRoom(room);
            return change;
          });
      })
      .flatMap(notificationService::sendNotification)
      .then();
  }

  private Mono<Void> answerAndEmit(
    Room room,
    Comment current,
    UUID childId,
    UUID initiatorId,
    CommentChange.UserRole role
  ) {
    return commentChangeService
      .create(
        new CommentChange(
          current.getId(),
          current.getRoomId(),
          CommentInterestBit.ANSWERED,
          null,
          childId.toString(),
          initiatorId,
          role,
          current.getCreatorId()
        )
      )
      .map(change -> {
        change.setComment(current);
        change.setRoom(room);
        return change;
      })
      .flatMap(notificationService::sendNotification)
      .then();
  }

  private CommentChange.UserRole getUserInformation(
    Room room,
    AuthenticatedUser user
  ) {
    for (RoomAccess access : user.getRoomAccesses()) {
      if (access.getRoomId().equals(room.getId())) {
        return CommentChange.UserRole.fromRole(access.getRole());
      }
    }
    return authorizationHelper.checkCreatorOfRoom(user, room)
      ? CommentChange.UserRole.CREATOR
      : CommentChange.UserRole.PARTICIPANT;
  }
}
