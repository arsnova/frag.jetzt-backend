package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class GPTRoomPresetTopic implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID settingId;
  private String description;
  private boolean active;
  // meta information
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public GPTRoomPresetTopic() {}

  public GPTRoomPresetTopic(UUID settingId, String description, boolean active) {
    this.settingId = settingId;
    this.description = description;
    this.active = active;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getSettingId() {
    return settingId;
  }

  public void setSettingId(UUID settingId) {
    this.settingId = settingId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((settingId == null) ? 0 : settingId.hashCode());
    result =
      prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + (active ? 1231 : 1237);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomPresetTopic other = (GPTRoomPresetTopic) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (settingId == null) {
      if (other.settingId != null) return false;
    } else if (!settingId.equals(other.settingId)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (active != other.active) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomPresetTopic [id=" +
      id +
      ", settingId=" +
      settingId +
      ", description=" +
      description +
      ", active=" +
      active +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
