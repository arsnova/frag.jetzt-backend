package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.handler.CommentCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.ImportComment;
import de.thm.arsnova.frag.jetzt.backend.model.RoomQuestionCounts;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.service.CommentFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

@RestController("CommentController")
@RequestMapping(CommentController.REQUEST_MAPPING)
public class CommentController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(CommentController.class);

    protected static final String REQUEST_MAPPING = "/comment";
    private static final String IMPORT_MAPPING = "/import" + POST_MAPPING;
    private static final String BULK_DELETE_MAPPING = POST_MAPPING + "bulkdelete";
    private static final String DELETE_BY_ROOM_MAPPING = POST_MAPPING + "byRoom";
    private static final String HIGHLIGHT_MAPPING = DEFAULT_ID_MAPPING + "/highlight";
    private static final String LOWLIGHT_MAPPING = DEFAULT_ID_MAPPING + "/lowlight";

    private final CommentCommandHandler commandHandler;
    private final CommentService service;
    private final CommentFindQueryService findQueryService;

    @Autowired
    public CommentController(
            CommentCommandHandler commandHandler,
            CommentService service,
            CommentFindQueryService findQueryService
    ) {
        this.commandHandler = commandHandler;
        this.service = service;
        this.findQueryService = findQueryService;
    }

    @GetMapping(GET_MAPPING)
    public Mono<Comment> get(@PathVariable UUID id) {
        return service.get(id)
                      .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @PostMapping(POST_MAPPING)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Comment> post(
            @RequestBody final Comment comment
    ) {
        // convert into command for the command handler
        final CreateCommentPayload payload = new CreateCommentPayload(comment);
        final CreateComment command = new CreateComment(payload);

        return commandHandler.handle(command);
    }

    @PostMapping(IMPORT_MAPPING)
    public Flux<Comment> post(@RequestBody final List<ImportComment> comments) {
        final ImportCommentsPayload payload = new ImportCommentsPayload(comments);
        final ImportComments command = new ImportComments(payload);

        return commandHandler.handle(command);
    }

    @PutMapping(PUT_MAPPING)
    public Mono<Comment> put(@RequestBody final Comment entity) {
        UpdateCommentPayload p = new UpdateCommentPayload(entity);

        return this.commandHandler.handle(new UpdateComment(p));
    }

    @PostMapping(FIND_MAPPING)
    public Flux<Comment> find(@RequestBody final FindQuery<Comment> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

    @PostMapping(COUNT_MAPPING)
    public Flux<RoomQuestionCounts> findAndCount(@RequestBody final List<Comment> rooms) {
        return findQueryService.resolveCountForRooms(rooms);
    }

    @PatchMapping(PATCH_MAPPING)
    public Mono<Comment> patch(@PathVariable final UUID id, @RequestBody final Map<String, Object> changes) {
        PatchCommentPayload p = new PatchCommentPayload(id, changes);
        PatchComment command = new PatchComment(p);

        return this.commandHandler.handle(command);
    }

    @PostMapping(HIGHLIGHT_MAPPING)
    public Mono<Comment> highlight(@PathVariable final UUID id) {
        HighlightCommentPayload p = new HighlightCommentPayload();
        p.setId(id);
        p.setLights(true);
        return this.commandHandler.handle(new HighlightComment(p));
    }

    @PostMapping(LOWLIGHT_MAPPING)
    public Mono<Comment> lowlight(@PathVariable final UUID id) {
        HighlightCommentPayload p = new HighlightCommentPayload();
        p.setId(id);
        p.setLights(false);
        return this.commandHandler.handle(new HighlightComment(p));
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable UUID id) {
        DeleteCommentPayload p = new DeleteCommentPayload(id);
        DeleteComment command = new DeleteComment(p);

        return commandHandler.handle(command);
    }

    @PostMapping(BULK_DELETE_MAPPING)
    public Flux<Void> bulkDelete(
            @RequestBody final UUID[] ids
    ) {
        return Flux.fromIterable(Arrays.asList(ids))
                   .map(id -> new DeleteComment(new DeleteCommentPayload(id)))
                   .flatMap(commandHandler::handle);
    }

    @DeleteMapping(DELETE_BY_ROOM_MAPPING)
    public Mono<Object> deleteByRoom(
            @RequestParam final UUID roomId
    ) {
        DeleteCommentsByRoomPayload p = new DeleteCommentsByRoomPayload(roomId);
        DeleteCommentsByRoom command = new DeleteCommentsByRoom(p);

        return commandHandler.handle(command);
    }

}
