package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class LivepollSessionPatched
  extends WebSocketEvent<LivepollSessionPatchedPayload> {

  public LivepollSessionPatched() {
    super(LivepollSessionPatched.class.getSimpleName());
  }

  public LivepollSessionPatched(
    LivepollSessionPatchedPayload payload,
    UUID roomId
  ) {
    super(LivepollSessionPatched.class.getSimpleName(), roomId);
    this.payload = payload;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    LivepollSessionPatched that = (LivepollSessionPatched) o;
    return this.getPayload().equals(that.getPayload());
  }
}
