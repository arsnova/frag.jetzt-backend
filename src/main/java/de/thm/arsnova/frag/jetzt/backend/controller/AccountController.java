package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.service.AccountFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.AccountServiceBase;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("AccountController")
@RequestMapping("/user")
public class AccountController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    AccountController.class
  );

  protected static final String REQUEST_MAPPING = "/user";
  private static final String ROOM_HISTORY_MAPPING =
    DEFAULT_ID_MAPPING + "/roomHistory";
  private static final String ROOM_HISTORY_DELETE_MAPPING =
    ROOM_HISTORY_MAPPING + "/{roomId}";
  private static final String GET_ROOM_ACCESS_MAPPING =
    DEFAULT_ID_MAPPING + "/roomAccess";

  private final AccountService service;
  private final AccountServiceBase accountService;
  private final AccountFindQueryService findQueryService;
  private final RoomAccessService roomAccessService;

  @Autowired
  public AccountController(
    AccountService service,
    AccountServiceBase accountService,
    AccountFindQueryService findQueryService,
    RoomAccessService roomAccessService
  ) {
    this.service = service;
    this.accountService = accountService;
    this.findQueryService = findQueryService;
    this.roomAccessService = roomAccessService;
  }

  @GetMapping("/")
  public Flux<Account> get(@RequestParam UUID[] ids) {
    return service.get(ids).switchIfEmpty(Mono.error(NotFoundException::new));
  }

  @GetMapping(GET_MAPPING)
  public Mono<Account> get(@PathVariable UUID id) {
    return accountService
      .get(id)
      .switchIfEmpty(Mono.error(NotFoundException::new));
  }

  @GetMapping(GET_ROOM_ACCESS_MAPPING)
  public Flux<RoomAccess> getRoomAccess(@PathVariable UUID id) {
    return roomAccessService.getByAccountId(id);
  }

  @PostMapping(FIND_MAPPING)
  public Flux<Account> find(@RequestBody final FindQuery<Account> findQuery) {
    logger.debug("Resolving find query: {}", findQuery);
    return findQueryService.resolveQuery(findQuery);
  }

  @PostMapping(ROOM_HISTORY_MAPPING)
  public Mono<RoomAccess> postRoomHistoryEntry(
    @PathVariable final UUID id,
    @RequestBody final RoomAccess roomAccess
  ) {
    roomAccess.setAccountId(id);
    return roomAccessService.createRoomHistory(roomAccess);
  }

  @DeleteMapping(ROOM_HISTORY_DELETE_MAPPING)
  public Mono<Void> deleteRoomHistoryEntry(
    @PathVariable final UUID id,
    @PathVariable final UUID roomId
  ) {
    return roomAccessService.delete(roomId, id);
  }

  @DeleteMapping(DELETE_MAPPING)
  public Mono<Void> delete(@PathVariable final UUID id) {
    return service.delete(id);
  }
}
