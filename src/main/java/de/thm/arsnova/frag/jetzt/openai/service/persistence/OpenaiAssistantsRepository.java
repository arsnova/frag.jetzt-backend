package de.thm.arsnova.frag.jetzt.openai.service.persistence;

import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiAssistants;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface OpenaiAssistantsRepository
  extends ReactiveCrudRepository<OpenaiAssistants, UUID> {
  Flux<OpenaiAssistants> findAllByRoomId(UUID roomId);

  Mono<Void> deleteByRoomIdAndOpenaiId(UUID roomId, String openaiId);
}
