package de.thm.arsnova.frag.jetzt.notification.service;

import de.thm.arsnova.frag.jetzt.backend.config.NotificationConfig;
import de.thm.arsnova.frag.jetzt.notification.CommentChangeEventSource;
import de.thm.arsnova.frag.jetzt.notification.model.AngularClickAction;
import de.thm.arsnova.frag.jetzt.notification.model.AngularClickAction.ActionOperation;
import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import de.thm.arsnova.frag.jetzt.notification.model.ICommentSubscription.SubscriptionResult;
import de.thm.arsnova.frag.jetzt.notification.model.WebNotification;
import de.thm.arsnova.frag.jetzt.notification.model.WebNotificationAction;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import jakarta.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class WebNotificationService {

  private static final Pattern regex = Pattern.compile("\\{\\{([^\\}]*)\\}\\}");

  private static final class Reducer {

    UUID own;
    SubscriptionResult type;
    HashSet<UUID> otherOther = new HashSet<>();
  }

  private final WebPushService pushService;
  private final ChangeSubscriptionService subscriptionService;
  private final CommentChangeEventSource eventSource;
  private final NotificationConfig notificationConfig;

  @Autowired
  public WebNotificationService(
    WebPushService pushService,
    ChangeSubscriptionService subscriptionService,
    CommentChangeEventSource eventSource,
    NotificationConfig notificationConfig
  ) {
    this.pushService = pushService;
    this.subscriptionService = subscriptionService;
    this.eventSource = eventSource;
    this.notificationConfig = notificationConfig;
  }

  public Mono<Void> sendNotification(CommentChange change) {
    this.eventSource.sendNotification(change);
    return Flux
      .concat(
        subscriptionService.getAllPlainCommentSubscriptions(
          change.getCommentId()
        ),
        subscriptionService.getAllPlainRoomSubscriptions(change.getRoomId())
      )
      .reduce(
        new Reducer(),
        (acc, e) -> {
          SubscriptionResult r = e.hasSubscribed(change);
          if (r == SubscriptionResult.NONE) {
            return acc;
          }
          if (r != SubscriptionResult.OTHER_OTHER) {
            acc.own = e.getAccountId();
            acc.type = r;
            return acc;
          }
          acc.otherOther.add(e.getAccountId());
          return acc;
        }
      )
      .flatMap(reduced -> {
        Mono<?> result = Mono.empty();
        if (reduced.type != null) {
          result =
            result.then(sendSingleMessage(reduced.own, reduced.type, change));
        }
        if (reduced.otherOther.size() > 0) {
          result =
            result.then(sendMultipleMessages(reduced.otherOther, change));
        }
        return result;
      })
      .then();
  }

  public Mono<Void> sendSingleMessage(
    UUID accountId,
    SubscriptionResult result,
    CommentChange change
  ) {
    byte[] payload = buildNotification(change, result);
    if (payload == null) {
      return Mono.empty();
    }
    return pushService
      .getUserSubscriptions(accountId)
      .flatMap(subscription ->
        pushService.sendNotification(subscription, payload)
      )
      .then();
  }

  public Mono<Void> sendMultipleMessages(
    HashSet<UUID> accountIds,
    CommentChange change
  ) {
    byte[] payload = buildNotification(change, SubscriptionResult.OTHER_OTHER);
    if (payload == null) {
      return Mono.empty();
    }
    return Flux
      .fromIterable(accountIds)
      .flatMap(accountId ->
        pushService
          .getUserSubscriptions(accountId)
          .flatMap(subscription ->
            pushService.sendNotification(subscription, payload)
          )
      )
      .then();
  }

  private byte[] buildNotification(
    CommentChange change,
    SubscriptionResult result
  ) {
    Map<String, String> vars = buildVars(change);
    Map<String, Object> lang = notificationConfig.getEn();
    String action0 = substituteVariables(lang.get("action0").toString(), vars);
    String action1 = substituteVariables(lang.get("action1").toString(), vars);
    String title = getBasedOnIntent(change, lang, result, "title", false);
    String body = getBasedOnIntent(change, lang, result, "value", true);
    String badge = getBasedOnIntent(change, lang, result, "badge", false);
    String icon = getBasedOnIntent(change, lang, result, "icon", false);
    String image = getBasedOnIntent(change, lang, result, "image", false);
    if (body == null || title == null) {
      return null;
    }
    title = substituteVariables(title, vars);
    body = substituteVariables(body, vars);
    WebNotification n = new WebNotification(title);
    n.setBody(body);
    List<WebNotificationAction> actions = new ArrayList<>();
    if (
      change.getType() != CommentInterestBit.CHANGE_ACK ||
      change.getCurrentValueString().equals("1")
    ) {
      actions.add(new WebNotificationAction("goto", action0, null));
    }
    actions.add(new WebNotificationAction("overview", action1, null));
    n.setActions(actions);
    n.setData(buildActionMap(change));
    if (badge != null) {
      badge = substituteVariables(badge, vars);
      n.setBadge(badge);
    }
    if (icon != null) {
      icon = substituteVariables(icon, vars);
      n.setIcon(icon);
    }
    if (image != null) {
      image = substituteVariables(image, vars);
      n.setImage(image);
    }
    return pushService.encodeMessage(n);
  }

  private String getBasedOnIntent(
    CommentChange change,
    Map<String, Object> lang,
    SubscriptionResult result,
    String intent,
    boolean allowDefault
  ) {
    /*
     * If a string is found, return it.
     * 1. get top level (type)
     * 2. if map, try the following over and over again:
     *   a) get vale for intent (only if string)
     *   b) get value for result
     *   c) get value for current state in format STATE_{{current}}
     *   d) get value for previous and state in format STATE_{{previous}}_{{current}}
     */
    Object temp = lang.get(change.getType().name());
    if (temp == null) {
      return null;
    }
    if (temp instanceof String) {
      return (String) temp;
    }
    while (temp instanceof Map) {
      // a)
      Object innerTemp = ((Map<?, ?>) temp).get(intent);
      if (innerTemp instanceof String) {
        return (String) innerTemp;
      }
      // b)
      innerTemp = ((Map<?, ?>) temp).get(result.name());
      if (innerTemp instanceof String) {
        if (allowDefault) {
          return (String) innerTemp;
        }
      } else if (innerTemp != null) {
        temp = innerTemp;
        continue;
      }
      // c)
      innerTemp =
        ((Map<?, ?>) temp).get("STATE_" + change.getCurrentValueString());
      if (innerTemp instanceof String) {
        if (allowDefault) {
          return (String) innerTemp;
        }
      } else if (innerTemp != null) {
        temp = innerTemp;
        continue;
      }
      // d)
      innerTemp =
        ((Map<?, ?>) temp).get(
            "STATE_" +
            change.getPreviousValueString() +
            "_" +
            change.getCurrentValueString()
          );
      if (innerTemp instanceof String) {
        if (allowDefault) {
          return (String) innerTemp;
        }
      } else if (innerTemp != null) {
        temp = innerTemp;
        continue;
      }
      break;
    }
    temp = lang.get(intent);
    return temp instanceof String ? (String) temp : null;
  }

  private Object buildActionMap(CommentChange change) {
    return Map.of(
      "onActionClick",
      Map.of(
        "default",
        new AngularClickAction(ActionOperation.FocusLastWindow, "/home"),
        "goto",
        new AngularClickAction(
          ActionOperation.NavigateLastWindow,
          "/creator/room/" +
          change.getRoom().getShortId() +
          "/comment/" +
          change.getCommentId() +
          "/conversation"
        ),
        "overview",
        new AngularClickAction(ActionOperation.NavigateLastWindow, "/user")
      )
    );
  }

  private Map<String, String> buildVars(CommentChange change) {
    Map<String, String> vars = new HashMap<>();
    vars.put("previous", change.getPreviousValueString());
    vars.put("current", change.getCurrentValueString());
    vars.put("roomName", change.getRoom().getName());
    if (change.getType() == CommentInterestBit.CHANGE_SCORE) {
      String[] scores = change.getPreviousValueString().split("/");
      vars.put("previousScore", scores[0]);
      vars.put("previousUpvotes", scores[1]);
      vars.put("previousDownvotes", scores[2]);
      scores = change.getCurrentValueString().split("/");
      vars.put("currentScore", scores[0]);
      vars.put("currentUpvotes", scores[1]);
      vars.put("currentDownvotes", scores[2]);
    }
    return vars;
  }

  private String substituteVariables(
    String template,
    Map<String, String> vars
  ) {
    StringBuilder builder = new StringBuilder();
    Matcher matcher = regex.matcher(template);
    int last = 0;
    while (matcher.find()) {
      if (matcher.start() > last) {
        builder.append(template.subSequence(last, matcher.start()));
      }
      String resolved = vars.get(matcher.group(1));
      if (resolved != null) {
        builder.append(resolved);
      } else {
        builder.append(matcher.group(0));
      }
      last = matcher.end();
    }
    if (last < template.length()) {
      builder.append(template.subSequence(last, template.length()));
    }
    return builder.toString();
  }
}
