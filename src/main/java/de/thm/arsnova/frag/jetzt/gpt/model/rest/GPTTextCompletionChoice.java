package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GPTTextCompletionChoice implements IGPTCompletionChoice {

    private int index;
    private String finishReason;
    private String text;
    private GPTTextCompletionLogProbs logprobs;

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @JsonProperty("finish_reason")
    @Override
    public String getFinishReason() {
        return finishReason;
    }

    public void setFinishReason(String finishReason) {
        this.finishReason = finishReason;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public GPTTextCompletionLogProbs getLogprobs() {
        return logprobs;
    }

    public void setLogprobs(GPTTextCompletionLogProbs logprobs) {
        this.logprobs = logprobs;
    }
    
}
