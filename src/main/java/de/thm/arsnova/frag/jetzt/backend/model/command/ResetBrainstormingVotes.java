package de.thm.arsnova.frag.jetzt.backend.model.command;

public class ResetBrainstormingVotes extends WebSocketCommand<ResetBrainstormingVotesPayload> {

    public ResetBrainstormingVotes() {
        super(ResetBrainstormingVotes.class.getSimpleName());
    }

    public ResetBrainstormingVotes(ResetBrainstormingVotesPayload payload) {
        super(ResetBrainstormingVotes.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResetBrainstormingVotes that = (ResetBrainstormingVotes) o;
        return this.getPayload().equals(that.getPayload());
    }
}
