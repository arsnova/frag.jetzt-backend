package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTAPISetting;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelResponse;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTAPISettingRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTVoucherRepository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTAPISettingService {

  private static final Pattern API_KEY_PATTERN = Pattern.compile(
    "^sk-[a-zA-Z0-9-_]+$"
  );
  private static final Pattern ORG_PATTERN = Pattern.compile(
    "^org-[a-zA-Z0-9-_]+$"
  );

  private final GPTAPISettingRepository repository;
  private final AuthorizationHelper helper;
  private final QuotaManageService quotaManageService;
  private final GPTRequestService requestService;

  @Autowired
  public GPTAPISettingService(
    GPTAPISettingRepository repository,
    GPTVoucherRepository voucherRepository,
    AuthorizationHelper helper,
    QuotaManageService quotaManageService,
    GPTRequestService requestService
  ) {
    this.repository = repository;
    this.helper = helper;
    this.quotaManageService = quotaManageService;
    this.requestService = requestService;
  }

  public Mono<GPTAPISetting> createApiKey(GPTAPISetting setting) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        setting.setId(null);
        setting.setAccountId(user.getAccountId());
        return validateKey(setting);
      })
      .flatMap(repository::save);
  }

  public Mono<Void> deleteApiKey(UUID uuid) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(uuid))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        tuple.getT2().getAccountId().equals(tuple.getT1().getAccountId())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(tuple -> {
        GPTAPISetting setting = tuple.getT2();
        if (setting.getQuotaId() != null) {
          return quotaManageService
            .delete(setting.getQuotaId())
            .then(repository.deleteById(uuid));
        }
        return repository.deleteById(uuid);
      });
  }

  public Flux<GPTAPISetting> getAllApiKeysByAccount() {
    return helper
      .getCurrentUser()
      .flatMapMany(user -> repository.findAllByAccountId(user.getAccountId()));
  }

  public Mono<Quota> createApiKeyQuota(UUID apiKeyId, Quota quota) {
    return ensureQuota(apiKeyId, null)
      .flatMap(setting -> {
        return quotaManageService
          .create(quota)
          .flatMap(q -> {
            setting.setQuotaId(q.getId());
            return repository.save(setting).map(s -> q);
          });
      });
  }

  public Mono<Quota> patchApiKeyQuota(
    UUID apiKeyId,
    UUID quotaId,
    Map<String, Object> map
  ) {
    return ensureQuota(apiKeyId, quotaId)
      .flatMap(setting -> quotaManageService.patch(quotaId, map));
  }

  public Mono<Void> deleteApiKeyQuota(UUID apiKeyId, UUID quotaId) {
    return ensureQuota(apiKeyId, quotaId)
      .flatMap(setting -> {
        setting.setQuotaId(null);
        return repository
          .save(setting)
          .then(quotaManageService.delete(quotaId));
      });
  }

  public Mono<GPTModelResponse> getModels(UUID id) {
    return repository
      .findById(id)
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .flatMap(setting ->
        requestService.requestModels(
          new GPTModelRequest(setting.getApiKey(), setting.getApiOrganization())
        )
      );
  }

  private Mono<GPTAPISetting> ensureQuota(UUID apiKeyId, UUID quotaId) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(apiKeyId))
      .filter(tuple ->
        tuple.getT2().getAccountId().equals(tuple.getT1().getAccountId())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .filter(tuple -> Objects.equals(tuple.getT2().getQuotaId(), quotaId))
      .switchIfEmpty(
        Mono.error(new BadRequestException("Quota does not match id"))
      )
      .map(tuple -> tuple.getT2());
  }

  private Mono<GPTAPISetting> validateKey(GPTAPISetting setting) {
    return Mono
      .just(setting)
      .filter(s ->
        s.getApiKey() != null &&
        API_KEY_PATTERN.matcher(s.getApiKey()).matches()
      )
      .switchIfEmpty(
        Mono.error(new IllegalArgumentException("Invalid API key"))
      )
      .filter(s ->
        s.getApiOrganization() == null ||
        s.getApiOrganization().isEmpty() ||
        ORG_PATTERN.matcher(s.getApiOrganization()).matches()
      )
      .switchIfEmpty(
        Mono.error(new IllegalArgumentException("Invalid organization"))
      );
  }
}
