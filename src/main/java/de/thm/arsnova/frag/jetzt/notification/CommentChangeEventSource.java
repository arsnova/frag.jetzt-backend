package de.thm.arsnova.frag.jetzt.notification;

import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import de.thm.arsnova.frag.jetzt.notification.model.event.CommentChangeEvent;
import de.thm.arsnova.frag.jetzt.notification.model.event.CommentChangeEventPayload;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentChangeEventSource {

  private final AmqpTemplate messagingTemplate;

  @Autowired
  public CommentChangeEventSource(AmqpTemplate messagingTemplate) {
    this.messagingTemplate = messagingTemplate;
  }

  public void sendNotification(CommentChange change) {
    CommentChangeEventPayload payload = new CommentChangeEventPayload(change);
    messagingTemplate.convertAndSend(
      "amq.topic",
      change.getRoomId() + ".comment-change.stream",
      new CommentChangeEvent(payload)
    );
    if (change.getType() != CommentInterestBit.CREATED) {
      messagingTemplate.convertAndSend(
        "amq.topic",
        change.getRoomId() +
        ".comment-change." +
        change.getCommentId() +
        ".stream",
        new CommentChangeEvent(payload)
      );
    }
  }
}
