package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.sql.Timestamp;
import java.util.UUID;

@Repository
public interface AccountRepository extends ReactiveCrudRepository<Account, UUID> {
    Flux<Account> findAllByEmailIgnoreCase(String email);
    Flux<Account> findAllByUpdatedAtIsNullAndCreatedAtLessThan(Timestamp time);
    Flux<Account> findAllByUpdatedAtIsNotNullAndUpdatedAtLessThan(Timestamp time);
    Mono<Account> findByKeycloakIdAndKeycloakUserId(UUID keycloakId, UUID keycloakUserId);
    Mono<Account> findByKeycloakIdAndKeycloakUserIdIsNullAndEmailIgnoreCase(UUID keycloakId, String email);
    Mono<Void> deleteByKeycloakIdAndKeycloakUserId(UUID keycloakId, UUID keycloakUserId);
}
