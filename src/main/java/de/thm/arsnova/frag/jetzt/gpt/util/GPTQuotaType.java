package de.thm.arsnova.frag.jetzt.gpt.util;

import java.time.LocalDateTime;

public enum GPTQuotaType {
  DAILY {
    @Override
    public boolean hasChanged(LocalDateTime before, LocalDateTime now) {
      return (
        before.getYear() != now.getYear() ||
        before.getMonth() != now.getMonth() ||
        before.getDayOfMonth() != now.getDayOfMonth()
      );
    }
  },
  WEEKLY {
    @Override
    public boolean hasChanged(LocalDateTime before, LocalDateTime now) {
      before = before.plusDays(7 - before.getDayOfWeek().getValue());
      return now.isAfter(
        LocalDateTime.of(
          before.getYear(),
          before.getMonth(),
          before.getDayOfMonth(),
          23,
          59,
          59,
          999_999_999
        )
      );
    }
  },
  MONTHLY {
    @Override
    public boolean hasChanged(LocalDateTime before, LocalDateTime now) {
      return (
        before.getYear() != now.getYear() || before.getMonth() != now.getMonth()
      );
    }
  },
  YEARLY {
    @Override
    public boolean hasChanged(LocalDateTime before, LocalDateTime now) {
      return before.getYear() != now.getYear();
    }
  },
  GLOBAL {
    @Override
    public boolean hasChanged(LocalDateTime before, LocalDateTime now) {
        return false;
    }
  };

  public abstract boolean hasChanged(LocalDateTime before, LocalDateTime now);
}
