package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.CommentEventSource;
import de.thm.arsnova.frag.jetzt.backend.model.command.Downvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.ResetVote;
import de.thm.arsnova.frag.jetzt.backend.model.command.Upvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.VotePayload;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.VoteService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.notification.handler.CommentChangeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class VoteCommandHandler {

  private static final Logger logger = LoggerFactory.getLogger(
    VoteCommandHandler.class
  );

  private final VoteService service;
  private final CommentEventSource eventer;
  private final CommentService commentService;
  private final CommentChangeHandler commentChangeHandler;
  private final AuthorizationHelper authorizationHelper;

  @Autowired
  public VoteCommandHandler(
    VoteService service,
    CommentEventSource eventer,
    CommentService commentService,
    CommentChangeHandler commentChangeHandler,
    AuthorizationHelper authorizationHelper
  ) {
    this.service = service;
    this.eventer = eventer;
    this.commentService = commentService;
    this.commentChangeHandler = commentChangeHandler;
    this.authorizationHelper = authorizationHelper;
  }

  public Mono<de.thm.arsnova.frag.jetzt.backend.model.Upvote> handle(
    Upvote vote
  ) {
    logger.trace("got new command: " + vote.toString());

    VotePayload p = vote.getPayload();

    return authorizationHelper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          commentService
            .get(p.getCommentId())
            .switchIfEmpty(
              Mono.error(new NotFoundException("Comment not found"))
            ),
          service.createUpvote(
            user,
            new de.thm.arsnova.frag.jetzt.backend.model.Upvote(
              p.getUserId(),
              p.getCommentId()
            )
          )
        )
      )
      .doOnSuccess(it -> {
        eventer.ScoreChanged(p.getCommentId());
        commentChangeHandler
          .changeScoreAndEmit(it.getT2(), it.getT1())
          .subscribe();
      })
      .map(tuple -> tuple.getT3());
  }

  public Mono<de.thm.arsnova.frag.jetzt.backend.model.Downvote> handle(
    Downvote vote
  ) {
    logger.trace("got new command: " + vote.toString());

    VotePayload p = vote.getPayload();

    return authorizationHelper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          commentService
            .get(p.getCommentId())
            .switchIfEmpty(
              Mono.error(new NotFoundException("Comment not found"))
            ),
          service.createDownvote(
            user,
            new de.thm.arsnova.frag.jetzt.backend.model.Downvote(
              p.getUserId(),
              p.getCommentId()
            )
          )
        )
      )
      .doOnSuccess(it -> {
        eventer.ScoreChanged(p.getCommentId());
        commentChangeHandler
          .changeScoreAndEmit(it.getT2(), it.getT1())
          .subscribe();
      })
      .map(tuple -> tuple.getT3());
  }

  public Mono<Void> handle(ResetVote vote) {
    logger.trace("got new command: " + vote.toString());

    VotePayload p = vote.getPayload();
    return authorizationHelper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          commentService
            .get(p.getCommentId())
            .switchIfEmpty(
              Mono.error(new NotFoundException("Comment not found"))
            ),
          service.resetVote(p.getCommentId(), user).thenReturn(true)
        )
      )
      .doOnSuccess(it -> {
        eventer.ScoreChanged(p.getCommentId());
        commentChangeHandler
          .changeScoreAndEmit(it.getT2(), it.getT1())
          .subscribe();
      })
      .then();
  }
}
