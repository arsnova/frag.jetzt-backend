package de.thm.arsnova.frag.jetzt.quota.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaEntry;
import reactor.core.publisher.Flux;

@Repository
public interface QuotaEntryRepository extends ReactiveCrudRepository<QuotaEntry, UUID> {
    
    Flux<QuotaEntry> findAllByQuotaId(UUID quotaId);

    Flux<Void> deleteAllByQuotaId(UUID quotaId);

}
