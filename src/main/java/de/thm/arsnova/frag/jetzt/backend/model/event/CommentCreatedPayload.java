package de.thm.arsnova.frag.jetzt.backend.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class CommentCreatedPayload implements WebSocketPayload {
    private UUID id;

    public CommentCreatedPayload() {
    }

    public CommentCreatedPayload(Comment c) {
        if (c != null) {
            id = c.getId();
        }
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CommentCreatedPayload{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentCreatedPayload that = (CommentCreatedPayload) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
