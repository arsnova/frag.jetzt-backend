package de.thm.arsnova.frag.jetzt.gpt.util.encoding;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ongres.scram.common.bouncycastle.base64.Base64;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GPTDefaultEncodings {

  private static class CharRange {

    final int start;
    final int end;

    public CharRange(char start, char end) {
      this.start = start;
      this.end = end + 1;
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(
    GPTDefaultEncodings.class
  );
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final HashMap<Character, Integer> BYTE_DECODER = new HashMap<>();
  private static final Map<GPTTokenByte, Integer> TEMPLATE;
  private static final String ENDOFTEXT = "<|endoftext|>";
  private static final String FIM_PREFIX = "<|fim_prefix|>";
  private static final String FIM_MIDDLE = "<|fim_middle|>";
  private static final String FIM_SUFFIX = "<|fim_suffix|>";
  private static final String ENDOFPROMPT = "<|endofprompt|>";

  static {
    final int max = 1 << Byte.SIZE;
    ArrayList<CharRange> ranges = new ArrayList<>();
    ranges.add(new CharRange('!', '~'));
    ranges.add(new CharRange('¡', '¬'));
    ranges.add(new CharRange('®', 'ÿ'));
    HashMap<GPTTokenByte, Integer> rankMap = new HashMap<>();
    int rankCounter = 0;
    for (CharRange range : ranges) {
      for (int i = range.start; i < range.end; i++) {
        rankMap.put(new GPTTokenByte(new byte[] { (byte) i }), rankCounter++);
      }
    }
    int start = 0;
    int end;
    int n = 0;
    for (int i = 0; i < ranges.size(); i++) {
      CharRange currentRange = ranges.get(i);
      end = currentRange.start;
      for (int j = start; j < end; j++) {
        final int key = j;
        final char character = (char) (max + n++);
        BYTE_DECODER.put(character, key);
        rankMap.put(new GPTTokenByte(new byte[] { (byte) j }), rankCounter++);
      }
      start = end;
      end = currentRange.end;
      for (int j = start; j < end; j++) {
        final int key = j;
        final char character = (char) j;
        BYTE_DECODER.put(character, key);
      }
      start = end;
    }
    for (int i = start; i < max; i++) {
      final int key = i;
      final char character = (char) (max + n++);
      BYTE_DECODER.put(character, key);
      rankMap.put(new GPTTokenByte(new byte[] { (byte) i }), rankCounter++);
    }
    if (BYTE_DECODER.size() != max) {
      throw new IllegalStateException("Should have 256 characters");
    }
    if (rankMap.size() != max) {
      throw new IllegalStateException("rankMap should have 256 characters");
    }
    TEMPLATE = new HashMap<>(rankMap);
  }

  private static GPTEncoder gpt2Encoder;
  private static GPTEncoder r50kBaseEncoder;
  private static GPTEncoder p50kBaseEncoder;
  private static GPTEncoder p50kEditEncoder;
  private static GPTEncoder cl100kBaseEncoder;
  private static GPTEncoder o200kBaseEncoder;

  public static GPTEncoder getByName(String name) {
    if ("gpt2".equals(name)) {
      return getGpt2Encoder();
    } else if ("r50k_base".equals(name)) {
      return getR50kBaseEncoder();
    } else if ("p50k_base".equals(name)) {
      return getP50kBaseEncoder();
    } else if ("p50k_edit".equals(name)) {
      return getP50kEditEncoder();
    } else if ("cl100k_base".equals(name)) {
      return getCl100kBaseEncoder();
    } else if ("o200k_base".equals(name)) {
      return getO200kBaseEncoder();
    }
    return null;
  }

  public static GPTEncoder getGpt2Encoder() {
    if (gpt2Encoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 50256);
        gpt2Encoder =
          new GPTEncoder(
            "gpt2",
            Pattern.compile(
              "'s|'t|'re|'ve|'m|'ll|'d| ?\\p{L}+| ?\\p{N}+| ?[^\\s\\p{L}\\p{N}]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadMergeableBPERanks(
              loadResource("gpt/gpt2/vocab.bpe"),
              loadResource("gpt/gpt2/encoder.json")
            ),
            map,
            50257
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of gpt2", e);
      }
    }
    return gpt2Encoder;
  }

  public static GPTEncoder getR50kBaseEncoder() {
    if (r50kBaseEncoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 50256);
        r50kBaseEncoder =
          new GPTEncoder(
            "r50k_base",
            Pattern.compile(
              "'s|'t|'re|'ve|'m|'ll|'d| ?\\p{L}+| ?\\p{N}+| ?[^\\s\\p{L}\\p{N}]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadTiktokenBPE(loadResource("gpt/r50kBase/r50k_base.tiktoken")),
            new HashMap<>(map),
            50257
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of r50k_base", e);
      }
    }
    return r50kBaseEncoder;
  }

  public static GPTEncoder getP50kBaseEncoder() {
    if (p50kBaseEncoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 50256);
        p50kBaseEncoder =
          new GPTEncoder(
            "p50k_base",
            Pattern.compile(
              "'s|'t|'re|'ve|'m|'ll|'d| ?\\p{L}+| ?\\p{N}+| ?[^\\s\\p{L}\\p{N}]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadTiktokenBPE(loadResource("gpt/p50kBase/p50k_base.tiktoken")),
            new HashMap<>(map),
            50281
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of p50k_base", e);
      }
    }
    return p50kBaseEncoder;
  }

  public static GPTEncoder getP50kEditEncoder() {
    if (p50kEditEncoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 50256);
        map.put(FIM_PREFIX, 50281);
        map.put(FIM_MIDDLE, 50282);
        map.put(FIM_SUFFIX, 50283);
        p50kEditEncoder =
          new GPTEncoder(
            "p50k_edit",
            Pattern.compile(
              "'s|'t|'re|'ve|'m|'ll|'d| ?\\p{L}+| ?\\p{N}+| ?[^\\s\\p{L}\\p{N}]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadTiktokenBPE(loadResource("gpt/p50kBase/p50k_base.tiktoken")),
            map,
            null
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of p50k_edit", e);
      }
    }
    return p50kEditEncoder;
  }

  public static GPTEncoder getO200kBaseEncoder() {
    if (o200kBaseEncoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 199999);
        map.put(ENDOFPROMPT, 200018);
        o200kBaseEncoder =
          new GPTEncoder(
            "o200k_base",
            Pattern.compile(
              "[^\\r\\n\\p{L}\\p{N}]?[\\p{Lu}\\p{Lt}\\p{Lm}\\p{Lo}\\p{M}]*[\\p{Ll}\\p{Lm}\\p{Lo}\\p{M}]+(i:'s|'t|'re|'ve|'m|'ll|'d)?|[^\\r\\n\\p{L}\\p{N}]?[\\p{Lu}\\p{Lt}\\p{Lm}\\p{Lo}\\p{M}]+[\\p{Ll}\\p{Lm}\\p{Lo}\\p{M}]*(?i:'s|'t|'re|'ve|'m|'ll|'d)?|\\p{N}{1,3}| ?[^\\s\\p{L}\\p{N}]+[\\r\\n/]*|\\s*[\\r\\n]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadTiktokenBPE(
              loadResource("gpt/o200kBase/o200k_base.tiktoken")
            ),
            map,
            null
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of o200k_base", e);
      }
    }
    return o200kBaseEncoder;
  }

  public static GPTEncoder getCl100kBaseEncoder() {
    if (cl100kBaseEncoder == null) {
      try {
        HashMap<String, Integer> map = new HashMap<>();
        map.put(ENDOFTEXT, 100257);
        map.put(FIM_PREFIX, 100258);
        map.put(FIM_MIDDLE, 100259);
        map.put(FIM_SUFFIX, 100260);
        map.put(ENDOFPROMPT, 100276);
        cl100kBaseEncoder =
          new GPTEncoder(
            "cl100k_base",
            Pattern.compile(
              "(?i:'s|'t|'re|'ve|'m|'ll|'d)|[^\\r\\n\\p{L}\\p{N}]?\\p{L}+|\\p{N}{1,3}| ?[^\\s\\p{L}\\p{N}]+[\\r\\n]*|\\s*[\\r\\n]+|\\s+(?!\\S)|\\s+",
              Pattern.UNICODE_CASE
            ),
            loadTiktokenBPE(
              loadResource("gpt/cl100kBase/cl100k_base.tiktoken")
            ),
            map,
            null
          );
      } catch (IOException | RuntimeException e) {
        logger.error("Error during load of cl100k_base", e);
      }
    }
    return cl100kBaseEncoder;
  }

  private static String loadResource(String resource) throws IOException {
    InputStream is =
      GPTDefaultEncodings.class.getClassLoader().getResourceAsStream(resource);
    if (is == null) {
      throw new IllegalArgumentException(
        "Failed to load Resource! Requested: " + resource
      );
    }
    try (BufferedReader r = new BufferedReader(new InputStreamReader(is))) {
      return r.lines().collect(Collectors.joining("\n", "", ""));
    }
  }

  private static HashMap<GPTTokenByte, Integer> loadTiktokenBPE(
    String tiktoken
  ) throws NumberFormatException {
    final HashMap<GPTTokenByte, Integer> map = new HashMap<>();
    final String[] lines = tiktoken.split("\n");
    for (String line : lines) {
      if (line.trim().length() < 1) {
        continue;
      }
      String[] entries = line.split(" ", 3);
      if (entries.length != 2) {
        throw new IllegalArgumentException(
          "tiktoken file is not well formatted!"
        );
      }
      map.put(
        new GPTTokenByte(Base64.decode(entries[0])),
        Integer.parseInt(entries[1])
      );
    }
    return map;
  }

  private static HashMap<GPTTokenByte, Integer> loadMergeableBPERanks(
    String vocabFile,
    String json
  ) throws JsonMappingException, JsonProcessingException {
    final HashMap<GPTTokenByte, Integer> map = new HashMap<>(TEMPLATE);
    final String[] lines = vocabFile.split("\n");
    int n = map.size();
    for (int i = 1; i < lines.length; i++) {
      String[] line = lines[i].split("(\\s+)");
      int j = 0;
      String first = null;
      while (j < line.length) {
        if (line[j].trim().length() > 0) {
          first = line[j];
          ++j;
          break;
        }
        ++j;
      }
      String second = null;
      while (j < line.length) {
        if (line[j].trim().length() > 0) {
          second = line[j];
          break;
        }
        ++j;
      }
      char[] firstChars = first.toCharArray();
      char[] secondChars = second.toCharArray();
      byte[] result = new byte[firstChars.length + secondChars.length];
      int index = 0;
      for (char c : firstChars) {
        result[index++] = BYTE_DECODER.get(c).byteValue();
      }
      for (char c : secondChars) {
        result[index++] = BYTE_DECODER.get(c).byteValue();
      }
      map.put(new GPTTokenByte(result), n++);
    }
    // verify
    HashMap<?, ?> encoderJson = mapper.readValue(json, HashMap.class);
    HashMap<GPTTokenByte, Integer> test = new HashMap<>();
    encoderJson.forEach((k, v) -> {
      String temp = ((String) k);
      if (temp.equals("<|endoftext|>") || temp.equals("<|startoftext|>")) {
        return;
      }
      char[] key = temp.toCharArray();
      byte[] result = new byte[key.length];
      for (int i = 0; i < key.length; i++) {
        result[i] = BYTE_DECODER.get(key[i]).byteValue();
      }
      test.put(new GPTTokenByte(result), ((Number) v).intValue());
    });
    if (!map.equals(test)) {
      throw new IllegalStateException("Something went wrong during parsing!");
    }
    return map;
  }
}
