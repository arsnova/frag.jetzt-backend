package de.thm.arsnova.frag.jetzt.backend.model.command;

public class ImportComments extends WebSocketCommand<ImportCommentsPayload> {

    public ImportComments() {
        super(ImportComments.class.getSimpleName());
    }

    public ImportComments(ImportCommentsPayload p) {
        super(ImportComments.class.getSimpleName());
        this.payload = p;
    }
}
