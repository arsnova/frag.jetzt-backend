package de.thm.arsnova.frag.jetzt.paypal.controller;

import de.thm.arsnova.frag.jetzt.paypal.service.PayPalService;
import de.thm.arsnova.frag.jetzt.paypal.service.PayPalService.CapturedResult;
import de.thm.arsnova.frag.jetzt.paypal.service.PayPalWebhookService;
import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController("PayPalController")
@RequestMapping(PayPalController.REQUEST_MAPPING)
public class PayPalController {

  @Data
  @NoArgsConstructor
  private static final class OrderCreate {

    private String amount;
    private String currency;
    private String language;
  }

  protected static final String REQUEST_MAPPING = "/paypal";
  private static final String CREATE_ORDER = "/create-order/";
  private static final String CAPTURE_ORDER = "/capture-order/{orderId}/";
  private static final String CAPTURED_QUOTA = "/captured-quota/";
  private static final String WEBHOOK = "/webhook";

  private final PayPalService service;
  private final PayPalWebhookService webhookService;

  public PayPalController(
    PayPalService service,
    PayPalWebhookService webhookService
  ) {
    this.service = service;
    this.webhookService = webhookService;
  }

  @PostMapping(CREATE_ORDER)
  public Mono<Object> createOrder(@RequestBody OrderCreate order) {
    return this.service.createOrder(
        order.getAmount(),
        order.getCurrency(),
        order.getLanguage()
      );
  }

  @PostMapping(CAPTURE_ORDER)
  public Mono<Map> captureOrder(@PathVariable("orderId") String orderId) {
    return this.service.captureOrder(orderId);
  }

  @PostMapping(WEBHOOK)
  public Mono<Void> receiveWebhookData(
    @RequestHeader Map<String, String> headers,
    @RequestBody String body
  ) {
    webhookService.receiveWebhookData(headers, body);
    return Mono.empty();
  }

  @GetMapping(CAPTURED_QUOTA)
  public Mono<CapturedResult> getCapturedQuota() {
    return this.service.getCapturedQuota();
  }
}
