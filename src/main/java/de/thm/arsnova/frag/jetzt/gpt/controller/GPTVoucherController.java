package de.thm.arsnova.frag.jetzt.gpt.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTVoucher;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelResponse;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTVoucherService;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("GPTVoucherController")
@RequestMapping(GPTVoucherController.REQUEST_MAPPING)
public class GPTVoucherController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/gpt-api";
  private static final String MODELS = "/models";
  private static final String VOUCHER = "/voucher";
  private static final String ALL = "/all";
  private static final String CLAIMABLE = "/claimable";
  private static final String CLAIM = "/claim";
  private static final String QUOTA = "/quota";

  private final GPTVoucherService gptVoucherService;

  @Autowired
  public GPTVoucherController(GPTVoucherService gptVoucherService) {
    this.gptVoucherService = gptVoucherService;
  }

  @GetMapping(VOUCHER + MODELS)
  public Mono<GPTModelResponse> getModels() {
    return gptVoucherService.getModels();
  }

  @PostMapping(VOUCHER + "/{code}")
  public Mono<GPTVoucher> createVoucher(@PathVariable String code) {
    return gptVoucherService.createVoucher(code);
  }

  @DeleteMapping(VOUCHER + DELETE_MAPPING)
  public Mono<Void> deleteVoucher(@PathVariable UUID id) {
    return gptVoucherService.deleteVoucher(id);
  }

  @GetMapping(VOUCHER)
  public Flux<GPTVoucher> getVouchers() {
    return gptVoucherService.getAllVouchersByAccount();
  }

  @GetMapping(VOUCHER + ALL)
  public Flux<GPTVoucher> getAllVouchers() {
    return gptVoucherService.getAllVouchers();
  }

  @GetMapping(VOUCHER + CLAIMABLE + "/{code}")
  public Mono<GPTVoucher> getClaimableVoucher(@PathVariable String code) {
    return gptVoucherService.isClaimable(code);
  }

  @PostMapping(VOUCHER + CLAIM + "/{code}")
  public Mono<GPTVoucher> claimVoucher(@PathVariable String code) {
    return gptVoucherService.claim(code);
  }

  @PostMapping(VOUCHER + GET_MAPPING + QUOTA)
  public Mono<Quota> createQuota(
    @PathVariable UUID id,
    @RequestBody Quota quota
  ) {
    return gptVoucherService.createVoucherQuota(id, quota);
  }

  @PatchMapping(VOUCHER + GET_MAPPING + QUOTA + "/{quotaId}")
  public Mono<Quota> patchQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId,
    @RequestBody Map<String, Object> patch
  ) {
    return gptVoucherService.patchVoucherQuota(id, quotaId, patch);
  }

  @DeleteMapping(VOUCHER + GET_MAPPING + QUOTA + "/{quotaId}")
  public Mono<Void> deleteQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId
  ) {
    return gptVoucherService.deleteVoucherQuota(id, quotaId);
  }
}
