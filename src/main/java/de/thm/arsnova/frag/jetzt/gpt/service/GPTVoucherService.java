package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTVoucher;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelResponse;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTVoucherRepository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTVoucherService {

  private final GPTVoucherRepository repository;
  private final AuthorizationHelper helper;
  private final QuotaManageService quotaManageService;
  private final GPTRequestService requestService;

  @Autowired
  public GPTVoucherService(
    GPTVoucherRepository repository,
    AuthorizationHelper helper,
    QuotaManageService quotaManageService,
    GPTRequestService requestService
  ) {
    this.repository = repository;
    this.helper = helper;
    this.quotaManageService = quotaManageService;
    this.requestService = requestService;
  }

  public Mono<GPTVoucher> createVoucher(String code) {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(ignored -> repository.save(new GPTVoucher(code, null, null)));
  }

  public Mono<Void> deleteVoucher(UUID uuid) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(uuid))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        tuple.getT1().getAccount().hasRole(AccountRole.ADMIN_DASHBOARD)
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(tuple -> {
        GPTVoucher voucher = tuple.getT2();
        if (voucher.getQuotaId() != null) {
          return quotaManageService
            .delete(voucher.getQuotaId())
            .then(repository.deleteById(uuid));
        }
        return repository.deleteById(uuid);
      });
  }

  public Flux<GPTVoucher> getAllVouchersByAccount() {
    return helper
      .getCurrentUser()
      .flatMapMany(user -> repository.findAllByAccountId(user.getAccountId()));
  }

  public Flux<GPTVoucher> getAllVouchers() {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMapMany(ignored -> repository.findAll());
  }

  public Mono<GPTVoucher> isClaimable(String code) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findByCode(code))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        tuple.getT2().getAccountId() == null ||
        tuple.getT2().getAccountId().equals(tuple.getT1().getAccountId())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .map(tuple -> tuple.getT2());
  }

  public Mono<GPTVoucher> claim(String code) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findByCode(code))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        tuple.getT2().getAccountId() == null ||
        tuple.getT2().getAccountId().equals(tuple.getT1().getAccountId())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(tuple -> {
        GPTVoucher voucher = tuple.getT2();
        voucher.setAccountId(tuple.getT1().getAccountId());
        return repository.save(voucher);
      });
  }

  public Mono<Quota> createVoucherQuota(UUID voucherId, Quota quota) {
    return ensureQuota(voucherId, null)
      .flatMap(voucher -> {
        return quotaManageService
          .create(quota)
          .flatMap(q -> {
            voucher.setQuotaId(q.getId());
            return repository.save(voucher).map(v -> q);
          });
      });
  }

  public Mono<Quota> patchVoucherQuota(
    UUID voucherId,
    UUID quotaId,
    Map<String, Object> map
  ) {
    return helper
      .getCurrentUser()
      .filter(user ->
        !map.containsKey("entries") ||
        user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD)
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(ignored -> ensureQuota(voucherId, quotaId))
      .flatMap(voucher -> quotaManageService.patch(quotaId, map));
  }

  public Mono<Void> deleteVoucherQuota(UUID voucherId, UUID quotaId) {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(ignored -> ensureQuota(voucherId, quotaId))
      .flatMap(voucher -> {
        voucher.setQuotaId(null);
        return repository
          .save(voucher)
          .then(quotaManageService.delete(quotaId));
      });
  }

  private Mono<GPTVoucher> ensureQuota(UUID voucherId, UUID quotaId) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(voucherId))
      .filter(tuple ->
        Objects.equals(
          tuple.getT1().getAccountId(),
          tuple.getT2().getAccountId()
        ) ||
        tuple.getT1().getAccount().hasRole(AccountRole.ADMIN_DASHBOARD)
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .filter(tuple -> Objects.equals(tuple.getT2().getQuotaId(), quotaId))
      .switchIfEmpty(
        Mono.error(new BadRequestException("Quota does not match id"))
      )
      .map(tuple -> tuple.getT2());
  }

  public Mono<GPTModelResponse> getModels() {
    GPTConfig config = GPTConfig.getInstance();
    return requestService.requestModels(
      new GPTModelRequest(config.getApiKey(), config.getOrganization())
    );
  }
}
