package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import java.util.List;

public class GPTModelResponse {

  private String object; // list
  private List<GPTModelResponseModel> data;

  public GPTModelResponse() {}

  public String getObject() {
    return object;
  }

  public void setObject(String object) {
    this.object = object;
  }

  public List<GPTModelResponseModel> getData() {
    return data;
  }

  public void setData(List<GPTModelResponseModel> data) {
    this.data = data;
  }
}
