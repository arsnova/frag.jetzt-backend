package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class CreateLivepollSessionPayload implements WebSocketPayload {

  public static class CustomEntry {

    private String icon;
    private String text;

    public CustomEntry(String icon, String text) {
      this.icon = icon;
      this.text = text;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return "CustomEntry [icon=" + icon + ", text=" + text + "]";
    }
  }

  private UUID roomId;
  private String template;
  private String title;
  private boolean resultVisible;
  private boolean viewsVisible;
  private int answerCount;
  private List<CustomEntry> customEntries;

  public CreateLivepollSessionPayload() {}

  public CreateLivepollSessionPayload(LivepollSession session) {
    roomId = session.getRoomId();
    template = session.getTemplate();
    title = session.getTitle();
    resultVisible = session.isResultVisible();
    viewsVisible = session.isViewsVisible();
    answerCount = session.getAnswerCount();
    if (session.getCustomEntries() != null) {
      customEntries =
        session
          .getCustomEntries()
          .stream()
          .map(x -> {
            return new CustomEntry(x.getIcon(), x.getText());
          })
          .collect(Collectors.toList());
    }
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isResultVisible() {
    return resultVisible;
  }

  public void setResultVisible(boolean resultVisible) {
    this.resultVisible = resultVisible;
  }

  public boolean isViewsVisible() {
    return viewsVisible;
  }

  public void setViewsVisible(boolean viewsVisible) {
    this.viewsVisible = viewsVisible;
  }

  public int getAnswerCount() {
    return answerCount;
  }

  public void setAnswerCount(int answerCount) {
    this.answerCount = answerCount;
  }

  public List<CustomEntry> getCustomEntries() {
    return customEntries;
  }

  public void setCustomEntries(List<CustomEntry> customEntries) {
    this.customEntries = customEntries;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((template == null) ? 0 : template.hashCode());
    result = prime * result + ((title == null) ? 0 : title.hashCode());
    result = prime * result + (resultVisible ? 1231 : 1237);
    result = prime * result + (viewsVisible ? 1231 : 1237);
    result = prime * result + answerCount;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CreateLivepollSessionPayload other = (CreateLivepollSessionPayload) obj;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (template == null) {
      if (other.template != null) return false;
    } else if (!template.equals(other.template)) return false;
    if (title == null) {
      if (other.title != null) return false;
    } else if (!title.equals(other.title)) return false;
    if (resultVisible != other.resultVisible) return false;
    if (viewsVisible != other.viewsVisible) return false;
    if (answerCount != other.answerCount) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "CreateLivepollSessionPayload [roomId=" +
      roomId +
      ", template=" +
      template +
      ", title=" +
      title +
      ", resultVisible=" +
      resultVisible +
      ", viewsVisible=" +
      viewsVisible +
      ", answerCount=" +
      answerCount +
      ", customEntries=" +
      customEntries +
      "]"
    );
  }
}
