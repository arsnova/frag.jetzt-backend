package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Motd;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.sql.Timestamp;
import java.util.UUID;

public interface MotdRepository extends ReactiveCrudRepository<Motd, UUID> {

    Flux<Motd> findByStartTimestampLessThanEqualAndEndTimestampGreaterThanEqual(Timestamp start, Timestamp end);

    Flux<Motd> findByEndTimestampLessThan(Timestamp now);
}
