package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class CommentCreated extends WebSocketEvent<CommentCreatedPayload> {
    public CommentCreated() {
        super(CommentCreated.class.getSimpleName());
    }

    public CommentCreated(CommentCreatedPayload p, UUID id) {
        super(CommentCreated.class.getSimpleName(), id);
        this.payload = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentCreated that = (CommentCreated) o;
        return this.getPayload().equals(that.getPayload());
    }
}
