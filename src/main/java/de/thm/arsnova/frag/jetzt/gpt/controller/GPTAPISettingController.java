package de.thm.arsnova.frag.jetzt.gpt.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTAPISetting;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelResponse;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTAPISettingService;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("GPTAPISettingController")
@RequestMapping(GPTAPISettingController.REQUEST_MAPPING)
public class GPTAPISettingController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/gpt-api";
  private static final String API_KEY = "/api-key";
  private static final String QUOTA = "/quota";
  private static final String MODELS = "/models";

  private final GPTAPISettingService gptAPISettingService;

  @Autowired
  public GPTAPISettingController(GPTAPISettingService gptAPISettingService) {
    this.gptAPISettingService = gptAPISettingService;
  }

  @GetMapping(API_KEY + GET_MAPPING + MODELS)
  public Mono<GPTModelResponse> getModels(@PathVariable UUID id) {
    return gptAPISettingService.getModels(id);
  }

  @PostMapping(API_KEY)
  public Mono<GPTAPISetting> createApiKey(
    @RequestBody GPTAPISetting gptAPISetting
  ) {
    return gptAPISettingService.createApiKey(gptAPISetting);
  }

  @DeleteMapping(API_KEY + DELETE_MAPPING)
  public Mono<Void> getApiKey(@PathVariable UUID id) {
    return gptAPISettingService.deleteApiKey(id);
  }

  @GetMapping(API_KEY)
  public Flux<GPTAPISetting> getApiKeys() {
    return gptAPISettingService.getAllApiKeysByAccount();
  }

  @PostMapping(API_KEY + GET_MAPPING + QUOTA)
  public Mono<Quota> createQuota(
    @PathVariable UUID id,
    @RequestBody Quota quota
  ) {
    return gptAPISettingService.createApiKeyQuota(id, quota);
  }

  @PatchMapping(API_KEY + GET_MAPPING + QUOTA + "/{quotaId}")
  public Mono<Quota> patchQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId,
    @RequestBody Map<String, Object> patch
  ) {
    return gptAPISettingService.patchApiKeyQuota(id, quotaId, patch);
  }

  @DeleteMapping(API_KEY + GET_MAPPING + QUOTA + "/{quotaId}")
  public Mono<Void> deleteQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId
  ) {
    return gptAPISettingService.deleteApiKeyQuota(id, quotaId);
  }
}
