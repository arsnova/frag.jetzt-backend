package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomAccessRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ConflictException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class RoomAccessService {
    private static final Logger logger = LoggerFactory.getLogger(RoomAccessService.class);

    @Value("${app.room.inactiveDeleteDuration}")
    private Integer inactiveDeleteDuration;

    private final AuthorizationHelper authorizationHelper;
    private final RoomAccessRepository repository;
    private final RoomRepository roomRepository;

    @Autowired
    public RoomAccessService(
            AuthorizationHelper authorizationHelper,
            RoomAccessRepository repository,
            RoomRepository roomRepository
    ) {
        this.authorizationHelper = authorizationHelper;
        this.repository = repository;
        this.roomRepository = roomRepository;
    }


    // Is scheduled for every hour
    @Scheduled(fixedDelay = 3600000)
    public void removeInactiveRooms() {
        final Timestamp expiredTimestamp = Timestamp.from(Instant.now().minus(inactiveDeleteDuration, ChronoUnit.DAYS));
        roomRepository.findByCreatedAtLessThanEqualAndLastVisitCreatorLessThanEqual(expiredTimestamp, expiredTimestamp)
                .filterWhen(room -> getByRoomId(room.getId())
                        .filter(roomAccess -> roomAccess.getRole() == RoomAccess.Role.EXECUTIVE_MODERATOR &&
                                roomAccess.getLastVisit().after(expiredTimestamp))
                        .count()
                        .map(count -> count == 0)) // Only continue, when no moderators had joined
                .flatMap(roomRepository::delete)
                .subscribe();
    }

    public Flux<RoomAccess> getByRoomId(UUID roomId) {
        return repository.findByRoomId(roomId);
    }

    public Flux<RoomAccess> getByAccountId(UUID accountId) {
        return repository.findByAccountId(accountId);
    }

    public Mono<RoomAccess> createRoomHistory(RoomAccess roomAccess) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(roomAccess.getRoomId()))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(tuple -> {
                    roomAccess.setAccountId(tuple.getT1().getAccountId());
                    if (authorizationHelper.isOnlyCreatorOfRoom(tuple.getT1(), tuple.getT2())) {
                        tuple.getT2().refreshLastVisitCreator();
                        return roomRepository.save(tuple.getT2()).then(Mono.empty());
                    }
                    return repository.findByRoomIdAndAccountId(roomAccess.getRoomId(), tuple.getT1().getAccountId())
                            .switchIfEmpty(Mono.just(roomAccess));
                })
                .flatMap(access -> {
                    if (access.getRole() == null) access.setRole(RoomAccess.Role.PARTICIPANT);
                    access.setLastVisit(Timestamp.from(Instant.now()));
                    return repository.save(access)
                            .flatMap(authorizationHelper::addOrChangeAccess);
                });
    }

    public Mono<RoomAccess> addModeratorByCode(UUID moderatorRoomId) {
        return roomRepository.findById(moderatorRoomId)
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(room -> Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(room.getModeratorRoomReference())))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> !authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(new ConflictException("Account is already owner or moderator of the room")))
                .flatMap(tuple2 -> addRoomAccess(tuple2.getT2().getId(), tuple2.getT1().getAccountId(), RoomAccess.Role.EXECUTIVE_MODERATOR));
    }

    public Mono<RoomAccess> addModerator(UUID roomId, UUID accountId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(roomId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> !tuple2.getT2().getOwnerId().equals(accountId)) // Check if targetuser is creator of the room
                .switchIfEmpty(Mono.error(new ConflictException("Account is already owner of the room and can´t be moderator")))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2())) // Check if executing user is Creator or Moderator
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple2 -> addRoomAccess(roomId, accountId, RoomAccess.Role.EXECUTIVE_MODERATOR));
    }

    private Mono<RoomAccess> addRoomAccess(UUID roomId, UUID accountId, RoomAccess.Role role) {
        return repository.findByRoomIdAndAccountId(roomId, accountId) // Find existing rights and don´t remove the existing history entry
                .switchIfEmpty(Mono.just(new RoomAccess(roomId, accountId, role, new Timestamp(new Date().getTime()))))
                .flatMap(roomAccess -> {
                    roomAccess.setRole(RoomAccess.Role.EXECUTIVE_MODERATOR);
                    return repository.save(roomAccess)
                            .flatMap(authorizationHelper::addOrChangeAccess)
                            .onErrorResume(e -> Mono.error(new NotFoundException("Account not found")));
                });
    }

    public Mono<RoomAccess> removeModerator(UUID roomId, UUID accountId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(roomId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> authorizationHelper.checkCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(authenticatedUser -> repository.findByRoomIdAndAccountId(roomId, accountId))
                .flatMap(roomAccess -> {
                    roomAccess.setRole(RoomAccess.Role.PARTICIPANT);
                    return repository.save(roomAccess)
                            .flatMap(authorizationHelper::addOrChangeAccess)
                            .onErrorResume(e -> Mono.error(new NotFoundException("Account not found")));
                })
                .switchIfEmpty(Mono.error(new NotFoundException("RoomAccess not found")));
    }

    public Mono<Void> delete(UUID roomId, UUID accountId) {
        return authorizationHelper.getCurrentUser()
                .filter(authenticatedUser -> authenticatedUser.getAccountId().equals(accountId))
                .switchIfEmpty(Mono.error(new UnauthorizedException("Deletion only allowed for the User itself")))
                .flatMap(authenticatedUser ->
                        repository.deleteByRoomIdAndAccountId(roomId, authenticatedUser.getAccountId())
                                .switchIfEmpty(authorizationHelper.removeAccess(roomId, accountId))
                );
    }
}
