package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Motd;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.MotdFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.MotdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("MotdController")
@RequestMapping(MotdController.REQUEST_MAPPING)
public class MotdController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    MotdController.class
  );

  protected static final String REQUEST_MAPPING = "/motds";
  private static final String ALL_MAPPING = "/all";

  private final MotdFindQueryService findQueryService;
  private final MotdService service;

  @Autowired
  public MotdController(
    MotdFindQueryService findQueryService,
    MotdService service
  ) {
    this.findQueryService = findQueryService;
    this.service = service;
  }

  @PostMapping(POST_MAPPING)
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Motd> create(@RequestBody final Motd motd) {
    return this.service.create(motd);
  }

  @GetMapping(ALL_MAPPING)
  public Flux<Motd> getAll() {
    return service.getAll();
  }

  @PostMapping(FIND_MAPPING)
  public Flux<Motd> find(@RequestBody final FindQuery<Motd> findQuery) {
    logger.debug("Resolving find query: {}", findQuery);
    return findQueryService.resolveQuery(findQuery);
  }
}
