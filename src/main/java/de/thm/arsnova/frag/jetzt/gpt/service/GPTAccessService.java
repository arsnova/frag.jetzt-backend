package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTGlobalAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTUserRepository;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService.QuotaStatus;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTAccessService {

  private final AuthorizationHelper helper;
  private final GPTConfig config;
  private final GPTUserRepository repository;
  private final RoomRepository roomRepository;
  private final QuotaManageService quotaManageService;
  private final GPTRoomSettingService roomSettingService;

  @Autowired
  public GPTAccessService(
    AuthorizationHelper helper,
    GPTConfig config,
    GPTUserRepository repository,
    RoomRepository roomRepository,
    QuotaManageService quotaManageService,
    GPTRoomSettingService roomSettingService
  ) {
    this.helper = helper;
    this.config = config;
    this.repository = repository;
    this.roomRepository = roomRepository;
    this.quotaManageService = quotaManageService;
    this.roomSettingService = roomSettingService;
  }

  public Mono<GPTGlobalAccessInfo> getGlobalAccessInfo() {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          repository.findByAccountId(user.getAccountId())
        )
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .map(tuple -> {
        return new GPTGlobalAccessInfo(
          tuple.getT2().isRequestForbidden(),
          tuple.getT1().getName() != null &&
          tuple.getT1().getName().length() > 1,
          config.getApiKey() != null,
          config.getRestrictions().isActive(),
          config.getRestrictions().isGlobalActive(),
          config.getRestrictions().getEndDate() != null &&
          config
            .getRestrictions()
            .getEndDate()
            .before(Timestamp.from(Instant.now())),
          tuple.getT2().getConsented() != null && tuple.getT2().getConsented()
        );
      });
  }

  public Mono<GPTRoomAccessInfo> getRoomAccessInfo(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          getGlobalAccessInfo(),
          Mono.just(user),
          roomRepository.findById(roomId),
          roomSettingService.get(roomId, true)
        )
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(tuple -> {
        GPTRoomSetting setting = tuple.getT4();
        boolean isMod = helper.checkOnlyModeratorOfRoom(
          tuple.getT2(),
          tuple.getT3()
        );
        boolean isOwner = helper.checkCreatorOfRoom(
          tuple.getT2(),
          tuple.getT3()
        );
        return Mono.zip(
          Mono.just(tuple.getT1()),
          Mono.just(setting.getApiKeys().size() > 0),
          getStatus(setting, isMod, isOwner),
          Mono.just(
            GPTRoomSettingRight.ALLOW_UNREGISTERED_USERS.isRightSet(
              setting.getRightsBitset()
            )
          )
        );
      })
      .map(tuple -> {
        return new GPTRoomAccessInfo(
          tuple.getT1(),
          tuple.getT2(),
          tuple.getT3(),
          tuple.getT4()
        );
      });
  }

  private Mono<List<QuotaStatus>> getStatus(
    GPTRoomSetting setting,
    boolean isMod,
    boolean isOwner
  ) {
    return Flux
      .fromIterable(setting.getApiKeys())
      .flatMap(e -> {
        List<UUID> list = new ArrayList<>();
        if (
          e.getApiSetting() != null && e.getApiSetting().getQuotaId() != null
        ) {
          list.add(e.getApiSetting().getQuotaId());
        }
        if (e.getVoucher() != null && e.getVoucher().getQuotaId() != null) {
          list.add(e.getVoucher().getQuotaId());
        }
        return Flux
          .fromIterable(list)
          .flatMap(id -> quotaManageService.getStatus(id));
      })
      .reduce(
        QuotaStatus.INVALID,
        (acc, e) -> {
          if (acc != QuotaStatus.INVALID) {
            return acc;
          }
          if (
            !e.isDisabled() &&
            e.isInAccessTime() &&
            e.getEntries().values().stream().allMatch(b -> b)
          ) {
            return e;
          }
          return acc;
        }
      )
      .flatMap(key -> {
        Mono<?> start = Mono.empty();
        List<QuotaStatus> list = new ArrayList<>();
        if (key != QuotaStatus.INVALID) {
          list.add(key);
        }
        if (setting.getRoomQuotaId() != null) {
          start =
            start
              .then(quotaManageService.getStatus(setting.getRoomQuotaId()))
              .flatMap(status -> {
                list.add(status);
                return Mono.empty();
              });
        }
        if (setting.getModeratorQuotaId() != null && isMod) {
          start =
            start
              .then(quotaManageService.getStatus(setting.getModeratorQuotaId()))
              .flatMap(status -> {
                list.add(status);
                return Mono.empty();
              });
        }
        if (setting.getParticipantQuotaId() != null && !isMod && !isOwner) {
          start =
            start
              .then(
                quotaManageService.getStatus(setting.getParticipantQuotaId())
              )
              .flatMap(status -> {
                list.add(status);
                return Mono.empty();
              });
        }
        return start.thenReturn(list);
      });
  }
}
