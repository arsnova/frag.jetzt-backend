package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class GPTRoomSetting implements Persistable<UUID> {

  public enum GPTRoomSettingRight {
    CHANGE_PARTICIPANT_QUOTA(0, true),
    CHANGE_MODERATOR_QUOTA(1, true),
    CHANGE_ROOM_QUOTA(2, true),
    CHANGE_ROOM_PRESETS(3, true),
    CHANGE_USAGE_TIMES(4, true),
    CHANGE_API_KEY(5, true),
    ALLOW_UNREGISTERED_USERS(6, true),
    DISABLE_ENHANCED_PROMPT(7, false),
    DISABLE_FORWARD_MESSAGE(8, false);

    private final int bitValue;
    private final int defaultValue;

    GPTRoomSettingRight(int bit, boolean defaultEnabled) {
      if (bit < 0 || bit > 31) {
        throw new IllegalArgumentException("bit must be between [0,31]!");
      }
      bitValue = 1 << bit;
      defaultValue = defaultEnabled ? bitValue : 0;
    }

    public boolean isRightSet(int bitset) {
      return (bitset & bitValue) != 0;
    }
  }

  @Id
  private UUID id;

  private UUID roomId;
  // quota
  private UUID roomQuotaId;
  private UUID participantQuotaId;
  private UUID moderatorQuotaId;
  // rights
  private int rightsBitset;
  // payment
  private long paymentCounter;
  // presets
  private String presetContext;
  private String presetLength;
  private String roleInstruction;
  private String defaultModel;
  // meta information
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  // additional
  @Transient
  private List<GPTRoomKey> apiKeys;

  @Transient
  private List<GPTRoomModel> apiModels;

  @Transient
  private long globalQuota;

  public GPTRoomSetting() {}

  public GPTRoomSetting(UUID roomId) {
    this.roomId = roomId;
    int i = 0;
    for (GPTRoomSettingRight r : GPTRoomSettingRight.values()) {
      i |= r.defaultValue;
    }
    this.rightsBitset = i;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public UUID getRoomQuotaId() {
    return roomQuotaId;
  }

  public void setRoomQuotaId(UUID roomQuotaId) {
    this.roomQuotaId = roomQuotaId;
  }

  public UUID getParticipantQuotaId() {
    return participantQuotaId;
  }

  public void setParticipantQuotaId(UUID participantQuotaId) {
    this.participantQuotaId = participantQuotaId;
  }

  public UUID getModeratorQuotaId() {
    return moderatorQuotaId;
  }

  public void setModeratorQuotaId(UUID moderatorQuotaId) {
    this.moderatorQuotaId = moderatorQuotaId;
  }

  public int getRightsBitset() {
    return rightsBitset;
  }

  public void setRightsBitset(int rightsBitset) {
    this.rightsBitset = rightsBitset;
  }

  public long getPaymentCounter() {
    return paymentCounter;
  }

  public void setPaymentCounter(long paymentCounter) {
    this.paymentCounter = paymentCounter;
  }

  public String getPresetContext() {
    return presetContext;
  }

  public void setPresetContext(String presetContext) {
    this.presetContext = presetContext;
  }

  public String getPresetLength() {
    return presetLength;
  }

  public void setPresetLength(String presetLength) {
    this.presetLength = presetLength;
  }

  public String getRoleInstruction() {
    return roleInstruction;
  }

  public void setRoleInstruction(String roleInstruction) {
    this.roleInstruction = roleInstruction;
  }

  public String getDefaultModel() {
    return defaultModel;
  }

  public void setDefaultModel(String defaultModel) {
    this.defaultModel = defaultModel;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<GPTRoomKey> getApiKeys() {
    return apiKeys;
  }

  public void setApiKeys(List<GPTRoomKey> apiKeys) {
    this.apiKeys = apiKeys;
  }

  public List<GPTRoomModel> getApiModels() {
    return apiModels;
  }

  public void setApiModels(List<GPTRoomModel> apiModels) {
    this.apiModels = apiModels;
  }

  public long getGlobalQuota() {
      return globalQuota;
  }

  public void setGlobalQuota(long globalQuota) {
      this.globalQuota = globalQuota;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result =
      prime * result + ((roomQuotaId == null) ? 0 : roomQuotaId.hashCode());
    result =
      prime *
      result +
      ((participantQuotaId == null) ? 0 : participantQuotaId.hashCode());
    result =
      prime *
      result +
      ((moderatorQuotaId == null) ? 0 : moderatorQuotaId.hashCode());
    result = prime * result + rightsBitset;
    result = prime * result + (int) (paymentCounter ^ (paymentCounter >>> 32));
    result =
      prime * result + ((presetContext == null) ? 0 : presetContext.hashCode());
    result =
      prime * result + ((presetLength == null) ? 0 : presetLength.hashCode());
    result =
      prime *
      result +
      ((roleInstruction == null) ? 0 : roleInstruction.hashCode());
    result =
      prime * result + ((defaultModel == null) ? 0 : defaultModel.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomSetting other = (GPTRoomSetting) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (roomQuotaId == null) {
      if (other.roomQuotaId != null) return false;
    } else if (!roomQuotaId.equals(other.roomQuotaId)) return false;
    if (participantQuotaId == null) {
      if (other.participantQuotaId != null) return false;
    } else if (
      !participantQuotaId.equals(other.participantQuotaId)
    ) return false;
    if (moderatorQuotaId == null) {
      if (other.moderatorQuotaId != null) return false;
    } else if (!moderatorQuotaId.equals(other.moderatorQuotaId)) return false;
    if (rightsBitset != other.rightsBitset) return false;
    if (paymentCounter != other.paymentCounter) return false;
    if (presetContext == null) {
      if (other.presetContext != null) return false;
    } else if (!presetContext.equals(other.presetContext)) return false;
    if (presetLength == null) {
      if (other.presetLength != null) return false;
    } else if (!presetLength.equals(other.presetLength)) return false;
    if (roleInstruction == null) {
      if (other.roleInstruction != null) return false;
    } else if (!roleInstruction.equals(other.roleInstruction)) return false;
    if (defaultModel == null) {
      if (other.defaultModel != null) return false;
    } else if (!defaultModel.equals(other.defaultModel)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomSetting [id=" +
      id +
      ", roomId=" +
      roomId +
      ", roomQuotaId=" +
      roomQuotaId +
      ", participantQuotaId=" +
      participantQuotaId +
      ", moderatorQuotaId=" +
      moderatorQuotaId +
      ", rightsBitset=" +
      rightsBitset +
      ", paymentCounter=" +
      paymentCounter +
      ", presetContext=" +
      presetContext +
      ", presetLength=" +
      presetLength +
      ", roleInstruction=" +
      roleInstruction +
      ", defaultModel=" +
      defaultModel +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", apiKeys=" +
      apiKeys +
      ", apiModels=" +
      apiModels +
      ", globalQuota=" +
      globalQuota +
      "]"
    );
  }
}
