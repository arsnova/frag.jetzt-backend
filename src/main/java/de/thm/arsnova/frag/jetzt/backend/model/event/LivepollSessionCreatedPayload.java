package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;

public class LivepollSessionCreatedPayload implements WebSocketPayload {

  private LivepollSession livepoll;

  public LivepollSessionCreatedPayload() {}

  public LivepollSessionCreatedPayload(LivepollSession session) {
    this.livepoll = session;
  }

  public LivepollSession getLivepoll() {
    return livepoll;
  }

  public void setLivepoll(LivepollSession livepoll) {
    this.livepoll = livepoll;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((livepoll == null) ? 0 : livepoll.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollSessionCreatedPayload other = (LivepollSessionCreatedPayload) obj;
    if (livepoll == null) {
      if (other.livepoll != null) return false;
    } else if (!livepoll.equals(other.livepoll)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "LivepollSessionCreatedPayload [livepoll=" + livepoll + "]";
  }
}
