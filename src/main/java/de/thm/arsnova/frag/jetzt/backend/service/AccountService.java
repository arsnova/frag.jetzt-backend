package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.AccountKeycloakRole;
import de.thm.arsnova.frag.jetzt.backend.model.FragjetztKeycloakEvent;
import de.thm.arsnova.frag.jetzt.backend.model.FragjetztKeycloakEvent.FragjetztEventType;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountKeycloakRoleRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.KeycloakProviderRepository;
import de.thm.arsnova.frag.jetzt.backend.util.IpParser;
import de.thm.arsnova.frag.jetzt.backend.util.IpParser.ValidIps;
import de.thm.arsnova.frag.jetzt.backend.util.KeycloakClient;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.gpt.service.creators.GPTCreateGPTUserService;
import de.thm.arsnova.frag.jetzt.paypal.service.persistence.PaymentRepository;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AccountService {

  private static final Logger logger = LoggerFactory.getLogger(
    AccountService.class
  );

  @Value("${server.root-url}")
  private String rootUrl;

  @Value("${app.registration.domains}")
  private String[] allowedEmailDomains;

  @Value("${app.credentials.emaillength}")
  private int maxCredentialsMailLength;

  private final AccountRepository repository;
  private final AccountKeycloakRoleRepository roleRepository;
  private final KeycloakProviderRepository providerRepository;
  private final GPTCreateGPTUserService createGPTUserService;
  private final PaymentRepository paymentRepository;
  private final AuthorizationHelper authorizationHelper;
  private final AccountServiceBase accountServiceBase;
  private Pattern mailPattern;

  @Autowired
  public AccountService(
    AccountRepository repository,
    AccountKeycloakRoleRepository roleRepository,
    KeycloakProviderRepository providerRepository,
    GPTCreateGPTUserService createGPTUserService,
    PaymentRepository paymentRepository,
    AuthorizationHelper authorizationHelper,
    AccountServiceBase accountServiceBase
  ) {
    this.repository = repository;
    this.roleRepository = roleRepository;
    this.providerRepository = providerRepository;
    this.createGPTUserService = createGPTUserService;
    this.paymentRepository = paymentRepository;
    this.authorizationHelper = authorizationHelper;
    this.accountServiceBase = accountServiceBase;
  }

  // Is scheduled for every 10 Minutes
  @Scheduled(fixedDelay = 600000)
  public void deleteAccountsWithMissedActivation() {
    logger.info("Deleting stale accounts");
    final Timestamp expired = Timestamp.from(
      Instant.now().minus(180, ChronoUnit.DAYS)
    );
    paymentRepository
      .countByAccountIdGrouped()
      .reduce(
        new HashMap<UUID, Long>(),
        (acc, e) -> {
          acc.put(e.getAccountId(), e.getCount());
          return acc;
        }
      )
      .flatMapMany(cache ->
        Flux
          .concat(
            repository.findAllByUpdatedAtIsNullAndCreatedAtLessThan(expired),
            repository.findAllByUpdatedAtIsNotNullAndUpdatedAtLessThan(expired)
          )
          .map(u -> u.getId())
          .filter(e -> cache.getOrDefault(e, 0L) == 0L)
      )
      .buffer(100)
      .flatMap(accounts -> {
        if (accounts.isEmpty()) {
          return Mono.empty();
        }
        return repository.deleteAllById(accounts);
      })
      .count()
      .doOnSuccess(count -> {
        if (count > 0) {
          logger.error("[INFO] " + count + " inactive accounts deleted.");
        }
      })
      .subscribe();
  }

  public Flux<String> getAllEmails() {
    return repository
      .findAll()
      .filter(acc -> acc.getEmail() != null)
      .map(acc -> acc.getEmail());
  }

  public Flux<Account> get(final UUID[] id) {
    return repository
      .findAllById(Arrays.asList(id))
      .flatMap(accountServiceBase::applyTransient);
  }

  public Mono<Account> createGuest() {
    logger.trace("Creating new guest account");
    return createNew().flatMap(accountServiceBase::applyTransient);
  }

  public Flux<Account> getByEmail(final String email) {
    return repository.findAllByEmailIgnoreCase(email);
  }

  public Mono<Void> onEvent(
    UUID id,
    FragjetztKeycloakEvent event,
    byte[] address
  ) {
    return providerRepository
      .findById(id)
      .filter(provider -> {
        List<ValidIps> list;
        try {
          list = IpParser.parseIpRanges(provider.getAllowedIps());
        } catch (UnknownHostException e) {
          e.printStackTrace();
          return false;
        }
        if (list.isEmpty()) {
          return true;
        }
        if (address == null) {
          return false;
        }
        for (ValidIps ips : list) {
          if (ips.contains(address)) {
            return true;
          }
        }
        logger.warn("Unwanted access from: {}", ipToString(address));
        return false;
      })
      .flatMap(provider -> {
        if (!provider.getEventPassword().equals(event.getEventPassword())) {
          return Mono.empty();
        }
        try {
          UUID userId = UUID.fromString(event.getUserId());
          if (event.getEventType() == FragjetztEventType.DELETE_ACCOUNT) {
            return repository
              .deleteByKeycloakIdAndKeycloakUserId(id, userId)
              .then();
          } else if (
            event.getEventType() == FragjetztEventType.UPDATE_ACCOUNT
          ) {
            String email = event.getDetails().get("email");
            if (email == null) {
              return Mono.empty();
            }
            return validateEmail(email)
              .flatMap(ok ->
                repository.findByKeycloakIdAndKeycloakUserId(id, userId)
              )
              .flatMap(account -> {
                account.setEmail(email);
                return repository.save(account);
              })
              .then();
          }
          return Mono.empty();
        } catch (Exception e) {
          return Mono.empty();
        }
      });
  }

  public Mono<Account> getOrCreateByKeycloakToken(
    UUID keycloakId,
    String token
  ) {
    return providerRepository // find keycloak provider
      .findById(keycloakId)
      .switchIfEmpty(
        Mono.error(new BadRequestException("login.provider-not-found"))
      )
      // verify against keycloak provider
      .flatMap(provider -> {
        String url = provider.getUrl() + "/realms/" + provider.getRealm();
        return new KeycloakClient(url)
          .verifyToken(token)
          .switchIfEmpty(
            Mono.error(new ForbiddenException("login.token-invalid"))
          );
      })
      .flatMap(claims -> {
        final String email = claims.get("email", String.class);
        final UUID keycloakUserId = UUID.fromString(
          claims.get("sub", String.class)
        );
        @SuppressWarnings("unchecked")
        final Map<String, Object> realmAccess = claims.get(
          "realm_access",
          Map.class
        );
        @SuppressWarnings("unchecked")
        final List<String> roles = (List<String>) realmAccess.get("roles");
        // verify email
        return validateEmail(email)
          .switchIfEmpty(
            Mono.error(new ForbiddenException("login.mail-not-allowed"))
          )
          // find account by keycloak user id
          .flatMap(success ->
            repository.findByKeycloakIdAndKeycloakUserId(
              keycloakId,
              keycloakUserId
            )
          )
          // or find account by email (where keycloak user id = null)
          .switchIfEmpty(
            repository.findByKeycloakIdAndKeycloakUserIdIsNullAndEmailIgnoreCase(
              keycloakId,
              email
            )
          )
          // or create a new account
          .switchIfEmpty(createNew())
          .flatMap(user -> {
            // update
            user.setEmail(email);
            user.setKeycloakId(keycloakId);
            user.setKeycloakUserId(keycloakUserId);
            user.refreshLastLogin();
            user.refreshLastActive();
            Set<String> newRoles = new HashSet<>(roles);
            user.setRoles(newRoles);
            return repository
              .save(user)
              .flatMap(newUser -> {
                return roleRepository
                  .findAllByAccountId(user.getId())
                  .collectList()
                  .flatMap(oldRoles -> {
                    Set<String> toAdd = new HashSet<>(newRoles);
                    for (String key : newRoles) {
                      boolean deleted = false;
                      for (int i = oldRoles.size() - 1; i >= 0; i--) {
                        if (oldRoles.get(i).getRole().equals(key)) {
                          oldRoles.remove(i);
                          deleted = true;
                        }
                      }
                      if (deleted) {
                        toAdd.remove(key);
                      }
                    }
                    Mono<Boolean> operations = Mono.just(true);
                    if (toAdd.size() > 0) {
                      List<AccountKeycloakRole> toAddList = new ArrayList<>();
                      for (String role : toAdd) {
                        toAddList.add(
                          new AccountKeycloakRole(user.getId(), role)
                        );
                      }
                      operations =
                        operations.then(
                          roleRepository
                            .saveAll(toAddList)
                            .then(Mono.just(true))
                        );
                    }
                    if (oldRoles.size() > 0) {
                      Set<UUID> toDelete = new HashSet<>();
                      for (AccountKeycloakRole keycloakRole : oldRoles) {
                        toDelete.add(keycloakRole.getId());
                      }
                      operations =
                        operations.then(
                          roleRepository
                            .deleteAllById(toDelete)
                            .then(Mono.just(true))
                        );
                    }
                    return operations.map(success -> user);
                  });
              });
          });
      });
  }

  public <T> Mono<T> refreshLogin(UUID accountId, Mono<T> t) {
    return repository
      .findById(accountId)
      .flatMap(user -> {
        user.refreshLastLogin();
        user.refreshLastActive();
        return repository.save(user);
      })
      .then(t);
  }

  private Mono<Account> createNew() {
    Account account = new Account();
    return repository
      .save(account)
      .flatMap(acc ->
        createGPTUserService.createNew(acc.getId()).thenReturn(acc)
      );
  }

  public Mono<Void> delete(final UUID id) {
    return authorizationHelper
      .getCurrentUser()
      .filter(authenticatedUser ->
        authenticatedUser.getAccountId().equals(id) &&
        authenticatedUser.getName() == null
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(authenticatedUser ->
        repository.deleteById(authenticatedUser.getAccountId())
      );
  }

  private void parseMailAddressPattern() {
    List<String> allowedEmailDomains = Arrays.asList(this.allowedEmailDomains);
    if (allowedEmailDomains == null || allowedEmailDomains.isEmpty()) {
      return;
    }
    final List<String> patterns = new ArrayList<>();
    if (allowedEmailDomains.contains("*")) {
      patterns.add("([a-z0-9-]+\\.)+[a-z0-9-]+");
      patterns.add("admin");
    } else {
      final Pattern patternPattern = Pattern.compile(
        "[a-z0-9.*-]+",
        Pattern.CASE_INSENSITIVE
      );
      for (final String patternStr : allowedEmailDomains) {
        if (patternPattern.matcher(patternStr).matches()) {
          patterns.add(
            patternStr.replaceAll("[.]", "[.]").replaceAll("[*]", "[a-z0-9-]+?")
          );
        }
      }
    }
    mailPattern =
      Pattern.compile(
        "[a-z0-9._-]+?@(" + String.join("|", patterns) + ")",
        Pattern.CASE_INSENSITIVE
      );
    logger.info(
      "Allowed e-mail addresses (pattern) for registration: '{}'.",
      mailPattern.pattern()
    );
  }

  private Mono<String> validateEmail(String email) {
    final String lcEmail = email.toLowerCase();
    if (mailPattern == null) {
      parseMailAddressPattern();
    }
    if (mailPattern == null || !mailPattern.matcher(lcEmail).matches()) {
      logger.info(
        "User registration failed. {} does not match pattern.",
        lcEmail
      );
      return Mono.error(new ForbiddenException("Email not allowed"));
    }
    return Mono
      .just(email)
      .filter(c -> email.length() <= maxCredentialsMailLength)
      .switchIfEmpty(
        Mono.error(new BadRequestException("Maximal email length exceeded"))
      );
  }

  private String ipToString(byte[] address) {
    if (address == null || (address.length != 4 && address.length != 16)) {
      return "unknown";
    }
    if (address.length == 4) {
      StringBuilder b = new StringBuilder();
      for (int i = 0; i < 4; i++) {
        if (i > 0) {
          b.append(".");
        }
        b.append(Integer.toString(address[i] & 0xFF));
      }
      return b.toString();
    }
    StringBuilder b = new StringBuilder();
    final int size = address.length / 2;
    for (int i = 0; i < size; i++) {
      if (i > 0) {
        b.append(":");
      }
      b.append(
        Integer.toHexString(
          ((address[i * 2] & 0xFF) << 8) | (address[i * 2 + 1] & 0xFF)
        )
      );
    }
    return b.toString();
  }
}
