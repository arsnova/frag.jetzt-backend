package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Breakdown {
    // optional
    @JsonProperty("item_total")
    private Price itemTotal;
    // optional
    private Price shipping;
    // optional
    private Price handling;
    // optional
    @JsonProperty("tax_total")
    private Price taxTotal;
    // optional
    private Price insurance;
    // optional
    @JsonProperty("shipping_discount")
    private Price shippingDiscount;
    // optional
    private Price discount;
}
