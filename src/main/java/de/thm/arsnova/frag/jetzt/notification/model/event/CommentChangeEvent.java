package de.thm.arsnova.frag.jetzt.notification.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.event.WebSocketEvent;

public class CommentChangeEvent extends WebSocketEvent<CommentChangeEventPayload> {

    public CommentChangeEvent() {
        super(CommentChangeEvent.class.getSimpleName());
    }

    public CommentChangeEvent(CommentChangeEventPayload payload) {
        super(CommentChangeEvent.class.getSimpleName(), payload.getRoomId());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentChangeEvent that = (CommentChangeEvent) o;
        return this.getPayload().equals(that.getPayload());
    }

}
