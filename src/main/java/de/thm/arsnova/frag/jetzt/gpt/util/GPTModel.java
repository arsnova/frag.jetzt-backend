package de.thm.arsnova.frag.jetzt.gpt.util;

import de.thm.arsnova.frag.jetzt.gpt.util.encoding.GPTDefaultEncodings;
import de.thm.arsnova.frag.jetzt.gpt.util.encoding.GPTEncoder;
import de.thm.arsnova.frag.jetzt.quota.model.QuotaUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GPTModel {

  private static final Map<String, GPTModel> allowedModels;

  static {
    HashMap<String, GPTModel> map = new HashMap<>();
    map.put(
      "gpt-4o",
      new GPTModel(
        "gpt-4o",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.ASSISTANT_WITH_RETRIEVAL, Endpoint.CHAT },
        new QuotaUnit(5, 6),
        new QuotaUnit(15, 6),
        128000,
        4096,
        8,
        GPTDefaultEncodings.getO200kBaseEncoder()
      )
    );
    map.put(
      "gpt-4-turbo",
      new GPTModel(
        "gpt-4-turbo",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.ASSISTANT_WITH_RETRIEVAL, Endpoint.CHAT },
        new QuotaUnit(1, 5),
        new QuotaUnit(3, 5),
        128000,
        4096,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-4-32k",
      new GPTModel(
        "gpt-4-32k",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.CHAT },
        new QuotaUnit(6, 5),
        new QuotaUnit(12, 5),
        32768,
        4096,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-4",
      new GPTModel(
        "gpt-4",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.CHAT },
        new QuotaUnit(3, 5),
        new QuotaUnit(6, 5),
        8192,
        4096,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-3.5-turbo",
      new GPTModel(
        "gpt-3.5-turbo",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.CHAT },
        new QuotaUnit(5, 7),
        new QuotaUnit(15, 7),
        16384,
        4096,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-3.5-turbo-instruct",
      new GPTModel(
        "gpt-3.5-turbo-instruct",
        new Endpoint[] { Endpoint.ASSISTANT, Endpoint.CHAT },
        new QuotaUnit(15, 7),
        new QuotaUnit(2, 6),
        4096,
        4096,
        5,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    allowedModels = Collections.unmodifiableMap(map);
  }

  public enum Endpoint {
    ASSISTANT_WITH_RETRIEVAL,
    ASSISTANT,
    CHAT,
  }

  public static Map<String, GPTModel> getAllowedModels() {
    return allowedModels;
  }

  private final String name;
  private final Endpoint[] endpoints;
  private final QuotaUnit costPerPromptToken;
  private final QuotaUnit costPerCompletionToken;
  private final int maxTokens;
  private final int maxOutputTokens;
  /**
   * When using the streaming endpoint, it is not possible to decide how many tokens are generated when an error occurs.
   * Also, when a request is interrupted (for both streaming and non-streaming endpoints) due to a network connection error
   * or premature closure, the text is not sent in full, so no additional tokens can be calculated.<br/>
   * <br/>
   * If a non-streaming request is closed or an error occurs, the full maxTokens are charged as the cost of the request.<br/>
   * <br/>
   * When a streaming request is closed or an error occurs, the currently received completion text and
   * an additional offset (not exceeding maxTokens) are charged as the request cost.
   */
  private final int interruptTokens;
  private final GPTEncoder encoder;

  public GPTModel(
    String name,
    Endpoint[] endpoints,
    QuotaUnit costPerPromptToken,
    QuotaUnit costPerCompletionToken,
    int maxTokens,
    int maxOutputTokens,
    int interruptTokens,
    GPTEncoder encoder
  ) {
    this.name = name;
    this.endpoints = endpoints;
    this.costPerPromptToken = costPerPromptToken;
    this.costPerCompletionToken = costPerCompletionToken;
    this.maxTokens = maxTokens;
    this.maxOutputTokens = maxOutputTokens;
    this.interruptTokens = interruptTokens;
    this.encoder = encoder;
  }

  public String getName() {
      return name;
  }

  public Endpoint[] getEndpoints() {
    return endpoints;
  }

  public QuotaUnit getCostPerPromptToken() {
    return costPerPromptToken;
  }

  public QuotaUnit getCostPerCompletionToken() {
    return costPerCompletionToken;
  }

  public int getMaxOutputTokens() {
    return maxOutputTokens;
  }

  public int getMaxTokens() {
    return maxTokens;
  }

  public int getInterruptTokens() {
    return interruptTokens;
  }

  public GPTEncoder getEncoder() {
    return encoder;
  }
}
