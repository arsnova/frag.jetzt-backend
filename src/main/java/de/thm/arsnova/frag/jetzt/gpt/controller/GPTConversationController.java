package de.thm.arsnova.frag.jetzt.gpt.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversation;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversationEntry;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTConversationService;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("GPTConversationController")
@RequestMapping(GPTConversationController.REQUEST_MAPPING)
public class GPTConversationController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/gpt-conversation";
  private static final String ALL = "/all";
  private static final String ADD_MESSAGE = "/add-message";
  private static final String DELETE_MESSAGES = "/delete-messages/{id}/{index}";

  private final GPTConversationService service;

  @Autowired
  public GPTConversationController(GPTConversationService service) {
    this.service = service;
  }

  @PostMapping(POST_MAPPING)
  public Mono<GPTConversation> post(@RequestBody GPTConversation conversation) {
    return service.update(conversation);
  }

  @PatchMapping(PATCH_MAPPING)
  public Mono<GPTConversation> post(
    @PathVariable("id") UUID id,
    @RequestBody Map<String, Object> changes
  ) {
    return service.patch(id, changes);
  }

  @DeleteMapping(DELETE_MAPPING)
  public Mono<Void> delete(@PathVariable("id") UUID id) {
    return service.delete(id);
  }

  @GetMapping(ALL)
  public Flux<GPTConversation> getAll() {
    return service.getAll();
  }

  @PostMapping(ADD_MESSAGE)
  public Mono<GPTConversationEntry> postMessage(
    @RequestBody GPTConversationEntry entry
  ) {
    return service.addEntry(entry);
  }

  @DeleteMapping(DELETE_MESSAGES)
  public Mono<Void> deleteMessages(
    @PathVariable("id") UUID id,
    @PathVariable("index") int index
  ) {
    return service.deleteEntries(id, index);
  }
}
