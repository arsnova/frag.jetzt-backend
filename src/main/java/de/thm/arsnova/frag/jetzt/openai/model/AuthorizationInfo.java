package de.thm.arsnova.frag.jetzt.openai.model;

import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

public class AuthorizationInfo {

  private String apiKey;
  private String apiOrganization;
  private String apiProject;

  public AuthorizationInfo() {}

  public AuthorizationInfo(String apiKey, String apiOrganization, String apiProject) {
    this.apiKey = apiKey;
    this.apiOrganization = apiOrganization;
    this.apiProject = apiProject;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getApiOrganization() {
    return apiOrganization;
  }

  public void setApiOrganization(String apiOrganization) {
    this.apiOrganization = apiOrganization;
  }

  public String getApiProject() {
      return apiProject;
  }

  public void setApiProject(String apiProject) {
      this.apiProject = apiProject;
  }

  public <T extends RequestHeadersSpec<?>> T apply(T spec) {
    if (apiOrganization != null) {
        spec = (T) spec.header("OpenAI-Organization", apiOrganization);
    }
    if (apiProject != null) {
        spec = (T) spec.header("OpenAI-Project", apiProject);
    }
    return (T) spec.header("Authorization", "Bearer " + apiKey);
  }
}
