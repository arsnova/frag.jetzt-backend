package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomModel;
import reactor.core.publisher.Flux;

@Repository
public interface GPTRoomModelRepository extends ReactiveCrudRepository<GPTRoomModel, UUID> {
    
    Flux<GPTRoomModel> findAllBySettingId(UUID settingId);

    Flux<Void> deleteAllBySettingId(UUID settingId);

}
