package de.thm.arsnova.frag.jetzt.backend.config;

import de.thm.arsnova.frag.jetzt.backend.security.AuthenticationManager;
import de.thm.arsnova.frag.jetzt.backend.security.SecurityContextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
public class WebSecurityConfig {

  private final SecurityContextRepository securityContextRepository;
  private final AuthenticationManager authenticationManager;

  private static final String[] AUTH_WHITELIST = {
    "/auth/login/guest",
    "/auth/login/registered/{keycloakId}",
    "/auth/login",
    "/rating/accumulated",
    "/keycloak-provider/providers/all",
    "/keycloak-provider/event/{keycloakId}",
    "/motds/all",
    "/paypal/webhook",
    "/api-docs",
    "/api-docs/*",
    "/swagger-ui.html",
    "/webjars/swagger-ui/*"
  };

  @Autowired
  public WebSecurityConfig(
    SecurityContextRepository securityContextRepository,
    AuthenticationManager authenticationManager
  ) {
    this.securityContextRepository = securityContextRepository;
    this.authenticationManager = authenticationManager;
  }

  @Bean
  public SecurityWebFilterChain securityWebFilterChain(
    ServerHttpSecurity http
  ) {
    http
      .exceptionHandling(exceptionHandling ->
        exceptionHandling.authenticationEntryPoint((swe, e) ->
          Mono.fromRunnable(() ->
            swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED)
          )
        )
      )
      .csrf(csrf -> csrf.disable())
      .formLogin(formLogin -> formLogin.disable())
      .httpBasic(httpBasic -> httpBasic.disable())
      .securityContextRepository(securityContextRepository)
      .authenticationManager(authenticationManager)
      .authorizeExchange(exchanges ->
        exchanges
          .pathMatchers(HttpMethod.OPTIONS)
          .permitAll()
          .pathMatchers(AUTH_WHITELIST)
          .permitAll()
          .anyExchange()
          .authenticated()
      );
    return http.build();
  }
}
