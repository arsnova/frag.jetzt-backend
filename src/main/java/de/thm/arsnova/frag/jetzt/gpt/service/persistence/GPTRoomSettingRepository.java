package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import reactor.core.publisher.Mono;

@Repository
public interface GPTRoomSettingRepository extends ReactiveCrudRepository<GPTRoomSetting, UUID> {

    Mono<GPTRoomSetting> findByRoomId(UUID roomId);

}
