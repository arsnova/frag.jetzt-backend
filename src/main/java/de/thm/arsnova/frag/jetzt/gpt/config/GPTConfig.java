package de.thm.arsnova.frag.jetzt.gpt.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.thm.arsnova.frag.jetzt.backend.util.DataDirectory;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GPTConfig {

  private static final Logger logger = LoggerFactory.getLogger(GPTConfig.class);
  private static final ObjectMapper mapper = new ObjectMapper(
    new YAMLFactory()
  );
  private static final GPTConfig instance;

  public static GPTConfig getInstance() {
    return instance;
  }

  static {
    instance = load();
  }

  private static GPTConfig load() {
    File f = DataDirectory.get("gpt_config.yml");
    if (!f.exists()) {
      GPTConfig c = new GPTConfig();
      c.file = f;
      return c;
    }
    try {
      GPTConfig config = mapper.readValue(f, GPTConfig.class);
      config.file = f;
      return config;
    } catch (IOException e) {
      logger.error("Failed to parse GPTConfig file, using default!", e);
      return new GPTConfig();
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class GPTRestrictions {

    private boolean active = false;
    private int globalAccumulatedQuota = 500; // 10^-2 USD
    private boolean globalActive = false;
    private Timestamp endDate = Timestamp.from(Instant.now());

    @JsonIgnore
    public boolean canBeAccessed() {
      return (
        active &&
        (endDate == null || endDate.toInstant().isAfter(Instant.now()))
      );
    }

    public Timestamp getEndDate() {
      return endDate;
    }

    public void setEndDate(Timestamp endDate) {
      this.endDate = endDate;
    }

    public boolean isGlobalActive() {
      return globalActive;
    }

    public void setGlobalActive(boolean globalActive) {
      this.globalActive = globalActive;
    }

    public boolean isActive() {
      return active;
    }

    public void setActive(boolean active) {
      this.active = active;
    }

    public int getGlobalAccumulatedQuota() {
      return this.globalAccumulatedQuota;
    }

    public void setGlobalAccumulatedQuota(int quota) {
      this.globalAccumulatedQuota = quota;
    }

    @JsonIgnore
    public long getGlobalQuota() {
      if (!globalActive || !canBeAccessed()) {
        return 0;
      }
      return globalAccumulatedQuota * 1_000_000L;
    }

    @Override
    public String toString() {
      return (
        "GPTRestrictions [active=" +
        active +
        ", globalActive=" +
        globalActive +
        ", globalAccumulatedQuota=" +
        globalAccumulatedQuota +
        ", endDate=" +
        endDate +
        "]"
      );
    }

    public GPTRestrictions clone() {
      GPTRestrictions cloned = new GPTRestrictions();
      cloned.active = active;
      cloned.globalAccumulatedQuota = globalAccumulatedQuota;
      cloned.globalActive = globalActive;
      cloned.endDate = endDate;
      return cloned;
    }
  }

  @JsonIgnore
  private File file = null;

  private String apiKey = null;
  private String organization = null;
  private GPTRestrictions restrictions = new GPTRestrictions();

  public GPTRestrictions getRestrictions() {
    return restrictions;
  }

  public void setRestrictions(GPTRestrictions restrictions) {
    this.restrictions = restrictions;
  }

  public String getOrganization() {
    return organization;
  }

  public void setOrganization(String organization) {
    this.organization = organization;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public void save() {
    if (this.file == null) {
      return;
    }
    try {
      mapper.writeValue(file, this);
    } catch (IOException e) {
      logger.error("Could not save ChatGPT Config!", e);
    }
  }

  @Override
  public String toString() {
    return (
      "GPTConfig [file=" +
      file +
      ", apiKey=" +
      apiKey +
      ", organization=" +
      organization +
      ", restrictions=" +
      restrictions +
      "]"
    );
  }

  public GPTConfig clone() {
    GPTConfig cloned = new GPTConfig();
    cloned.apiKey =
      apiKey == null
        ? null
        : "***************************************************";
    cloned.organization = organization;
    cloned.restrictions = restrictions.clone();
    return cloned;
  }
}
