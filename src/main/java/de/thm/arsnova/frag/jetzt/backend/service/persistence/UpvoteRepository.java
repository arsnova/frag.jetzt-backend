package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Upvote;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface UpvoteRepository extends ReactiveCrudRepository<Upvote, UUID> {
    Flux<Upvote> findByCommentId(UUID commentId);

    Mono<Upvote> findByCommentIdAndAccountId(UUID commentId, UUID accountId);

    Flux<Upvote> findAllByAccountIdAndCommentIdIn(UUID accountId, List<UUID> commentId);

    Mono<Void> deleteByAccountIdAndCommentId(UUID accountId, UUID commentId);
}
