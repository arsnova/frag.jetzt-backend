package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.List;
import java.util.Objects;

public class BrainstormingCategoriesUpdatedPayload implements WebSocketPayload {

    private List<BrainstormingCategory> categoryList;

    public BrainstormingCategoriesUpdatedPayload(List<BrainstormingCategory> categoryList){
        this.categoryList = categoryList;
    }

    public List<BrainstormingCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<BrainstormingCategory> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public String toString() {
        return "BrainstormingCategoriesUpdatedPayload{" +
                "categoryList=" + categoryList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCategoriesUpdatedPayload that = (BrainstormingCategoriesUpdatedPayload) o;
        return Objects.equals(categoryList, that.categoryList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryList);
    }
}
