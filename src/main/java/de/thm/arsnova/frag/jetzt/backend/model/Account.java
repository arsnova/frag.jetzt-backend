package de.thm.arsnova.frag.jetzt.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

public class Account implements Persistable<UUID> {

  public enum AccountRole {
    ADMIN_DASHBOARD("admin-dashboard"),
    ADMIN_ALL_ROOMS_OWNER("admin-all-rooms-owner");

    private final String roleString;

    AccountRole(String roleString) {
      this.roleString = roleString;
    }

    public String getRoleString() {
      return roleString;
    }
  }

  @Id
  private UUID id;

  private String email;
  private Timestamp lastLogin = Timestamp.from(Instant.now());
  private Timestamp lastActive;
  private UUID keycloakId;
  private UUID keycloakUserId;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  @Transient
  private Set<String> roles = new HashSet<>();

  @Override
  public boolean isNew() {
    return id == null;
  }

  public Account() {}

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Timestamp getLastLogin() {
    return lastLogin;
  }

  public void refreshLastLogin() {
    lastLogin = Timestamp.from(Instant.now());
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Timestamp getLastActive() {
    return lastActive;
  }

  public void refreshLastActive() {
    lastActive = Timestamp.from(Instant.now());
  }

  public Set<String> getRoles() {
    return roles;
  }

  public void setRoles(Set<String> roles) {
    this.roles = roles;
  }

  @JsonIgnore
  public boolean hasRole(AccountRole role) {
    return this.roles.contains(role.roleString);
  }

  public UUID getKeycloakId() {
    return keycloakId;
  }

  public void setKeycloakId(UUID keycloakId) {
    this.keycloakId = keycloakId;
  }

  public UUID getKeycloakUserId() {
    return keycloakUserId;
  }

  public void setKeycloakUserId(UUID keycloakUserId) {
    this.keycloakUserId = keycloakUserId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((lastLogin == null) ? 0 : lastLogin.hashCode());
    result =
      prime * result + ((lastActive == null) ? 0 : lastActive.hashCode());
    result =
      prime * result + ((keycloakId == null) ? 0 : keycloakId.hashCode());
    result =
      prime *
      result +
      ((keycloakUserId == null) ? 0 : keycloakUserId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Account other = (Account) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (email == null) {
      if (other.email != null) return false;
    } else if (!email.equals(other.email)) return false;
    if (lastLogin == null) {
      if (other.lastLogin != null) return false;
    } else if (!lastLogin.equals(other.lastLogin)) return false;
    if (lastActive == null) {
      if (other.lastActive != null) return false;
    } else if (!lastActive.equals(other.lastActive)) return false;
    if (keycloakId == null) {
      if (other.keycloakId != null) return false;
    } else if (!keycloakId.equals(other.keycloakId)) return false;
    if (keycloakUserId == null) {
      if (other.keycloakUserId != null) return false;
    } else if (!keycloakUserId.equals(other.keycloakUserId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "Account [id=" +
      id +
      ", email=" +
      email +
      ", lastLogin=" +
      lastLogin +
      ", lastActive=" +
      lastActive +
      ", keycloakId=" +
      keycloakId +
      ", keycloakUserId=" +
      keycloakUserId +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", roles=" +
      roles +
      "]"
    );
  }
}
