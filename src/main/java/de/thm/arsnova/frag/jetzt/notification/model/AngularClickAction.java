package de.thm.arsnova.frag.jetzt.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class AngularClickAction {

  public enum ActionOperation {
    @JsonProperty("openWindow")
    OpenWindow,
    @JsonProperty("focusLastFocusedOrOpen")
    FocusLastWindow,
    @JsonProperty("navigateLastFocusedOrOpen")
    NavigateLastWindow,
    @JsonProperty("sendRequest")
    SendRequest,
  }

  /**
   * Operation to perform when the notification is clicked.
   */
  @NonNull
  private ActionOperation operation;

  /**
   * URL to open in the browser when the notification is clicked.
   * Can be relative to the current origin or an absolute URL.
   */
  @Nullable
  private String url;

  public AngularClickAction() {}

  public AngularClickAction(ActionOperation operation, String url) {
    this.setOperation(operation);
    this.setUrl(url);
  }

  public ActionOperation getOperation() {
    return operation;
  }

  public void setOperation(ActionOperation operation) {
    if (operation == null) throw new IllegalArgumentException(
      "Operation must not be null"
    );
    this.operation = operation;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
