package de.thm.arsnova.frag.jetzt.notification.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import de.thm.arsnova.frag.jetzt.notification.util.Experimental;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * See https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification#parameters or
 * here https://developer.mozilla.org/en-US/docs/Web/API/Notification#instance_properties
 */
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
@JsonTypeName(value = "notification")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebNotification {

  /**
   * The title that must be shown within the notification
   */
  @NonNull
  private String title;

  /**
   * An array of actions to display in the notification. Each element in the array is an object with the following members: {@link WebNotificationAction}
   */
  @Nullable
  @Experimental
  private List<WebNotificationAction> actions;

  /**
   * A string containing the URL of an image to represent the notification when there is not enough space to display the notification itself
   * such as for example, the Android Notification Bar. On Android devices, the badge should accommodate devices up to 4x resolution, about
   * 96 by 96 px, and the image will be automatically masked.
   */
  @Nullable
  private String badge;

  /**
   * A string representing an extra content to display within the notification.
   */
  @Nullable
  private String body;

  /**
   * Arbitrary data that you want to be associated with the notification. This can be of any data type.
   * For Angular see: https://angular.io/guide/service-worker-notifications#notification-click-handling
   */
  @Nullable
  private Object data;

  /**
   * The direction of the notification; it can be auto, ltr or rtl.
   */
  @Nullable
  private String dir;

  /**
   * A string containing the URL of an image to be used as an icon by the notification.
   */
  @Nullable
  private String icon;

  /**
   * A string containing the URL of an image to be displayed in the notification.
   * 
   * <em>Note: About 320px by 240px in size is a good size to use for the image.</em>
   */
  @Nullable
  @Experimental
  private String image;

  /**
   * Specify the lang used within the notification. This string must be a valid language tag according to RFC 5646
   */
  @Nullable
  private String lang;

  /**
   * A boolean that indicates whether to suppress vibrations and audible alerts when reusing a tag value.
   * If options's renotify is true and options's tag is the empty string a TypeError will be thrown.
   * The default is false.
   */
  @Nullable
  @Experimental
  private Boolean renotify;

  /**
   * Indicates that on devices with sufficiently large screens, a notification should remain active until the
   * user clicks or dismisses it. If this value is absent or false, the desktop version of Chrome will auto-minimize
   * notifications after approximately twenty seconds. The default value is false.
   */
  @Nullable
  private Boolean requireInteraction;

  /**
   * When set indicates that no sounds or vibrations should be made.
   * If options's silent is true and options's vibrate is present a TypeError exception will be thrown.
   * The default value is false.
   */
  @Nullable
  private Boolean silent;

  /**
   * An ID for a given notification that allows you to find, replace,
   * or remove the notification using a script if necessary.
   */
  @Nullable
  private String tag;

  /**
   * A timestamp, given as Unix time in milliseconds, representing the time associated with the notification.
   * This could be in the past when a notification is used for a message that couldn't
   * immediately be delivered because the device was offline, or in the future for a meeting that is about to start.
   */
  @Nullable
  @JsonFormat(
    shape = JsonFormat.Shape.NUMBER,
    without = JsonFormat.Feature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS
  )
  private Instant timestamp;

  /**
   * A vibration pattern to run with the display of the notification.
   * A vibration pattern can be an array with as few as one member.
   * The values are times in milliseconds where the even indices (0, 2, 4, etc.) indicate how long to vibrate
   * and the odd indices indicate how long to pause.
   * For example, [300, 100, 400] would vibrate 300ms, pause 100ms, then vibrate 400ms.
   */
  @Nullable
  @Experimental
  private int[] vibrate;

  public WebNotification() {}

  public WebNotification(@NonNull String title) {
    this.setTitle(title);
  }

  /**
   * @see #title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @see #title
   */
  public void setTitle(String title) {
    if (title == null) throw new IllegalArgumentException(
      "title must not be null"
    );
    this.title = title;
  }

  /**
   * @see #actions
   */
  public List<WebNotificationAction> getActions() {
    return actions;
  }

  /**
   * @see #actions
   */
  public void setActions(List<WebNotificationAction> actions) {
    this.actions = actions;
  }

  /**
   * @see #badge
   */
  public String getBadge() {
    return badge;
  }

  /**
   * @see #badge
   */
  public void setBadge(String badge) {
    this.badge = badge;
  }

  /**
   * @see #body
   */
  public String getBody() {
    return body;
  }

  /**
   * @see #body
   */
  public void setBody(String body) {
    this.body = body;
  }

  /**
   * @see #data
   */
  public Object getData() {
    return data;
  }

  /**
   * @see #data
   */
  public void setData(Object data) {
    this.data = data;
  }

  /**
   * @see #dir
   */
  public String getDir() {
    return dir;
  }

  /**
   * @see #dir
   */
  public void setDir(String dir) {
    this.dir = dir;
  }

  /**
   * @see #icon
   */
  public String getIcon() {
    return icon;
  }

  /**
   * @see #icon
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }

  /**
   * @see #image
   */
  public String getImage() {
    return image;
  }

  /**
   * @see #image
   */
  public void setImage(String image) {
    this.image = image;
  }

  /**
   * @see #lang
   */
  public String getLang() {
    return lang;
  }

  /**
   * @see #lang
   */
  public void setLang(String lang) {
    this.lang = lang;
  }

  /**
   * @see #renotify
   */
  public Boolean getRenotify() {
    return renotify;
  }

  /**
   * @see #renotify
   */
  public void setRenotify(Boolean renotify) {
    this.renotify = renotify;
  }

  /**
   * @see #requireInteraction
   */
  public Boolean getRequireInteraction() {
    return requireInteraction;
  }

  /**
   * @see #requireInteraction
   */
  public void setRequireInteraction(Boolean requireInteraction) {
    this.requireInteraction = requireInteraction;
  }

  /**
   * @see #silent
   */
  public Boolean getSilent() {
    return silent;
  }

  /**
   * @see #silent
   */
  public void setSilent(Boolean silent) {
    this.silent = silent;
  }

  /**
   * @see #tag
   */
  public String getTag() {
    return tag;
  }

  /**
   * @see #tag
   */
  public void setTag(String tag) {
    this.tag = tag;
  }

  /**
   * @see #timestamp
   */
  public Instant getTimestamp() {
    return timestamp;
  }

  /**
   * @see #timestamp
   */
  public void setTimestamp(Instant timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * @see #vibrate
   */
  public int[] getVibrate() {
    return vibrate;
  }

  /**
   * @see #vibrate
   */
  public void setVibrate(int[] vibrate) {
    this.vibrate = vibrate;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((title == null) ? 0 : title.hashCode());
    result = prime * result + ((actions == null) ? 0 : actions.hashCode());
    result = prime * result + ((badge == null) ? 0 : badge.hashCode());
    result = prime * result + ((body == null) ? 0 : body.hashCode());
    result = prime * result + ((data == null) ? 0 : data.hashCode());
    result = prime * result + ((dir == null) ? 0 : dir.hashCode());
    result = prime * result + ((icon == null) ? 0 : icon.hashCode());
    result = prime * result + ((image == null) ? 0 : image.hashCode());
    result = prime * result + ((lang == null) ? 0 : lang.hashCode());
    result = prime * result + ((renotify == null) ? 0 : renotify.hashCode());
    result =
      prime *
      result +
      ((requireInteraction == null) ? 0 : requireInteraction.hashCode());
    result = prime * result + ((silent == null) ? 0 : silent.hashCode());
    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
    result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
    result = prime * result + Arrays.hashCode(vibrate);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    WebNotification other = (WebNotification) obj;
    if (title == null) {
      if (other.title != null) return false;
    } else if (!title.equals(other.title)) return false;
    if (actions == null) {
      if (other.actions != null) return false;
    } else if (!actions.equals(other.actions)) return false;
    if (badge == null) {
      if (other.badge != null) return false;
    } else if (!badge.equals(other.badge)) return false;
    if (body == null) {
      if (other.body != null) return false;
    } else if (!body.equals(other.body)) return false;
    if (data == null) {
      if (other.data != null) return false;
    } else if (!data.equals(other.data)) return false;
    if (dir == null) {
      if (other.dir != null) return false;
    } else if (!dir.equals(other.dir)) return false;
    if (icon == null) {
      if (other.icon != null) return false;
    } else if (!icon.equals(other.icon)) return false;
    if (image == null) {
      if (other.image != null) return false;
    } else if (!image.equals(other.image)) return false;
    if (lang == null) {
      if (other.lang != null) return false;
    } else if (!lang.equals(other.lang)) return false;
    if (renotify == null) {
      if (other.renotify != null) return false;
    } else if (!renotify.equals(other.renotify)) return false;
    if (requireInteraction == null) {
      if (other.requireInteraction != null) return false;
    } else if (
      !requireInteraction.equals(other.requireInteraction)
    ) return false;
    if (silent == null) {
      if (other.silent != null) return false;
    } else if (!silent.equals(other.silent)) return false;
    if (tag == null) {
      if (other.tag != null) return false;
    } else if (!tag.equals(other.tag)) return false;
    if (timestamp == null) {
      if (other.timestamp != null) return false;
    } else if (!timestamp.equals(other.timestamp)) return false;
    if (!Arrays.equals(vibrate, other.vibrate)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "Notification [title=" +
      title +
      ", actions=" +
      actions +
      ", badge=" +
      badge +
      ", body=" +
      body +
      ", data=" +
      data +
      ", dir=" +
      dir +
      ", icon=" +
      icon +
      ", image=" +
      image +
      ", lang=" +
      lang +
      ", renotify=" +
      renotify +
      ", requireInteraction=" +
      requireInteraction +
      ", silent=" +
      silent +
      ", tag=" +
      tag +
      ", timestamp=" +
      timestamp +
      ", vibrate=" +
      Arrays.toString(vibrate) +
      "]"
    );
  }
}
