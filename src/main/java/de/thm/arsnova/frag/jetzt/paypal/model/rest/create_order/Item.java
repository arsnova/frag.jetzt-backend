package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {

  public enum Category {
    DIGITAL_GOODS,
    PHYSICAL_GOODS,
    DONATION,
  }

  // required, 1-127 characters
  private String name;
  // required, 1-10 characters
  private String quantity;
  // optional, 1-127 characters
  private String description;
  // optional, 1-127 characters
  private String sku;
  // optional, 1-2048 characters
  private String url;
  // optional, 1-20 characters
  private Category category;

  // optional, 1-2048 characters, jpg, gif, png
  @JsonProperty("image_url")
  private String imageUrl;

  // required
  @JsonProperty("unit_amount")
  private Price unitAmount;

  // optional
  private Price tax;
  // optional
  private UPCCode upc;
}
