package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketMessage;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

public class WebSocketCommand<P extends WebSocketPayload> extends WebSocketMessage<P> {
    public WebSocketCommand(String type) {
        super(type);
    }
}
