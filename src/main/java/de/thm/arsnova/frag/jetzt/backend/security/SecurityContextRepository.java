package de.thm.arsnova.frag.jetzt.backend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class SecurityContextRepository implements ServerSecurityContextRepository {

    private final String TOKEN_PREFIX = "Bearer ";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public Mono<Void> save(ServerWebExchange swe, SecurityContext sc) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange swe) {
        String authHeader = swe.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if (authHeader == null) authHeader = "";
        return Mono.just(authHeader)
                // filter for Bearer Authorization Header
                .filter(potentialHeader -> potentialHeader.startsWith(TOKEN_PREFIX))
                // remove the Bearer Prefix
                .map(bearerHeader -> bearerHeader.substring(TOKEN_PREFIX.length()))
                // create Authentication object
                .map(token -> new UsernamePasswordAuthenticationToken(token, token))
                // authenticate (a.k.a. verify token)
                .flatMap(authentication -> authenticationManager.authenticate(authentication))
                // create SecurityContext from authenticated Authentication
                .map(SecurityContextImpl::new);
    }

    public Mono<AuthenticatedUser> getAuthenticatedUser(boolean refresh) {
        return ReactiveSecurityContextHolder.getContext()
                .map(securityContext -> {
                    AuthenticatedUser user = (AuthenticatedUser) securityContext.getAuthentication();
                    if (!refresh) {
                        return user;
                    } else {
                        String token = jwtUtil.generateToken(user.getAccountId(), user.getType());
                        AuthenticatedUser newUser = new AuthenticatedUser(user, token);
                        securityContext.setAuthentication(newUser);
                        return newUser;
                    }
                });
    }
}