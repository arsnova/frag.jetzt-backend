package de.thm.arsnova.frag.jetzt.openai.service.actors;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.util.DataDirectory;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.UploadedFile;
import de.thm.arsnova.frag.jetzt.openai.service.persistence.UploadFileRepository;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;
import java.util.zip.CRC32;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

@Service
public class UploadFileService {

  private final UploadFileRepository repository;
  private final AuthorizationHelper helper;
  private final MessageDigest sha1;
  private final MessageDigest md5;

  public UploadFileService(
    UploadFileRepository repository,
    AuthorizationHelper helper
  ) throws NoSuchAlgorithmException {
    this.repository = repository;
    this.helper = helper;
    sha1 = MessageDigest.getInstance("SHA-1");
    md5 = MessageDigest.getInstance("MD5");
  }

  public Mono<Tuple2<Path, String>> getById(UUID id) {
    return repository
      .findById(id)
      .map(file ->
        Tuples.of(
          DataDirectory.getFolder("uploads").resolve(file.getId().toString()),
          file.getFileName()
        )
      );
  }

  public Mono<UploadedFile> handleFileUpload(FilePart file) {
    return calculateHash(file)
      .flatMap(hash ->
        findByFileNameAndHash(file.filename(), hash)
          .switchIfEmpty(this.downloadAndCreate(file, hash))
      );
  }

  private Mono<UploadedFile> downloadAndCreate(FilePart file, byte[] hash) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        repository.save(
          new UploadedFile(user.getAccountId(), file.filename(), hash)
        )
      )
      .flatMap(uploadFile -> {
        Path newLocation = DataDirectory
          .getFolder("uploads")
          .resolve(uploadFile.getId().toString());
        return file.transferTo(newLocation).thenReturn(uploadFile);
      });
  }

  private Mono<UploadedFile> findByFileNameAndHash(
    String fileName,
    byte[] hash
  ) {
    return repository
      .findAllByFileName(fileName)
      .filter(file -> MessageDigest.isEqual(file.getFileInfo(), hash))
      .next();
  }

  private Mono<byte[]> calculateHash(FilePart file) {
    return file
      .content()
      .next() // only accept one file
      .map(data -> {
        final int length = data.readableByteCount();
        byte[] output = new byte[length];
        data.read(output);
        data.readPosition(0);
        if (length <= 80) {
          return output;
        }

        ByteBuffer outputBuffer = ByteBuffer.allocate(88);
        // crc32  = 4 bytes
        CRC32 fileCRC32 = new CRC32();
        fileCRC32.update(output);
        outputBuffer.putInt((int) fileCRC32.getValue());
        // md5    = 16 bytes
        byte[] temp = md5.digest(output);
        if (temp.length != 16) {
          throw new IllegalStateException("MD5 hash is not 16 bytes long");
        }
        outputBuffer.put(temp);
        // sha1   = 20 bytes
        temp = sha1.digest(output);
        if (temp.length != 20) {
          throw new IllegalStateException("SHA-1 hash is not 20 bytes long");
        }
        // 48 bytes
        outputBuffer.put(output, 0, 16);
        outputBuffer.put(output, length - 16, 16);
        Random random = new Random(length);
        for (int i = 0; i < 16; i++) {
          outputBuffer.put(output, random.nextInt(16, length - 16), 1);
        }
        // total = 4 + 16 + 20 + 16 + 16 + 16 = 88 bytes
        return outputBuffer.array();
      });
  }
}
