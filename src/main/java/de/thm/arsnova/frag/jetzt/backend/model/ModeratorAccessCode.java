package de.thm.arsnova.frag.jetzt.backend.model;

import java.util.Objects;

public class ModeratorAccessCode {

    private String accessCode;

    public ModeratorAccessCode() {

    }

    public ModeratorAccessCode(String accessCode){
        this.accessCode = accessCode;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    @Override
    public String toString() {
        return "ModeratorAccessCode{" +
                "accessCode='" + accessCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModeratorAccessCode that = (ModeratorAccessCode) o;
        return Objects.equals(accessCode, that.accessCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessCode);
    }
}
