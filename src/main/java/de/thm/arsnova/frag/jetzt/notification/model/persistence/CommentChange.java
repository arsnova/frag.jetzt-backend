package de.thm.arsnova.frag.jetzt.notification.model.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class CommentChange implements Persistable<UUID> {

  public enum UserRole {
    PARTICIPANT,
    MODERATOR,
    CREATOR;

    public static UserRole fromRole(RoomAccess.Role role) {
      switch (role) {
        case PARTICIPANT:
          return PARTICIPANT;
        case EDITING_MODERATOR:
          return MODERATOR;
        case EXECUTIVE_MODERATOR:
          return MODERATOR;
        default:
          throw new IllegalArgumentException();
      }
    }
  }

  @Id
  private UUID id;

  @NonNull
  private UUID commentId;

  @NonNull
  private UUID roomId;

  @NonNull
  private CommentInterestBit type;

  @Nullable
  private String previousValueString;

  @Nullable
  private String currentValueString;

  @NonNull
  private UUID initiatorId;

  @NonNull
  private UserRole initiatorRole;

  @NonNull
  private UUID commentCreatorId;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private Comment comment;

  @Transient
  private Room room;

  public CommentChange() {}

  public CommentChange(
    UUID commentId,
    UUID roomId,
    CommentInterestBit type,
    String previousValueString,
    String currentValueString,
    UUID initiatorId,
    UserRole initiatorRole,
    UUID commentCreatorId
  ) {
    this.commentId = commentId;
    this.roomId = roomId;
    this.type = type;
    this.previousValueString = previousValueString;
    this.currentValueString = currentValueString;
    this.initiatorId = initiatorId;
    this.initiatorRole = initiatorRole;
    this.commentCreatorId = commentCreatorId;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getCommentId() {
    return commentId;
  }

  public void setCommentId(UUID commentId) {
    this.commentId = commentId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public CommentInterestBit getType() {
    return type;
  }

  public void setType(CommentInterestBit type) {
    this.type = type;
  }

  public String getPreviousValueString() {
    return previousValueString;
  }

  public void setPreviousValueString(String previousValueString) {
    this.previousValueString = previousValueString;
  }

  public String getCurrentValueString() {
    return currentValueString;
  }

  public void setCurrentValueString(String currentValueString) {
    this.currentValueString = currentValueString;
  }

  public UUID getInitiatorId() {
    return initiatorId;
  }

  public void setInitiatorId(UUID initiatorId) {
    this.initiatorId = initiatorId;
  }

  public UserRole getInitiatorRole() {
    return initiatorRole;
  }

  public void setInitiatorRole(UserRole initiatorRole) {
    this.initiatorRole = initiatorRole;
  }

  public UUID getCommentCreatorId() {
    return commentCreatorId;
  }

  public void setCommentCreatorId(UUID commentCreatorId) {
    this.commentCreatorId = commentCreatorId;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Comment getComment() {
    return comment;
  }

  public void setComment(Comment comment) {
    this.comment = comment;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result =
      prime *
      result +
      ((previousValueString == null) ? 0 : previousValueString.hashCode());
    result =
      prime *
      result +
      ((currentValueString == null) ? 0 : currentValueString.hashCode());
    result =
      prime * result + ((initiatorId == null) ? 0 : initiatorId.hashCode());
    result =
      prime * result + ((initiatorRole == null) ? 0 : initiatorRole.hashCode());
    result =
      prime *
      result +
      ((commentCreatorId == null) ? 0 : commentCreatorId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CommentChange other = (CommentChange) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (commentId == null) {
      if (other.commentId != null) return false;
    } else if (!commentId.equals(other.commentId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (type != other.type) return false;
    if (previousValueString == null) {
      if (other.previousValueString != null) return false;
    } else if (
      !previousValueString.equals(other.previousValueString)
    ) return false;
    if (currentValueString == null) {
      if (other.currentValueString != null) return false;
    } else if (
      !currentValueString.equals(other.currentValueString)
    ) return false;
    if (initiatorId == null) {
      if (other.initiatorId != null) return false;
    } else if (!initiatorId.equals(other.initiatorId)) return false;
    if (initiatorRole != other.initiatorRole) return false;
    if (commentCreatorId == null) {
      if (other.commentCreatorId != null) return false;
    } else if (!commentCreatorId.equals(other.commentCreatorId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "CommentChange [id=" +
      id +
      ", commentId=" +
      commentId +
      ", roomId=" +
      roomId +
      ", type=" +
      type +
      ", previousValueString=" +
      previousValueString +
      ", currentValueString=" +
      currentValueString +
      ", initiatorId=" +
      initiatorId +
      ", initiatorRole=" +
      initiatorRole +
      ", commentCreatorId=" +
      commentCreatorId +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", comment=" +
      comment +
      ", room=" +
      room +
      "]"
    );
  }
}
