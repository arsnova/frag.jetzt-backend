package de.thm.arsnova.frag.jetzt.notification.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class WebSubscription implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @NonNull
  private String endpoint;

  @NonNull
  private String key;

  @NonNull
  private String auth;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public WebSubscription() {}

  public WebSubscription(
    UUID accountId,
    String endpoint,
    String key,
    String auth
  ) {
    this.accountId = accountId;
    this.endpoint = endpoint;
    this.key = key;
    this.auth = auth;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public String getEndpoint() {
    return endpoint;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getAuth() {
    return auth;
  }

  public void setAuth(String auth) {
    this.auth = auth;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((endpoint == null) ? 0 : endpoint.hashCode());
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    result = prime * result + ((auth == null) ? 0 : auth.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    WebSubscription other = (WebSubscription) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (endpoint == null) {
      if (other.endpoint != null) return false;
    } else if (!endpoint.equals(other.endpoint)) return false;
    if (key == null) {
      if (other.key != null) return false;
    } else if (!key.equals(other.key)) return false;
    if (auth == null) {
      if (other.auth != null) return false;
    } else if (!auth.equals(other.auth)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "WebSubscription [id=" +
      id +
      ", accountId=" +
      accountId +
      ", endpoint=" +
      endpoint +
      ", key=" +
      key +
      ", auth=" +
      auth +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
