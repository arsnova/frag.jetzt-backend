package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingCategoriesUpdated extends WebSocketEvent<BrainstormingCategoriesUpdatedPayload> {

    public BrainstormingCategoriesUpdated() {
        super(BrainstormingCategoriesUpdated.class.getSimpleName());
    }

    public BrainstormingCategoriesUpdated(BrainstormingCategoriesUpdatedPayload payload, UUID roomId) {
        super(BrainstormingCategoriesUpdated.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCategoriesUpdated that = (BrainstormingCategoriesUpdated) o;
        return this.getPayload().equals(that.getPayload());
    }

}
