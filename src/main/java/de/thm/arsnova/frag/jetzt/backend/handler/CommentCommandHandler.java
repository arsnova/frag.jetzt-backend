package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.model.BonusToken;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.ImportComment;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.model.event.*;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.BonusTokenService;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.notification.handler.CommentChangeHandler;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

@Component
public class CommentCommandHandler {

  private static final Logger logger = LoggerFactory.getLogger(
    CommentCommandHandler.class
  );

  private final AuthorizationHelper authorizationHelper;
  private final AmqpTemplate messagingTemplate;
  private final CommentService service;
  private final BonusTokenService bonusTokenService;
  private final RoomService roomService;
  private final CommentChangeHandler commentChangeHandler;

  @Autowired
  public CommentCommandHandler(
    AuthorizationHelper authorizationHelper,
    AmqpTemplate messagingTemplate,
    CommentService service,
    BonusTokenService bonusTokenService,
    RoomService roomService,
    CommentChangeHandler commentChangeHandler
  ) {
    this.authorizationHelper = authorizationHelper;
    this.messagingTemplate = messagingTemplate;
    this.service = service;
    this.bonusTokenService = bonusTokenService;
    this.roomService = roomService;
    this.commentChangeHandler = commentChangeHandler;
  }

  @Transactional
  public Flux<Comment> handle(ImportComments command) {
    logger.trace("got new command: " + command.toString());

    HashMap<String, UUID> references = new HashMap<>();
    AtomicReference<Room> r = new AtomicReference<>(null);
    return sortImportComments(command.getPayload().getComments())
      .flatMap(comments -> service.beginImport().then(Mono.just(comments)))
      .flatMap(this::validateImportComments)
      .flatMapMany(tuple -> {
        r.set(tuple.getT2());
        return Flux.fromIterable(tuple.getT1());
      })
      .concatMap(t -> {
        int i = t.getNumber().lastIndexOf('/');
        Comment c;
        if (i > -1) {
          c =
            ImportComment.toComment(
              t,
              references.get(t.getNumber().substring(0, i))
            );
        } else {
          c = ImportComment.toComment(t, null);
        }
        if (c == null) {
          return Mono.empty();
        }
        return service
          .importComment(r.get(), c)
          .map(newComment -> {
            references.put(newComment.getNumber(), newComment.getId());
            return newComment;
          })
          .onErrorResume(e -> Mono.empty());
      })
      .collectList()
      .doOnSuccess(comments ->
        comments.forEach(c -> {
          sendCommentCreated(c, !c.isAck());
          commentChangeHandler
            .createAndEmit(
              c,
              r.get(),
              c.getCreatorId(),
              CommentChange.UserRole.PARTICIPANT
            )
            .subscribe();
        })
      )
      .switchIfEmpty(Mono.just(new ArrayList<>()))
      .flatMapMany(comments ->
        service.endImport().thenMany(Flux.fromIterable(comments))
      )
      .onErrorResume(e -> service.endImport().then(Mono.error(e)));
  }

  public Mono<Comment> handle(CreateComment command) {
    logger.trace("got new command: " + command.toString());

    Comment newComment = new Comment();
    CreateCommentPayload payload = command.getPayload();

    return roomService
      .get(payload.getRoomId())
      .flatMap(room -> {
        newComment.setRoomId(payload.getRoomId());
        newComment.setCreatorId(payload.getCreatorId());
        newComment.setBody(payload.getBody());
        newComment.setTag(payload.getTag());
        newComment.setRead(false);
        newComment.setCorrect(0);
        newComment.setFavorite(false);
        newComment.setBookmark(false);
        newComment.setAck(room.isDirectSend());
        newComment.setKeywordsFromSpacy(payload.getKeywordsFromSpacy());
        newComment.setKeywordsFromQuestioner(
          payload.getKeywordsFromQuestioner()
        );
        newComment.setLanguage(payload.getLanguage());
        newComment.setQuestionerName(payload.getQuestionerName());
        newComment.setBrainstormingSessionId(
          payload.getBrainstormingSessionId()
        );
        newComment.setBrainstormingWordId(payload.getBrainstormingWordId());
        newComment.setCommentReference(payload.getCommentReference());
        newComment.setApproved(payload.isApproved());
        newComment.setGptWriterState(payload.getGptWriterState());

        return authorizationHelper
          .getCurrentUser()
          .flatMap(user ->
            Mono.zip(Mono.just(user), service.create(user, newComment))
          )
          .doOnSuccess(it -> {
            sendCommentCreated(it.getT2(), !room.isDirectSend());
            commentChangeHandler
              .createAndEmit(it.getT2(), it.getT1())
              .subscribe();
          })
          .map(it -> it.getT2());
      });
  }

  public Mono<Comment> handle(PatchComment command) {
    logger.trace("got new command: " + command.toString());

    PatchCommentPayload p = command.getPayload();
    return service
      .get(p.getId())
      .switchIfEmpty(Mono.error(new NotFoundException("Comment not found")))
      .flatMap(c -> {
        final Comment old = c.clone();
        boolean wasFavorited = c.isFavorite();
        boolean wasAck = c.isAck();

        return authorizationHelper
          .getCurrentUser()
          .flatMap(user ->
            Mono.zip(Mono.just(user), service.patch(c, user, p.getChanges()))
          )
          .doOnSuccess(patched -> {
            sendCommentPatched(c, p.getChanges(), !wasAck);
            service
              .get(old.getId())
              .flatMap(current ->
                commentChangeHandler.patchAndEmit(old, current, patched.getT1())
              )
              .subscribe();
          })
          .map(patched -> patched.getT2())
          .map(patched -> {
            if (wasAck != patched.isAck()) sendCommentCreated(
              patched,
              !patched.isAck()
            );

            return patched;
          })
          .flatMap(patched -> {
            if (!wasFavorited && patched.isFavorite()) {
              BonusToken bt = new BonusToken();
              bt.setRoomId(patched.getRoomId());
              bt.setCommentId(patched.getId());
              bt.setAccountId(patched.getCreatorId());
              return bonusTokenService.create(bt).map(bonusToken -> patched);
            } else if (wasFavorited && !patched.isFavorite()) {
              return bonusTokenService
                .deleteByCommentIdAndAccountId(
                  patched.getId(),
                  patched.getCreatorId()
                )
                .then(Mono.just(patched))
                .map(aVoid -> patched);
            } else return Mono.just(patched);
          });
      });
  }

  public Mono<Comment> handle(UpdateComment command) {
    logger.trace("got new command: " + command.toString());

    UpdateCommentPayload p = command.getPayload();
    return this.service.get(p.getId())
      .flatMap(old -> {
        old.setBody(p.getBody());
        old.setRead(p.isRead());
        old.setFavorite(p.isFavorite());
        old.setBookmark(p.isBookmark());
        old.setCorrect(p.getCorrect());
        old.setTag(p.getTag());
        old.setKeywordsFromSpacy(p.getKeywordsFromSpacy());
        old.setKeywordsFromQuestioner(p.getKeywordsFromQuestioner());
        old.setLanguage(p.getLanguage());

        return this.service.update(old)
          .doOnSuccess(updated -> {
            if (!old.isAck() && updated.isAck()) sendCommentCreated(
              updated,
              false
            ); else if (old.isAck() && !updated.isAck()) sendCommentCreated(
              updated,
              true
            );

            sendCommentUpdated(updated, old.getRoomId(), false);
          });
      });
  }

  public Mono<Void> handle(DeleteComment command) {
    logger.trace("got new command: " + command.toString());

    UUID id = command.getPayload().getId();
    return Mono
      .zip(service.get(id), authorizationHelper.getCurrentUser())
      .flatMap(tuple ->
        service
          .delete(tuple.getT1().getId(), tuple.getT2())
          .doOnSuccess(aVoid -> {
            Comment comment = tuple.getT1();
            AuthenticatedUser user = tuple.getT2();
            sendCommentDeleted(comment, !comment.isAck());
            this.commentChangeHandler.deleteAndEmit(comment, user).subscribe();
          })
      );
  }

  public Mono<Comment> handle(HighlightComment command) {
    logger.trace("got new command: " + command.toString());

    UUID id = command.getPayload().getId();
    return service
      .get(id)
      .map(comment -> {
        sendCommentHighlighted(
          comment,
          command.getPayload().getLights(),
          !comment.isAck()
        );
        return comment;
      });
  }

  public Mono<Object> handle(DeleteCommentsByRoom command) {
    logger.trace("got new command: " + command.toString());

    UUID roomId = command.getPayload().getRoomId();
    return service
      .deleteAllByRoomId(roomId)
      .flatMap(comments -> {
        comments.forEach(c -> sendCommentDeleted(c, !c.isAck()));
        return Mono.empty();
      });
  }

  private void sendCommentCreated(Comment comment, boolean moderationStream) {
    CommentCreatedPayload commentCreatedPayload = new CommentCreatedPayload(
      comment
    );
    CommentCreated event = new CommentCreated(
      commentCreatedPayload,
      comment.getRoomId()
    );
    sendAmqpMessage(comment.getRoomId(), moderationStream, event);
  }

  private void sendCommentPatched(
    Comment comment,
    Map<String, Object> changes,
    boolean moderationStream
  ) {
    CommentPatchedPayload payload = new CommentPatchedPayload(
      comment.getId(),
      changes
    );
    CommentPatched event = new CommentPatched(payload, comment.getRoomId());
    sendAmqpMessage(comment.getRoomId(), moderationStream, event);
  }

  private void sendCommentUpdated(
    Comment comment,
    UUID roomId,
    boolean moderationStream
  ) {
    CommentUpdatedPayload payload = new CommentUpdatedPayload(comment);
    CommentUpdated event = new CommentUpdated(payload, comment.getRoomId());
    sendAmqpMessage(roomId, moderationStream, event);
  }

  private void sendCommentHighlighted(
    Comment comment,
    boolean lights,
    boolean moderationStream
  ) {
    CommentHighlightedPayload p = new CommentHighlightedPayload(
      comment,
      lights
    );
    CommentHighlighted event = new CommentHighlighted(p, comment.getRoomId());
    sendAmqpMessage(comment.getRoomId(), moderationStream, event);
  }

  private void sendCommentDeleted(Comment comment, boolean moderationStream) {
    CommentDeletedPayload p = new CommentDeletedPayload();
    p.setId(comment.getId());
    CommentDeleted event = new CommentDeleted(p, comment.getRoomId());
    sendAmqpMessage(comment.getRoomId(), moderationStream, event);
  }

  private void sendAmqpMessage(
    UUID roomId,
    boolean moderationStream,
    Object o
  ) {
    messagingTemplate.convertAndSend(
      "amq.topic",
      roomId + ".comment" + (moderationStream ? ".moderator" : "") + ".stream",
      o
    );
  }

  private Mono<List<ImportComment>> sortImportComments(
    List<ImportComment> comments
  ) {
    try {
      return Mono.just(
        comments
          .stream()
          .filter(comment -> comment != null && comment.getNumber() != null)
          .map(comment -> {
            String[] data = comment.getNumber().split("/");
            int[] number = new int[data.length];
            for (int i = 0; i < data.length; i++) {
              number[i] = Integer.parseInt(data[i]);
            }
            return Tuples.of(comment, number);
          })
          .sorted((t1, t2) -> {
            // sort ascending: 1, 1/1, 1/1/1, 1/1/2, 1/2, 2, 3, ...
            int[] a = t1.getT2();
            int[] b = t2.getT2();
            int minLen = Math.min(a.length, b.length);
            for (int j = 0, equals = 0; j < minLen; j++) {
              equals = a[j] - b[j];
              if (equals != 0) {
                return equals;
              }
            }
            return a.length - b.length;
          })
          .map(Tuple2::getT1)
          .collect(Collectors.toList())
      );
    } catch (NumberFormatException e) {
      return Mono.error(
        new IllegalArgumentException("Number is not correct formatted!")
      );
    }
  }

  private Mono<Tuple2<List<ImportComment>, Room>> validateImportComments(
    List<ImportComment> list
  ) {
    if (list.size() < 1) {
      return Mono.empty();
    }
    final UUID roomId = list.get(0).getRoomId();
    return Mono
      .just(list)
      .filter(comments -> roomId != null)
      .switchIfEmpty(
        Mono.error(new IllegalArgumentException("Room id must be present!"))
      )
      .filter(comments ->
        comments.stream().allMatch(c -> c.getRoomId().equals(roomId))
      )
      .switchIfEmpty(
        Mono.error(new IllegalArgumentException("Room ids are not the same!"))
      )
      .flatMap(comments -> roomService.get(roomId))
      .map(room -> Tuples.of(list, room));
  }
}
