package de.thm.arsnova.frag.jetzt.notification.service;

import java.sql.Timestamp;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import reactor.core.publisher.Flux;

@Service
public class CommentChangeFindQueryService {

  private static final Logger logger = LoggerFactory.getLogger(
    CommentChangeFindQueryService.class
  );

  private final CommentChangeService service;

  @Autowired
  public CommentChangeFindQueryService(final CommentChangeService service) {
    this.service = service;
  }

  public Flux<CommentChange> resolveQuery(
    final FindQuery<CommentChange> findQuery
  ) {
    final Map<String, Object> external = findQuery.getExternalFilters();
    if (external != null && external.containsKey("lastAction")) {
      final Timestamp timestamp = new Timestamp(
        ((Number) external.get("lastAction")).longValue()
      );
      return service.receiveMissedInformationSince(timestamp);
    }
    return Flux.empty();
  }
}
