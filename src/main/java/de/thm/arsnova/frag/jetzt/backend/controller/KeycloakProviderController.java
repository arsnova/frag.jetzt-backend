package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.FragjetztKeycloakEvent;
import de.thm.arsnova.frag.jetzt.backend.model.KeycloakProvider;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.KeycloakProviderService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("KeycloakProviderController")
@RequestMapping(KeycloakProviderController.REQUEST_MAPPING)
public class KeycloakProviderController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    KeycloakProviderController.class
  );

  protected static final String REQUEST_MAPPING = "/keycloak-provider";
  private static final String EVENT = "/event/{keycloakId}";
  private static final String ALL = "/providers/all";

  private final KeycloakProviderService service;
  private final AccountService accountService;

  @Autowired
  public KeycloakProviderController(
    KeycloakProviderService service,
    AccountService accountService
  ) {
    this.service = service;
    this.accountService = accountService;
  }

  @GetMapping(ALL)
  public Flux<KeycloakProvider> get() {
    return service.getAll();
  }

  @GetMapping(GET_MAPPING)
  public Mono<KeycloakProvider> getById(@PathVariable("id") UUID id) {
    return service.getById(id);
  }

  @PatchMapping(PATCH_MAPPING)
  public Mono<KeycloakProvider> patch(
    @PathVariable("id") UUID id,
    @RequestBody Map<String, Object> changes
  ) {
    return service.patch(id, changes);
  }

  @PostMapping(POST_MAPPING)
  public Mono<KeycloakProvider> put(@RequestBody KeycloakProvider provider) {
    return service.create(provider);
  }

  @DeleteMapping(DELETE_MAPPING)
  public Mono<Void> delete(@PathVariable("id") UUID id) {
    return service.deleteById(id);
  }

  @PostMapping(EVENT)
  public Mono<Void> onEvent(
    @PathVariable("keycloakId") UUID id,
    @RequestBody FragjetztKeycloakEvent event,
    ServerHttpRequest request
  ) {
    return accountService.onEvent(id, event, getIp(request));
  }

  private byte[] getIp(ServerHttpRequest request) {
    List<String> list = request.getHeaders().get("X-Forwarded-For");
    if (list == null || list.isEmpty()) {
      return request.getRemoteAddress().getAddress().getAddress();
    }
    try {
      return InetAddress.getByName(list.get(0)).getAddress();
    } catch (UnknownHostException e) {
      e.printStackTrace();
      return null;
    }
  }
}
