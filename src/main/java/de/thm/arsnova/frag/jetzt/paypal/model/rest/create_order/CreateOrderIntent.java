package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

public enum CreateOrderIntent {
    CAPTURE,
    AUTHORIZE;
}
