package de.thm.arsnova.frag.jetzt.backend.model.command;

public class PatchRoom extends WebSocketCommand<PatchRoomPayload> {

    public PatchRoom() {
        super(PatchRoom.class.getSimpleName());
    }

    public PatchRoom(PatchRoomPayload payload) {
        super(PatchRoom.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatchRoom that = (PatchRoom) o;
        return this.getPayload().equals(that.getPayload());
    }
}
