package de.thm.arsnova.frag.jetzt.backend.model.command;

public class UpdateBrainstormingCategories  extends WebSocketCommand<UpdateBrainstormingCategoriesPayload>  {

    public UpdateBrainstormingCategories() {
        super(UpdateBrainstormingCategories.class.getSimpleName());
    }

    public UpdateBrainstormingCategories(UpdateBrainstormingCategoriesPayload payload) {
        super(UpdateBrainstormingCategories.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateBrainstormingCategories that = (UpdateBrainstormingCategories) o;
        return this.getPayload().equals(that.getPayload());
    }
}
