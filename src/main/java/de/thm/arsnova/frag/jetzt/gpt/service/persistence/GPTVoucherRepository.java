package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTVoucher;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface GPTVoucherRepository extends ReactiveCrudRepository<GPTVoucher, UUID> {
  Mono<GPTVoucher> findByCode(String code);

  Flux<GPTVoucher> findAllByAccountId(UUID accountId);
}
