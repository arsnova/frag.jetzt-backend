package de.thm.arsnova.frag.jetzt.notification.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.WebSubscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface WebSubscriptionRepository extends ReactiveCrudRepository<WebSubscription, UUID> {
  Flux<WebSubscription> findAllByAccountId(UUID accountId);

  Mono<Void> deleteByIdAndAccountId(UUID id, UUID accountId);
}
