package de.thm.arsnova.frag.jetzt.openai.service.persistence;

import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiThreads;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface OpenaiThreadsRepository
  extends ReactiveCrudRepository<OpenaiThreads, UUID> {
  Flux<OpenaiThreads> findAllByRoomIdAndAccountId(UUID roomId, UUID accountId);
  Mono<OpenaiThreads> findByAccountIdAndId(UUID accountId, UUID id);
}
