package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class RoomPatched extends WebSocketEvent<RoomPatchedPayload> {
    public RoomPatched() {
        super(RoomPatched.class.getSimpleName());
    }

    public RoomPatched(RoomPatchedPayload p, UUID id) {
        super(RoomPatched.class.getSimpleName(), id);
        this.payload = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomPatched that = (RoomPatched) o;
        return this.getPayload().equals(that.getPayload());
    }
}
