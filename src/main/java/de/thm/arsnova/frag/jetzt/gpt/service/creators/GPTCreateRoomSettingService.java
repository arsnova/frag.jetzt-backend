package de.thm.arsnova.frag.jetzt.gpt.service.creators;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomSettingRepository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaEntry;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaEntry.ResetStrategy;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTCreateRoomSettingService {

  private final QuotaManageService quotaManageService;
  private final GPTRoomSettingRepository repository;

  @Autowired
  public GPTCreateRoomSettingService(
    QuotaManageService quotaManageService,
    GPTRoomSettingRepository repository
  ) {
    this.quotaManageService = quotaManageService;
    this.repository = repository;
  }

  public Mono<GPTRoomSetting> createNew(UUID roomId) {
    GPTRoomSetting roomSetting = new GPTRoomSetting(roomId);
    // initialize quotas
    Quota roomQuota = new Quota(false, "UTC");
    roomQuota.setEntries(
      List.of(
        new QuotaEntry(null, null, -1, ResetStrategy.NEVER, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY_FLOWING, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.DAILY, 1)
      )
    );
    Quota moderatorQuota = new Quota(false, "UTC");
    moderatorQuota.setEntries(
      List.of(
        new QuotaEntry(null, null, -1, ResetStrategy.NEVER, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY_FLOWING, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.DAILY, 1)
      )
    );
    Quota participantQuota = new Quota(false, "UTC");
    participantQuota.setEntries(
      List.of(
        new QuotaEntry(null, null, -1, ResetStrategy.NEVER, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.MONTHLY_FLOWING, 1),
        new QuotaEntry(null, null, -1, ResetStrategy.DAILY, 1)
      )
    );
    return quotaManageService
      .create(roomQuota)
      .flatMap(quota -> {
        roomSetting.setRoomQuotaId(quota.getId());
        return quotaManageService.create(participantQuota);
      })
      .flatMap(quota -> {
        roomSetting.setParticipantQuotaId(quota.getId());
        return quotaManageService.create(moderatorQuota);
      })
      .flatMap(quota -> {
        roomSetting.setModeratorQuotaId(quota.getId());
        return repository.save(roomSetting);
      });
  }
}
