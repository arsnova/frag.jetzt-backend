package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileObject {

  private String id;
  private int bytes;

  // UNIX Timestamp in seconds
  @JsonProperty("created_at")
  private int createdAt;

  private String filename;
  // Is always "file"
  private String object;
  // Can be "assistants", "assistants_output", "batch", "batch_output", "fine-tune", "fine-tune-results" and "vision"
  private String purpose;

  public FileObject() {}

  public FileObject(
    String id,
    int bytes,
    int createdAt,
    String filename,
    String object,
    String purpose
  ) {
    this.id = id;
    this.bytes = bytes;
    this.createdAt = createdAt;
    this.filename = filename;
    this.object = object;
    this.purpose = purpose;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getBytes() {
    return bytes;
  }

  public void setBytes(int bytes) {
    this.bytes = bytes;
  }

  public int getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(int createdAt) {
    this.createdAt = createdAt;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getObject() {
    return object;
  }

  public void setObject(String object) {
    this.object = object;
  }

  public String getPurpose() {
    return purpose;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }
}
