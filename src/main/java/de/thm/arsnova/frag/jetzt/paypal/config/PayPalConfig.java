package de.thm.arsnova.frag.jetzt.paypal.config;

import java.util.List;
import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app.paypal")
@Data
@NoArgsConstructor
public class PayPalConfig {

  private String apiUrl;
  private String clientId;
  private String clientSecret;
  private String merchantId;
  private String webhookId;
  private List<PayPalAllowedUnit> allowedUnits;
  private Map<String, String> productName;
}
