package de.thm.arsnova.frag.jetzt.notification.service.persistence;

import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushCommentSubscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PushCommentSubscriptionRepository extends ReactiveCrudRepository<PushCommentSubscription, UUID> {
    
    Flux<PushCommentSubscription> findAllByCommentId(UUID commentId);

    Flux<PushCommentSubscription> findAllByAccountIdAndRoomIdIn(UUID accountId, List<UUID> roomId);

    Mono<PushCommentSubscription> findByCommentIdAndAccountId(UUID commentId, UUID accountId);

    Mono<Void> deleteByCommentIdAndAccountId(UUID commentId, UUID accountId);

    Flux<Void> deleteAllByRoomIdAndAccountId(UUID roomId, UUID accountId);

}
