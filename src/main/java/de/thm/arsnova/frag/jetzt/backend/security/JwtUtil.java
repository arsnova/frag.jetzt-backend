package de.thm.arsnova.frag.jetzt.backend.security;

import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@Component
public class JwtUtil {

    @Value("${spring.jwt.secret}")
    private String secret;

    @Value("${spring.jwt.expiration}")
    private String expirationTime;

    public static final String CLAIM_KEY = "role";


    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    public UUID getAccountIdFromToken(String token) {
        return UUID.fromString(getAllClaimsFromToken(token).getSubject());
    }

    public Date getExpirationDateFromToken(String token) {
        return getAllClaimsFromToken(token).getExpiration();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(UUID accountId, String type) {
        if (accountId != null)
            return doGenerateToken(accountId, type);
        else
            throw new ForbiddenException("Account ID Should not be NULL");
    }

    private String doGenerateToken(UUID accountId, String type) {
        final Date createdDate = new Date();
        final Date expirationDate;
        if (type.equals("guest")) {
            expirationDate = new Date(Long.MAX_VALUE);
        } else {
            long expirationTimeLong = Long.parseLong(expirationTime); //in second
            expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);
        }

        return Jwts.builder()
                .setSubject(accountId.toString())
                .claim(CLAIM_KEY, Collections.emptyList())
                .claim("type", type)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }
}
