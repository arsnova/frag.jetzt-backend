package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class CreateBrainstormingVotePayload implements WebSocketPayload {

    private UUID wordId;

    private boolean isUpvote;

    public CreateBrainstormingVotePayload() {

    }

    public CreateBrainstormingVotePayload(UUID wordId, boolean isUpvote) {
        this.wordId = wordId;
        this.isUpvote = isUpvote;
    }

    public UUID getWordId() {
        return wordId;
    }

    public void setWordId(UUID wordId) {
        this.wordId = wordId;
    }

    @JsonProperty("isUpvote")
    public boolean isUpvote() {
        return isUpvote;
    }

    @JsonProperty("isUpvote")
    public void setUpvote(boolean upvote) {
        isUpvote = upvote;
    }

    @Override
    public String toString() {
        return "CreateBrainstormingVotePayload{" +
                "wordId=" + wordId +
                ", isUpvote=" + isUpvote +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstormingVotePayload that = (CreateBrainstormingVotePayload) o;
        return isUpvote == that.isUpvote && Objects.equals(wordId, that.wordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordId, isUpvote);
    }
}
