package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.*;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingSessionRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import de.thm.arsnova.frag.jetzt.notification.service.CommentChangeService;
import de.thm.arsnova.frag.jetzt.notification.service.WebNotificationService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class CommentService {

  @Value("${app.comment.length}")
  private int maxCommentLength;

  @Value("${app.comment.privileged_length}")
  private int maxCommentPrivilegedLength;

  @Value("${app.comment.taglength}")
  private int maxCommentTagLength;

  @Value("${app.comment.questionernamelength}")
  private int maxCommentQuestionerNameLength;

  private static final Logger logger = LoggerFactory.getLogger(
    CommentService.class
  );

  private final AuthorizationHelper authorizationHelper;
  private final CommentRepository repository;
  private final RoomRepository roomRepository;
  private final BrainstormingSessionRepository sessionRepository;
  private final AccountService accountService;
  private final CommentChangeService commentChangeService;
  private final AmqpTemplate messagingTemplate;
  private final WebNotificationService notificationService;

  @Autowired
  public CommentService(
    AuthorizationHelper authorizationHelper,
    CommentRepository repository,
    RoomRepository roomRepository,
    AccountService accountService,
    CommentChangeService commentChangeService,
    AmqpTemplate messagingTemplate,
    BrainstormingSessionRepository sessionRepository,
    WebNotificationService notificationService
  ) {
    this.authorizationHelper = authorizationHelper;
    this.repository = repository;
    this.roomRepository = roomRepository;
    this.accountService = accountService;
    this.commentChangeService = commentChangeService;
    this.messagingTemplate = messagingTemplate;
    this.sessionRepository = sessionRepository;
    this.notificationService = notificationService;
  }

  public Mono<Comment> get(UUID id) {
    return repository.findById(id);
  }

  public Flux<Comment> get(List<UUID> ids) {
    List<Mono<Comment>> list = new ArrayList<>();
    ids.forEach(c -> list.add(get(c)));

    return Flux.merge(list);
  }

  public Flux<Comment> getByRoomId(UUID roomId) {
    return repository.findByRoomId(roomId).cast(Comment.class);
  }

  public Mono<Comment> create(AuthenticatedUser user, Comment c) {
    logger.trace("Creating new comment: " + c.toString());
    c.setId(null);
    c.setCreatorId(user.getAccountId());
    return Mono
      .just(c)
      .flatMap(this::validateComment)
      .flatMap(repository::save)
      .map(comment -> comment.getId())
      .flatMap(repository::findById);
  }

  public Mono<Void> beginImport() {
    return repository.disableTriggers();
  }

  public Mono<Void> endImport() {
    return repository.enableTriggers();
  }

  public Mono<Comment> importComment(Room r, Comment c) {
    logger.trace("Importing comment: " + c.toString());
    c.setId(null);
    return this.validateComment(c)
      .flatMap(repository::save)
      .map(e -> e.getId())
      .flatMap(repository::findById);
  }

  public Mono<Comment> patch(
    final Comment entity,
    final AuthenticatedUser user,
    final Map<String, Object> changes
  ) {
    if (entity.getId() == null) return Mono.error(
      new BadRequestException("The commentId must be provided")
    );
    return Mono
      .just(parseCommentChanges(entity, changes))
      .flatMap(this::validateComment)
      .flatMap(comment ->
        validateAccessRightsWithCreator(user, comment, true)
          .map(authenticatedUser -> comment)
      )
      .onErrorResume(throwable ->
        throwable instanceof ClassCastException
          ? Mono.error(new BadRequestException("Invalid Value of Change"))
          : Mono.error(throwable)
      )
      .flatMap(comment ->
        repository.save(comment).flatMap(c -> repository.findById(c.getId()))
      );
  }

  /**
   * @deprecated Use {@link #patch(Comment, Map)} instead
   */
  public Mono<Comment> update(final Comment c) {
    if (c.getId() == null) return Mono.error(
      new BadRequestException("The commentId must be provided")
    );
    return validateComment(c)
      .flatMap(comment ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(authenticatedUser ->
            validateAccessRightsWithCreator(authenticatedUser, comment, true)
          )
          .flatMap(authenticatedUser -> repository.findById(c.getId()))
          .filter(old ->
            old.getCreatorId() == comment.getCreatorId() &&
            old.getRoomId() != comment.getRoomId()
          )
          .switchIfEmpty(
            Mono.error(
              new ForbiddenException(
                "You are not allowed to change these values"
              )
            )
          )
          .map(authenticatedUser -> comment)
      )
      .flatMap(repository::save);
  }

  public Mono<Void> delete(UUID id, AuthenticatedUser user) {
    return get(id)
      .flatMap(comment ->
        validateAccessRightsWithCreator(user, comment, false)
          .map(ignored -> comment)
      )
      .flatMap(comment -> {
        comment.markAsDeleted();
        return repository.save(comment);
      })
      .then();
  }

  public Mono<List<Comment>> deleteAllByRoomId(UUID roomId) {
    // No Comment change possible, since references are deleted
    return Mono
      .zip(
        authorizationHelper.getCurrentUser(),
        roomRepository.findById(roomId)
      )
      .filter(tuple ->
        authorizationHelper.checkModeratorOrCreatorOfRoom(
          tuple.getT1(),
          tuple.getT2()
        )
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMapMany(authenticatedUser -> getByRoomId(roomId))
      .filter(c -> c.getDeletedAt() == null)
      .collectList()
      .flatMap(comments ->
        repository.deleteAllByRoomId(roomId).count().map(l -> comments)
      );
  }

  private Comment parseCommentChanges(
    Comment comment,
    Map<String, Object> changes
  ) throws ClassCastException {
    changes.forEach((key, value) -> {
      switch (key) {
        case "body":
          comment.setBody((String) value);
          break;
        case "read":
          comment.setRead((boolean) value);
          break;
        case "favorite":
          comment.setFavorite((boolean) value);
          break;
        case "bookmark":
          comment.setBookmark((boolean) value);
          break;
        case "correct":
          comment.setCorrect((int) value);
          break;
        case "ack":
          comment.setAck((boolean) value);
          break;
        case "tag":
          comment.setTag((String) value);
          break;
        case "keywordsFromSpacy":
          comment.setKeywordsFromSpacy((String) value);
          break;
        case "keywordsFromQuestioner":
          comment.setKeywordsFromQuestioner((String) value);
          break;
        case "language":
          comment.setLanguage(Comment.Language.valueOf((String) value));
          break;
        case "brainstormingWordId":
          comment.setBrainstormingWordId(UUID.fromString((String) value));
          break;
        case "approved":
          comment.setApproved((Boolean) value);
          break;
        case "roomId":
        case "creatorId":
        case "questionerName":
        case "commentReference":
        case "gptWriterState":
          throw new ForbiddenException(
            "You are not allowed to change these values"
          );
        default:
          throw new BadRequestException("Invalid ChangeAttribute provided");
      }
    });
    return comment;
  }

  private Mono<Comment> validateComment(Comment comment) {
    return Mono
      .zip(
        roomRepository.findById(comment.getRoomId()),
        authorizationHelper.getCurrentUser()
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(tuple2 ->
        Mono.zip(
          Mono.just(comment),
          Mono.just(tuple2.getT1()),
          Mono.just(
            authorizationHelper.checkModeratorOrCreatorOfRoom(
              tuple2.getT2(),
              tuple2.getT1()
            )
          ),
          comment.getCommentReference() != null
            ? get(comment.getCommentReference())
            : Mono.just(new Comment())
        )
      )
      .filter(tuple4 -> tuple4.getT3() || !tuple4.getT2().getQuestionsBlocked())
      .switchIfEmpty(
        Mono.error(new BadRequestException("Questions are blocked"))
      )
      .filter(tuple4 ->
        tuple4.getT3() ||
        tuple4.getT4().isNew() ||
        tuple4.getT4().getCommentDepth() +
        1 <=
        tuple4.getT2().getConversationDepth()
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Conversation depth limit reached"))
      )
      .filter(tuple4 -> {
        final int length = tuple4.getT3()
          ? maxCommentPrivilegedLength
          : maxCommentLength;
        return (
          tuple4.getT1().getBody() != null &&
          tuple4.getT1().getBody().length() <= length
        );
      })
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Maximal Comment length exceeded or empty")
        )
      )
      .map(Tuple2::getT1)
      .filter(c ->
        c.getTag() == null || c.getTag().length() <= maxCommentTagLength
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Maximal Tag length exceeded"))
      )
      .filter(c ->
        c.getQuestionerName() == null ||
        c.getQuestionerName().length() <= maxCommentQuestionerNameLength
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Maximal Questioner name length exceeded")
        )
      )
      .filter(c ->
        (c.getBrainstormingSessionId() == null) ==
        (c.getBrainstormingWordId() == null)
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Brainstorming word and session are not correctly set"
          )
        )
      );
  }

  private Mono<AuthenticatedUser> validateAccessRightsWithCreator(
    AuthenticatedUser authenticatedUser,
    Comment comment,
    boolean checkChanges
  ) {
    return Mono
      .zip(
        checkChanges ? canAnyoneChange(comment) : Mono.just(false),
        checkChanges ? needsModerator(comment) : Mono.just(false)
      )
      .flatMap(accessArray -> {
        boolean canAnyoneChange = accessArray.getT1();
        boolean needsMod = accessArray.getT2();
        if (canAnyoneChange) {
          return Mono.just(authenticatedUser);
        } else if (
          authenticatedUser.getAccountId().equals(comment.getCreatorId()) &&
          !needsMod
        ) {
          return Mono.just(authenticatedUser);
        } else {
          return roomRepository
            .findById(comment.getRoomId())
            .filter(room ->
              authorizationHelper.checkModeratorOrCreatorOfRoom(
                authenticatedUser,
                room
              )
            )
            .map(room -> authenticatedUser);
        }
      })
      .switchIfEmpty(Mono.error(ForbiddenException::new));
  }

  private Mono<Boolean> canAnyoneChange(Comment changedComment) {
    return repository
      .findById(changedComment.getId())
      .map(comment -> {
        comment.setKeywordsFromSpacy(changedComment.getKeywordsFromSpacy());
        comment.setKeywordsFromQuestioner(
          changedComment.getKeywordsFromQuestioner()
        );
        comment.setLanguage(changedComment.getLanguage());
        return changedComment.equals(comment);
      });
  }

  private Mono<Boolean> needsModerator(Comment changedComment) {
    return repository
      .findById(changedComment.getId())
      .map(comment ->
        changedComment.isFavorite() != comment.isFavorite() ||
        changedComment.isBookmark() != comment.isBookmark() ||
        changedComment.isRead() != comment.isRead() ||
        changedComment.getCorrect() != comment.getCorrect() ||
        changedComment.isAck() != comment.isAck() ||
        !Objects.equals(
          changedComment.getBrainstormingWordId(),
          comment.getBrainstormingWordId()
        )
      );
  }

  public Mono<CommentChange.UserRole> getUserInformation(
    UUID roomId,
    AuthenticatedUser user
  ) {
    for (RoomAccess access : user.getRoomAccesses()) {
      if (access.getRoomId().equals(roomId)) {
        return Mono.just(CommentChange.UserRole.fromRole(access.getRole()));
      }
    }
    return roomRepository
      .findById(roomId)
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .map(room ->
        authorizationHelper.checkCreatorOfRoom(user, room)
          ? CommentChange.UserRole.CREATOR
          : CommentChange.UserRole.PARTICIPANT
      );
  }
}
