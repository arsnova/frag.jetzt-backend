package de.thm.arsnova.frag.jetzt.notification.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.notification.model.WebNotification;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.WebSubscription;
import de.thm.arsnova.frag.jetzt.notification.service.persistence.WebSubscriptionRepository;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.UUID;
import nl.martijndwars.webpush.Encoding;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushAsyncService;
import nl.martijndwars.webpush.cli.commands.GenerateKeyCommand;
import nl.martijndwars.webpush.cli.handlers.GenerateKeyHandler;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class WebPushService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
    WebPushService.class
  );

  private final String publicVapidKey;

  private final PushAsyncService pushAsyncService;
  private final AuthorizationHelper helper;
  private final WebSubscriptionRepository repository;
  private final Encoding notificationEncoding;
  private final ObjectMapper mapper = new ObjectMapper();

  @Autowired
  public WebPushService(
    AuthorizationHelper helper,
    WebSubscriptionRepository repository,
    @Value("${app.web_push.vapid_public_key}") String publicVapidKey,
    @Value("${app.web_push.vapid_private_key}") String privateVapidKey,
    @Value("${app.web_push.vapid_subject}") String vapidSubject,
    @Value("${app.web_push.notification_encoding}") String encoding
  ) throws GeneralSecurityException {
    if (
      publicVapidKey == null ||
      privateVapidKey == null ||
      vapidSubject == null ||
      publicVapidKey.isEmpty() ||
      privateVapidKey.isEmpty() ||
      vapidSubject.isEmpty()
    ) {
      failPrintVapidKeys();
      throw new IllegalStateException(
        "Vapid keys or subject not set\nPublic: " +
        publicVapidKey +
        "\nPrivate: " +
        privateVapidKey +
        "\nSubject: " +
        vapidSubject
      );
    }
    this.helper = helper;
    this.repository = repository;
    this.pushAsyncService =
      new PushAsyncService(publicVapidKey, privateVapidKey, vapidSubject);
    this.notificationEncoding = getEncoding(encoding);
    this.publicVapidKey = publicVapidKey;
  }

  public String getPublicVapidKey() {
    return publicVapidKey;
  }

  public byte[] encodeMessage(WebNotification notification) {
    try {
      return mapper.writeValueAsBytes(notification);
    } catch (JsonProcessingException e) {
      LOGGER.error("Could not write notification", e);
      return null;
    }
  }

  public Mono<Boolean> sendNotification(
    WebSubscription subscription,
    byte[] payload
  ) {
    try {
      Notification n = Notification
        .builder()
        .endpoint(subscription.getEndpoint())
        .userPublicKey(subscription.getKey())
        .userAuth(subscription.getAuth())
        .payload(payload)
        // topic is max 32 chars
        .build();
      return Mono
        .fromFuture(pushAsyncService.send(n, notificationEncoding))
        .flatMap(r -> {
          if (r.getStatusCode() == 404 || r.getStatusCode() == 410) {
            // Delete subscription
            return repository
              .deleteById(subscription.getId())
              .thenReturn(false);
          } else if (r.getStatusCode() >= 200 && r.getStatusCode() < 300) {
            return Mono.just(true);
          } else {
            return Mono.just(false);
          }
        });
    } catch (GeneralSecurityException | IOException | JoseException e) {
      LOGGER.error("Could not send message", e);
      return Mono.just(false);
    }
  }

  public Mono<WebSubscription> createSubscription(
    WebSubscription subscription
  ) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        subscription.setId(null);
        subscription.setAccountId(user.getAccountId());
        return repository.save(subscription);
      });
  }

  public Flux<WebSubscription> getSubscriptions() {
    return helper
      .getCurrentUser()
      .flatMapMany(user -> getUserSubscriptions(user.getAccountId()));
  }

  public Flux<WebSubscription> getUserSubscriptions(UUID accountId) {
    return repository.findAllByAccountId(accountId);
  }

  public Mono<Void> deleteSubscription(UUID id) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        repository.deleteByIdAndAccountId(id, user.getAccountId())
      );
  }

  private Encoding getEncoding(String encoding) {
    for (Encoding e : Encoding.values()) {
      if (e.name().equalsIgnoreCase(encoding)) {
        return e;
      }
    }
    return Encoding.AES128GCM;
  }

  private void failPrintVapidKeys() {
    GenerateKeyCommand cmd = new GenerateKeyCommand();
    GenerateKeyHandler handler = new GenerateKeyHandler(cmd);
    System.out.println(
      "You need to generate VAPID keys before running the app.\nHere are generated keys:"
    );
    try {
      handler.run();
    } catch (GeneralSecurityException | IOException e) {
      LOGGER.error("Could not generate keys", e);
    }
    System.out.println(
      "Dont forget the vapid subject in form of:\n  mailto:name@example-domain.com"
    );
  }
}
