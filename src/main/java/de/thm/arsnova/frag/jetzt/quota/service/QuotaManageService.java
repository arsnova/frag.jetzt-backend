package de.thm.arsnova.frag.jetzt.quota.service;

import de.thm.arsnova.frag.jetzt.quota.model.QuotaUnit;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaEntry;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class QuotaManageService {

  public static final class QuotaStatus {

    public static final QuotaStatus INVALID = new QuotaStatus();

    private boolean disabled;
    private boolean inAccessTime;
    private Map<UUID, Boolean> entries;

    private QuotaStatus() {}

    private QuotaStatus(
      boolean disabled,
      boolean inAccessTime,
      Map<UUID, Boolean> entries
    ) {
      this.disabled = disabled;
      this.inAccessTime = inAccessTime;
      this.entries = entries;
    }

    public boolean isDisabled() {
      return disabled;
    }

    public void setDisabled(boolean disabled) {
      this.disabled = disabled;
    }

    public boolean isInAccessTime() {
      return inAccessTime;
    }

    public void setInAccessTime(boolean inAccessTime) {
      this.inAccessTime = inAccessTime;
    }

    public Map<UUID, Boolean> getEntries() {
      return entries;
    }

    public void setEntries(Map<UUID, Boolean> entries) {
      this.entries = entries;
    }
  }

  public static final class Reservation {

    public static final Reservation INVALID = new Reservation(
      QuotaUnit.ZERO,
      null,
      null,
      null
    );

    private QuotaUnit reserved;
    private List<UUID> quotaIds;
    private HashMap<UUID, HashMap<UUID, Long>> reservationTracker;
    private final UUID additionalInfo;

    public Reservation(
      QuotaUnit reserved,
      HashMap<UUID, HashMap<UUID, Long>> reservationTracker,
      List<UUID> quotaIds,
      UUID additionalInfo
    ) {
      this.reserved = reserved;
      this.reservationTracker = reservationTracker;
      this.quotaIds = quotaIds;
      this.additionalInfo = additionalInfo;
    }

    public QuotaUnit getReserved() {
      return reserved;
    }

    public UUID getAdditionalInfo() {
      return additionalInfo;
    }
  }

  private static final class Entry {

    private Quota quota;
    private int reservationCount;

    public Entry(Quota quota) {
      this.quota = quota;
    }

    public Quota getQuota() {
      return quota;
    }

    public int getReservationCount() {
      return reservationCount;
    }

    public void incrementReservationCount() {
      reservationCount++;
    }

    public void decrementReservationCount() {
      reservationCount--;
    }
  }

  private static final int EXP = 8;
  private final Map<UUID, Entry> quotas = new HashMap<>();
  private final QuotaService service;

  @Autowired
  public QuotaManageService(QuotaService service) {
    this.service = service;
  }

  // every 10 minutes
  @Scheduled(fixedRate = 1000 * 60 * 10)
  public void saveQuotas() {
    Flux
      .fromArray(quotas.values().toArray(new Entry[0]))
      .map(e -> e.getQuota())
      .flatMap(e -> service.save(e, false, true))
      .subscribe();
    synchronized (quotas) {
      for (Map.Entry<UUID, Entry> e : quotas
        .entrySet()
        .toArray(new Map.Entry[0])) {
        if (e.getValue().getReservationCount() < 1) {
          quotas.remove(e.getKey());
        }
      }
    }
  }

  public Mono<Reservation> tryReserve(
    QuotaUnit amount,
    Set<UUID> quotaIds,
    boolean allowLess,
    UUID additionalInfo
  ) {
    return get(quotaIds)
      .flatMap(entries -> {
        if (entries.size() < quotaIds.size()) {
          return Mono.error(new IllegalArgumentException("quotas not found"));
        }
        return Mono.just(
          makeReservation(entries, amount, allowLess, additionalInfo)
        );
      });
  }

  public Mono<Quota> get(UUID quotaId) {
    Entry entry;
    synchronized (quotas) {
      entry = quotas.get(quotaId);
    }
    if (entry != null) {
      return Mono.just(entry.getQuota());
    }
    return service.getById(quotaId);
  }

  public Mono<QuotaStatus> getStatus(UUID quotaId) {
    Entry entry;
    synchronized (quotas) {
      entry = quotas.get(quotaId);
    }
    if (entry != null) {
      synchronized (entry) {
        return Mono.just(this.getStatus(entry.getQuota()));
      }
    }
    return service.getById(quotaId).map(this::getStatus);
  }

  public synchronized Mono<Void> finishReservation(
    Reservation reservation,
    QuotaUnit used
  ) {
    List<Entry> entries = new ArrayList<>();
    synchronized (quotas) {
      for (UUID quotaId : reservation.quotaIds) {
        Entry entry = quotas.get(quotaId);
        if (entry != null) {
          entries.add(entry);
        }
      }
    }
    if (entries.size() < 1) {
      // reservation probably timed out
      return Mono.empty();
    }
    long requested = reservation.reserved.toPlain(EXP);
    long usedCount = used.toPlain(EXP);
    long notUsed = requested - usedCount;
    Mono<Void> start = Mono.empty();
    for (Entry e : entries) {
      Quota quota = e.getQuota();
      HashMap<UUID, Long> quotaReservation = reservation.reservationTracker.get(
        quota.getId()
      );
      for (QuotaEntry qe : quota.getEntries()) {
        finish(quotaReservation.get(qe.getId()), qe, notUsed);
      }
      e.decrementReservationCount();
    }
    return start;
  }

  public Mono<Quota> patch(UUID quotaId, Map<String, Object> changes) {
    Mono<?> start = Mono.just(true);
    synchronized (quotas) {
      Entry e = quotas.remove(quotaId);
      if (e != null) {
        start = service.save(e.getQuota(), false, true);
      }
    }
    return start
      .then(service.patch(quotaId, changes))
      .doOnSuccess(quota -> {
        synchronized (quotas) {
          quotas.put(quotaId, new Entry(quota));
        }
      });
  }

  public Mono<Void> delete(UUID quotaId) {
    synchronized (quotas) {
      quotas.remove(quotaId);
    }
    return service.delete(quotaId);
  }

  public Mono<Quota> create(Quota quota) {
    if (quotas.containsKey(quota.getId())) {
      return Mono.error(new IllegalArgumentException("quota already exists"));
    }
    return service.create(quota);
  }

  private QuotaStatus getStatus(Quota quota) {
    final ZoneId id = ZoneId.of(quota.getTimezone());
    final Instant now = Instant.now();
    final ZonedDateTime current = now.atZone(id);
    HashMap<UUID, Boolean> map = new HashMap<>();
    for (QuotaEntry qe : quota.getEntries()) {
      boolean isInUse = isInUse(qe, now);
      boolean hasQuota = qe.getQuota() < 0;
      if (!hasQuota) {
        long quotaValue = qe
          .getResetStrategy()
          .getQuota(qe.getResetFactor(), current, qe.getQuota());
        hasQuota = qe.getCounter() < quotaValue;
      }
      map.put(qe.getId(), !isInUse || hasQuota);
    }
    return new QuotaStatus(
      quota.isDisabled(),
      isInAccessTime(quota, current),
      map
    );
  }

  private synchronized Reservation makeReservation(
    List<Entry> entries,
    QuotaUnit amount,
    boolean allowLess,
    UUID additionalInfo
  ) {
    long plainAmount = amount.toPlain(EXP);
    final Instant now = Instant.now();
    for (Entry e : entries) {
      Quota quota = e.getQuota();
      final ZoneId id = ZoneId.of(quota.getTimezone());
      final ZonedDateTime current = now.atZone(id);
      if (quota.isDisabled() || !isInAccessTime(quota, current)) {
        return Reservation.INVALID;
      }
      final long max = quota.getMaxRequest();
      if (max > 0 && max < plainAmount) {
        if (!allowLess) {
          return Reservation.INVALID;
        }
        plainAmount = max;
      }
      long l = plainAmount;
      for (QuotaEntry qe : quota.getEntries()) {
        l = minimum(l, qe, current, now);
      }
      if (l < plainAmount) {
        if (!allowLess) {
          return Reservation.INVALID;
        }
        plainAmount = l;
      }
      if (plainAmount <= 0) {
        return Reservation.INVALID;
      }
    }
    HashMap<UUID, HashMap<UUID, Long>> reservationTracker = new HashMap<>();
    for (Entry e : entries) {
      Quota quota = e.getQuota();
      HashMap<UUID, Long> map = new HashMap<>();
      for (QuotaEntry qe : quota.getEntries()) {
        map.put(qe.getId(), reserve(plainAmount, qe, now));
      }
      e.incrementReservationCount();
      synchronized (quotas) {
        if (!quotas.containsKey(e.quota.getId())) {
          quotas.put(e.quota.getId(), e);
        }
      }
      reservationTracker.put(quota.getId(), map);
    }
    List<UUID> ids = entries
      .stream()
      .map(e -> e.quota.getId())
      .collect(Collectors.toList());
    return new Reservation(
      new QuotaUnit(plainAmount, EXP),
      reservationTracker,
      ids,
      additionalInfo
    );
  }

  private void finish(Long resetCount, QuotaEntry e, long notUsed) {
    if (resetCount == null || e.getResetCounter() != resetCount) {
      return;
    }
    e.setCounter(e.getCounter() - notUsed);
  }

  private Long reserve(long amount, QuotaEntry entry, Instant current) {
    if (!isInUse(entry, current)) {
      return null;
    }
    entry.setCounter(entry.getCounter() + amount);
    return entry.getResetCounter();
  }

  private long minimum(
    long amount,
    QuotaEntry entry,
    ZonedDateTime current,
    Instant now
  ) {
    if (!isInUse(entry, now)) {
      return amount;
    }
    ZonedDateTime last = entry
      .getLastReset()
      .toInstant()
      .atZone(current.getZone());
    if (
      entry.getResetStrategy().isReset(entry.getResetFactor(), last, current)
    ) {
      entry.setCounter(0);
      entry.setLastReset(Timestamp.from(now));
      entry.setResetCounter(entry.getResetCounter() + 1);
    }
    if (entry.getQuota() < 0) {
      return amount;
    }
    long quota = entry
      .getResetStrategy()
      .getQuota(entry.getResetFactor(), current, entry.getQuota());
    long max = quota - entry.getCounter();
    return amount <= max ? amount : max;
  }

  private boolean isInUse(QuotaEntry entry, Instant i) {
    if (
      entry.getStartDate() != null &&
      entry.getStartDate().toInstant().compareTo(i) > 0
    ) {
      return false;
    }
    return (
      entry.getEndDate() == null ||
      entry.getEndDate().toInstant().compareTo(i) >= 0
    );
  }

  private boolean isInAccessTime(Quota quota, ZonedDateTime current) {
    if (quota.getAccessTimes().isEmpty()) {
      return true;
    }
    return quota
      .getAccessTimes()
      .stream()
      .anyMatch(e -> {
        return e
          .getRecurringStrategy()
          .isInside(
            e.getRecurringFactor(),
            e.getStartDate(),
            e.getEndDate(),
            current
          );
      });
  }

  private Mono<List<Entry>> get(Set<UUID> quotaIds) {
    List<Entry> entries = new ArrayList<>();
    synchronized (quotas) {
      Set<UUID> toFetch = new HashSet<>();
      for (UUID key : quotaIds) {
        Entry entry = quotas.get(key);
        if (entry != null) {
          entries.add(entry);
        } else {
          toFetch.add(key);
        }
      }
      if (toFetch.isEmpty()) {
        return Mono.just(entries);
      }
      return service
        .getAllByIds(quotaIds)
        .map(e -> {
          Entry quotaEntry = new Entry(e);
          quotas.put(e.getId(), quotaEntry);
          return quotaEntry;
        })
        .collectList()
        .map(list -> {
          entries.addAll(list);
          return entries;
        });
    }
  }
}
