package de.thm.arsnova.frag.jetzt.backend.model;

import java.util.Objects;

public class RoomQuestionCounts {

    private long questionCount;
    private long responseCount;

    public RoomQuestionCounts() {

    }

    public RoomQuestionCounts(long questionCount, long responseCount) {
        this.questionCount = questionCount;
        this.responseCount = responseCount;
    }

    public long getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(long questionCount) {
        this.questionCount = questionCount;
    }

    public long getResponseCount() {
        return responseCount;
    }

    public void setResponseCount(long responseCount) {
        this.responseCount = responseCount;
    }

    @Override
    public String toString() {
        return "RoomQuestionCounts{" +
                "questionCount=" + questionCount +
                ", responseCount=" + responseCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomQuestionCounts that = (RoomQuestionCounts) o;
        return questionCount == that.questionCount && responseCount == that.responseCount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionCount, responseCount);
    }
}
