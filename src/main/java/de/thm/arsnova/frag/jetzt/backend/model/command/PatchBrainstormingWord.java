package de.thm.arsnova.frag.jetzt.backend.model.command;

public class PatchBrainstormingWord extends WebSocketCommand<PatchBrainstormingWordPayload> {

    public PatchBrainstormingWord() {
        super(PatchBrainstormingWord.class.getSimpleName());
    }

    public PatchBrainstormingWord(PatchBrainstormingWordPayload payload) {
        super(PatchBrainstormingWord.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatchBrainstormingWord that = (PatchBrainstormingWord) o;
        return this.getPayload().equals(that.getPayload());
    }
}
