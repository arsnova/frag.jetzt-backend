package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.CommentNotification;
import de.thm.arsnova.frag.jetzt.backend.service.CommentNotificationFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.CommentNotificationService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("CommentNotificationController")
@RequestMapping(CommentNotificationController.REQUEST_MAPPING)
public class CommentNotificationController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(CommentNotificationController.class);

    protected static final String REQUEST_MAPPING = "/comment-notification";

    private final CommentNotificationService service;
    private final CommentNotificationFindQueryService findQueryService;

    @Autowired
    public CommentNotificationController(
            CommentNotificationService service,
            CommentNotificationFindQueryService findQueryService
    ) {
        this.service = service;
        this.findQueryService = findQueryService;
    }


    @GetMapping(GET_MAPPING)
    public Mono<CommentNotification> get(@PathVariable UUID id) {
        return service.get(id).switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @PostMapping(FIND_MAPPING)
    public Flux<CommentNotification> find(@RequestBody final FindQuery<CommentNotification> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

    @PostMapping(POST_MAPPING)
    public Mono<CommentNotification> post(@RequestBody CommentNotification commentNotification) {
        return service.create(commentNotification.getRoomId(), commentNotification.getNotificationSetting());
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable UUID id) {
        return service.deleteById(id);
    }
}
