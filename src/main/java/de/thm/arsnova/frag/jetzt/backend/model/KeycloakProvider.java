package de.thm.arsnova.frag.jetzt.backend.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class KeycloakProvider implements Persistable<UUID> {

  @Id
  private UUID id;

  private int priority;
  private String url;
  private String frontendUrl;
  private String eventPassword;
  private String allowedIps;
  private String realm;
  private String clientId;
  private String nameDe;
  private String nameEn;
  private String nameFr;
  private String descriptionDe;
  private String descriptionEn;
  private String descriptionFr;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public KeycloakProvider() {}

  public KeycloakProvider(
    int priority,
    String url,
    String frontendUrl,
    String eventPassword,
    String allowedIps,
    String realm,
    String clientId,
    String nameDe,
    String nameEn,
    String nameFr,
    String descriptionDe,
    String descriptionEn,
    String descriptionFr
  ) {
    this.priority = priority;
    this.url = url;
    this.frontendUrl = frontendUrl;
    this.eventPassword = eventPassword;
    this.allowedIps = allowedIps;
    this.realm = realm;
    this.clientId = clientId;
    this.nameDe = nameDe;
    this.nameEn = nameEn;
    this.nameFr = nameFr;
    this.descriptionDe = descriptionDe;
    this.descriptionEn = descriptionEn;
    this.descriptionFr = descriptionFr;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getFrontendUrl() {
    return frontendUrl;
  }

  public void setFrontendUrl(String frontendUrl) {
    this.frontendUrl = frontendUrl;
  }

  public String getEventPassword() {
    return eventPassword;
  }

  public void setEventPassword(String eventPassword) {
    this.eventPassword = eventPassword;
  }

  public String getAllowedIps() {
    return allowedIps;
  }

  public void setAllowedIps(String allowedIps) {
    this.allowedIps = allowedIps;
  }

  public String getRealm() {
    return realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getNameDe() {
    return nameDe;
  }

  public void setNameDe(String nameDe) {
    this.nameDe = nameDe;
  }

  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }

  public String getNameFr() {
    return nameFr;
  }

  public void setNameFr(String nameFr) {
    this.nameFr = nameFr;
  }

  public String getDescriptionDe() {
    return descriptionDe;
  }

  public void setDescriptionDe(String descriptionDe) {
    this.descriptionDe = descriptionDe;
  }

  public String getDescriptionEn() {
    return descriptionEn;
  }

  public void setDescriptionEn(String descriptionEn) {
    this.descriptionEn = descriptionEn;
  }

  public String getDescriptionFr() {
    return descriptionFr;
  }

  public void setDescriptionFr(String descriptionFr) {
    this.descriptionFr = descriptionFr;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + priority;
    result = prime * result + ((url == null) ? 0 : url.hashCode());
    result =
      prime * result + ((frontendUrl == null) ? 0 : frontendUrl.hashCode());
    result =
      prime * result + ((eventPassword == null) ? 0 : eventPassword.hashCode());
    result =
      prime * result + ((allowedIps == null) ? 0 : allowedIps.hashCode());
    result = prime * result + ((realm == null) ? 0 : realm.hashCode());
    result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
    result = prime * result + ((nameDe == null) ? 0 : nameDe.hashCode());
    result = prime * result + ((nameEn == null) ? 0 : nameEn.hashCode());
    result = prime * result + ((nameFr == null) ? 0 : nameFr.hashCode());
    result =
      prime * result + ((descriptionDe == null) ? 0 : descriptionDe.hashCode());
    result =
      prime * result + ((descriptionEn == null) ? 0 : descriptionEn.hashCode());
    result =
      prime * result + ((descriptionFr == null) ? 0 : descriptionFr.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    KeycloakProvider other = (KeycloakProvider) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (priority != other.priority) return false;
    if (url == null) {
      if (other.url != null) return false;
    } else if (!url.equals(other.url)) return false;
    if (frontendUrl == null) {
      if (other.frontendUrl != null) return false;
    } else if (!frontendUrl.equals(other.frontendUrl)) return false;
    if (eventPassword == null) {
      if (other.eventPassword != null) return false;
    } else if (!eventPassword.equals(other.eventPassword)) return false;
    if (allowedIps == null) {
      if (other.allowedIps != null) return false;
    } else if (!allowedIps.equals(other.allowedIps)) return false;
    if (realm == null) {
      if (other.realm != null) return false;
    } else if (!realm.equals(other.realm)) return false;
    if (clientId == null) {
      if (other.clientId != null) return false;
    } else if (!clientId.equals(other.clientId)) return false;
    if (nameDe == null) {
      if (other.nameDe != null) return false;
    } else if (!nameDe.equals(other.nameDe)) return false;
    if (nameEn == null) {
      if (other.nameEn != null) return false;
    } else if (!nameEn.equals(other.nameEn)) return false;
    if (nameFr == null) {
      if (other.nameFr != null) return false;
    } else if (!nameFr.equals(other.nameFr)) return false;
    if (descriptionDe == null) {
      if (other.descriptionDe != null) return false;
    } else if (!descriptionDe.equals(other.descriptionDe)) return false;
    if (descriptionEn == null) {
      if (other.descriptionEn != null) return false;
    } else if (!descriptionEn.equals(other.descriptionEn)) return false;
    if (descriptionFr == null) {
      if (other.descriptionFr != null) return false;
    } else if (!descriptionFr.equals(other.descriptionFr)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "KeycloakProvider [id=" +
      id +
      ", priority=" +
      priority +
      ", url=" +
      url +
      ", frontendUrl=" +
      frontendUrl +
      ", eventPassword=" +
      eventPassword +
      ", allowedIps=" +
      allowedIps +
      ", realm=" +
      realm +
      ", clientId=" +
      clientId +
      ", nameDe=" +
      nameDe +
      ", nameEn=" +
      nameEn +
      ", nameFr=" +
      nameFr +
      ", descriptionDe=" +
      descriptionDe +
      ", descriptionEn=" +
      descriptionEn +
      ", descriptionFr=" +
      descriptionFr +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
