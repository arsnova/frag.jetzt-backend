package de.thm.arsnova.frag.jetzt.quota.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaAccessTime;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaEntry;
import de.thm.arsnova.frag.jetzt.quota.service.persistence.QuotaAccessTimeRepository;
import de.thm.arsnova.frag.jetzt.quota.service.persistence.QuotaEntryRepository;
import de.thm.arsnova.frag.jetzt.quota.service.persistence.QuotaRepository;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class QuotaService {

  private final QuotaRepository repository;
  private final QuotaEntryRepository entryRepository;
  private final QuotaAccessTimeRepository accessTimeRepository;
  private final ObjectMapper mapper;

  @Autowired
  public QuotaService(
    QuotaRepository repository,
    QuotaEntryRepository entryRepository,
    QuotaAccessTimeRepository accessTimeRepository,
    ObjectMapper mapper
  ) {
    this.repository = repository;
    this.entryRepository = entryRepository;
    this.accessTimeRepository = accessTimeRepository;
    this.mapper = mapper;
  }

  public Mono<Quota> create(Quota quota) {
    quota.setId(null);
    return validate(quota)
      .flatMap(this::validate)
      .flatMap(q -> save(q, true, true));
  }

  public Flux<Quota> getAllByIds(Set<UUID> ids) {
    return repository
      .findAllById(ids)
      .flatMap(quota -> {
        return Mono
          .zip(
            entryRepository.findAllByQuotaId(quota.getId()).collectList(),
            accessTimeRepository.findAllByQuotaId(quota.getId()).collectList()
          )
          .map(tuple -> {
            quota.setEntries(tuple.getT1());
            quota.setAccessTimes(tuple.getT2());
            return quota;
          });
      });
  }

  public Mono<Quota> getById(UUID id) {
    return Mono
      .zip(
        repository.findById(id),
        entryRepository.findAllByQuotaId(id).collectList(),
        accessTimeRepository.findAllByQuotaId(id).collectList()
      )
      .map(tuple -> {
        Quota quota = tuple.getT1();
        quota.setEntries(tuple.getT2());
        quota.setAccessTimes(tuple.getT3());
        return quota;
      });
  }

  public Mono<Quota> patch(UUID id, Map<String, Object> changes) {
    return getById(id)
      .map(quota -> parseChanges(quota, changes))
      .flatMap(this::validate)
      .flatMap(quota ->
        save(
          quota,
          changes.containsKey("accessTimes"),
          changes.containsKey("entries")
        )
      );
  }

  public Mono<Void> delete(UUID id) {
    return repository.deleteById(id);
  }

  public Mono<Quota> save(
    Quota quota,
    boolean accessTimeChanged,
    boolean entryChanged
  ) {
    List<Publisher<?>> publishers = new ArrayList<>();
    if (entryChanged) {
      publishers.add(entryRepository.deleteAllByQuotaId(quota.getId()));
    }
    if (accessTimeChanged) {
      publishers.add(accessTimeRepository.deleteAllByQuotaId(quota.getId()));
    }
    return Mono
      .when(publishers)
      .then(repository.save(quota))
      .flatMap(q -> {
        List<Publisher<?>> savePublishers = new ArrayList<>();
        if (entryChanged && quota.getEntries() != null) {
          for (QuotaEntry entry : quota.getEntries()) {
            entry.setId(null);
            entry.setQuotaId(q.getId());
          }
          savePublishers.add(entryRepository.saveAll(quota.getEntries()));
        }
        if (accessTimeChanged && quota.getAccessTimes() != null) {
          for (QuotaAccessTime accessTime : quota.getAccessTimes()) {
            accessTime.setId(null);
            accessTime.setQuotaId(q.getId());
          }
          savePublishers.add(
            accessTimeRepository.saveAll(quota.getAccessTimes())
          );
        }
        return Mono.when(savePublishers).thenReturn(q);
      });
  }

  private Mono<Quota> validate(Quota quota) {
    return Mono
      .just(quota)
      .filter(e ->
        e.getTimezone() != null &&
        ZoneId.getAvailableZoneIds().contains(e.getTimezone())
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Missing or invalid timezone"))
      )
      .filter(e ->
        e.getEntries() == null ||
        e.getEntries().stream().allMatch(entry -> entry.getResetFactor() > 0)
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Invalid reset factor"))
      );
  }

  private Quota parseChanges(Quota quota, Map<String, Object> changes) {
    changes.forEach((key, value) -> {
      switch (key) {
        case "disabled":
          quota.setDisabled((boolean) value);
          break;
        case "timezone":
          quota.setTimezone((String) value);
          break;
        case "maxRequest":
          quota.setMaxRequest((long) value);
          break;
        case "entries":
          if (value == null) {
            quota.setEntries(null);
            break;
          }
          quota.setEntries(
            ((List<?>) value).stream()
              .map(e -> mapper.convertValue(e, QuotaEntry.class))
              .collect(Collectors.toList())
          );
          break;
        case "accessTimes":
          if (value == null) {
            quota.setAccessTimes(null);
            break;
          }
          quota.setAccessTimes(
            ((List<?>) value).stream()
              .map(e -> mapper.convertValue(e, QuotaAccessTime.class))
              .collect(Collectors.toList())
          );
          break;
        case "id":
        case "createdAt":
        case "updatedAt":
          throw new ForbiddenException("Can not change field: " + key);
        default:
          throw new BadRequestException("Unknown field: " + key);
      }
    });
    return quota;
  }
}
