package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GPTChatCompletionDeltaChoice implements IGPTCompletionChoice {

  private int index;
  private String finishReason;
  private GPTChatCompletionDelta delta;

  @Override
  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  @JsonProperty("finish_reason")
  @Override
  public String getFinishReason() {
    return finishReason;
  }

  public void setFinishReason(String finishReason) {
    this.finishReason = finishReason;
  }

  public GPTChatCompletionDelta getDelta() {
    return delta;
  }

  public void setDelta(GPTChatCompletionDelta delta) {
    this.delta = delta;
  }
}
