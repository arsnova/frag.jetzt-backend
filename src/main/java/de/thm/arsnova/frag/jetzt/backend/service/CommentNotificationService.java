package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.*;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentNotificationRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class CommentNotificationService {

  private static final Logger logger = LoggerFactory.getLogger(
    CommentNotificationService.class
  );

  @Value("${app.mail.sender-name}")
  private String mailSenderName;

  @Value("${app.mail.sender-address}")
  private String mailSenderAddress;

  @Value("${app.comment_notification.subject}")
  private String mailSubject;

  @Value("${app.comment_notification.body}")
  private String mailBody;

  @Value("${app.comment_notification.question_template}")
  private String mailQuestionTemplate;

  private final CommentNotificationRepository repository;
  private final CommentRepository commentRepository;
  private final JavaMailSender mailSender;
  private final RoomRepository roomRepository;
  private final AccountServiceBase accountServiceBase;
  private final RoomAccessService roomAccessService;
  private final AuthorizationHelper authorizationHelper;

  @Autowired
  public CommentNotificationService(
    CommentNotificationRepository repository,
    CommentRepository commentRepository,
    JavaMailSender mailSender,
    RoomRepository roomRepository,
    AccountServiceBase accountServiceBase,
    RoomAccessService roomAccessService,
    AuthorizationHelper authorizationHelper
  ) {
    this.repository = repository;
    this.commentRepository = commentRepository;
    this.mailSender = mailSender;
    this.roomRepository = roomRepository;
    this.accountServiceBase = accountServiceBase;
    this.roomAccessService = roomAccessService;
    this.authorizationHelper = authorizationHelper;
  }

  // runs every minute on second 0
  @Scheduled(cron = "0 * * * * *")
  public void sendCurrentNotifications() {
    final Instant currentTime = Instant.now();
    short currentNotificationTime = getCurrentNotificationTime();
    repository
      .findByNotificationSetting(currentNotificationTime)
      .flatMap(notification ->
        repository
          .findByAccountIdAndRoomIdOrderByNotificationSettingAsc(
            notification.getAccountId(),
            notification.getRoomId()
          )
          .collectList()
      )
      .flatMap(commentNotifications -> {
        short currentSetting = 0;
        short previousSetting = 0;
        UUID roomId = null;
        UUID accountId = null;
        for (int i = 0; i < commentNotifications.size(); i++) {
          final short current = commentNotifications
            .get(i)
            .getNotificationSetting();
          if (current == currentNotificationTime) {
            currentSetting = current;
            roomId = commentNotifications.get(i).getRoomId();
            accountId = commentNotifications.get(i).getAccountId();
            int index = i - 1;
            if (index < 0) {
              index += commentNotifications.size();
            }
            previousSetting =
              commentNotifications.get(index).getNotificationSetting();
            break;
          }
        }
        if (roomId == null) {
          return Mono.empty();
        }
        return sendNotification(
          roomId,
          accountId,
          currentTime,
          previousSetting,
          currentSetting
        );
      })
      .subscribe();
  }

  public Mono<CommentNotification> create(
    UUID roomId,
    short notificationSetting
  ) {
    return Mono
      .zip(
        roomRepository.findById(roomId),
        authorizationHelper.getCurrentUser()
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(tuple2 -> {
        CommentNotification not = new CommentNotification();
        not.setId(null);
        not.setAccountId(tuple2.getT2().getAccountId());
        not.setRoomId(roomId);
        not.setNotificationSetting(notificationSetting);
        return repository.save(not);
      });
  }

  public Mono<CommentNotification> get(UUID id) {
    return repository.findById(id);
  }

  public Flux<CommentNotification> getByRoomId(UUID roomId) {
    return Mono
      .zip(
        roomRepository.findById(roomId),
        authorizationHelper.getCurrentUser()
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flux()
      .flatMap(tuple2 ->
        repository.findByAccountIdAndRoomIdOrderByNotificationSettingAsc(
          tuple2.getT2().getAccountId(),
          roomId
        )
      );
  }

  public Mono<Void> deleteById(UUID id) {
    return Mono
      .zip(repository.findById(id), authorizationHelper.getCurrentUser())
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(tuple2 ->
        tuple2.getT1().getAccountId().equals(tuple2.getT2().getAccountId())
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(tuple2 -> repository.deleteById(tuple2.getT1().getId()));
  }

  private Mono<Boolean> sendNotification(
    UUID roomId,
    UUID accountId,
    Instant currentTime,
    short previousSetting,
    short currentSetting
  ) {
    Timestamp start = calculateStartTimestamp(
      currentTime,
      previousSetting,
      currentSetting
    );
    return this.roomAccessService.getByRoomId(roomId)
      .filter(roomAccess ->
        roomAccess.getRole() == RoomAccess.Role.EXECUTIVE_MODERATOR &&
        roomAccess.getAccountId().equals(accountId)
      )
      .count()
      .flatMap(moderatorCount ->
        Mono.zip(
          commentRepository
            .findByRoomIdAndCreatedAtGreaterThanEqualAndDeletedAtIsNullOrderByNumberAsc(
              roomId,
              start
            )
            .cast(Comment.class)
            .collectList(),
          roomRepository.findById(roomId),
          accountServiceBase.get(accountId),
          Mono.just(moderatorCount > 0)
        )
      )
      .publishOn(Schedulers.boundedElastic())
      .map(tuple4 -> {
        List<Comment> comments = tuple4.getT1();
        Room room = tuple4.getT2();
        Account account = tuple4.getT3();
        boolean isModerator = tuple4.getT4();
        String role = "participant";
        if (account.getId().equals(room.getOwnerId())) {
          role = "creator";
        } else if (isModerator) {
          role = "moderator";
        } else {
          comments.removeIf(c -> !c.isAck());
        }
        if (comments.size() < 1) {
          return false;
        }
        String shortId = "NotAvailable";
        try {
          shortId =
            URLEncoder.encode(
              room.getShortId(),
              StandardCharsets.UTF_8.displayName()
            );
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
        }
        final String commentUrl =
          "https://frag.jetzt/" + role + "/room/" + shortId + "/comments";
        final String preUrl =
          "https://frag.jetzt/" + role + "/room/" + shortId + "/comment/";
        StringBuilder builder = new StringBuilder();
        for (Comment comment : comments) {
          builder.append(
            MessageFormat.format(
              mailQuestionTemplate,
              comment.getNumber(),
              preUrl + comment.getId()
            )
          );
        }
        sendEmail(
          account.getEmail(),
          MessageFormat.format(mailSubject, room.getName()),
          MessageFormat.format(
            mailBody,
            room.getName(),
            commentUrl,
            comments.size(),
            builder.toString()
          )
        );
        return true;
      });
  }

  private void sendEmail(String destMail, String subject, String body) {
    final MimeMessage msg = mailSender.createMimeMessage();
    final MimeMessageHelper helper = new MimeMessageHelper(
      msg,
      StandardCharsets.UTF_8.displayName()
    );
    try {
      helper.setFrom(mailSenderName + "<" + mailSenderAddress + ">");
      helper.setTo(destMail);
      helper.setSubject(subject);
      helper.setText(body);

      logger.info(
        "Sending mail \"{}\" from \"{}\" to \"{}\"",
        mailSubject,
        msg.getFrom(),
        destMail
      );
      mailSender.send(msg);
    } catch (final MailException | MessagingException e) {
      logger.warn("Mail \"{}\" could not be sent.", mailSubject, e);
    }
  }

  private Timestamp calculateStartTimestamp(
    Instant currentTime,
    short previous,
    short current
  ) {
    int currentDayOfWeek = current >> 11;
    int previousDayOfWeek = previous >> 11;
    int elapsedDays = currentDayOfWeek - previousDayOfWeek;
    if (elapsedDays <= 0) {
      elapsedDays += 7;
    }
    int currentMinute = current - (currentDayOfWeek << 11);
    int previousMinute = previous - (previousDayOfWeek << 11);
    return Timestamp.from(
      currentTime.minus(
        elapsedDays * 24 * 60 + currentMinute - previousMinute,
        ChronoUnit.MINUTES
      )
    );
  }

  private short getCurrentNotificationTime() {
    LocalDateTime time = Timestamp.from(Instant.now()).toLocalDateTime();
    return (short) (
      (time.getDayOfWeek().ordinal() << 11) |
      (time.getHour() * 60 + time.getMinute())
    );
  }
}
