package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LivepollSessionRepository extends ReactiveCrudRepository<LivepollSession, UUID> {
  Flux<LivepollSession> findAllByRoomId(UUID roomId);

  Mono<LivepollSession> findByRoomIdAndActive(UUID roomId, boolean active);
}
