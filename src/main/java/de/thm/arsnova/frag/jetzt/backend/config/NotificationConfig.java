package de.thm.arsnova.frag.jetzt.backend.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("app.notification")
public class NotificationConfig {

  private final Map<String, Object> de = new HashMap<>();
  private final Map<String, Object> en = new HashMap<>();
  private final Map<String, Object> fr = new HashMap<>();

  public Map<String, Object> getDe() {
    return de;
  }

  public Map<String, Object> getEn() {
    return en;
  }

  public Map<String, Object> getFr() {
    return fr;
  }
}
