package de.thm.arsnova.frag.jetzt.notification.model;

import java.util.UUID;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;

public interface ICommentSubscription {
  public enum SubscriptionResult {
    // creatorId, initiatorId
    OWN_OWN, // max 1
    OWN_OTHER, // max 1
    OTHER_OWN, // max n
    OTHER_OTHER, // max n
    NONE,
  }

  UUID getAccountId();

  SubscriptionResult hasSubscribed(CommentChange change);
}
