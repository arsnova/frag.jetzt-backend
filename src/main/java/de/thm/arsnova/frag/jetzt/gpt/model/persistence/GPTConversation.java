package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class GPTConversation implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @Nullable
  private UUID roomId;

  @NonNull
  private String model;

  @Nullable
  private Float temperature;

  @Nullable
  private Float topP;

  @Nullable
  private Float presencePenalty;

  @Nullable
  private Float frequencyPenalty;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private List<GPTConversationEntry> messages;

  public GPTConversation() {}

  public GPTConversation(
    UUID accountId,
    UUID roomId,
    String model,
    Float temperature,
    Float topP,
    Float presencePenalty,
    Float frequencyPenalty
  ) {
    this.accountId = accountId;
    this.roomId = roomId;
    this.model = model;
    this.temperature = temperature;
    this.topP = topP;
    this.presencePenalty = presencePenalty;
    this.frequencyPenalty = frequencyPenalty;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public Float getTemperature() {
    return temperature;
  }

  public void setTemperature(Float temperature) {
    this.temperature = temperature;
  }

  public Float getTopP() {
    return topP;
  }

  public void setTopP(Float topP) {
    this.topP = topP;
  }

  public Float getPresencePenalty() {
    return presencePenalty;
  }

  public void setPresencePenalty(Float presencePenalty) {
    this.presencePenalty = presencePenalty;
  }

  public Float getFrequencyPenalty() {
    return frequencyPenalty;
  }

  public void setFrequencyPenalty(Float frequencyPenalty) {
    this.frequencyPenalty = frequencyPenalty;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<GPTConversationEntry> getMessages() {
    return messages;
  }

  public void setMessages(List<GPTConversationEntry> messages) {
    this.messages = messages;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((model == null) ? 0 : model.hashCode());
    result =
      prime * result + ((temperature == null) ? 0 : temperature.hashCode());
    result = prime * result + ((topP == null) ? 0 : topP.hashCode());
    result =
      prime *
      result +
      ((presencePenalty == null) ? 0 : presencePenalty.hashCode());
    result =
      prime *
      result +
      ((frequencyPenalty == null) ? 0 : frequencyPenalty.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTConversation other = (GPTConversation) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (model == null) {
      if (other.model != null) return false;
    } else if (!model.equals(other.model)) return false;
    if (temperature == null) {
      if (other.temperature != null) return false;
    } else if (!temperature.equals(other.temperature)) return false;
    if (topP == null) {
      if (other.topP != null) return false;
    } else if (!topP.equals(other.topP)) return false;
    if (presencePenalty == null) {
      if (other.presencePenalty != null) return false;
    } else if (!presencePenalty.equals(other.presencePenalty)) return false;
    if (frequencyPenalty == null) {
      if (other.frequencyPenalty != null) return false;
    } else if (!frequencyPenalty.equals(other.frequencyPenalty)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTConversation [id=" +
      id +
      ", accountId=" +
      accountId +
      ", roomId=" +
      roomId +
      ", model=" +
      model +
      ", temperature=" +
      temperature +
      ", topP=" +
      topP +
      ", presencePenalty=" +
      presencePenalty +
      ", frequencyPenalty=" +
      frequencyPenalty +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", messages=" +
      messages +
      "]"
    );
  }
}
