package de.thm.arsnova.frag.jetzt.gpt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomKey;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTAPISettingRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomKeyRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTVoucherRepository;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTRoomKeyService {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private GPTRoomKeyRepository repository;
  private GPTAPISettingRepository settingRepository;
  private GPTVoucherRepository voucherRepository;

  @Autowired
  public GPTRoomKeyService(
    GPTRoomKeyRepository repository,
    GPTAPISettingRepository settingRepository,
    GPTVoucherRepository voucherRepository
  ) {
    this.repository = repository;
    this.settingRepository = settingRepository;
    this.voucherRepository = voucherRepository;
  }

  public Flux<GPTRoomKey> getAllBySettingId(UUID settingId, boolean internal) {
    return this.repository.findAllBySettingId(settingId)
      .flatMap(key -> this.applyTransient(key, internal));
  }

  public Flux<GPTRoomKey> patchAllBySettingId(
    UUID settingId,
    List<Map<String, ?>> keys
  ) {
    return repository
      .deleteAllBySettingId(settingId)
      .thenMany(Flux.fromIterable(keys))
      .map(key -> MAPPER.convertValue(key, GPTRoomKey.class))
      .map(e -> {
        e.setSettingId(settingId);
        return e;
      })
      .flatMap(key -> this.repository.save(key));
  }

  private Mono<GPTRoomKey> applyTransient(GPTRoomKey key, boolean internal) {
    Mono<?> start = Mono.just(true);
    if (key.getApiSettingId() != null) {
      start =
        start
          .then(settingRepository.findById(key.getApiSettingId()))
          .flatMap(apiSetting -> {
            if (!internal) {
              apiSetting.setApiKey("*****");
              apiSetting.setApiOrganization("*****");
            }
            key.setApiSetting(apiSetting);
            return Mono.empty();
          });
    }
    if (key.getVoucherId() != null) {
      start =
        start
          .then(voucherRepository.findById(key.getVoucherId()))
          .flatMap(voucher -> {
            if (!internal) {
              voucher.setCode(null);
            }
            key.setVoucher(voucher);
            return Mono.empty();
          });
    }
    return start.thenReturn(key);
  }
}
