package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversationEntry;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface GPTConversationEntryRepository extends ReactiveCrudRepository<GPTConversationEntry, UUID> {

    Flux<GPTConversationEntry> findAllByConversationId(UUID conversationId);

    Flux<Void> deleteAllByConversationIdAndIndexGreaterThanEqual(UUID conversationId, int index);
    
    Mono<Long> countByConversationId(UUID conversationId);
}
