package de.thm.arsnova.frag.jetzt.backend.model.event.livepoll;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import java.util.List;

public class LivepollResultPayload implements WebSocketPayload {

  private List<Integer> votes;

  public LivepollResultPayload() {}

  public LivepollResultPayload(List<Integer> votes) {
    this.votes = votes;
  }

  public List<Integer> getVotes() {
    return votes;
  }

  public void setVotes(List<Integer> votes) {
    this.votes = votes;
  }
}
