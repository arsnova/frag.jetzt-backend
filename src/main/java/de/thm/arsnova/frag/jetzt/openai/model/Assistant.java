package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.lang.Nullable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Assistant {

  @Nullable
  private String id;

  // Will always be "assistant"
  @Nullable
  private String object;

  @JsonProperty("created_at")
  @Nullable
  private Integer createdAt;

  @Nullable
  private String model;

  // max 256 chars
  @Nullable
  private String name;

  // max 512 chars
  @Nullable
  private String description;

  // max 256,000 chars
  @Nullable
  private String instructions;

  @Nullable
  private List<Object> tools;

  @JsonProperty("tool_resources")
  @Nullable
  private Map<String, Object> toolResources;

  // max 16 entries
  // keys max 64 chars
  // values max 512 chars
  @Nullable
  private Map<String, String> metadata;

  // between 0 and 2, default: 1
  @Nullable
  private Double temperature;

  // top_p between 0 and 1, default: 1
  // use either this or temperature
  @JsonProperty("top_p")
  @Nullable
  private Double topP;

  @JsonProperty("response_format")
  @Nullable
  private Object responseFormat;

  public Assistant() {}

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getObject() {
    return object;
  }

  public void setObject(String object) {
    this.object = object;
  }

  public Integer getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Integer createdAt) {
    this.createdAt = createdAt;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getInstructions() {
    return instructions;
  }

  public void setInstructions(String instructions) {
    this.instructions = instructions;
  }

  public List<Object> getTools() {
    return tools;
  }

  public void setTools(List<Object> tools) {
    this.tools = tools;
  }

  public Map<String, Object> getToolResources() {
    return toolResources;
  }

  public void setToolResources(Map<String, Object> toolResources) {
    this.toolResources = toolResources;
  }

  public Map<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(Map<String, String> metadata) {
    this.metadata = metadata;
  }

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature(Double temperature) {
    this.temperature = temperature;
  }

  public Double getTopP() {
    return topP;
  }

  public void setTopP(Double topP) {
    this.topP = topP;
  }

  public Object getResponseFormat() {
    return responseFormat;
  }

  public void setResponseFormat(Object responseFormat) {
    this.responseFormat = responseFormat;
  }

  @JsonIgnore
  public void addFileSearch(List<String> fileIds) {
    if (tools == null) {
      tools = new ArrayList<>();
    }
    if (!tools.contains(ToolFileSearch.INSTANCE)) {
      tools.add(ToolFileSearch.INSTANCE);
    }
    Map<String, Object> fileSearch = Map.of(
      "vector_stores",
      Map.of("file_ids", fileIds)
    );
    if (toolResources == null) {
      toolResources = Map.of("file_search", fileSearch);
    } else {
      toolResources.put("file_search", fileSearch);
    }
  }
}
