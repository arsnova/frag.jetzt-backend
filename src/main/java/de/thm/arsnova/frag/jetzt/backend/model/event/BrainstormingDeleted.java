package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingDeleted extends WebSocketEvent<BrainstormingDeletedPayload> {

    public BrainstormingDeleted() {
        super(BrainstormingDeleted.class.getSimpleName());
    }

    public BrainstormingDeleted(BrainstormingDeletedPayload payload, UUID roomId) {
        super(BrainstormingDeleted.class.getSimpleName(), roomId);
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingDeleted that = (BrainstormingDeleted) o;
        return this.getPayload().equals(that.getPayload());
    }
}
