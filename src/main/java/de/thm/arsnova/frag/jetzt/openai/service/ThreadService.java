package de.thm.arsnova.frag.jetzt.openai.service;

import de.thm.arsnova.frag.jetzt.openai.model.AuthorizationInfo;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadContinue;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadMessageList;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadStarter;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ThreadService {

  private final WebClient restWebClient = WebClient.create(
    "https://api.openai.com/v1/threads"
  );

  public Flux<ServerSentEvent<Object>> createThreadAndRun(
    AuthorizationInfo info,
    ThreadStarter starter
  ) {
    return info
      .apply(restWebClient.post().uri("/runs"))
      .header("OpenAI-Beta", "assistants=v2")
      .bodyValue(starter)
      .exchangeToFlux(response ->
        ErrorHandler
          .handleFluxError(
            ServerSentEvent.class,
            "Thread create failed",
            response
          )
          .map(e -> (ServerSentEvent<Object>) e)
      )
      .onErrorResume(
        DecodingException.class,
        err -> {
          String message = err.getMessage();
          if (
            message == null ||
            !message.startsWith(
              "JSON decoding error: Unrecognized token 'DONE':"
            )
          ) {
            return Flux.error(err);
          }
          return Flux.just(
            ServerSentEvent
              .builder((Object) "{\"done\": true}")
              .event("finished")
              .build()
          );
        }
      );
  }

  public Flux<ServerSentEvent<Object>> run(
    AuthorizationInfo info,
    String threadId,
    ThreadContinue threadContinue
  ) {
    return info
      .apply(restWebClient.post().uri("/{threadId}/runs", threadId))
      .header("OpenAI-Beta", "assistants=v2")
      .bodyValue(threadContinue)
      .exchangeToFlux(response ->
        ErrorHandler
          .handleFluxError(ServerSentEvent.class, "Thread run failed", response)
          .map(e -> (ServerSentEvent<Object>) e)
      )
      .onErrorResume(
        DecodingException.class,
        err -> {
          String message = err.getMessage();
          if (
            message == null ||
            !message.startsWith(
              "JSON decoding error: Unrecognized token 'DONE':"
            )
          ) {
            return Flux.error(err);
          }
          return Flux.just(
            ServerSentEvent
              .builder((Object) "{\"done\": true}")
              .event("finished")
              .build()
          );
        }
      );
  }

  public Mono<ThreadMessageList> getMessages(
    AuthorizationInfo info,
    String threadId,
    String after
  ) {
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("limit", "100");
    if (after != null) {
      map.add("after", after);
    }
    return info
      .apply(
        restWebClient
          .get()
          .uri(uriBuilder ->
            uriBuilder
              .path("/{threadId}/messages")
              .queryParams(map)
              .build(threadId)
          )
      )
      .header("OpenAI-Beta", "assistants=v2")
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          ThreadMessageList.class,
          "Thread message retrieval failed",
          response
        )
      );
  }

  public Mono<Void> deleteThread(AuthorizationInfo info, String threadId) {
    return info
      .apply(restWebClient.delete().uri("/{threadId}", threadId))
      .header("OpenAI-Beta", "assistants=v2")
      .exchangeToMono(response ->
        ErrorHandler.handleError(Void.class, "Thread deletion failed", response)
      );
  }
}
