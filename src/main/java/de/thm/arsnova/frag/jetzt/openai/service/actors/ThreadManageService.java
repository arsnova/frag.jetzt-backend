package de.thm.arsnova.frag.jetzt.openai.service.actors;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomKey;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTRoomSettingService;
import de.thm.arsnova.frag.jetzt.openai.model.Assistant;
import de.thm.arsnova.frag.jetzt.openai.model.AuthorizationInfo;
import de.thm.arsnova.frag.jetzt.openai.model.FileObject;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadContinue;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadMessageList;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadStarter;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiAssistants;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiThreads;
import de.thm.arsnova.frag.jetzt.openai.service.AssistantService;
import de.thm.arsnova.frag.jetzt.openai.service.FileUploadService;
import de.thm.arsnova.frag.jetzt.openai.service.ThreadService;
import de.thm.arsnova.frag.jetzt.openai.service.persistence.OpenaiAssistantsRepository;
import de.thm.arsnova.frag.jetzt.openai.service.persistence.OpenaiThreadsRepository;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ThreadManageService {

  private final AuthorizationHelper helper;
  private final OpenaiAssistantsRepository assistantsRepository;
  private final OpenaiThreadsRepository threadsRepository;
  private final ThreadService service;
  private final AssistantService assistantService;
  private final GPTRoomSettingService settingService;
  private final GPTConfig config;
  private final RoomService roomService;
  private final UploadFileService uploadFileService;
  private final FileUploadService fileUploadService;

  public ThreadManageService(
    AuthorizationHelper helper,
    OpenaiAssistantsRepository assistantsRepository,
    OpenaiThreadsRepository threadsRepository,
    ThreadService service,
    GPTRoomSettingService settingService,
    GPTConfig config,
    RoomService roomService,
    AssistantService assistantService,
    UploadFileService uploadFileService,
    FileUploadService fileUploadService
  ) {
    this.helper = helper;
    this.assistantsRepository = assistantsRepository;
    this.threadsRepository = threadsRepository;
    this.service = service;
    this.settingService = settingService;
    this.config = config;
    this.roomService = roomService;
    this.assistantService = assistantService;
    this.uploadFileService = uploadFileService;
    this.fileUploadService = fileUploadService;
  }

  public Mono<FileObject> uploadFile(UUID roomId, UUID fileId) {
    return Mono
      .zip(getByRoomId(roomId), uploadFileService.getById(fileId))
      .flatMap(tuple ->
        fileUploadService.uploadFile(
          tuple.getT1(),
          tuple.getT2().getT1(),
          tuple.getT2().getT2()
        )
      );
  }

  public Mono<FileObject> getFileInfo(UUID roomId, String fileId) {
    return getByRoomId(roomId)
      .flatMap(info -> fileUploadService.getFileInfo(info, fileId));
  }

  public Flux<DataBuffer> downloadFile(UUID roomId, String fileId) {
    return getByRoomId(roomId)
      .flatMapMany(info -> fileUploadService.getFileContent(info, fileId));
  }

  public Mono<OpenaiAssistants> createAssistant(UUID roomId, Assistant a) {
    return Mono
      .zip(helper.getCurrentUser(), roomService.get(roomId))
      .filter(tuple ->
        helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2())
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("No rights to create assistant."))
      )
      .flatMap(tuple -> getByRoomId(tuple.getT2().getId()))
      .flatMap(info -> {
        return assistantService
          .createAssistant(info, a)
          .flatMap(assistant ->
            assistantsRepository.save(
              new OpenaiAssistants(roomId, assistant.getId())
            )
          );
      });
  }

  public Mono<Void> deleteAssistant(UUID assistantRefId) {
    return assistantsRepository
      .findById(assistantRefId)
      .switchIfEmpty(Mono.error(new NotFoundException("Assistant not found.")))
      .flatMap(assistant -> {
        return Mono
          .zip(helper.getCurrentUser(), roomService.get(assistant.getRoomId()))
          .filter(tuple ->
            helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2())
          )
          .switchIfEmpty(
            Mono.error(
              new BadRequestException("No rights to delete assistant.")
            )
          )
          .flatMap(tuple -> getByRoomId(tuple.getT2().getId()))
          .flatMap(info -> {
            return assistantService
              .deleteAssistant(info, assistant.getOpenaiId())
              .then(assistantsRepository.deleteById(assistantRefId));
          });
      });
  }

  public Mono<Assistant> updateAssistant(
    UUID assistantRefId,
    Map<String, Object> patch
  ) {
    return assistantsRepository
      .findById(assistantRefId)
      .switchIfEmpty(Mono.error(new NotFoundException("Assistant not found.")))
      .flatMap(assistant -> {
        return Mono
          .zip(helper.getCurrentUser(), roomService.get(assistant.getRoomId()))
          .filter(tuple ->
            helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2())
          )
          .switchIfEmpty(
            Mono.error(
              new BadRequestException("No rights to update assistant.")
            )
          )
          .flatMap(tuple -> getByRoomId(tuple.getT2().getId()))
          .flatMap(info -> {
            return assistantService.updateAssistant(
              info,
              assistant.getOpenaiId(),
              patch
            );
          });
      });
  }

  public Mono<List<OpenaiAssistants>> getAssistants(UUID roomId) {
    return assistantsRepository.findAllByRoomId(roomId).collectList();
  }

  public Mono<Assistant> getAssistant(UUID roomId, String assistantId) {
    return getByRoomId(roomId)
      .flatMap(info -> assistantService.getAssistant(info, assistantId))
      .onErrorResume(
        ResponseStatusException.class,
        e -> {
          if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return assistantsRepository
              .deleteByRoomIdAndOpenaiId(roomId, assistantId)
              .then(Mono.error(e));
          }
          return Mono.error(e);
        }
      );
  }

  public Flux<ServerSentEvent<?>> createThread(
    UUID roomId,
    ThreadStarter starter
  ) {
    return Mono
      .zip(helper.getCurrentUser(), getByRoomId(roomId))
      .flatMapMany(tuple -> {
        return service
          .createThreadAndRun(tuple.getT2(), starter)
          .flatMap(event -> {
            if (event.event().equals("thread.created")) {
              Map<String, Object> map = (Map<String, Object>) event.data();
              OpenaiThreads thread = new OpenaiThreads(
                tuple.getT1().getAccountId(),
                roomId,
                (String) map.get("id")
              );
              return threadsRepository
                .save(thread)
                .flatMapMany(t -> {
                  return Flux.concat(
                    Flux.just(event),
                    Flux.just(
                      ServerSentEvent.builder(t).event("fj.created").build()
                    )
                  );
                });
            }
            return Flux.just(event);
          });
      });
  }

  public Flux<ServerSentEvent<?>> continueThread(
    UUID threadId,
    ThreadContinue continuer
  ) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        threadsRepository.findByAccountIdAndId(user.getAccountId(), threadId)
      )
      .switchIfEmpty(Mono.error(new NotFoundException("Thread not found.")))
      .flatMap(thread ->
        Mono.zip(
          getByRoomId(thread.getRoomId()),
          Mono.just(thread.getOpenaiId())
        )
      )
      .flatMapMany(tuple -> service.run(tuple.getT1(), tuple.getT2(), continuer)
      );
  }

  public Mono<List<OpenaiThreads>> getThreads(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        return threadsRepository
          .findAllByRoomIdAndAccountId(roomId, user.getAccountId())
          .collectList();
      });
  }

  public Mono<ThreadMessageList> getMessages(UUID threadId, String after) {
    return threadsRepository
      .findById(threadId)
      .flatMap(thread ->
        Mono.zip(
          getByRoomId(thread.getRoomId()),
          Mono.just(thread.getOpenaiId())
        )
      )
      .flatMap(tuple -> service.getMessages(tuple.getT1(), tuple.getT2(), after)
      );
  }

  public Mono<Void> deleteThread(UUID threadId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        threadsRepository.findByAccountIdAndId(user.getAccountId(), threadId)
      )
      .switchIfEmpty(Mono.error(new NotFoundException("Thread not found.")))
      .flatMap(thread ->
        Mono.zip(
          getByRoomId(thread.getRoomId()),
          Mono.just(thread.getOpenaiId())
        )
      )
      .flatMap(tuple -> service.deleteThread(tuple.getT1(), tuple.getT2()))
      .then(threadsRepository.deleteById(threadId));
  }

  private Mono<AuthorizationInfo> getByRoomId(UUID roomId) {
    return settingService
      .get(roomId, true)
      .flatMap(setting -> {
        for (GPTRoomKey key : setting.getApiKeys()) {
          if (key.getApiSetting() != null) {
            return Mono.just(
              new AuthorizationInfo(
                key.getApiSetting().getApiKey(),
                key.getApiSetting().getApiOrganization(),
                null
              )
            );
          }
          if (key.getVoucher() != null) {
            return Mono.just(
              new AuthorizationInfo(
                config.getApiKey(),
                config.getOrganization(),
                null
              )
            );
          }
        }
        if (
          config.getApiKey() != null && config.getRestrictions().canBeAccessed()
        ) {
          return Mono.just(
            new AuthorizationInfo(
              config.getApiKey(),
              config.getOrganization(),
              null
            )
          );
        }
        return Mono.error(new NotFoundException("No API key found"));
      });
  }
}
