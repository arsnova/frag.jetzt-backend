package de.thm.arsnova.frag.jetzt.backend.model.command;

public class CreateBrainstormingVote extends WebSocketCommand<CreateBrainstormingVotePayload> {

    public CreateBrainstormingVote() {
        super(CreateBrainstormingVote.class.getSimpleName());
    }

    public CreateBrainstormingVote(CreateBrainstormingVotePayload payload) {
        super(CreateBrainstormingVote.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstormingVote that = (CreateBrainstormingVote) o;
        return this.getPayload().equals(that.getPayload());
    }

}
