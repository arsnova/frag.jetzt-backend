package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class LivepollSessionDeleted
  extends WebSocketEvent<LivepollSessionDeletedPayload> {

  public LivepollSessionDeleted() {
    super(LivepollSessionCreated.class.getSimpleName());
  }

  public LivepollSessionDeleted(
    LivepollSessionDeletedPayload payload,
    UUID roomId
  ) {
    super(LivepollSessionCreated.class.getSimpleName(), roomId);
    this.payload = payload;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    LivepollSessionDeleted that = (LivepollSessionDeleted) o;
    return this.getPayload().equals(that.getPayload());
  }
}
