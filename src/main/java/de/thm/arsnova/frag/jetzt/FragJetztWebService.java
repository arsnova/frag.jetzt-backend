package de.thm.arsnova.frag.jetzt;

import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;

@SpringBootApplication
@PropertySource(
  value = { "classpath:application.yml" },
  ignoreResourceNotFound = true,
  encoding = "UTF-8"
)
@EnableR2dbcRepositories
@EnableScheduling
@OpenAPIDefinition(
  info = @Info(
    title = "frag.jetzt-backend",
    version = "1.0",
    description = "Handles most of the interactions of frag.jetzt",
    contact = @Contact(name = "Ruben Bimberg", email = "ruben.bimberg@gmx.de"),
    license = @License(
      name = "MIT",
      url = "https://git.thm.de/arsnova/frag.jetzt-backend/-/blob/staging/LICENSE?ref_type=heads"
    )
  ),
  servers = {
    @Server(url = "/api", description = "Production deployment"),
    @Server(url = "/", description = "Local deployment"),
  }
)
public class FragJetztWebService {

  public static void main(String[] args) {
    if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
      Security.addProvider(new BouncyCastleProvider());
    }
    SpringApplication.run(FragJetztWebService.class, args);
  }
}
