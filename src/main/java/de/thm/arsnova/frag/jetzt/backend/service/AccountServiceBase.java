package de.thm.arsnova.frag.jetzt.backend.service;

import java.util.HashSet;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountKeycloakRoleRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountRepository;
import reactor.core.publisher.Mono;

@Service
public class AccountServiceBase {
    
  private static final Logger logger = LoggerFactory.getLogger(
    AccountService.class
  );

  private final AccountRepository repository;
  private final AccountKeycloakRoleRepository roleRepository;

  @Autowired
  public AccountServiceBase(
    AccountRepository repository,
    AccountKeycloakRoleRepository roleRepository
  ) {
    this.repository = repository;
    this.roleRepository = roleRepository;
  }


  public Mono<Account> get(final UUID id) {
    return repository.findById(id).flatMap(this::applyTransient);
  }

  public <T> Mono<T> refreshLastActive(Account account, Mono<T> t) {
    account.refreshLastActive();
    return repository.save(account).then(t);
  }

  public Mono<Account> applyTransient(Account account) {
    return roleRepository
      .findAllByAccountId(account.getId())
      .collect(
        () -> new HashSet<String>(),
        (set, entry) -> {
          set.add(entry.getRole());
        }
      )
      .map(roles -> {
        account.setRoles(roles);
        return account;
      });
  }
}
