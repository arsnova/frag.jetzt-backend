package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class PatchBrainstormingPayload implements WebSocketPayload {

    private UUID id;
    private Map<String, Object> changes;

    public PatchBrainstormingPayload() {

    }

    public PatchBrainstormingPayload(UUID id, Map<String, Object> changes) {
        this.id = id;
        this.changes = changes;
    }

    public Map<String, Object> getChanges() {
        return changes;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return "PatchBrainstormingPayload{" +
                "id=" + id +
                ", changes=" + changes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatchBrainstormingPayload that = (PatchBrainstormingPayload) o;
        return Objects.equals(id, that.id) && Objects.equals(changes, that.changes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, changes);
    }
}
