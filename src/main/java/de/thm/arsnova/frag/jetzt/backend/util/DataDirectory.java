package de.thm.arsnova.frag.jetzt.backend.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataDirectory {

  public static File get(String name) {
    Path path = Paths.get("data");
    path.toFile().mkdirs();
    return path.resolve(name).toFile();
  }

  public static Path getFolder(String name) {
    Path path = Paths.get("data", name);
    path.toFile().mkdirs();
    return path;
  }
}
