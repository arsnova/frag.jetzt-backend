package de.thm.arsnova.frag.jetzt.openai.controller;

import de.thm.arsnova.frag.jetzt.openai.model.Assistant;
import de.thm.arsnova.frag.jetzt.openai.model.FileObject;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadContinue;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadMessageList;
import de.thm.arsnova.frag.jetzt.openai.model.ThreadStarter;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiAssistants;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.OpenaiThreads;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.UploadedFile;
import de.thm.arsnova.frag.jetzt.openai.service.actors.ThreadManageService;
import de.thm.arsnova.frag.jetzt.openai.service.actors.UploadFileService;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.Part;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("AssistantsController")
@RequestMapping(AssistantsController.REQUEST_MAPPING)
public class AssistantsController {

  protected static final String REQUEST_MAPPING = "/assistants";

  private final UploadFileService uploadFileService;
  private final ThreadManageService threadManageService;

  public AssistantsController(
    UploadFileService uploadFileService,
    ThreadManageService threadManageService
  ) {
    this.uploadFileService = uploadFileService;
    this.threadManageService = threadManageService;
  }

  @PostMapping("/upload")
  public Mono<UploadedFile> upload(
    @RequestBody Mono<MultiValueMap<String, Part>> form
  ) {
    return form.flatMap(m -> {
      List<Part> parts = m.get("file");
      if (parts == null || parts.isEmpty()) {
        return Mono.error(new Exception("No file part found"));
      }
      for (Part p : parts) {
        if (p instanceof FilePart) {
          FilePart filePart = (FilePart) p;
          return uploadFileService.handleFileUpload(filePart);
        }
      }
      return Mono.empty();
    });
  }

  @PostMapping("/upload/{roomId}/{file}")
  public Mono<FileObject> upload(
    @PathVariable("roomId") UUID roomId,
    @PathVariable("file") UUID fileId
  ) {
    return threadManageService.uploadFile(roomId, fileId);
  }

  @GetMapping("/file/{roomId}/{fileId}")
  public Mono<FileObject> file(
    @PathVariable("roomId") UUID roomId,
    @PathVariable("fileId") String fileId
  ) {
    return threadManageService.getFileInfo(roomId, fileId);
  }

  @GetMapping("/file-content/{roomId}/{fileId}")
  public Flux<DataBuffer> fileContent(
    @PathVariable("roomId") UUID roomId,
    @PathVariable("fileId") String fileId
  ) {
    return threadManageService.downloadFile(roomId, fileId);
  }

  @PostMapping("/create-assistant/{roomId}")
  public Mono<OpenaiAssistants> createAssistant(
    @PathVariable("roomId") UUID roomId,
    @RequestBody Assistant a
  ) {
    return threadManageService.createAssistant(roomId, a);
  }

  @DeleteMapping("/delete-assistant/{assistantRefId}")
  public Mono<Void> deleteAssistant(
    @PathVariable("assistantRefId") UUID id
  ) {
    return threadManageService.deleteAssistant(id);
  }

  @PatchMapping("/update-assistant/{assistantRefId}")
  public Mono<Assistant> updateAssistant(
    @PathVariable("assistantRefId") UUID id,
    @RequestBody Map<String, Object> patch
  ) {
    return threadManageService.updateAssistant(id, patch);
  }

  @GetMapping("/assistant-list/{roomId}")
  public Mono<List<OpenaiAssistants>> assistantList(
    @PathVariable("roomId") UUID roomId
  ) {
    return threadManageService.getAssistants(roomId);
  }

  @GetMapping("/assistant/{roomId}/{assistantId}")
  public Mono<Assistant> assistant(
    @PathVariable("roomId") UUID roomId,
    @PathVariable("assistantId") String assistantId
  ) {
    return threadManageService.getAssistant(roomId, assistantId);
  }

  @PostMapping("/thread-create/{roomId}")
  public Flux<ServerSentEvent<?>> threadCreate(
    @PathVariable("roomId") UUID roomId,
    @RequestBody ThreadStarter starter
  ) {
    return threadManageService.createThread(roomId, starter);
  }

  @PostMapping("/thread-run/{threadId}")
  public Flux<ServerSentEvent<?>> threadRun(
    @PathVariable("threadId") UUID threadId,
    @RequestBody ThreadContinue continuer
  ) {
    return threadManageService.continueThread(threadId, continuer);
  }

  @GetMapping("/thread-list/{roomId}")
  public Mono<List<OpenaiThreads>> threadList(
    @PathVariable("roomId") UUID roomId
  ) {
    return threadManageService.getThreads(roomId);
  }

  @GetMapping("/thread-messages/{threadId}")
  public Mono<ThreadMessageList> threadMessages(
    @PathVariable("threadId") UUID threadId,
    @RequestParam(name = "after", required = false) String after
  ) {
    return threadManageService.getMessages(threadId, after);
  }

  @DeleteMapping("/thread-delete/{threadId}")
  public Mono<Void> threadDelete(
    @PathVariable("threadId") UUID threadId
  ) {
    return threadManageService.deleteThread(threadId);
  }
}
