package de.thm.arsnova.frag.jetzt.notification.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushCommentSubscription;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushRoomSubscription;
import de.thm.arsnova.frag.jetzt.notification.service.persistence.PushCommentSubscriptionRepository;
import de.thm.arsnova.frag.jetzt.notification.service.persistence.PushRoomSubscriptionRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ChangeSubscriptionService {

  private final AuthorizationHelper helper;
  private final PushCommentSubscriptionRepository commentRepository;
  private final PushRoomSubscriptionRepository roomRepository;

  @Autowired
  public ChangeSubscriptionService(
    AuthorizationHelper helper,
    PushCommentSubscriptionRepository commentRepository,
    PushRoomSubscriptionRepository roomRepository
  ) {
    this.helper = helper;
    this.commentRepository = commentRepository;
    this.roomRepository = roomRepository;
  }

  public Mono<PushCommentSubscription> createOrUpdateCommentSubscription(
    PushCommentSubscription subscription
  ) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        subscription.setId(null);
        subscription.setAccountId(user.getAccountId());
        return commentRepository
          .deleteByCommentIdAndAccountId(
            subscription.getCommentId(),
            user.getAccountId()
          )
          .then(commentRepository.save(subscription));
      });
  }

  public Mono<Void> deleteCommentSubscription(UUID commentId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        commentRepository.deleteByCommentIdAndAccountId(
          commentId,
          user.getAccountId()
        )
      );
  }

  public Mono<PushCommentSubscription> getCommentSubscription(UUID commentId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        commentRepository.findByCommentIdAndAccountId(
          commentId,
          user.getAccountId()
        )
      );
  }

  public Mono<PushRoomSubscription> createOrUpdateRoomSubscription(
    PushRoomSubscription subscription
  ) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        subscription.setId(null);
        subscription.setAccountId(user.getAccountId());
        return roomRepository
          .deleteByRoomIdAndAccountId(
            subscription.getRoomId(),
            user.getAccountId()
          )
          .then(
            roomRepository
              .save(subscription)
              .flatMap(this::applyTransientFields)
          );
      });
  }

  public Mono<PushRoomSubscription> getRoomSubscription(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        roomRepository
          .findByRoomIdAndAccountId(roomId, user.getAccountId())
          .flatMap(this::applyTransientFields)
      );
  }

  public Mono<Void> deleteCommentSubscriptions(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        commentRepository
          .deleteAllByRoomIdAndAccountId(roomId, user.getAccountId())
          .then()
      );
  }

  public Mono<Void> deleteRoomSubscription(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        roomRepository.deleteByRoomIdAndAccountId(roomId, user.getAccountId())
      );
  }

  public Flux<PushRoomSubscription> getRoomSubscriptions() {
    return helper
      .getCurrentUser()
      .flatMapMany(user -> roomRepository.findAllByAccountId(user.getAccountId()))
      .flatMap(this::applyTransientFields);
  }

  public Flux<PushRoomSubscription> getAllPlainRoomSubscriptions(UUID roomId) {
    return roomRepository.findAllByRoomId(roomId);
  }

  public Flux<PushCommentSubscription> getAllPlainCommentSubscriptions(UUID commentId) {
    return commentRepository.findAllByCommentId(commentId);
  }

  private Mono<PushRoomSubscription> applyTransientFields(
    PushRoomSubscription subscription
  ) {
    return getCommentSubscriptionsByRoom(subscription.getRoomId())
      .collectList()
      .map(subscriptions -> {
        subscription.setCommentSubscriptions(subscriptions);
        return subscription;
      });
  }

  private Flux<PushCommentSubscription> getCommentSubscriptionsByRoom(
    UUID roomId
  ) {
    return helper
      .getCurrentUser()
      .flatMapMany(user ->
        commentRepository.findAllByAccountIdAndRoomIdIn(
          user.getAccountId(),
          List.of(roomId)
        )
      );
  }
}
