package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.backend.model.KeycloakProvider;

@Repository
public interface KeycloakProviderRepository extends ReactiveCrudRepository<KeycloakProvider, UUID> {
    
}
