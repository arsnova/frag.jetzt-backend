package de.thm.arsnova.frag.jetzt.quota.model.persistence;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class QuotaAccessTime implements Persistable<UUID> {

  private static class DayEntryStrategy extends EntryStrategy {

    private final int day;

    public DayEntryStrategy(int day) {
      super(String.valueOf(day));
      this.day = day;
    }

    @Override
    boolean isInside(
      LocalTime startDate,
      LocalTime endDate,
      ZonedDateTime date
    ) {
      return (
        date.getDayOfMonth() == day && isInsideTime(startDate, endDate, date)
      );
    }
  }

  public abstract static class EntryStrategy {

    private static final HashMap<String, EntryStrategy> strategies = new HashMap<>();
    public static final EntryStrategy MONDAYS = new EntryStrategy("MONDAYS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.MONDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy TUESDAYS = new EntryStrategy("TUESDAYS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.TUESDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy WEDNESDAYS = new EntryStrategy(
      "WEDNESDAYS"
    ) {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.WEDNESDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy THURSDAYS = new EntryStrategy(
      "THURSDAYS"
    ) {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.THURSDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy FRIDAYS = new EntryStrategy("FRIDAYS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.FRIDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy SATURDAYS = new EntryStrategy(
      "SATURDAYS"
    ) {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.SATURDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy SUNDAYS = new EntryStrategy("SUNDAYS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() == DayOfWeek.SUNDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy DAILY = new EntryStrategy("DAILY") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return isInsideTime(startDate, endDate, date);
      }
    };
    public static final EntryStrategy WEEKDAYS = new EntryStrategy("WEEKDAYS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          date.getDayOfWeek() != DayOfWeek.SATURDAY &&
          date.getDayOfWeek() != DayOfWeek.SUNDAY &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };
    public static final EntryStrategy WEEKENDS = new EntryStrategy("WEEKENDS") {
      @Override
      boolean isInside(
        LocalTime startDate,
        LocalTime endDate,
        ZonedDateTime date
      ) {
        return (
          (
            date.getDayOfWeek() == DayOfWeek.SATURDAY ||
            date.getDayOfWeek() == DayOfWeek.SUNDAY
          ) &&
          isInsideTime(startDate, endDate, date)
        );
      }
    };

    public final String description;

    private EntryStrategy(String description) {
      strategies.put(description.toLowerCase(), this);
      this.description = description;
    }

    public static boolean isInsideTime(
      LocalTime startDate,
      LocalTime endDate,
      ZonedDateTime date
    ) {
      LocalTime time = date.toLocalTime();
      return startDate.compareTo(time) <= 0 && endDate.compareTo(time) >= 0;
    }

    public static EntryStrategy fromString(String description) {
      EntryStrategy s = strategies.get(description.toLowerCase());
      if (s != null) {
        return s;
      }
      int i;
      try {
        i = Integer.parseInt(description, 10);
      } catch (NumberFormatException e) {
        return null;
      }
      if (i < 1 || i > 31) {
        return null;
      }
      String strategy = String.valueOf(i);
      return strategies.computeIfAbsent(strategy, k -> new DayEntryStrategy(i));
    }

    abstract boolean isInside(
      LocalTime startDate,
      LocalTime endDate,
      ZonedDateTime date
    );
  }

  public enum RecurringStrategy {
    DAILY {
      @Override
      public boolean isInside(
        int factor,
        LocalDate startDate,
        LocalDate endDate,
        ZonedDateTime date
      ) {
        if (factor <= 0) {
          return NEVER.isInside(factor, startDate, endDate, date);
        }
        LocalDate localDate = date.toLocalDate();
        long between =
          ChronoUnit.DAYS.between(startDate, localDate) / factor * factor;
        LocalDate tempStart = startDate.plusDays(between);
        LocalDate tempEnd = endDate.plusDays(between);
        return (
          tempStart.compareTo(localDate) <= 0 &&
          tempEnd.compareTo(localDate) >= 0
        );
      }
    },
    WEEKLY {
      @Override
      public boolean isInside(
        int factor,
        LocalDate startDate,
        LocalDate endDate,
        ZonedDateTime date
      ) {
        if (factor <= 0) {
          return NEVER.isInside(factor, startDate, endDate, date);
        }
        LocalDate localDate = date.toLocalDate();
        long between =
          ChronoUnit.WEEKS.between(startDate, localDate) / factor * factor;
        LocalDate tempStart = startDate.plusDays(between);
        LocalDate tempEnd = endDate.plusDays(between);
        return (
          tempStart.compareTo(localDate) <= 0 &&
          tempEnd.compareTo(localDate) >= 0
        );
      }
    },
    MONTHLY {
      @Override
      public boolean isInside(
        int factor,
        LocalDate startDate,
        LocalDate endDate,
        ZonedDateTime date
      ) {
        if (factor <= 0) {
          return NEVER.isInside(factor, startDate, endDate, date);
        }
        LocalDate localDate = date.toLocalDate();
        long between =
          ChronoUnit.MONTHS.between(startDate, localDate) / factor * factor;
        LocalDate tempStart = startDate.plusMonths(between);
        LocalDate tempEnd = endDate.plusMonths(between);
        return (
          tempStart.compareTo(localDate) <= 0 &&
          tempEnd.compareTo(localDate) >= 0
        );
      }
    },
    YEARLY {
      @Override
      public boolean isInside(
        int factor,
        LocalDate startDate,
        LocalDate endDate,
        ZonedDateTime date
      ) {
        if (factor <= 0) {
          return NEVER.isInside(factor, startDate, endDate, date);
        }
        LocalDate localDate = date.toLocalDate();
        long between =
          ChronoUnit.YEARS.between(startDate, localDate) / factor * factor;
        LocalDate tempStart = startDate.plusYears(between);
        LocalDate tempEnd = endDate.plusYears(between);
        return (
          tempStart.compareTo(localDate) <= 0 &&
          tempEnd.compareTo(localDate) >= 0
        );
      }
    },
    NEVER {
      @Override
      public boolean isInside(
        int factor,
        LocalDate startDate,
        LocalDate endDate,
        ZonedDateTime date
      ) {
        LocalDate localDate = date.toLocalDate();
        return (
          startDate.compareTo(localDate) <= 0 &&
          endDate.compareTo(localDate) >= 0
        );
      }
    };

    public abstract boolean isInside(
      int factor,
      LocalDate startDate,
      LocalDate endDate,
      ZonedDateTime date
    );
  }

  @Id
  private UUID id;

  @NonNull
  private UUID quotaId;

  @NonNull
  private LocalDate startDate;

  @NonNull
  private LocalDate endDate;

  @NonNull
  private RecurringStrategy recurringStrategy;

  @NonNull
  private int recurringFactor;

  @NonNull
  private String strategy;

  @NonNull
  private LocalTime startTime;

  @NonNull
  private LocalTime endTime;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public QuotaAccessTime() {}

  public QuotaAccessTime(
    UUID quotaId,
    LocalDate startDate,
    LocalDate endDate,
    RecurringStrategy recurringStrategy,
    int recurringFactor
  ) {
    this.quotaId = quotaId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.recurringStrategy = recurringStrategy;
    this.recurringFactor = recurringFactor;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getQuotaId() {
    return quotaId;
  }

  public void setQuotaId(UUID quotaId) {
    this.quotaId = quotaId;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public RecurringStrategy getRecurringStrategy() {
    return recurringStrategy;
  }

  public void setRecurringStrategy(RecurringStrategy recurringStrategy) {
    this.recurringStrategy = recurringStrategy;
  }

  public int getRecurringFactor() {
    return recurringFactor;
  }

  public void setRecurringFactor(int recurringFactor) {
    this.recurringFactor = recurringFactor;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public String getStrategy() {
    return strategy;
  }

  public void setStrategy(String strategy) {
    this.strategy = strategy;
  }

  public LocalTime getStartTime() {
    return startTime;
  }

  public void setStartTime(LocalTime startTime) {
    this.startTime = startTime;
  }

  public LocalTime getEndTime() {
    return endTime;
  }

  public void setEndTime(LocalTime endTime) {
    this.endTime = endTime;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((quotaId == null) ? 0 : quotaId.hashCode());
    result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
    result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
    result =
      prime *
      result +
      ((recurringStrategy == null) ? 0 : recurringStrategy.hashCode());
    result = prime * result + recurringFactor;
    result = prime * result + ((strategy == null) ? 0 : strategy.hashCode());
    result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
    result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    QuotaAccessTime other = (QuotaAccessTime) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (quotaId == null) {
      if (other.quotaId != null) return false;
    } else if (!quotaId.equals(other.quotaId)) return false;
    if (startDate == null) {
      if (other.startDate != null) return false;
    } else if (!startDate.equals(other.startDate)) return false;
    if (endDate == null) {
      if (other.endDate != null) return false;
    } else if (!endDate.equals(other.endDate)) return false;
    if (recurringStrategy != other.recurringStrategy) return false;
    if (recurringFactor != other.recurringFactor) return false;
    if (strategy == null) {
      if (other.strategy != null) return false;
    } else if (!strategy.equals(other.strategy)) return false;
    if (startTime == null) {
      if (other.startTime != null) return false;
    } else if (!startTime.equals(other.startTime)) return false;
    if (endTime == null) {
      if (other.endTime != null) return false;
    } else if (!endTime.equals(other.endTime)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "QuotaAccessTime [id=" +
      id +
      ", quotaId=" +
      quotaId +
      ", startDate=" +
      startDate +
      ", endDate=" +
      endDate +
      ", recurringStrategy=" +
      recurringStrategy +
      ", recurringFactor=" +
      recurringFactor +
      ", strategy=" +
      strategy +
      ", startTime=" +
      startTime +
      ", endTime=" +
      endTime +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
