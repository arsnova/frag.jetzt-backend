package de.thm.arsnova.frag.jetzt.openai.model.persistence;

import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class UploadedFile implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private String fileName;
  private byte[] fileInfo;
  private Instant createdAt = Instant.now();
  private Instant updatedAt;

  public UploadedFile(UUID accountId, String fileName, byte[] fileInfo) {
    this.accountId = accountId;
    this.fileName = fileName;
    this.fileInfo = fileInfo;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public byte[] getFileInfo() {
    return fileInfo;
  }

  public void setFileInfo(byte[] fileInfo) {
    this.fileInfo = fileInfo;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public Instant getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
    result = prime * result + Arrays.hashCode(fileInfo);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UploadedFile other = (UploadedFile) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (fileName == null) {
      if (other.fileName != null) return false;
    } else if (!fileName.equals(other.fileName)) return false;
    if (!Arrays.equals(fileInfo, other.fileInfo)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "UploadedFile [id=" +
      id +
      ", accountId=" +
      accountId +
      ", fileName=" +
      fileName +
      ", fileInfo=" +
      Arrays.toString(fileInfo) +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
