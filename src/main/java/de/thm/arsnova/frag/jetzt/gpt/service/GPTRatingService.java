package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRating;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRatingRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTRatingService {

  private final AuthorizationHelper helper;
  private final GPTRatingRepository repository;

  @Value("${app.gpt.ratingLength}")
  private int maximalRatingLength;

  @Autowired
  public GPTRatingService(
    AuthorizationHelper helper,
    GPTRatingRepository repository
  ) {
    this.helper = helper;
    this.repository = repository;
  }

  public Mono<RatingResult> getRatingInfo() {
    return repository.getRatings();
  }

  public Mono<List<String>> getRatingLists() {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("Only administrators can read rating texts")
        )
      )
      .flatMap(ignore ->
        repository
          .findAll()
          .filter(e -> e.getRatingText() != null)
          .map(e -> e.getRatingText())
          .collectList()
      );
  }

  public Mono<GPTRating> getRating() {
    return helper
      .getCurrentUser()
      .flatMap(user -> repository.findByAccountId(user.getAccountId()));
  }

  public Mono<GPTRating> makeRating(float rating, String ratingText) {
    if (rating < 0 || rating > 5) {
      return Mono.error(
        new BadRequestException("Rating must be between 0 and 5!")
      );
    } else if (
      ratingText != null && ratingText.length() > maximalRatingLength
    ) {
      return Mono.error(
        new BadRequestException(
          "Rating text can not be larger than " +
          maximalRatingLength +
          " characters!"
        )
      );
    }
    return helper
      .getCurrentUser()
      .filter(user -> user.getName() != null && user.getName().length() > 1)
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("You must have an email to make a vote!")
        )
      )
      .flatMap(user ->
        repository
          .findByAccountId(user.getAccountId())
          .switchIfEmpty(
            Mono.just(new GPTRating(user.getAccountId(), rating, ratingText))
          )
      )
      .flatMap(data -> {
        data.setRating(rating);
        data.setRatingText(ratingText);
        return repository.save(data);
      });
  }
}
