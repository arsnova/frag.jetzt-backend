package de.thm.arsnova.frag.jetzt.quota.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;

@Repository
public interface QuotaRepository extends ReactiveCrudRepository<Quota, UUID> {
    
}
