package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.backend.model.AccountKeycloakRole;
import reactor.core.publisher.Flux;

@Repository
public interface AccountKeycloakRoleRepository extends ReactiveCrudRepository<AccountKeycloakRole, UUID> {
    
    Flux<AccountKeycloakRole> findAllByAccountId(UUID accountId);

}
