package de.thm.arsnova.frag.jetzt.backend;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.model.event.*;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingWordRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoomEventSource {

  private final AmqpTemplate messagingTemplate;
  private final BrainstormingWordRepository wordRepository;

  @Autowired
  public RoomEventSource(
    AmqpTemplate messagingTemplate,
    BrainstormingWordRepository wordRepository
  ) {
    this.messagingTemplate = messagingTemplate;
    this.wordRepository = wordRepository;
  }

  public void sessionDeleted(UUID sessionId, UUID roomId) {
    BrainstormingDeletedPayload payload = new BrainstormingDeletedPayload(
      sessionId
    );
    send(new BrainstormingDeleted(payload, roomId));
  }

  public void sessionCreated(BrainstormingSession session) {
    BrainstormingCreatedPayload payload = new BrainstormingCreatedPayload(
      session
    );
    send(new BrainstormingCreated(payload, session.getRoomId()));
  }

  public void sessionWordCreated(BrainstormingWord word, UUID roomId) {
    BrainstormingWordCreatedPayload payload = new BrainstormingWordCreatedPayload(
      word
    );
    send(new BrainstormingWordCreated(payload, roomId));
  }

  public void sessionVotesReset(UUID sessionId, UUID roomId) {
    BrainstormingVotesResetPayload payload = new BrainstormingVotesResetPayload(
      sessionId
    );
    send(new BrainstormingVotesReset(payload, roomId));
  }

  public void sessionCategorizationReset(UUID sessionId, UUID roomId) {
    BrainstormingCategorizationResetPayload payload = new BrainstormingCategorizationResetPayload(
      sessionId
    );
    send(new BrainstormingCategorizationReset(payload, roomId));
  }

  public void sessionWordPatched(
    UUID wordId,
    UUID roomId,
    Map<String, Object> changes
  ) {
    BrainstormingWordPatchedPayload payload = new BrainstormingWordPatchedPayload(
      wordId,
      changes
    );
    send(new BrainstormingWordPatched(payload, roomId));
  }

  public void sessionCategoriesUpdated(
    UUID roomId,
    List<BrainstormingCategory> categoryList
  ) {
    BrainstormingCategoriesUpdatedPayload payload = new BrainstormingCategoriesUpdatedPayload(
      categoryList
    );
    send(new BrainstormingCategoriesUpdated(payload, roomId));
  }

  public void sessionPatched(
    UUID sessionId,
    UUID roomId,
    Map<String, Object> changes
  ) {
    BrainstormingPatchedPayload payload = new BrainstormingPatchedPayload(
      sessionId,
      changes
    );
    send(new BrainstormingPatched(payload, roomId));
  }

  public void sessionWordVoted(UUID wordId, UUID roomId) {
    wordRepository
      .findById(wordId)
      .subscribe(word -> {
        BrainstormingVoteUpdatedPayload payload = new BrainstormingVoteUpdatedPayload(
          word
        );
        send(new BrainstormingVoteUpdated(payload, roomId));
      });
  }

  public void livepollCreated(LivepollSession session) {
    LivepollSessionCreatedPayload payload = new LivepollSessionCreatedPayload(
      session
    );
    send(new LivepollSessionCreated(payload, session.getRoomId()));
  }

  public void livepollPatched(
    UUID livepollId,
    UUID roomId,
    HashMap<String, Object> changes
  ) {
    LivepollSessionPatchedPayload payload = new LivepollSessionPatchedPayload(
      livepollId,
      changes
    );
    send(new LivepollSessionPatched(payload, roomId));
  }

  public void livepollDeleted(UUID livepollId, UUID roomId) {
    LivepollSessionDeletedPayload payload = new LivepollSessionDeletedPayload(
      livepollId
    );
    send(new LivepollSessionDeleted(payload, roomId));
  }

  public void roomChanged(UUID id, Map<String, Object> changeMap) {
    if (changeMap.size() < 1) {
      return;
    }
    RoomPatchedPayload p = new RoomPatchedPayload(id, changeMap);
    send(new RoomPatched(p, id));
  }

  private void send(WebSocketEvent<?> event) {
    if (event.getRoomId() == null) throw new IllegalArgumentException(
      "Event must have roomId!"
    );

    messagingTemplate.convertAndSend(
      "amq.topic",
      event.getRoomId() + ".room.stream",
      event
    );
  }
}
