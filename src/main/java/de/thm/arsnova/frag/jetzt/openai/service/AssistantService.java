package de.thm.arsnova.frag.jetzt.openai.service;

import de.thm.arsnova.frag.jetzt.openai.model.Assistant;
import de.thm.arsnova.frag.jetzt.openai.model.AuthorizationInfo;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class AssistantService {

  private final WebClient restWebClient = WebClient.create(
    "https://api.openai.com/v1/assistants"
  );

  public Mono<Assistant> createAssistant(AuthorizationInfo info, Assistant a) {
    return info
      .apply(restWebClient.post())
      .header("OpenAI-Beta", "assistants=v2")
      .bodyValue(a)
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          Assistant.class,
          "Assistant creation failed",
          response
        )
      );
  }

  public Mono<Assistant> getAssistant(AuthorizationInfo info, String id) {
    return info
      .apply(restWebClient.get().uri("/{id}", id))
      .header("OpenAI-Beta", "assistants=v2")
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          Assistant.class,
          "Assistant retrieval failed",
          response
        )
      );
  }

  public Mono<Void> deleteAssistant(AuthorizationInfo info, String id) {
    return info
      .apply(restWebClient.delete().uri("/{id}", id))
      .header("OpenAI-Beta", "assistants=v2")
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          Void.class,
          "Assistant deletion failed",
          response
        )
      );
  }

  public Mono<Assistant> updateAssistant(AuthorizationInfo info, String id, Map<String, Object> a) {
    return info
      .apply(restWebClient.post().uri("/{id}", id))
      .header("OpenAI-Beta", "assistants=v2")
      .bodyValue(a)
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          Assistant.class,
          "Assistant update failed",
          response
        )
      );
  }
}
