package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class RoomCreated extends WebSocketEvent<RoomCreatedPayload> {
    public RoomCreated() {
        super(RoomCreated.class.getSimpleName());
    }

    public RoomCreated(RoomCreatedPayload p, UUID id) {
        super(RoomCreated.class.getSimpleName(), id);
        this.payload = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomCreated that = (RoomCreated) o;
        return this.getPayload().equals(that.getPayload());
    }
}
