package de.thm.arsnova.frag.jetzt.quota.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService;
import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService.QuotaStatus;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController("QuotaController")
@RequestMapping(QuotaController.REQUEST_MAPPING)
public class QuotaController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/quota";
  private static final String STATUS = "/status";

  private final QuotaManageService quotaManageService;

  @Autowired
  public QuotaController(QuotaManageService quotaManageService) {
    this.quotaManageService = quotaManageService;
  }

  @GetMapping(GET_MAPPING)
  public Mono<Quota> getQuota(@PathVariable UUID id) {
    return quotaManageService.get(id);
  }

  @GetMapping(GET_MAPPING + STATUS)
  public Mono<QuotaStatus> getQuotaStatus(@PathVariable UUID id) {
    return quotaManageService.getStatus(id);
  }
}
