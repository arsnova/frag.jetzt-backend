package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class RecreateModeratorCodePayload implements WebSocketPayload {

    private UUID id;

    public RecreateModeratorCodePayload() {

    }

    public RecreateModeratorCodePayload(UUID roomId){
        this.id = roomId;
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RecreateModeratorCodePayload{" +
                "id=" + id +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecreateModeratorCodePayload that = (RecreateModeratorCodePayload) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
