package de.thm.arsnova.frag.jetzt.gpt.model.rest;

public class GPTCompletionDoneMarker implements IGPTStreamObject {

  private boolean done = true;

  public boolean isDone() {
    return done;
  }

  public void setDone(boolean done) {
    this.done = done;
  }
}
