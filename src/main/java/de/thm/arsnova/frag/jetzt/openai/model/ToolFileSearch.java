package de.thm.arsnova.frag.jetzt.openai.model;

public class ToolFileSearch implements AssistantTool {

  public static final ToolFileSearch INSTANCE = new ToolFileSearch();
  private final String type;

  private ToolFileSearch() {
    this.type = "file_search";
  }

  @Override
  public String getType() {
    return type;
  }
}
