package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.MailScheduler;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController("EmailController")
@RequestMapping(EmailController.REQUEST_MAPPING)
public class EmailController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    EmailController.class
  );

  public static final class EmailRequest {

    String message;
    String subject;

    public String getSubject() {
      return subject;
    }

    public void setSubject(String subject) {
      this.subject = subject;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }

  protected static final String REQUEST_MAPPING = "/admin-email";
  private static final String SEND_MAIL_MAPPING = "/send";

  private final AccountService service;
  private final AuthorizationHelper helper;
  private final MailScheduler mailScheduler;

  @Autowired
  public EmailController(
    AccountService service,
    AuthorizationHelper helper,
    MailScheduler mailScheduler
  ) {
    this.service = service;
    this.helper = helper;
    this.mailScheduler = mailScheduler;
  }

  @PostMapping(SEND_MAIL_MAPPING)
  public Mono<Void> sendMail(
    @RequestBody final FindQuery<EmailRequest> findQuery
  ) {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(ignore -> service.getAllEmails().collectList())
      .map(emails -> {
        mailScheduler.addEmails(
          emails,
          findQuery.getProperties().subject,
          findQuery.getProperties().message
        );
        return emails;
      })
      .then();
  }

  @PostMapping(FIND_MAPPING)
  public Mono<List<String>> find(
    @RequestBody final FindQuery<EmailRequest> findQuery
  ) {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(ignore -> service.getAllEmails().collectList());
  }
}
