package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class AccountFindQueryService {

    private final AccountService service;

    @Autowired
    public AccountFindQueryService(AccountService service) {
        this.service = service;
    }

    public Flux<Account> resolveQuery(final FindQuery<Account> findQuery) {
        if (findQuery.getProperties().getEmail() != null) {
            return service.getByEmail(findQuery.getProperties().getEmail());
        }
        return Flux.empty();
    }
}
