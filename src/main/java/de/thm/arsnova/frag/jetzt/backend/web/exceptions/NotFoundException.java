package de.thm.arsnova.frag.jetzt.backend.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Not Found means status code 404.
 */
public class NotFoundException extends ResponseStatusException {

    public NotFoundException() {
        super(HttpStatus.NOT_FOUND);
    }

    public NotFoundException(final String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

    public NotFoundException(final String message, final Throwable e) {
        super(HttpStatus.NOT_FOUND, message, e);
    }
}
