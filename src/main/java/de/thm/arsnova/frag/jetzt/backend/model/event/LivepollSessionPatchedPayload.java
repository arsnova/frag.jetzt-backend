package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import java.util.HashMap;
import java.util.UUID;

public class LivepollSessionPatchedPayload implements WebSocketPayload {

  private UUID id;
  private HashMap<String, Object> changes;

  public LivepollSessionPatchedPayload() {}

  public LivepollSessionPatchedPayload(
    UUID id,
    HashMap<String, Object> changes
  ) {
    this.id = id;
    this.changes = changes;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public HashMap<String, Object> getChanges() {
    return changes;
  }

  public void setChanges(HashMap<String, Object> changes) {
    this.changes = changes;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((changes == null) ? 0 : changes.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollSessionPatchedPayload other = (LivepollSessionPatchedPayload) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (changes == null) {
      if (other.changes != null) return false;
    } else if (!changes.equals(other.changes)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "LivepollSessionPatchedPayload [id=" + id + ", changes=" + changes + "]"
    );
  }
}
