package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class BrainstormingWordCreatedPayload implements WebSocketPayload {

    private UUID id;
    private UUID sessionId;
    private String name;
    private String correctedWord;

    public BrainstormingWordCreatedPayload() {

    }

    public BrainstormingWordCreatedPayload(BrainstormingWord word) {
        this.id = word.getId();
        this.sessionId = word.getSessionId();
        this.name = word.getWord();
        this.correctedWord = word.getCorrectedWord();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCorrectedWord() {
        return correctedWord;
    }

    public void setCorrectedWord(String correctedWord) {
        this.correctedWord = correctedWord;
    }

    @Override
    public String toString() {
        return "BrainstormingWordCreatedPayload{" +
                "id=" + id +
                ", sessionId=" + sessionId +
                ", name='" + name + '\'' +
                ", correctedWord='" + correctedWord + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BrainstormingWordCreatedPayload that = (BrainstormingWordCreatedPayload) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sessionId, that.sessionId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(correctedWord, that.correctedWord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sessionId, name, correctedWord);
    }
}
