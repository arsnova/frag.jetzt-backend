package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table("gpt_api_setting")
public class GPTAPISetting implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @Nullable
  private UUID quotaId;

  @NonNull
  private String apiKey;

  @Nullable
  private String apiOrganization;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public GPTAPISetting() {}

  public GPTAPISetting(
    @NonNull UUID accountId,
    @Nullable UUID quotaId,
    @NonNull String apiKey,
    @Nullable String apiOrganization
  ) {
    this.accountId = accountId;
    this.quotaId = quotaId;
    this.apiKey = apiKey;
    this.apiOrganization = apiOrganization;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getQuotaId() {
    return quotaId;
  }

  public void setQuotaId(UUID quotaId) {
    this.quotaId = quotaId;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getApiOrganization() {
    return apiOrganization;
  }

  public void setApiOrganization(String apiOrganization) {
    this.apiOrganization = apiOrganization;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((quotaId == null) ? 0 : quotaId.hashCode());
    result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
    result =
      prime *
      result +
      ((apiOrganization == null) ? 0 : apiOrganization.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTAPISetting other = (GPTAPISetting) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (quotaId == null) {
      if (other.quotaId != null) return false;
    } else if (!quotaId.equals(other.quotaId)) return false;
    if (apiKey == null) {
      if (other.apiKey != null) return false;
    } else if (!apiKey.equals(other.apiKey)) return false;
    if (apiOrganization == null) {
      if (other.apiOrganization != null) return false;
    } else if (!apiOrganization.equals(other.apiOrganization)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTAPISettings [id=" +
      id +
      ", accountId=" +
      accountId +
      ", quotaId=" +
      quotaId +
      ", apiKey=" +
      apiKey +
      ", apiOrganization=" +
      apiOrganization +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
