package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class GPTRoomKey implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID settingId;

  @Nullable
  private UUID apiSettingId;

  @Nullable
  private UUID voucherId;

  private int index;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private GPTAPISetting apiSetting;

  @Transient
  private GPTVoucher voucher;

  public GPTRoomKey() {}

  public GPTRoomKey(
    UUID settingId,
    UUID apiSettingId,
    UUID voucherId,
    int index
  ) {
    this.settingId = settingId;
    this.apiSettingId = apiSettingId;
    this.voucherId = voucherId;
    this.index = index;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getSettingId() {
    return settingId;
  }

  public void setSettingId(UUID settingId) {
    this.settingId = settingId;
  }

  public UUID getApiSettingId() {
    return apiSettingId;
  }

  public void setApiSettingId(UUID apiSettingId) {
    this.apiSettingId = apiSettingId;
  }

  public UUID getVoucherId() {
    return voucherId;
  }

  public void setVoucherId(UUID voucherId) {
    this.voucherId = voucherId;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public GPTAPISetting getApiSetting() {
    return apiSetting;
  }

  public void setApiSetting(GPTAPISetting apiSetting) {
    this.apiSetting = apiSetting;
  }

  public GPTVoucher getVoucher() {
    return voucher;
  }

  public void setVoucher(GPTVoucher voucher) {
    this.voucher = voucher;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((settingId == null) ? 0 : settingId.hashCode());
    result =
      prime * result + ((apiSettingId == null) ? 0 : apiSettingId.hashCode());
    result = prime * result + ((voucherId == null) ? 0 : voucherId.hashCode());
    result = prime * result + index;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomKey other = (GPTRoomKey) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (settingId == null) {
      if (other.settingId != null) return false;
    } else if (!settingId.equals(other.settingId)) return false;
    if (apiSettingId == null) {
      if (other.apiSettingId != null) return false;
    } else if (!apiSettingId.equals(other.apiSettingId)) return false;
    if (voucherId == null) {
      if (other.voucherId != null) return false;
    } else if (!voucherId.equals(other.voucherId)) return false;
    if (index != other.index) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomKey [id=" +
      id +
      ", settingId=" +
      settingId +
      ", apiSettingId=" +
      apiSettingId +
      ", voucherId=" +
      voucherId +
      ", index=" +
      index +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", apiSetting=" +
      apiSetting +
      ", voucher=" +
      voucher +
      "]"
    );
  }
}
