package de.thm.arsnova.frag.jetzt.backend.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class AuthenticatedUser implements Authentication {

  private final String accountId, token, type, email;
  private final List<RoomAccess> roomAccesses;
  private final Collection<? extends GrantedAuthority> roles;
  private boolean authenticated = true;
  private Account account;

  public AuthenticatedUser(
    String token,
    String accountId,
    String type,
    String email,
    List<RoomAccess> roomAccesses,
    Collection<? extends GrantedAuthority> roles,
    Account account
  ) {
    this.token = token;
    this.accountId = accountId;
    this.type = type;
    this.email = email;
    this.roomAccesses = roomAccesses;
    this.roles = roles;
    this.account = account;
  }

  public AuthenticatedUser(AuthenticatedUser toCopy, String newToken) {
    token = newToken;
    accountId = toCopy.accountId;
    type = toCopy.type;
    email = toCopy.email;
    roomAccesses = toCopy.roomAccesses;
    roles = toCopy.roles;
    account = toCopy.account;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles;
  }

  @Override
  public Object getCredentials() {
    return accountId;
  }

  @Override
  public Object getDetails() {
    return token;
  }

  @Override
  public Object getPrincipal() {
    return accountId;
  }

  @Override
  public boolean isAuthenticated() {
    return authenticated;
  }

  @Override
  public void setAuthenticated(boolean b) throws IllegalArgumentException {
    authenticated = b;
  }

  @Override
  public String getName() {
    return email;
  }

  public String getType() {
    return type;
  }

  public UUID getAccountId() {
    return UUID.fromString(accountId);
  }

  public List<RoomAccess> getRoomAccesses() {
    return roomAccesses;
  }

  @JsonIgnore
  public Account getAccount() {
    return account;
  }

  @Override
  public String toString() {
    return (
      "AuthenticatedUser{" +
      "userId='" +
      accountId +
      '\'' +
      ", token='" +
      token +
      '\'' +
      ", type='" +
      type +
      '\'' +
      ", roles=" +
      roles +
      ", authenticated=" +
      authenticated +
      '}'
    );
  }
}
