package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRequestStatistic;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface GPTRequestStatisticRepository extends ReactiveCrudRepository<GPTRequestStatistic, UUID> {
  Flux<GPTRequestStatistic> getAllByRoomIdOrderByCreatedAtAsc(UUID roomId);
}
