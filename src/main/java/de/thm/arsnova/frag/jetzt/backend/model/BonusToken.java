package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class BonusToken implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID roomId;
    private UUID commentId;
    private UUID accountId;
    private String token;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Override
    public boolean isNew() {
        return id == null;
    }

    public BonusToken() {
    }

    public BonusToken(UUID roomId, UUID commentId, UUID accountId, String token) {
        this.roomId = roomId;
        this.commentId = commentId;
        this.accountId = accountId;
        this.token = token;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusToken that = (BonusToken) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(roomId, that.roomId) &&
                Objects.equals(commentId, that.commentId) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(token, that.token) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, commentId, accountId, token, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return "BonusToken{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", commentId=" + commentId +
                ", accountId=" + accountId +
                ", token='" + token + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

}
