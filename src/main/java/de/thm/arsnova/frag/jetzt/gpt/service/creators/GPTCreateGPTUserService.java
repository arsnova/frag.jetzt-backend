package de.thm.arsnova.frag.jetzt.gpt.service.creators;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTUser;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTUserRepository;
import reactor.core.publisher.Mono;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GPTCreateGPTUserService {

  private final GPTUserRepository repository;

  @Autowired
  public GPTCreateGPTUserService(GPTUserRepository repository) {
    this.repository = repository;
  }

  public Mono<GPTUser> createNew(UUID accountId) {
    return repository.save(new GPTUser(accountId));
  }
}
