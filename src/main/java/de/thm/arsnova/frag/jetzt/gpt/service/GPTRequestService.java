package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionDelta;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionDeltaChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTCompletionDoneMarker;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModelResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionLogProbs;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.IGPTCompletionChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.IGPTStreamObject;
import de.thm.arsnova.frag.jetzt.gpt.util.EndpointIdentifier;
import de.thm.arsnova.frag.jetzt.gpt.util.SectionManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTRequestService {

  private static final Logger logger = LoggerFactory.getLogger(
    GPTRequestService.class
  );
  private final WebClient restWebClient = WebClient.create(
    "https://api.openai.com/v1"
  );
  private final SectionManager<EndpointIdentifier> section = new SectionManager<>();

  @Autowired
  public GPTRequestService() {}

  public Mono<GPTModelResponse> requestModels(GPTModelRequest request) {
    if (request.getApiKey() == null) {
      return Mono.error(new BadRequestException("ApiKey is null"));
    }
    RequestHeadersSpec<?> client = restWebClient
      .get()
      .uri("/models")
      .header("Authorization", "Bearer " + request.getApiKey());
    if (request.getApiOrg() != null) {
      client.header("OpenAI-Organization", request.getApiOrg());
    }
    return client.retrieve().bodyToMono(GPTModelResponse.class);
  }

  public Mono<GPTModerationResponse> requestModeration(
    GPTModerationRequest request
  ) {
    if (request.getApiKey() == null) {
      return Mono.error(new BadRequestException("ApiKey is null"));
    }
    RequestBodySpec spec = restWebClient.post().uri("/moderations");
    spec.header("Authorization", "Bearer " + request.getApiKey());
    request.setApiKey(null);
    if (request.getApiOrganization() != null) {
      spec.header("OpenAI-Organization", request.getApiOrganization());
      request.setApiOrganization(null);
    }
    return spec
      .bodyValue(request)
      .retrieve()
      .bodyToMono(GPTModerationResponse.class);
  }

  public Mono<GPTTextCompletionResponse> requestTextCompletion(
    UUID accountId,
    GPTTextCompletionRequest request
  ) {
    request.setStream(false);
    request.setUser(accountId.toString());
    if (request.getApiKey() == null) {
      return Mono.error(new BadRequestException("ApiKey is null"));
    }
    RequestBodySpec spec = restWebClient.post().uri("/completions");
    spec.header("Authorization", "Bearer " + request.getApiKey());
    request.setApiKey(null);
    if (request.getApiOrganization() != null) {
      spec.header("OpenAI-Organization", request.getApiOrganization());
      request.setApiOrganization(null);
    }
    return section.restrictedSection(
      spec
        .bodyValue(request)
        .retrieve()
        .bodyToMono(GPTTextCompletionResponse.class),
      new EndpointIdentifier(accountId, "completion"),
      true
    );
  }

  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamTextCompletion(
    UUID accountId,
    GPTTextCompletionRequest request
  ) {
    request.setStream(true);
    request.setUser(accountId.toString());
    if (request.getApiKey() == null) {
      return Flux.error(new BadRequestException("ApiKey is null"));
    }
    RequestBodySpec spec = restWebClient.post().uri("/completions");
    spec.header("Authorization", "Bearer " + request.getApiKey());
    request.setApiKey(null);
    if (request.getApiOrganization() != null) {
      spec.header("OpenAI-Organization", request.getApiOrganization());
      request.setApiOrganization(null);
    }
    return section.restrictedSection(
      handleServerSentEvents(
        spec.bodyValue(request).retrieve().bodyToFlux(ServerSentEvent.class)
      ),
      new EndpointIdentifier(accountId, "completion"),
      true
    );
  }

  public Mono<GPTChatCompletionResponse> requestChatCompletion(
    UUID accountId,
    GPTChatCompletionRequest request
  ) {
    request.setStream(false);
    request.setUser(accountId.toString());
    if (request.getApiKey() == null) {
      return Mono.error(new BadRequestException("ApiKey is null"));
    }
    RequestBodySpec spec = restWebClient.post().uri("/chat/completions");
    spec.header("Authorization", "Bearer " + request.getApiKey());
    request.setApiKey(null);
    if (request.getApiOrganization() != null) {
      spec.header("OpenAI-Organization", request.getApiOrganization());
      request.setApiOrganization(null);
    }
    return section.restrictedSection(
      spec
        .bodyValue(request)
        .retrieve()
        .bodyToMono(GPTChatCompletionResponse.class),
      new EndpointIdentifier(accountId, "completion"),
      true
    );
  }

  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamChatCompletion(
    UUID accountId,
    GPTChatCompletionRequest request
  ) {
    request.setStream(true);
    request.setUser(accountId.toString());
    if (request.getApiKey() == null) {
      return Flux.error(new BadRequestException("ApiKey is null"));
    }
    RequestBodySpec spec = restWebClient.post().uri("/chat/completions");
    spec.header("Authorization", "Bearer " + request.getApiKey());
    request.setApiKey(null);
    if (request.getApiOrganization() != null) {
      spec.header("OpenAI-Organization", request.getApiOrganization());
      request.setApiOrganization(null);
    }
    return section.restrictedSection(
      handleServerSentEvents(
        spec.bodyValue(request).retrieve().bodyToFlux(ServerSentEvent.class)
      ),
      new EndpointIdentifier(accountId, "completion"),
      true
    );
  }

  public Mono<Boolean> abortRequest(UUID accountId) {
    return Mono.just(
      section.interruptSection(new EndpointIdentifier(accountId, "completion"))
    );
  }

  private Flux<ServerSentEvent<IGPTStreamObject>> handleServerSentEvents(
    @SuppressWarnings("rawtypes") Flux<ServerSentEvent> events
  ) {
    return events
      .flatMap(event -> {
        Object data = event.data();
        if (!(data instanceof HashMap)) {
          logger.error(
            "Found wrong formatted data on OpenAI Stream Endpoint: {}\n\n{}",
            data != null ? data.getClass() : "null",
            data
          );
          return Flux.empty();
        }
        HashMap<?, ?> dataMap = (HashMap<?, ?>) data;
        Object current = dataMap.get("error");
        if (current != null) {
          logger.error("An OpenAI error occured:\n\n{}", dataMap);
          return Flux.error(makeError(current));
        }
        IGPTStreamObject response = makeResponse(dataMap);
        if (response == null) {
          return Flux.empty();
        }
        return Flux.just(ServerSentEvent.builder(response).build());
      })
      .onErrorResume(
        DecodingException.class,
        err -> {
          String message = err.getMessage();
          if (
            message == null ||
            !message.startsWith(
              "JSON decoding error: Unrecognized token 'DONE':"
            )
          ) {
            logger.error(
              "An error occured during parse of server sent events",
              err
            );
            return Flux.error(err);
          }
          IGPTStreamObject done = new GPTCompletionDoneMarker();
          return Flux.just(ServerSentEvent.builder(done).build());
        }
      );
  }

  private ResponseStatusException makeError(Object current) {
    Object errorMessage = null;
    if (current instanceof HashMap) {
      HashMap<?, ?> error = (HashMap<?, ?>) current;
      errorMessage = error.get("message");
      if (errorMessage == null) {
        errorMessage = error.get("internal_message");
      }
    }
    if (errorMessage == null) {
      errorMessage = current;
    }
    return new ResponseStatusException(
      HttpStatus.SERVICE_UNAVAILABLE,
      errorMessage.toString()
    );
  }

  private GPTCompletionResponse<IGPTCompletionChoice> makeResponse(
    HashMap<?, ?> dataMap
  ) {
    GPTCompletionResponse<IGPTCompletionChoice> response = new GPTCompletionResponse<>();
    response.setId((String) dataMap.get("id"));
    response.setObject((String) dataMap.get("object"));
    Number n = (Number) dataMap.get("created");
    response.setCreated(n != null ? new Timestamp(n.longValue()) : null);
    response.setModel((String) dataMap.get("model"));
    Object current = dataMap.get("choices");
    if (!(current instanceof List)) {
      return null;
    }
    List<?> choiceList = (List<?>) current;
    List<IGPTCompletionChoice> choices = new ArrayList<>(choiceList.size());
    for (Object choice : choiceList) {
      if (!(choice instanceof HashMap)) {
        continue;
      }
      HashMap<?, ?> choiceMap = (HashMap<?, ?>) choice;
      if (choiceMap.get("delta") instanceof HashMap) {
        choices.add(parseChatChoice(choiceMap));
        continue;
      }
      choices.add(parseTextChoice(choiceMap));
    }
    response.setChoices(choices);
    return response;
  }

  private GPTChatCompletionDeltaChoice parseChatChoice(
    HashMap<?, ?> choiceMap
  ) {
    HashMap<?, ?> deltaMap = (HashMap<?, ?>) choiceMap.get("delta");
    GPTChatCompletionDeltaChoice deltaChoice = new GPTChatCompletionDeltaChoice();
    deltaChoice.setIndex((Integer) choiceMap.get("index"));
    deltaChoice.setFinishReason((String) choiceMap.get("finish_reason"));
    GPTChatCompletionDelta delta = new GPTChatCompletionDelta();
    delta.setRole((String) deltaMap.get("role"));
    delta.setContent((String) deltaMap.get("content"));
    deltaChoice.setDelta(delta);
    return deltaChoice;
  }

  private GPTTextCompletionChoice parseTextChoice(HashMap<?, ?> choiceMap) {
    GPTTextCompletionChoice textChoice = new GPTTextCompletionChoice();
    textChoice.setIndex((Integer) choiceMap.get("index"));
    textChoice.setFinishReason((String) choiceMap.get("finish_reason"));
    textChoice.setText((String) choiceMap.get("text"));
    Object current = choiceMap.get("logprobs");
    if (current instanceof HashMap) {
      HashMap<?, ?> logprobsMap = (HashMap<?, ?>) current;
      GPTTextCompletionLogProbs logProbs = new GPTTextCompletionLogProbs();
      logProbs.setTextOffset((List<Integer>) logprobsMap.get("text_offset"));
      logProbs.setTokens((List<String>) logprobsMap.get("tokens"));
      logProbs.setTokenLogprobs(
        (List<Double>) logprobsMap.get("token_logprobs")
      );
      logProbs.setTopLogprobs(
        (List<Map<String, Double>>) logprobsMap.get("top_logprobs")
      );
      textChoice.setLogprobs(logProbs);
    }
    return textChoice;
  }
}
