package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.handler.BrainstormingCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingVote;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.service.BrainstormingSessionFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.BrainstormingSessionService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@RestController("BrainstormingController")
@RequestMapping(BrainstormingController.REQUEST_MAPPING)
public class BrainstormingController extends AbstractEntityController {

    private static class WrappedString {
        private String text;

        public WrappedString() {
        }

        public WrappedString(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return "WrappedString{" +
                    "text='" + text + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            WrappedString that = (WrappedString) o;
            return Objects.equals(text, that.text);
        }

        @Override
        public int hashCode() {
            return Objects.hash(text);
        }
    }
    private static final Logger logger = LoggerFactory.getLogger(BrainstormingController.class);

    protected static final String REQUEST_MAPPING = "/brainstorming";
    private static final String WORD_ID_MAPPING = "/{wordId}";
    private static final String UPVOTE_MAPPING = "/upvote";
    private static final String DOWNVOTE_MAPPING = "/downvote";
    private static final String RESET_VOTE_MAPPING = "/reset-vote";
    private static final String RESET_RATING_MAPPING = "/reset-rating";
    private static final String RESET_CATEGORIZATION_MAPPING = "/reset-categorization";
    private static final String CATEGORY_MAPPING = "/category/{roomId}";

    private static final String WORD_MAPPING = "/word/{word}";
    private static final String PATCH_WORD_MAPPING = "/patch-word/{wordId}";

    private final BrainstormingCommandHandler commandHandler;
    private final BrainstormingSessionService service;
    private final BrainstormingSessionFindQueryService findQueryService;

    @Autowired
    public BrainstormingController(
            BrainstormingCommandHandler commandHandler,
            BrainstormingSessionService service,
            BrainstormingSessionFindQueryService findQueryService
    ) {
        this.commandHandler = commandHandler;
        this.service = service;
        this.findQueryService = findQueryService;
    }

    @GetMapping(GET_MAPPING)
    public Mono<BrainstormingSession> get(@PathVariable UUID id) {
        return this.service.get(id)
                .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @PatchMapping(PATCH_MAPPING)
    public Mono<BrainstormingSession> patch(@PathVariable UUID id, @RequestBody final Map<String, Object> changes) {
        PatchBrainstormingPayload payload = new PatchBrainstormingPayload(id, changes);
        return this.commandHandler.handle(new PatchBrainstorming(payload));
    }

    @PostMapping(GET_MAPPING + WORD_MAPPING + POST_MAPPING)
    public Mono<BrainstormingWord> post(@PathVariable UUID id, @PathVariable final String word, @RequestBody WrappedString wrappedString) {
        CreateBrainstormingWordPayload payload = new CreateBrainstormingWordPayload(word, id, wrappedString.text);
        return this.commandHandler.handle(new CreateBrainstormingWord(payload));
    }

    @PatchMapping(PATCH_WORD_MAPPING)
    public Mono<BrainstormingWord> patchWord(@PathVariable UUID wordId, @RequestBody Map<String, Object> changes){
        PatchBrainstormingWordPayload payload = new PatchBrainstormingWordPayload(wordId, changes);
        return this.commandHandler.handle(new PatchBrainstormingWord(payload));
    }

    @PostMapping(POST_MAPPING)
    public Mono<BrainstormingSession> post(@RequestBody final BrainstormingSession session) {
        CreateBrainstormingPayload payload = new CreateBrainstormingPayload(session);
        return this.commandHandler.handle(new CreateBrainstorming(payload));
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable UUID id) {
        DeleteBrainstormingPayload payload = new DeleteBrainstormingPayload(id);
        return this.commandHandler.handle(new DeleteBrainstorming(payload));
    }

    @PostMapping(WORD_ID_MAPPING + UPVOTE_MAPPING)
    public Mono<BrainstormingVote> upvote(@PathVariable UUID wordId) {
        CreateBrainstormingVotePayload payload = new CreateBrainstormingVotePayload(wordId, true);
        return this.commandHandler.handle(new CreateBrainstormingVote(payload));
    }

    @PostMapping(WORD_ID_MAPPING + DOWNVOTE_MAPPING)
    public Mono<BrainstormingVote> downvote(@PathVariable UUID wordId) {
        CreateBrainstormingVotePayload payload = new CreateBrainstormingVotePayload(wordId, false);
        return this.commandHandler.handle(new CreateBrainstormingVote(payload));
    }

    @DeleteMapping(WORD_ID_MAPPING + RESET_VOTE_MAPPING)
    public Mono<Void> deleteVote(@PathVariable UUID wordId) {
        DeleteBrainstormingVotePayload payload = new DeleteBrainstormingVotePayload(wordId);
        return commandHandler.handle(new DeleteBrainstormingVote(payload));
    }

    @GetMapping(CATEGORY_MAPPING)
    public Flux<BrainstormingCategory> getCategories(@PathVariable UUID roomId) {
        return this.service.getCategories(roomId);
    }

    @PostMapping(CATEGORY_MAPPING + POST_MAPPING)
    public Flux<BrainstormingCategory> updateCategories(@PathVariable UUID roomId, @RequestBody List<String> categories) {
        UpdateBrainstormingCategoriesPayload payload = new UpdateBrainstormingCategoriesPayload(roomId, categories);
        return commandHandler.handle(new UpdateBrainstormingCategories(payload));
    }

    @PostMapping(GET_MAPPING + RESET_RATING_MAPPING)
    public Mono<Void> deleteAllVotes(@PathVariable UUID id) {
        ResetBrainstormingVotesPayload payload = new ResetBrainstormingVotesPayload(id);
        return commandHandler.handle(new ResetBrainstormingVotes(payload));
    }

    @PostMapping(GET_MAPPING + RESET_CATEGORIZATION_MAPPING)
    public Mono<Void> deleteAllCategories(@PathVariable UUID id) {
        ResetBrainstormingCategorizationPayload payload = new ResetBrainstormingCategorizationPayload(id);
        return commandHandler.handle(new ResetBrainstormingCategorization(payload));
    }

    @PostMapping(FIND_MAPPING)
    public Flux<BrainstormingSession> find(@RequestBody final FindQuery<BrainstormingSession> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

}
