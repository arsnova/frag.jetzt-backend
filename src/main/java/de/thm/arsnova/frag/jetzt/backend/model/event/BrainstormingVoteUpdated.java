package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingVoteUpdated extends WebSocketEvent<BrainstormingVoteUpdatedPayload> {

    public BrainstormingVoteUpdated() {
        super(BrainstormingVoteUpdated.class.getSimpleName());
    }

    public BrainstormingVoteUpdated(BrainstormingVoteUpdatedPayload payload, UUID roomId) {
        super(BrainstormingVoteUpdated.class.getSimpleName(), roomId);
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingVoteUpdated that = (BrainstormingVoteUpdated) o;
        return this.getPayload().equals(that.getPayload());
    }
}
