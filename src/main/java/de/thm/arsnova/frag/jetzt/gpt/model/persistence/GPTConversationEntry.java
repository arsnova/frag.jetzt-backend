package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class GPTConversationEntry implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID conversationId;

  @Nullable
  private Integer index;

  @NonNull
  private String role;

  @NonNull
  private String content;

  @Nullable
  private String name;

  @Nullable
  private String functionCall;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public GPTConversationEntry() {}

  public GPTConversationEntry(
    UUID conversationId,
    Integer index,
    String role,
    String content,
    String name,
    String functionCall
  ) {
    this.conversationId = conversationId;
    this.index = index;
    this.role = role;
    this.content = content;
    this.name = name;
    this.functionCall = functionCall;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getConversationId() {
    return conversationId;
  }

  public void setConversationId(UUID conversationId) {
    this.conversationId = conversationId;
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFunctionCall() {
    return functionCall;
  }

  public void setFunctionCall(String functionCall) {
    this.functionCall = functionCall;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result =
      prime *
      result +
      ((conversationId == null) ? 0 : conversationId.hashCode());
    result = prime * result + index;
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    result = prime * result + ((content == null) ? 0 : content.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result =
      prime * result + ((functionCall == null) ? 0 : functionCall.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTConversationEntry other = (GPTConversationEntry) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (conversationId == null) {
      if (other.conversationId != null) return false;
    } else if (!conversationId.equals(other.conversationId)) return false;
    if (index != other.index) return false;
    if (role == null) {
      if (other.role != null) return false;
    } else if (!role.equals(other.role)) return false;
    if (content == null) {
      if (other.content != null) return false;
    } else if (!content.equals(other.content)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (functionCall == null) {
      if (other.functionCall != null) return false;
    } else if (!functionCall.equals(other.functionCall)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTConversationEntry [id=" +
      id +
      ", conversationId=" +
      conversationId +
      ", index=" +
      index +
      ", role=" +
      role +
      ", content=" +
      content +
      ", name=" +
      name +
      ", functionCall=" +
      functionCall +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
