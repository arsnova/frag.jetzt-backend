package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Rating;
import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import de.thm.arsnova.frag.jetzt.backend.service.RatingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("RatingController")
@RequestMapping(RatingController.REQUEST_MAPPING)
public class RatingController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(RatingController.class);

    protected static final String REQUEST_MAPPING = "/rating";

    protected static final String AVERAGE_RATING = "/accumulated";

    private final RatingService service;

    @Autowired
    public RatingController(RatingService service) {
        this.service = service;
    }

    @GetMapping(GET_MAPPING)
    public Mono<Rating> getByAccountId(@PathVariable final UUID id) {
        return this.service.getByAccountId(id);
    }

    @PostMapping(POST_MAPPING)
    public Mono<Rating> create(@RequestBody Rating rating) {
        return this.service.create(rating);
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> deleteByAccountId(@PathVariable final UUID id) {
        return this.service.deleteByAccountId(id);
    }

    @GetMapping(AVERAGE_RATING)
    public Mono<RatingResult> getRatings() {
        return this.service.getRatings();
    }
}
