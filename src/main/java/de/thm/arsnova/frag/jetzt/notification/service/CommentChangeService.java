package de.thm.arsnova.frag.jetzt.notification.service;

import de.thm.arsnova.frag.jetzt.notification.model.ICommentSubscription.SubscriptionResult;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import de.thm.arsnova.frag.jetzt.notification.service.persistence.CommentChangeRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CommentChangeService {

  private static final Logger logger = LoggerFactory.getLogger(
    CommentChangeService.class
  );

  private final CommentChangeRepository repository;
  private final ChangeSubscriptionService subscriptionService;

  @Autowired
  public CommentChangeService(
    CommentChangeRepository repository,
    ChangeSubscriptionService subscriptionService
  ) {
    this.repository = repository;
    this.subscriptionService = subscriptionService;
  }

  // runs every hour on second 0 and minute 0
  @Scheduled(cron = "0 0 * * * *")
  public void deleteOutdatedChanges() {
    final Timestamp timestamp = Timestamp.from(
      Instant.now().minus(8, ChronoUnit.DAYS)
    );
    repository.deleteByCreatedAtLessThanEqual(timestamp).subscribe();
  }

  public Mono<CommentChange> get(UUID id) {
    return repository.findById(id);
  }

  public Mono<CommentChange> create(CommentChange change) {
    return repository.save(change);
  }

  public Flux<CommentChange> createAll(List<CommentChange> changes) {
    return repository.saveAll(changes);
  }

  public Flux<CommentChange> receiveMissedInformationSince(
    Timestamp timestamp
  ) {
    return subscriptionService
      .getRoomSubscriptions()
      .flatMap(subscription -> {
        if (
          subscription.getOwnCommentBits() == 0 &&
          subscription.getOtherCommentBits() == 0
        ) {
          List<UUID> commentIds = subscription
            .getCommentSubscriptions()
            .stream()
            .filter(e -> e.getInterestBits() != 0)
            .map(e -> e.getCommentId())
            .collect(Collectors.toList());
          if (commentIds.isEmpty()) {
            return Flux.empty();
          }
          return repository
            .findAllByCommentIdInAndCreatedAtGreaterThan(commentIds, timestamp)
            .filter(e ->
              subscription
                .getCommentSubscriptions()
                .stream()
                .anyMatch(s -> s.hasSubscribed(e) != SubscriptionResult.NONE)
            );
        }
        return repository
          .findAllByRoomIdAndCreatedAtGreaterThan(
            subscription.getRoomId(),
            timestamp
          )
          .filter(e ->
            subscription.hasSubscribed(e) != SubscriptionResult.NONE ||
            subscription
              .getCommentSubscriptions()
              .stream()
              .anyMatch(s -> s.hasSubscribed(e) != SubscriptionResult.NONE)
          );
      });
  }
}
