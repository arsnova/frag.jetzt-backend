package de.thm.arsnova.frag.jetzt.paypal.config;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayPalAllowedUnit {
    private String minPrice;
    private String maxPrice;
    private String currencyCode;
}
