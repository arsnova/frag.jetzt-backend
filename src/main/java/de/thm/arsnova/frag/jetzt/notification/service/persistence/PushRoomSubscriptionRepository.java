package de.thm.arsnova.frag.jetzt.notification.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushRoomSubscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PushRoomSubscriptionRepository extends ReactiveCrudRepository<PushRoomSubscription, UUID> {
    Flux<PushRoomSubscription> findAllByRoomId(UUID roomId);

    Flux<PushRoomSubscription> findAllByAccountId(UUID accountId);

    Mono<PushRoomSubscription> findByRoomIdAndAccountId(UUID roomId, UUID accountId);

    Mono<Void> deleteByRoomIdAndAccountId(UUID roomId, UUID accountId);
}
