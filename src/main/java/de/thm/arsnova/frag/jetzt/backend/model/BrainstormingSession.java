package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Table
public class BrainstormingSession implements Persistable<UUID> {

    public static class BrainstormingWordWithMeta {
        private BrainstormingWord word;
        private Boolean ownHasUpvoted;

        public BrainstormingWordWithMeta() {
        }

        public BrainstormingWordWithMeta(BrainstormingWord word, Boolean ownHasUpvoted) {
            this.word = word;
            this.ownHasUpvoted = ownHasUpvoted;
        }

        public BrainstormingWord getWord() {
            return word;
        }

        public void setWord(BrainstormingWord word) {
            this.word = word;
        }

        public Boolean getOwnHasUpvoted() {
            return ownHasUpvoted;
        }

        public void setOwnHasUpvoted(Boolean ownHasUpvoted) {
            this.ownHasUpvoted = ownHasUpvoted;
        }

        @Override
        public String toString() {
            return "BrainstormingWordWithMeta{" +
                    "word=" + word +
                    ", ownHasUpvoted=" + ownHasUpvoted +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BrainstormingWordWithMeta that = (BrainstormingWordWithMeta) o;
            return Objects.equals(word, that.word) && Objects.equals(ownHasUpvoted, that.ownHasUpvoted);
        }

        @Override
        public int hashCode() {
            return Objects.hash(word, ownHasUpvoted);
        }
    }

    @Id
    private UUID id;
    private UUID roomId;
    private String title;
    private boolean active;
    private int maxWordLength;
    private int maxWordCount;
    private String language;
    private boolean ratingAllowed;
    private boolean ideasFrozen;
    private Integer ideasTimeDuration;
    private Timestamp ideasEndTimestamp;
    @Transient
    private Map<UUID, BrainstormingWordWithMeta> wordsWithMeta;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public int getMaxWordLength() {
        return maxWordLength;
    }

    public void setMaxWordLength(int maxWordLength) {
        this.maxWordLength = maxWordLength;
    }

    public int getMaxWordCount() {
        return maxWordCount;
    }

    public void setMaxWordCount(int maxWordCount) {
        this.maxWordCount = maxWordCount;
    }

    public Map<UUID, BrainstormingWordWithMeta> getWordsWithMeta() {
        return wordsWithMeta;
    }

    public void setWordsWithMeta(Map<UUID, BrainstormingWordWithMeta> wordsWithMeta) {
        this.wordsWithMeta = wordsWithMeta;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }


    public boolean isRatingAllowed() {
        return ratingAllowed;
    }

    public void setRatingAllowed(boolean ratingAllowed) {
        this.ratingAllowed = ratingAllowed;
    }

    public boolean isIdeasFrozen() {
        return ideasFrozen;
    }

    public void setIdeasFrozen(boolean ideasFrozen) {
        this.ideasFrozen = ideasFrozen;
    }

    public Timestamp getIdeasEndTimestamp() {
        return ideasEndTimestamp;
    }

    public void setIdeasEndTimestamp(Timestamp ideasEndTimestamp) {
        this.ideasEndTimestamp = ideasEndTimestamp;
    }

    public Integer getIdeasTimeDuration() {
        return ideasTimeDuration;
    }

    public void setIdeasTimeDuration(Integer ideasTimeDuration) {
        this.ideasTimeDuration = ideasTimeDuration;
    }

    @Override
    public String toString() {
        return "BrainstormingSession{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", title='" + title + '\'' +
                ", language='" + language + '\'' +
                ", active=" + active +
                ", ideasFrozen=" + ideasFrozen +
                ", ideasEndTimestamp=" + ideasEndTimestamp +
                ", ideasTimeDuration=" + ideasTimeDuration +
                ", ratingAllowed=" + ratingAllowed +
                ", maxWordLength=" + maxWordLength +
                ", maxWordCount=" + maxWordCount +
                ", wordsWithMeta=" + wordsWithMeta +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingSession session = (BrainstormingSession) o;
        return active == session.active &&
                ideasFrozen == session.ideasFrozen &&
                ratingAllowed == session.ratingAllowed &&
                Objects.equals(ideasEndTimestamp, session.ideasEndTimestamp) &&
                maxWordLength == session.maxWordLength &&
                maxWordCount == session.maxWordCount &&
                Objects.equals(ideasTimeDuration, session.ideasTimeDuration) &&
                Objects.equals(language, session.language) &&
                Objects.equals(id, session.id) &&
                Objects.equals(roomId, session.roomId) &&
                Objects.equals(title, session.title) &&
                Objects.equals(wordsWithMeta, session.wordsWithMeta) &&
                Objects.equals(createdAt, session.createdAt) &&
                Objects.equals(updatedAt, session.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, title, active, maxWordLength, maxWordCount, wordsWithMeta, createdAt, updatedAt, ideasTimeDuration, language, ratingAllowed, ideasFrozen, ideasEndTimestamp);
    }
}
