package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class CreateBrainstormingWordPayload implements WebSocketPayload {

    private String name;
    private String realName;
    private UUID sessionId;

    public CreateBrainstormingWordPayload() {

    }

    public CreateBrainstormingWordPayload(String name, UUID sessionId, String realName) {
        this.name = name;
        this.sessionId = sessionId;
        this.realName = realName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "CreateBrainstormingWordPayload{" +
                "name='" + name + '\'' +
                ", realName='" + realName + '\'' +
                ", sessionId=" + sessionId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstormingWordPayload that = (CreateBrainstormingWordPayload) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(realName, that.realName) &&
                Objects.equals(sessionId, that.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, realName, sessionId);
    }
}
