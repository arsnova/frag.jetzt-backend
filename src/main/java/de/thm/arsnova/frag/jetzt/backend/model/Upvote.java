package de.thm.arsnova.frag.jetzt.backend.model;

import java.util.UUID;

public class Upvote extends AbstractVote {

    public Upvote(final UUID accountId, final UUID commentId) {
        super(accountId, commentId);
    }
}
