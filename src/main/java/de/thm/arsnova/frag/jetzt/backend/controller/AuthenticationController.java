package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.security.JwtUtil;
import de.thm.arsnova.frag.jetzt.backend.security.SecurityContextRepository;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import java.util.Collections;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController("AuthenticationController")
@RequestMapping(AuthenticationController.REQUEST_MAPPING)
public class AuthenticationController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    AuthenticationController.class
  );

  protected static final String REQUEST_MAPPING = "/auth";
  private static final String LOGIN_MAPPING = "/login";

  private final SecurityContextRepository securityContextRepository;
  private final AccountService accountService;
  private final RoomAccessService roomAccessService;
  private final JwtUtil jwtUtil;

  @Autowired
  public AuthenticationController(
    SecurityContextRepository securityContextRepository,
    AccountService accountService,
    RoomAccessService roomAccessService,
    JwtUtil jwtUtil
  ) {
    this.securityContextRepository = securityContextRepository;
    this.accountService = accountService;
    this.roomAccessService = roomAccessService;
    this.jwtUtil = jwtUtil;
  }

  @PostMapping(LOGIN_MAPPING)
  public Mono<AuthenticatedUser> login(
    @RequestParam(defaultValue = "false") final boolean refresh
  ) {
    return securityContextRepository
      .getAuthenticatedUser(refresh)
      .switchIfEmpty(Mono.error(new UnauthorizedException("Access denied")))
      .flatMap(user ->
        accountService.refreshLogin(user.getAccountId(), Mono.just(user))
      );
  }

  @PostMapping(LOGIN_MAPPING + "/registered/{keycloakId}")
  public Mono<AuthenticatedUser> loginRegistered(
    @PathVariable("keycloakId") final UUID id,
    @RequestBody final String token
  ) {
    return accountService
      .getOrCreateByKeycloakToken(id, token)
      .flatMap(account ->
        roomAccessService
          .getByAccountId(account.getId())
          .collectList()
          .map(roomAccesses ->
            new AuthenticatedUser(
              jwtUtil.generateToken(account.getId(), "registered"),
              account.getId().toString(),
              "registered",
              account.getEmail(),
              roomAccesses,
              Collections.emptyList(),
              account
            )
          )
          .flatMap(user ->
            accountService.refreshLogin(user.getAccountId(), Mono.just(user))
          )
      );
  }

  @PostMapping(LOGIN_MAPPING + "/guest")
  public Mono<AuthenticatedUser> postLoginGuest() {
    return accountService
      .createGuest()
      .map(account ->
        new AuthenticatedUser(
          jwtUtil.generateToken(account.getId(), "guest"),
          account.getId().toString(),
          "guest",
          account.getEmail(),
          // emptyList as RoomAccesses, because a new User doesn´t have them
          Collections.emptyList(),
          Collections.emptyList(),
          account
        )
      );
  }
}
