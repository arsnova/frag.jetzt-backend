package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTAPISetting;
import reactor.core.publisher.Flux;

@Repository
public interface GPTAPISettingRepository extends ReactiveCrudRepository<GPTAPISetting, UUID> {
    
    Flux<GPTAPISetting> findAllByAccountId(UUID accountId);

}
