package de.thm.arsnova.frag.jetzt.backend.model.command;

public class ResetBrainstormingCategorization extends WebSocketCommand<ResetBrainstormingCategorizationPayload> {

    public ResetBrainstormingCategorization() {
        super(ResetBrainstormingCategorization.class.getSimpleName());
    }

    public ResetBrainstormingCategorization(ResetBrainstormingCategorizationPayload payload) {
        super(ResetBrainstormingCategorization.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResetBrainstormingCategorization that = (ResetBrainstormingCategorization) o;
        return this.getPayload().equals(that.getPayload());
    }
}
