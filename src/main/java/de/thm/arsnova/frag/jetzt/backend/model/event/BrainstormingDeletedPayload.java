package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class BrainstormingDeletedPayload implements WebSocketPayload {

    private UUID id;

    public BrainstormingDeletedPayload() {

    }

    public BrainstormingDeletedPayload(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BrainstormingDeletedPayload{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingDeletedPayload that = (BrainstormingDeletedPayload) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
