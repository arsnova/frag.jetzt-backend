package de.thm.arsnova.frag.jetzt.backend.model;

import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.lang.NonNull;

public class Room implements Persistable<UUID> {

  public enum ProfanityFilter {
    ALL,
    LANGUAGE_SPECIFIC,
    PARTIAL_WORDS,
    NONE,
    DEACTIVATED,
  }

  public enum RoomMode {
    ARS,
    PLE,
  }

  @Id
  private UUID id;

  @NonNull
  private UUID ownerId;

  @NonNull
  private String shortId;

  @NonNull
  private String name;

  private String description;
  private boolean closed;
  private boolean directSend;
  private int threshold;
  private String blacklist;
  private boolean blacklistActive;
  private ProfanityFilter profanityFilter;
  private boolean questionsBlocked;
  private String tagCloudSettings;
  private UUID moderatorRoomReference;
  private int conversationDepth;
  private boolean bonusArchiveActive;
  private boolean quizActive;
  private boolean brainstormingActive;
  private String language;
  private boolean livepollActive;
  private boolean keywordExtractionActive;
  private boolean radarActive;
  private boolean focusActive;
  private boolean chatGptActive;
  private RoomMode mode;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;
  private Timestamp lastVisitCreator;

  @Transient
  private String[] tags;

  @Transient
  private BrainstormingSession brainstormingSession;

  @Transient
  private LivepollSession livepollSession;

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(UUID ownerId) {
    this.ownerId = ownerId;
  }

  public String getShortId() {
    return shortId;
  }

  public void setShortId(String shortId) {
    this.shortId = shortId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isClosed() {
    return closed;
  }

  public void setClosed(boolean closed) {
    this.closed = closed;
  }

  public boolean isDirectSend() {
    return directSend;
  }

  public void setDirectSend(boolean directSend) {
    this.directSend = directSend;
  }

  public int getThreshold() {
    return threshold;
  }

  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }

  public String getBlacklist() {
    return blacklist;
  }

  public void setBlacklist(String blacklist) {
    this.blacklist = blacklist;
  }

  public ProfanityFilter getProfanityFilter() {
    return profanityFilter;
  }

  public void setProfanityFilter(ProfanityFilter profanityFilter) {
    this.profanityFilter = profanityFilter;
  }

  public boolean getQuestionsBlocked() {
    return questionsBlocked;
  }

  public void setQuestionsBlocked(boolean questionsBlocked) {
    this.questionsBlocked = questionsBlocked;
  }

  public boolean isQuestionsBlocked() {
    return questionsBlocked;
  }

  public String[] getTags() {
    return tags;
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  public BrainstormingSession getBrainstormingSession() {
    return brainstormingSession;
  }

  public void setBrainstormingSession(
    BrainstormingSession brainstormingSession
  ) {
    this.brainstormingSession = brainstormingSession;
  }

  public String getTagCloudSettings() {
    return tagCloudSettings;
  }

  public void setTagCloudSettings(String tagCloudSettings) {
    this.tagCloudSettings = tagCloudSettings;
  }

  public UUID getModeratorRoomReference() {
    return moderatorRoomReference;
  }

  public void setModeratorRoomReference(UUID moderatorRoomReference) {
    this.moderatorRoomReference = moderatorRoomReference;
  }

  public int getConversationDepth() {
    return conversationDepth;
  }

  public void setConversationDepth(int conversationDepth) {
    this.conversationDepth = conversationDepth;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Timestamp getLastVisitCreator() {
    return lastVisitCreator;
  }

  public void refreshLastVisitCreator() {
    lastVisitCreator = Timestamp.from(Instant.now());
  }

  public boolean isBlacklistActive() {
    return blacklistActive;
  }

  public void setBlacklistActive(boolean blacklistActive) {
    this.blacklistActive = blacklistActive;
  }

  public boolean isBonusArchiveActive() {
    return bonusArchiveActive;
  }

  public void setBonusArchiveActive(boolean bonusArchiveActive) {
    this.bonusArchiveActive = bonusArchiveActive;
  }

  public boolean isQuizActive() {
    return quizActive;
  }

  public void setQuizActive(boolean quizActive) {
    this.quizActive = quizActive;
  }

  public boolean isBrainstormingActive() {
    return brainstormingActive;
  }

  public void setBrainstormingActive(boolean brainstormingActive) {
    this.brainstormingActive = brainstormingActive;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public boolean isLivepollActive() {
    return livepollActive;
  }

  public void setLivepollActive(boolean livepollActive) {
    this.livepollActive = livepollActive;
  }

  public LivepollSession getLivepollSession() {
    return livepollSession;
  }

  public void setLivepollSession(LivepollSession livepollSession) {
    this.livepollSession = livepollSession;
  }

  public boolean isKeywordExtractionActive() {
    return keywordExtractionActive;
  }

  public void setKeywordExtractionActive(boolean keywordExtractionActive) {
    this.keywordExtractionActive = keywordExtractionActive;
  }

  public boolean isRadarActive() {
    return radarActive;
  }

  public void setRadarActive(boolean radarActive) {
    this.radarActive = radarActive;
  }

  public boolean isFocusActive() {
    return focusActive;
  }

  public void setFocusActive(boolean focusActive) {
    this.focusActive = focusActive;
  }

  public boolean isChatGptActive() {
    return chatGptActive;
  }

  public void setChatGptActive(boolean chatGptActive) {
    this.chatGptActive = chatGptActive;
  }

  public RoomMode getMode() {
    return mode;
  }

  public void setMode(RoomMode mode) {
    this.mode = mode;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
    result = prime * result + ((shortId == null) ? 0 : shortId.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result =
      prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + (closed ? 1231 : 1237);
    result = prime * result + (directSend ? 1231 : 1237);
    result = prime * result + threshold;
    result = prime * result + ((blacklist == null) ? 0 : blacklist.hashCode());
    result = prime * result + (blacklistActive ? 1231 : 1237);
    result =
      prime *
      result +
      ((profanityFilter == null) ? 0 : profanityFilter.hashCode());
    result = prime * result + (questionsBlocked ? 1231 : 1237);
    result =
      prime *
      result +
      ((tagCloudSettings == null) ? 0 : tagCloudSettings.hashCode());
    result =
      prime *
      result +
      (
        (moderatorRoomReference == null) ? 0 : moderatorRoomReference.hashCode()
      );
    result = prime * result + conversationDepth;
    result = prime * result + (bonusArchiveActive ? 1231 : 1237);
    result = prime * result + (quizActive ? 1231 : 1237);
    result = prime * result + (brainstormingActive ? 1231 : 1237);
    result = prime * result + ((language == null) ? 0 : language.hashCode());
    result = prime * result + (livepollActive ? 1231 : 1237);
    result = prime * result + (keywordExtractionActive ? 1231 : 1237);
    result = prime * result + (radarActive ? 1231 : 1237);
    result = prime * result + (focusActive ? 1231 : 1237);
    result = prime * result + (chatGptActive ? 1231 : 1237);
    result = prime * result + ((mode == null) ? 0 : mode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Room other = (Room) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (ownerId == null) {
      if (other.ownerId != null) return false;
    } else if (!ownerId.equals(other.ownerId)) return false;
    if (shortId == null) {
      if (other.shortId != null) return false;
    } else if (!shortId.equals(other.shortId)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (closed != other.closed) return false;
    if (directSend != other.directSend) return false;
    if (threshold != other.threshold) return false;
    if (blacklist == null) {
      if (other.blacklist != null) return false;
    } else if (!blacklist.equals(other.blacklist)) return false;
    if (blacklistActive != other.blacklistActive) return false;
    if (profanityFilter != other.profanityFilter) return false;
    if (questionsBlocked != other.questionsBlocked) return false;
    if (tagCloudSettings == null) {
      if (other.tagCloudSettings != null) return false;
    } else if (!tagCloudSettings.equals(other.tagCloudSettings)) return false;
    if (moderatorRoomReference == null) {
      if (other.moderatorRoomReference != null) return false;
    } else if (
      !moderatorRoomReference.equals(other.moderatorRoomReference)
    ) return false;
    if (conversationDepth != other.conversationDepth) return false;
    if (bonusArchiveActive != other.bonusArchiveActive) return false;
    if (quizActive != other.quizActive) return false;
    if (brainstormingActive != other.brainstormingActive) return false;
    if (language == null) {
      if (other.language != null) return false;
    } else if (!language.equals(other.language)) return false;
    if (livepollActive != other.livepollActive) return false;
    if (keywordExtractionActive != other.keywordExtractionActive) return false;
    if (radarActive != other.radarActive) return false;
    if (focusActive != other.focusActive) return false;
    if (chatGptActive != other.chatGptActive) return false;
    if (mode != other.mode) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "Room [id=" +
      id +
      ", ownerId=" +
      ownerId +
      ", shortId=" +
      shortId +
      ", name=" +
      name +
      ", description=" +
      description +
      ", closed=" +
      closed +
      ", directSend=" +
      directSend +
      ", threshold=" +
      threshold +
      ", blacklist=" +
      blacklist +
      ", blacklistActive=" +
      blacklistActive +
      ", profanityFilter=" +
      profanityFilter +
      ", questionsBlocked=" +
      questionsBlocked +
      ", tagCloudSettings=" +
      tagCloudSettings +
      ", moderatorRoomReference=" +
      moderatorRoomReference +
      ", conversationDepth=" +
      conversationDepth +
      ", bonusArchiveActive=" +
      bonusArchiveActive +
      ", quizActive=" +
      quizActive +
      ", brainstormingActive=" +
      brainstormingActive +
      ", language=" +
      language +
      ", livepollActive=" +
      livepollActive +
      ", keywordExtractionActive=" +
      keywordExtractionActive +
      ", radarActive=" +
      radarActive +
      ", focusActive=" +
      focusActive +
      ", chatGptActive=" +
      chatGptActive +
      ", mode=" +
      mode +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", lastVisitCreator=" +
      lastVisitCreator +
      ", tags=" +
      Arrays.toString(tags) +
      ", brainstormingSession=" +
      brainstormingSession +
      ", livepollSession=" +
      livepollSession +
      "]"
    );
  }
}
