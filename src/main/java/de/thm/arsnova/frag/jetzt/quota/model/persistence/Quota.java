package de.thm.arsnova.frag.jetzt.quota.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class Quota implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private boolean disabled;

  @NonNull
  private String timezone;

  @NonNull
  private long maxRequest;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private List<QuotaEntry> entries;

  @Transient
  protected List<QuotaAccessTime> accessTimes;

  public Quota() {}

  public Quota(boolean disabled, String timezone) {
    this.disabled = disabled;
    this.timezone = timezone;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public boolean isDisabled() {
    return disabled;
  }

  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  public long getMaxRequest() {
    return maxRequest;
  }

  public void setMaxRequest(long maxRequest) {
    this.maxRequest = maxRequest;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<QuotaEntry> getEntries() {
    return entries;
  }

  public void setEntries(List<QuotaEntry> entries) {
    this.entries = entries;
  }

  public List<QuotaAccessTime> getAccessTimes() {
    return accessTimes;
  }

  public void setAccessTimes(List<QuotaAccessTime> accessTimes) {
    this.accessTimes = accessTimes;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + (disabled ? 1231 : 1237);
    result = prime * result + ((timezone == null) ? 0 : timezone.hashCode());
    result = prime * result + (int) (maxRequest ^ (maxRequest >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Quota other = (Quota) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (disabled != other.disabled) return false;
    if (timezone == null) {
      if (other.timezone != null) return false;
    } else if (!timezone.equals(other.timezone)) return false;
    if (maxRequest != other.maxRequest) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "Quota [id=" +
      id +
      ", disabled=" +
      disabled +
      ", timezone=" +
      timezone +
      ", maxRequest=" +
      maxRequest +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", entries=" +
      entries +
      ", accessTimes=" +
      accessTimes +
      "]"
    );
  }
}
