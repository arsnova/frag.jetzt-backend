package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.ImportComment;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.List;

public class ImportCommentsPayload implements WebSocketPayload {

    private List<ImportComment> comments;

    public ImportCommentsPayload(List<ImportComment> comments) {
        this.comments = comments;
    }

    public List<ImportComment> getComments() {
        return comments;
    }
}
