package de.thm.arsnova.frag.jetzt.openai.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ErrorHandler {

  public static <T> Mono<T> handleError(
    Class<T> success,
    String message,
    ClientResponse response
  ) {
    if (response.statusCode().is2xxSuccessful()) {
      if (success == Void.class) {
        return Mono.empty();
      }
      return response.bodyToMono(success);
    }
    return response
      .bodyToMono(String.class)
      .flatMap(body ->
        Mono.error(
          new ResponseStatusException(
            HttpStatus.FAILED_DEPENDENCY,
            message + "\n" + response.statusCode() + "\n" + body
          )
        )
      );
  }

  public static <T> Flux<T> handleFluxError(
    Class<T> success,
    String message,
    ClientResponse response
  ) {
    if (response.statusCode().is2xxSuccessful()) {
      if (success == Void.class) {
        return Flux.empty();
      }
      return response.bodyToFlux(success);
    }
    return response
      .bodyToMono(String.class)
      .flatMapMany(body ->
        Flux.error(
          new ResponseStatusException(
            HttpStatus.FAILED_DEPENDENCY,
            message + "\n" + response.statusCode() + "\n" + body
          )
        )
      );
  }
}
