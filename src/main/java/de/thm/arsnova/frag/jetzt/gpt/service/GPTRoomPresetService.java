package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomPreset;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomPresetTopic;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomPresetTopicRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomSettingRepository;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTRoomPresetService {

  private final GPTRoomPresetTopicRepository presetTopicRepository;
  private final GPTRoomSettingRepository settingRepository;
  private final AuthorizationHelper helper;
  private final RoomRepository roomRepository;

  @Autowired
  public GPTRoomPresetService(
    final AuthorizationHelper helper,
    final RoomRepository roomRepository,
    final GPTRoomPresetTopicRepository presetTopicRepository,
    final GPTRoomSettingRepository settingRepository
  ) {
    this.helper = helper;
    this.roomRepository = roomRepository;
    this.presetTopicRepository = presetTopicRepository;
    this.settingRepository = settingRepository;
  }

  public Mono<GPTRoomPreset> getPreset(UUID roomId) {
    return this.settingRepository.findByRoomId(roomId)
      .switchIfEmpty(
        Mono.error(new NotFoundException("Setting does not exist"))
      )
      .flatMap(setting ->
        Mono.zip(
          Mono.just(setting),
          presetTopicRepository
            .findAllBySettingId(setting.getId())
            .collectList()
        )
      )
      .map(tuple -> {
        return new GPTRoomPreset(tuple.getT1(), tuple.getT2());
      });
  }

  public Mono<GPTRoomPreset> setPreset(
    UUID roomId,
    Map<String, Object> changes
  ) {
    return Mono
      .zip(
        roomRepository.findById(roomId),
        settingRepository.findByRoomId(roomId),
        helper.getCurrentUser()
      )
      .switchIfEmpty(
        Mono.error(new NotFoundException("Settings or Room not present"))
      )
      .filter(tuple ->
        helper.checkModeratorOrCreatorOfRoom(tuple.getT3(), tuple.getT1())
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("You are not creator or moderator."))
      )
      .filter(tuple ->
        !helper.checkOnlyModeratorOfRoom(tuple.getT3(), tuple.getT1()) ||
        GPTRoomSettingRight.CHANGE_ROOM_PRESETS.isRightSet(
          tuple.getT2().getRightsBitset()
        )
      )
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException(
            "Moderators are not allowed to change presets."
          )
        )
      )
      .flatMap(tuple -> {
        GPTRoomSetting setting = tuple.getT2();
        boolean settingChanged = false;
        if (changes.containsKey("context")) {
          setting.setPresetContext((String) changes.get("context"));
          settingChanged = true;
        }
        if (changes.containsKey("length")) {
          setting.setPresetLength((String) changes.get("length"));
          settingChanged = true;
        }
        if (changes.containsKey("roleInstruction")) {
          String s = (String) changes.get("roleInstruction");
          if (s != null && s.length() > 2000) {
            throw new BadRequestException(
              "Role Instruction may be at most 2000 characters!"
            );
          }
          setting.setRoleInstruction(s);
          settingChanged = true;
        }
        Mono<?> mono = Mono.just(true);
        if (changes.containsKey("topics")) {
          List<GPTRoomPresetTopic> data =
            ((List<?>) changes.get("topics")).stream()
              .map(e -> {
                Map<?, ?> element = (Map<?, ?>) e;
                return new GPTRoomPresetTopic(
                  setting.getId(),
                  (String) element.get("description"),
                  (Boolean) element.get("active")
                );
              })
              .collect(Collectors.toList());
          mono =
            mono
              .flatMap(ignore ->
                presetTopicRepository
                  .deleteAllBySettingId(setting.getId())
                  .then(Mono.just(true))
              )
              .flatMap(ignore ->
                presetTopicRepository.saveAll(data).then(Mono.just(true))
              );
        }
        if (settingChanged) {
          mono =
            mono.flatMap(ignore ->
              settingRepository.save(setting).then(Mono.just(true))
            );
        }
        return mono.flatMap(ignore -> getPreset(roomId));
      });
  }
}
