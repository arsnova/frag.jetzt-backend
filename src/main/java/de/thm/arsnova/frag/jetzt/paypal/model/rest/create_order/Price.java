package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Data
@Jacksonized
@SuperBuilder
@AllArgsConstructor
public class Price {

  // required, 3 characters, https://developer.paypal.com/api/rest/reference/currency-codes/
  @JsonProperty("currency_code")
  private final String currencyCode;

  // required, 1-32 characters
  private final String value;
}
