package de.thm.arsnova.frag.jetzt.backend.model;

public class RatingResult {
    private float rating;
    private float fiveStarPercent;
    private float fourStarPercent;
    private float threeStarPercent;
    private float twoStarPercent;
    private float oneStarPercent;
    private long people;

    public RatingResult() {

    }

    public RatingResult(
            float rating,
            float fiveStarPercent,
            float fourStarPercent,
            float threeStarPercent,
            float twoStarPercent,
            float oneStarPercent,
            long people
    ) {
        this.rating = rating;
        this.fiveStarPercent = fiveStarPercent;
        this.fourStarPercent = fourStarPercent;
        this.threeStarPercent = threeStarPercent;
        this.twoStarPercent = twoStarPercent;
        this.oneStarPercent = oneStarPercent;
        this.people = people;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getFiveStarPercent() {
        return fiveStarPercent;
    }

    public void setFiveStarPercent(float fiveStarPercent) {
        this.fiveStarPercent = fiveStarPercent;
    }

    public float getFourStarPercent() {
        return fourStarPercent;
    }

    public void setFourStarPercent(float fourStarPercent) {
        this.fourStarPercent = fourStarPercent;
    }

    public float getThreeStarPercent() {
        return threeStarPercent;
    }

    public void setThreeStarPercent(float threeStarPercent) {
        this.threeStarPercent = threeStarPercent;
    }

    public float getTwoStarPercent() {
        return twoStarPercent;
    }

    public void setTwoStarPercent(float twoStarPercent) {
        this.twoStarPercent = twoStarPercent;
    }

    public float getOneStarPercent() {
        return oneStarPercent;
    }

    public void setOneStarPercent(float oneStarPercent) {
        this.oneStarPercent = oneStarPercent;
    }

    public long getPeople() {
        return people;
    }

    public void setPeople(long people) {
        this.people = people;
    }
}
