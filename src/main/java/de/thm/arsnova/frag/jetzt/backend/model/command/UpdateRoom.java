package de.thm.arsnova.frag.jetzt.backend.model.command;

public class UpdateRoom extends WebSocketCommand<UpdateRoomPayload> {

    public UpdateRoom() {
        super(UpdateRoom.class.getSimpleName());
    }

    public UpdateRoom(UpdateRoomPayload payload) {
        super(UpdateRoom.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateRoom that = (UpdateRoom) o;
        return this.getPayload().equals(that.getPayload());
    }
}
