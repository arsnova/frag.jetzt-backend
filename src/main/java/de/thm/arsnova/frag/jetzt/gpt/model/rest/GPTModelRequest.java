package de.thm.arsnova.frag.jetzt.gpt.model.rest;

public class GPTModelRequest {

  private String apiKey;
  private String apiOrg;

  public GPTModelRequest() {}

  public GPTModelRequest(String apiKey, String apiOrg) {
    this.apiKey = apiKey;
    this.apiOrg = apiOrg;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getApiOrg() {
    return apiOrg;
  }

  public void setApiOrg(String apiOrg) {
    this.apiOrg = apiOrg;
  }
}
