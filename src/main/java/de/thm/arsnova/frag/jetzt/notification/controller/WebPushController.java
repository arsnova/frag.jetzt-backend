package de.thm.arsnova.frag.jetzt.notification.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.WebSubscription;
import de.thm.arsnova.frag.jetzt.notification.service.WebPushService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("WebPushController")
@RequestMapping(WebPushController.REQUEST_MAPPING)
public class WebPushController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/webpush";
  private static final String PUBLIC_KEY_MAPPING = "/public-key";
  private static final String SUBSCRIBE_MAPPING = "/subscription";

  private final WebPushService service;

  @Autowired
  public WebPushController(WebPushService service) {
    this.service = service;
  }

  @GetMapping(PUBLIC_KEY_MAPPING)
  public String getPublicKey() {
    return service.getPublicVapidKey();
  }

  @GetMapping(SUBSCRIBE_MAPPING)
  public Flux<WebSubscription> getSubscriptions() {
    return service.getSubscriptions();
  }

  @PostMapping(SUBSCRIBE_MAPPING)
  public Mono<WebSubscription> subscribe(
    @RequestBody WebSubscription subscription
  ) {
    return service.createSubscription(subscription);
  }

  @DeleteMapping(SUBSCRIBE_MAPPING + DELETE_MAPPING)
  public Mono<Void> unsubscribe(@PathVariable("id") UUID id) {
    return service.deleteSubscription(id);
  }
}
