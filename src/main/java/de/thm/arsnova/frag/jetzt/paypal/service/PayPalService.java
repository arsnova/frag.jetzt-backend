package de.thm.arsnova.frag.jetzt.paypal.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.openai.service.ErrorHandler;
import de.thm.arsnova.frag.jetzt.paypal.config.PayPalAllowedUnit;
import de.thm.arsnova.frag.jetzt.paypal.config.PayPalConfig;
import de.thm.arsnova.frag.jetzt.paypal.model.Payment;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.CreateOrder;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.OAuthToken;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Item;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Item.Category;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Payee;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Price;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.PurchaseUnit;
import de.thm.arsnova.frag.jetzt.paypal.service.persistence.PaymentRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class PayPalService {

  private final Logger logger = LoggerFactory.getLogger(PayPalService.class);
  private final WebClient restWebClient;
  private final String basicAuthString;
  private String token = null;
  private Instant tokenExpiration = Instant.now();
  private final List<AllowedUnit> allowedUnits;
  private final Map<String, String> productName;
  private final String merchantId;
  private final AuthorizationHelper helper;
  private final PaymentRepository repository;

  private static class AllowedUnit {

    private BigDecimal min;
    private BigDecimal max;
    private String currency;

    AllowedUnit(PayPalAllowedUnit unit) {
      this.min = new BigDecimal(unit.getMinPrice());
      this.max = new BigDecimal(unit.getMaxPrice());
      this.currency = unit.getCurrencyCode().trim();
      if (this.min.compareTo(BigDecimal.ZERO) <= 0) {
        throw new IllegalArgumentException("Min price must be greater than 0");
      }
      if (this.max.compareTo(this.min) < 0) {
        throw new IllegalArgumentException(
          "Max price must be greater than min price"
        );
      }
      if (this.currency == null || this.currency.length() != 3) {
        throw new IllegalArgumentException("Currency must be a 3-letter code");
      }
    }

    public boolean isInRange(BigDecimal price) {
      return price.compareTo(min) >= 0 && price.compareTo(max) <= 0;
    }
  }

  @Data
  @AllArgsConstructor
  public static class CapturedResult {

    private String amount;
  }

  public PayPalService(
    PayPalConfig config,
    AuthorizationHelper helper,
    PaymentRepository repository
  ) {
    this.restWebClient = WebClient.create(config.getApiUrl());
    this.basicAuthString =
      "Basic " +
      Base64
        .getEncoder()
        .encodeToString(
          (config.getClientId() + ":" + config.getClientSecret()).getBytes()
        );
    this.allowedUnits =
      config
        .getAllowedUnits()
        .stream()
        .map(AllowedUnit::new)
        .collect(Collectors.toList());
    this.productName = config.getProductName();
    this.merchantId = config.getMerchantId();
    this.helper = helper;
    this.repository = repository;
  }

  public Mono<CapturedResult> getCapturedQuota() {
    return helper
      .getCurrentUser()
      .flatMap(user -> repository.sumAmountByAccountId(user.getAccountId()))
      .map(e -> new CapturedResult(e.toString()))
      .switchIfEmpty(Mono.just(new CapturedResult("0")));
  }

  public Mono<Map> captureOrder(String orderId) {
    return Mono
      .zip(getPayPalToken(), helper.getCurrentUser())
      .flatMap(zipped -> {
        return restWebClient
          .post()
          .uri("/v2/checkout/orders/" + orderId + "/capture")
          .header("Authorization", "Bearer " + zipped.getT1())
          .header("Content-Type", "application/json")
          .exchangeToMono(response ->
            ErrorHandler.handleError(
              Map.class,
              "Capture Order failed",
              response
            )
          )
          .doOnSuccess(e ->
            this.addFromCapture(orderId, zipped.getT2().getAccountId(), e)
          );
      });
  }

  public Mono<Object> createOrder(
    String amount,
    String currency,
    String language
  ) {
    BigDecimal total = new BigDecimal(amount);
    boolean valid = false;
    for (AllowedUnit u : allowedUnits) {
      if (!u.currency.equals(currency)) {
        continue;
      }
      if (u.isInRange(total)) {
        valid = true;
        break;
      }
      throw new IllegalArgumentException(
        "Amount not in allowed range [" + u.min + ", " + u.max + "]"
      );
    }
    if (!valid) {
      throw new IllegalArgumentException("Currency not allowed");
    }
    // Create order
    CreateOrder order = generateOrder(amount, currency, language);

    return getPayPalToken()
      .flatMap(token -> {
        return restWebClient
          .post()
          .uri("/v2/checkout/orders")
          .header("Authorization", "Bearer " + token)
          .header("Content-Type", "application/json")
          .bodyValue(order)
          .exchangeToMono(response ->
            ErrorHandler.handleError(Map.class, "Create Order failed", response)
          );
      });
  }

  private CreateOrder generateOrder(
    String amount,
    String currency,
    String language
  ) {
    String productName = this.productName.get(language);
    if (productName == null) {
      productName = this.productName.get("en");
    }
    CreateOrder order = new CreateOrder();
    order.setPurchaseUnits(
      List.of(
        PurchaseUnit
          .builder()
          .items(
            List.of(
              Item
                .builder()
                .category(Category.DIGITAL_GOODS)
                .name(productName)
                .quantity("1")
                .unitAmount(
                  Price.builder().currencyCode(currency).value(amount).build()
                )
                .build()
            )
          )
          .payee(new Payee(null, merchantId))
          .build()
      )
    );
    order.generateAmount();
    return order;
  }

  private Mono<String> getPayPalToken() {
    if (Instant.now().isBefore(tokenExpiration)) {
      return Mono.just(token);
    }
    return restWebClient
      .post()
      .uri("/v1/oauth2/token")
      .header("Authorization", this.basicAuthString)
      .header("Content-Type", "application/x-www-form-urlencoded")
      .body(BodyInserters.fromFormData("grant_type", "client_credentials"))
      .retrieve()
      .bodyToMono(OAuthToken.class)
      .map(e -> {
        this.token = e.getAccessToken();
        this.tokenExpiration = Instant.now().plusSeconds(e.getExpiresIn() - 60); // 60 seconds before expiration
        return this.token;
      });
  }

  private void addFromCapture(String orderId, UUID accountId, Map<?, ?> e) {
    final String oId = e.get("id").toString();
    if (!orderId.equals(oId)) {
      logger.error("Order ID mismatch: {} != {}", orderId, oId);
      return;
    }
    List<Map<String, ?>> units = (List<Map<String, ?>>) e.get("purchase_units");
    // iterate over all purchase units
    String currency = null;
    BigDecimal summed = BigDecimal.ZERO;
    for (Map<String, ?> unit : units) {
      List<Map<String, ?>> captures = (List<Map<String, ?>>) (
        (Map<String, ?>) unit.get("payments")
      ).get("captures");
      for (Map<String, ?> capture : captures) {
        Map<String, ?> amount = (Map<String, ?>) capture.get("amount");
        final String c = amount.get("currency_code").toString();
        if (currency == null) {
          currency = c;
        } else if (!currency.equals(c)) {
          logger.error("Currency code mismatch!");
          continue;
        }
        summed = summed.add(new BigDecimal(amount.get("value").toString()));
      }
    }
    Payment p = Payment
      .builder()
      .orderId(orderId)
      .accountId(accountId)
      .amount(summed)
      .currency(currency)
      .build();
    repository.save(p).subscribe();
  }
}
