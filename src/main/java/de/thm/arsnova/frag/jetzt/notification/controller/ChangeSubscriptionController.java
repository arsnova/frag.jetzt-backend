package de.thm.arsnova.frag.jetzt.notification.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushCommentSubscription;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.PushRoomSubscription;
import de.thm.arsnova.frag.jetzt.notification.service.ChangeSubscriptionService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("ChangeSubscriptionController")
@RequestMapping(ChangeSubscriptionController.REQUEST_MAPPING)
public class ChangeSubscriptionController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/change-subscription";
  private static final String ROOM_MAPPING = "/room";
  private static final String COMMENT_MAPPING = "/comment";
  private static final String ROOM_COMMENTS_MAPPING = "/room-comments";

  private final ChangeSubscriptionService service;

  @Autowired
  public ChangeSubscriptionController(ChangeSubscriptionService service) {
    this.service = service;
  }

  @PostMapping(ROOM_MAPPING)
  public Mono<PushRoomSubscription> createOrUpdateRoomSubscription(
    @RequestBody PushRoomSubscription subscription
  ) {
    return service.createOrUpdateRoomSubscription(subscription);
  }

  @GetMapping(ROOM_MAPPING)
  public Flux<PushRoomSubscription> getRoomSubscriptions() {
    return service.getRoomSubscriptions();
  }

  @GetMapping(ROOM_MAPPING + GET_MAPPING)
  public Mono<PushRoomSubscription> getRoomSubscription(
    @PathVariable("id") UUID id
  ) {
    return service.getRoomSubscription(id);
  }

  @DeleteMapping(ROOM_MAPPING + DELETE_MAPPING)
  public Mono<Void> deleteRoomSubscription(@PathVariable("id") UUID id) {
    return service.deleteRoomSubscription(id);
  }

  @DeleteMapping(ROOM_COMMENTS_MAPPING + DELETE_MAPPING)
  public Mono<Void> deleteRoomCommentsSubscription(
    @PathVariable("id") UUID id
  ) {
    return service.deleteCommentSubscriptions(id);
  }

  @PostMapping(COMMENT_MAPPING)
  public Mono<PushCommentSubscription> createOrUpdateCommentSubscription(
    @RequestBody PushCommentSubscription subscription
  ) {
    return service.createOrUpdateCommentSubscription(subscription);
  }

  @GetMapping(COMMENT_MAPPING + GET_MAPPING)
  public Mono<PushCommentSubscription> getCommentSubscription(
    @PathVariable("id") UUID id
  ) {
    return service.getCommentSubscription(id);
  }

  @DeleteMapping(COMMENT_MAPPING + DELETE_MAPPING)
  public Mono<Void> deleteCommentSubscription(@PathVariable("id") UUID id) {
    return service.deleteCommentSubscription(id);
  }
}
