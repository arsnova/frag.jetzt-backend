package de.thm.arsnova.frag.jetzt.backend.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class KeycloakClient {

  public static class PublicKeys {

    public List<Map<String, ?>> keys;
  }

  private static final Logger logger = LoggerFactory.getLogger(
    KeycloakClient.class
  );

  private final WebClient restWebClient;

  // .../realms/fragjetzt
  public KeycloakClient(String url) {
    restWebClient = WebClient.create(url);
  }

  public Mono<Claims> verifyToken(String token) {
    return getKeys()
      .flatMap(publicKeys -> {
        return Flux
          .fromIterable(publicKeys.keys)
          .flatMap(key -> check(key, token))
          .collectList()
          .filter(claims -> claims.size() > 0)
          .map(claims -> claims.get(0));
      });
  }

  private Mono<Claims> check(Map<String, ?> data, String token) {
    SigningKeyResolver resolver = new SigningKeyResolverAdapter() {
      public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {
        try {
          BigInteger modulus = new BigInteger(
            1,
            Base64.getUrlDecoder().decode((String) data.get("n"))
          );
          BigInteger exponent = new BigInteger(
            1,
            Base64.getUrlDecoder().decode((String) data.get("e"))
          );

          return KeyFactory
            .getInstance("RSA")
            .generatePublic(new RSAPublicKeySpec(modulus, exponent));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
          logger.error("Failed to resolve key: {}", e.getMessage(), e);
          return null;
        }
      }
    };

    try {
      Jws<Claims> jwsClaims = Jwts
        .parser()
        .setSigningKeyResolver(resolver)
        .parseClaimsJws(token);
      return Mono.just(jwsClaims.getBody());
    } catch (Exception e) {
      return Mono.empty();
    }
  }

  private Mono<PublicKeys> getKeys() {
    return restWebClient
      .get()
      .uri("/protocol/openid-connect/certs")
      .retrieve()
      .bodyToMono(PublicKeys.class);
  }
}
