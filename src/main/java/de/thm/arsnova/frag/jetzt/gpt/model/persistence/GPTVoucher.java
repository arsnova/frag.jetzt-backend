package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class GPTVoucher implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private String code;

  @Nullable
  private UUID accountId;

  @Nullable
  private UUID quotaId;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private Quota quota;

  public GPTVoucher() {}

  public GPTVoucher(
    @NonNull String code,
    @Nullable UUID accountId,
    @Nullable UUID quotaId
  ) {
    this.code = code;
    this.accountId = accountId;
    this.quotaId = quotaId;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getQuotaId() {
    return quotaId;
  }

  public void setQuotaId(UUID quotaId) {
    this.quotaId = quotaId;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Quota getQuota() {
    return quota;
  }

  public void setQuota(Quota quota) {
    this.quota = quota;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((code == null) ? 0 : code.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((quotaId == null) ? 0 : quotaId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTVoucher other = (GPTVoucher) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (code == null) {
      if (other.code != null) return false;
    } else if (!code.equals(other.code)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (quotaId == null) {
      if (other.quotaId != null) return false;
    } else if (!quotaId.equals(other.quotaId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTVoucher [id=" +
      id +
      ", code=" +
      code +
      ", accountId=" +
      accountId +
      ", quotaId=" +
      quotaId +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", quota=" +
      quota +
      "]"
    );
  }
}
