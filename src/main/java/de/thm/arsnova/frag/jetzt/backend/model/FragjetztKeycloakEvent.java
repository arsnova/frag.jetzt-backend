package de.thm.arsnova.frag.jetzt.backend.model;

import java.util.Map;

public class FragjetztKeycloakEvent {

  public static enum FragjetztEventType {
    UPDATE_ACCOUNT,
    DELETE_ACCOUNT,
  }

  private FragjetztEventType eventType;
  private String userId;
  private String eventPassword;
  private Map<String, String> details;

  public FragjetztKeycloakEvent() {}

  public FragjetztKeycloakEvent(
    FragjetztEventType eventType,
    String userId,
    Map<String, String> details
  ) {
    this.eventType = eventType;
    this.userId = userId;
    this.details = details;
  }

  public FragjetztEventType getEventType() {
    return eventType;
  }

  public void setEventType(FragjetztEventType eventType) {
    this.eventType = eventType;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getEventPassword() {
    return eventPassword;
  }

  public void setEventPassword(String eventPassword) {
    this.eventPassword = eventPassword;
  }

  public Map<String, String> getDetails() {
    return details;
  }

  public void setDetails(Map<String, String> details) {
    this.details = details;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((eventType == null) ? 0 : eventType.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    result =
      prime * result + ((eventPassword == null) ? 0 : eventPassword.hashCode());
    result = prime * result + ((details == null) ? 0 : details.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    FragjetztKeycloakEvent other = (FragjetztKeycloakEvent) obj;
    if (eventType != other.eventType) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    if (eventPassword == null) {
      if (other.eventPassword != null) return false;
    } else if (!eventPassword.equals(other.eventPassword)) return false;
    if (details == null) {
      if (other.details != null) return false;
    } else if (!details.equals(other.details)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "FragjetztKeycloakEvent [eventType=" +
      eventType +
      ", userId=" +
      userId +
      ", eventPassword=" +
      eventPassword +
      ", details=" +
      details +
      "]"
    );
  }
}
