package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ThreadMessageList {

  // always "list"
  private String object;
  private List<ThreadMessage> data;

  @JsonProperty("first_id")
  private String firstId;

  @JsonProperty("last_id")
  private String lastId;

  @JsonProperty("has_more")
  private Boolean hasMore;

  public ThreadMessageList() {}

  public String getObject() {
    return object;
  }

  public void setObject(String object) {
    this.object = object;
  }

  public List<ThreadMessage> getData() {
    return data;
  }

  public void setData(List<ThreadMessage> data) {
    this.data = data;
  }

  public String getFirstId() {
    return firstId;
  }

  public void setFirstId(String firstId) {
    this.firstId = firstId;
  }

  public String getLastId() {
    return lastId;
  }

  public void setLastId(String lastId) {
    this.lastId = lastId;
  }

  public Boolean getHasMore() {
    return hasMore;
  }

  public void setHasMore(Boolean hasMore) {
    this.hasMore = hasMore;
  }
}
