package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollVote;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LivepollVoteRepository
  extends ReactiveCrudRepository<LivepollVote, UUID> {
  Mono<LivepollVote> findByAccountIdAndSessionId(
    UUID accountId,
    UUID sessionId
  );

  Flux<LivepollVote> findAllBySessionId(UUID sessionId);

  Flux<Void> deleteAllBySessionId(UUID sessionId);
}
