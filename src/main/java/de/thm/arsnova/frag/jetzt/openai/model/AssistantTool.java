package de.thm.arsnova.frag.jetzt.openai.model;

public interface AssistantTool {
    String getType();
}
