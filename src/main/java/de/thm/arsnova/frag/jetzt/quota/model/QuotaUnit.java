package de.thm.arsnova.frag.jetzt.quota.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QuotaUnit implements Comparable<QuotaUnit> {

  public static final QuotaUnit ZERO = new QuotaUnit(0, 0);

  private final long value;
  private final int exponent;

  /**
   * Represents the unit US-Dollar ($) with more decimal places<br>
   * The current value in US-Dollar $ is represented with: value * 10^-exponent
   */
  @JsonCreator
  public QuotaUnit(
    @JsonProperty("value") Number value,
    @JsonProperty("exponent") int exponent
  ) {
    this.value = value.longValue();
    this.exponent = exponent;
  }

  public long getValue() {
    return value;
  }

  public int getExponent() {
    return exponent;
  }

  @Override
  public int compareTo(QuotaUnit u) {
    if (u.exponent > exponent) {
      long l = toPlain(u.exponent) - u.value;
      return l > 0 ? 1 : l < 0 ? -1 : 0;
    }
    long l = value - u.toPlain(exponent);
    return l > 0 ? 1 : l < 0 ? -1 : 0;
  }

  public QuotaUnit add(QuotaUnit u) {
    if (u.exponent > exponent) {
      return new QuotaUnit(toPlain(u.exponent) + u.value, u.exponent);
    }
    return new QuotaUnit(value + u.toPlain(exponent), exponent);
  }

  public QuotaUnit minus(QuotaUnit unit) {
    if (unit.exponent > exponent) {
      return new QuotaUnit(toPlain(unit.exponent) - unit.value, unit.exponent);
    }
    return new QuotaUnit(value - unit.toPlain(exponent), exponent);
  }

  public long divideSimple(QuotaUnit unit) {
    return (long) (
      value *
      Math.pow(10, unit.exponent) /
      (unit.value * Math.pow(10, exponent))
    );
  }

  public QuotaUnit multiply(Number n) {
    return new QuotaUnit(n.longValue() * value, exponent);
  }

  public QuotaUnit multiply(QuotaUnit u) {
    return new QuotaUnit(u.value * value, u.exponent + exponent);
  }

  public Long toPlain(int exponent) {
    return (long) (value * Math.pow(10, exponent - this.exponent));
  }

  public QuotaUnit zero() {
    return new QuotaUnit(0, exponent);
  }
}
