package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThreadMessage {

  // "user" or "assistant"
  private String role;
  // Either Sting or Array of content parts
  private Object content;
  private List<ThreadMessageAttachment> attachments;
  private Map<String, String> metadata;

  public ThreadMessage() {}

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public Object getContent() {
    return content;
  }

  public void setContent(Object content) {
    this.content = content;
  }

  public List<ThreadMessageAttachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<ThreadMessageAttachment> attachments) {
    this.attachments = attachments;
  }

  public Map<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(Map<String, String> metadata) {
    this.metadata = metadata;
  }

  @JsonIgnore
  public void addFile(String fileId) {
    if (attachments == null) {
      attachments = new ArrayList<>();
    }
    attachments.add(
      new ThreadMessageAttachment(fileId, List.of(ToolFileSearch.INSTANCE))
    );
  }

  @Override
  public String toString() {
    return (
      "ThreadMessage [role=" +
      role +
      ", content=" +
      content +
      ", attachments=" +
      attachments +
      ", metadata=" +
      metadata +
      "]"
    );
  }
}
