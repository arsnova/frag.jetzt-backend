package de.thm.arsnova.frag.jetzt.gpt.model.rest;

public class GPTChatCompletionDelta {

  private String role = null;
  private String content = null;

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
