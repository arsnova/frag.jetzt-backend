package de.thm.arsnova.frag.jetzt.backend.model.livepoll;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class LivepollCustomTemplateEntry implements Persistable<UUID> {

  private UUID id;
  private UUID sessionId;
  private int index;
  private String icon;
  private String text;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public LivepollCustomTemplateEntry(int index, String icon, String text) {
    this.index = index;
    this.icon = icon;
    this.text = text;
  }

  public LivepollCustomTemplateEntry() {}

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getSessionId() {
    return sessionId;
  }

  public void setSessionId(UUID sessionId) {
    this.sessionId = sessionId;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    result = prime * result + index;
    result = prime * result + ((icon == null) ? 0 : icon.hashCode());
    result = prime * result + ((text == null) ? 0 : text.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollCustomTemplateEntry other = (LivepollCustomTemplateEntry) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (sessionId == null) {
      if (other.sessionId != null) return false;
    } else if (!sessionId.equals(other.sessionId)) return false;
    if (index != other.index) return false;
    if (icon == null) {
      if (other.icon != null) return false;
    } else if (!icon.equals(other.icon)) return false;
    if (text == null) {
      if (other.text != null) return false;
    } else if (!text.equals(other.text)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "LivepollCustomTemplateEntry [id=" +
      id +
      ", sessionId=" +
      sessionId +
      ", index=" +
      index +
      ", icon=" +
      icon +
      ", text=" +
      text +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
