package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollCustomTemplateEntry;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollVote;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.LivepollCustomTemplateEntryRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.LivepollSessionRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.LivepollVoteRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

@Service
public class LivepollSessionService {

  private static final Logger logger = LoggerFactory.getLogger(
    LivepollSessionService.class
  );

  @Value("${app.livepoll.titleLength}")
  private int titleLength;

  @Value("${app.livepoll.maxAnswers}")
  private int maxAnswers;

  private final LivepollSessionRepository repository;
  private final AuthorizationHelper helper;
  private final RoomRepository roomRepository;
  private final LivepollCustomTemplateEntryRepository customTemplateEntryRepository;
  private final LivepollVoteRepository voteRepository;

  public LivepollSessionService(
    final LivepollSessionRepository repository,
    final AuthorizationHelper helper,
    final RoomRepository roomRepository,
    final LivepollCustomTemplateEntryRepository customTemplateEntryRepository,
    final LivepollVoteRepository voteRepository
  ) {
    this.repository = repository;
    this.helper = helper;
    this.roomRepository = roomRepository;
    this.customTemplateEntryRepository = customTemplateEntryRepository;
    this.voteRepository = voteRepository;
  }

  public Mono<LivepollSession> get(UUID id) {
    return repository.findById(id);
  }

  public Mono<Void> deleteVotes(UUID livepollSession) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(livepollSession))
      .switchIfEmpty(
        Mono.error(new BadRequestException("Session does not exist!"))
      )
      .filter(tuple -> tuple.getT2().isActive())
      .switchIfEmpty(Mono.error(new BadRequestException("Session is active!")))
      .flatMap(tuple ->
        Mono.zip(
          Mono.just(tuple.getT1()),
          Mono.just(tuple.getT2()),
          roomRepository.findById(tuple.getT2().getRoomId())
        )
      )
      .filter(tuple ->
        helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT3())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(tuple ->
        voteRepository.deleteAllBySessionId(tuple.getT2().getId()).then()
      );
  }

  public Mono<Tuple2<UUID, UUID>> delete(UUID livepollSession) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(livepollSession))
      .switchIfEmpty(
        Mono.error(new BadRequestException("Session does not exist!"))
      )
      .filter(tuple -> tuple.getT2().isActive())
      .switchIfEmpty(Mono.error(new BadRequestException("Session is active!")))
      .flatMap(tuple ->
        Mono.zip(
          Mono.just(tuple.getT1()),
          Mono.just(tuple.getT2()),
          roomRepository.findById(tuple.getT2().getRoomId())
        )
      )
      .filter(tuple ->
        helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT3())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(tuple ->
        repository
          .deleteById(tuple.getT2().getId())
          .then(
            Mono.just(Tuples.of(tuple.getT2().getId(), tuple.getT3().getId()))
          )
      );
  }

  public Mono<LivepollSession> create(LivepollSession entity) {
    return Mono
      .zip(helper.getCurrentUser(), roomRepository.findById(entity.getRoomId()))
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(tuple2 ->
        helper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2())
      )
      .switchIfEmpty(Mono.error(UnauthorizedException::new))
      .flatMap(ignored ->
        Mono.zip(
          validate(entity),
          repository
            .findByRoomIdAndActive(entity.getRoomId(), true)
            .switchIfEmpty(Mono.just(new LivepollSession()))
        )
      )
      .filter(tuple -> tuple.getT2().isNew())
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("There is already a running livepoll session!")
        )
      )
      .flatMap(tuple -> repository.save(tuple.getT1()))
      .flatMap(e -> {
        Mono<?> start = Mono.just("");
        if (e.getCustomEntries() != null) {
          List<LivepollCustomTemplateEntry> entries = e
            .getCustomEntries()
            .stream()
            .map(entry -> {
              entry.setSessionId(e.getId());
              return entry;
            })
            .collect(Collectors.toList());
          start = customTemplateEntryRepository.saveAll(entries).then();
        }
        return start.then(repository.findById(e.getId()));
      })
      .flatMap(this::applyTransientFields);
  }

  public Mono<LivepollSession> patch(
    LivepollSession entity,
    HashMap<String, Object> changes
  ) {
    return Mono
      .zip(helper.getCurrentUser(), roomRepository.findById(entity.getRoomId()))
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(tuple ->
        helper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2())
      )
      .switchIfEmpty(Mono.error(UnauthorizedException::new))
      .map(tuple -> this.parseChanges(entity, changes))
      .flatMap(this::validate)
      .flatMap(repository::save)
      .flatMap(e -> repository.findById(e.getId()))
      .flatMap(this::applyTransientFields);
  }

  public Mono<List<Integer>> getResults(UUID livepollId) {
    return voteRepository
      .findAllBySessionId(livepollId)
      .reduce(
        new ArrayList<Integer>(),
        (acc, value) -> {
          while (acc.size() <= value.getVoteIndex()) {
            acc.add(0);
          }
          acc.set(value.getVoteIndex(), acc.get(value.getVoteIndex()) + 1);
          return acc;
        }
      );
  }

  public Mono<LivepollSession> getActiveByRoomId(UUID roomId) {
    return this.repository.findByRoomIdAndActive(roomId, true)
      .flatMap(this::applyTransientFields);
  }

  public Flux<LivepollSession> getAllByRoomId(UUID roomId) {
    return this.repository.findAllByRoomId(roomId)
      .flatMap(this::applyTransientFields);
  }

  public Mono<LivepollVote> getVoteBySessionId(UUID sessionId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        voteRepository.findByAccountIdAndSessionId(
          user.getAccountId(),
          sessionId
        )
      );
  }

  public Mono<Void> createVoteBySessionId(UUID sessionId, int voteIndex) {
    if (voteIndex < 0) {
      return Mono.error(
        new BadRequestException("Vote Index can not be negative!")
      );
    }
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(sessionId))
      .switchIfEmpty(
        Mono.error(new BadRequestException("Session does not exist!"))
      )
      .filter(tuple -> voteIndex < tuple.getT2().getAnswerCount())
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Vote Index can not exceed answer count!")
        )
      )
      .flatMap(tuple ->
        getVoteBySessionId(sessionId)
          .switchIfEmpty(
            Mono.just(
              new LivepollVote(
                tuple.getT1().getAccountId(),
                sessionId,
                voteIndex
              )
            )
          )
      )
      .flatMap(vote -> {
        vote.setVoteIndex(voteIndex);
        return voteRepository.save(vote).then();
      });
  }

  public Mono<Void> deleteVoteBySessionId(UUID sessionId) {
    return getVoteBySessionId(sessionId)
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(voteRepository::delete);
  }

  private LivepollSession parseChanges(
    LivepollSession entity,
    HashMap<String, Object> changes
  ) {
    changes.forEach((key, value) -> {
      switch (key) {
        case "active":
          boolean active = (boolean) value;
          if (active) {
            throw new ForbiddenException(
              "Livepollsessions can only be deactivated!"
            );
          }
          entity.setActive(active);
          break;
        case "title":
          entity.setTitle((String) value);
          break;
        case "resultVisible":
          entity.setResultVisible((boolean) value);
          break;
        case "viewsVisible":
          entity.setViewsVisible((boolean) value);
          break;
        case "paused":
          entity.setPaused((boolean) value);
          break;
        case "id":
        case "roomId":
        case "template":
        case "createdAt":
        case "updatedAt":
        case "customEntries":
        case "answerCount":
          throw new ForbiddenException(
            "You are not allowed to change the " + key
          );
        default:
          throw new BadRequestException(
            "Invalid ChangeAttribute provided (" + key + ")"
          );
      }
    });
    return entity;
  }

  private Mono<LivepollSession> applyTransientFields(LivepollSession s) {
    return customTemplateEntryRepository
      .findBySessionId(s.getId())
      .collectList()
      .map(list -> {
        list.sort(
          new Comparator<LivepollCustomTemplateEntry>() {
            @Override
            public int compare(
              LivepollCustomTemplateEntry a,
              LivepollCustomTemplateEntry b
            ) {
              return Integer.compare(a.getIndex(), b.getIndex());
            }
          }
        );
        s.setCustomEntries(list);
        return s;
      });
  }

  private Mono<LivepollSession> validate(LivepollSession session) {
    return Mono
      .just(session)
      .filter(s -> s.getTitle() == null || s.getTitle().length() <= titleLength)
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Title should be null or less than " + titleLength + " characters!"
          )
        )
      )
      .filter(s -> s.getAnswerCount() > 0 && s.getAnswerCount() <= maxAnswers)
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "AnswerCount should be in between [1, " + maxAnswers + "]"
          )
        )
      );
  }
}
