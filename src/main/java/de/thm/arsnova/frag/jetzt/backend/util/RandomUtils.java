package de.thm.arsnova.frag.jetzt.backend.util;

import java.util.Random;

public class RandomUtils {

    public static String generateNumberToken(int length) {
        String possibleChars = "0123456789";
        return generateRandomString(possibleChars, length);
    }

    public static String generateAlphanumericToken(int length) {
        String possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return generateRandomString(possibleChars, length);
    }

    private static String generateRandomString(String chars, int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);
        while (sb.length() < length) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }
        return sb.toString();
    }
}
