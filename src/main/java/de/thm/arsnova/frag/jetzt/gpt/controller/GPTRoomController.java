package de.thm.arsnova.frag.jetzt.gpt.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTRoomSettingService;
import de.thm.arsnova.frag.jetzt.quota.model.persistence.Quota;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController("GPTRoomController")
@RequestMapping(GPTRoomController.REQUEST_MAPPING)
public class GPTRoomController extends AbstractEntityController {

  protected static final String REQUEST_MAPPING = "/gpt/room-setting";
  private static final String PARTICIPANT_QUOTA = "/participant-quota";
  private static final String MODERAOR_QUOTA = "/moderator-quota";
  private static final String ROOM_QUOTA = "/room-quota";

  private final GPTRoomSettingService roomService;

  @Autowired
  public GPTRoomController(GPTRoomSettingService roomService) {
    this.roomService = roomService;
  }

  @GetMapping(GET_MAPPING)
  public Mono<GPTRoomSetting> getRoomSetting(@PathVariable UUID id) {
    return roomService.getByRoomId(id);
  }

  @PatchMapping(PATCH_MAPPING)
  public Mono<GPTRoomSetting> patchRoomSetting(
    @PathVariable UUID id,
    @RequestBody Map<String, Object> changes
  ) {
    return roomService.patch(id, changes);
  }

  @PatchMapping(GET_MAPPING + PARTICIPANT_QUOTA + "/{quotaId}")
  public Mono<Quota> patchParticipantQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId,
    @RequestBody Map<String, Object> changes
  ) {
    return roomService.patchParticipantQuota(id, quotaId, changes);
  }

  @PatchMapping(GET_MAPPING + MODERAOR_QUOTA + "/{quotaId}")
  public Mono<Quota> patchModeratorQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId,
    @RequestBody Map<String, Object> changes
  ) {
    return roomService.patchModeratorQuota(id, quotaId, changes);
  }

  @PatchMapping(GET_MAPPING + ROOM_QUOTA + "/{quotaId}")
  public Mono<Quota> patchRoomQuota(
    @PathVariable UUID id,
    @PathVariable UUID quotaId,
    @RequestBody Map<String, Object> changes
  ) {
    return roomService.patchRoomQuota(id, quotaId, changes);
  }
}
