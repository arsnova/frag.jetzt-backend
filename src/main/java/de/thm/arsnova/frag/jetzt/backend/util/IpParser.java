package de.thm.arsnova.frag.jetzt.backend.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IpParser {

  public static class ValidIps {

    private byte[] from;
    private byte[] to;

    public ValidIps(byte[] from, byte[] to) {
      if (from.length != to.length) {
        throw new IllegalArgumentException(
          "Can not change Protocol inside IP Address!"
        );
      }
      if (from.length != 4 && from.length != 16) {
        throw new IllegalArgumentException("No Ipv4 or Ipv6!");
      }
      this.from = from;
      this.to = to;
    }

    public boolean contains(byte[] data) {
      if (data.length != from.length) {
        return false;
      }
      for (int i = 0; i < data.length; i++) {
        int v = data[i] & 0xFF;
        int min = from[i] & 0xFF;
        int max = to[i] & 0xFF;
        if (v > max || v < min) {
          return false;
        }
      }
      return true;
    }
  }

  public static List<ValidIps> parseIpRanges(String text)
    throws UnknownHostException {
    if (text.trim().isEmpty()) {
      return Collections.emptyList();
    }
    int start = 0;
    int end;
    ArrayList<ValidIps> list = new ArrayList<>();
    while ((end = text.indexOf("\\", start)) > 0) {
      list.add(parseValidIps(text.substring(start, end).trim()));
      start = end + 1;
    }
    if (start < text.length()) {
      list.add(parseValidIps(text.substring(start).trim()));
    }
    return list;
  }

  private static ValidIps parseValidIps(String ipRange)
    throws UnknownHostException {
    int sub = ipRange.indexOf("/");
    if (sub < 0) {
      byte[] data = InetAddress.getByName(ipRange).getAddress();
      return new ValidIps(data, data);
    }
    byte[] from = InetAddress.getByName(ipRange.substring(0, sub)).getAddress();
    if (ipRange.charAt(sub + 1) == '/') {
      byte[] to = InetAddress
        .getByName(ipRange.substring(sub + 2))
        .getAddress();
      return new ValidIps(from, to);
    }
    final int bitmask = Integer.parseInt(ipRange.substring(sub + 1), 10);
    byte[] to = new byte[from.length];
    int divBitmask = bitmask / 8;
    for (int i = 0; i < divBitmask; i++) {
      to[i] = from[i];
    }
    final int modBitMask = bitmask % 8;
    if (modBitMask > 0) {
      int logicalBitmask = 0;
      for (int i = 0; i < modBitMask; i++) {
        logicalBitmask = (logicalBitmask >> 1) | 0x80;
      }
      from[divBitmask] = (byte) (from[divBitmask] & logicalBitmask);
      to[divBitmask] = (byte) (from[divBitmask] | ~logicalBitmask);
      divBitmask += 1;
    }
    for (; divBitmask < to.length; divBitmask++) {
      to[divBitmask] = (byte) -1;
      from[divBitmask] = 0;
    }
    return new ValidIps(from, to);
  }
}
