package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UPCCode {

  public enum Type {
    UPC_A("UPC_A"),
    UPC_B("UPC_B"),
    UPC_C("UPC_C"),
    UPC_D("UPC_D"),
    UPC_E("UPC_E"),
    UPC_2("UPC_2"),
    UPC_5("UPC_5");

    private final String value;

    Type(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return value;
    }
  }

  // required, 1-5 characters
  private final Type type;
  // required, 6-17 characters
  private final String code;
}
