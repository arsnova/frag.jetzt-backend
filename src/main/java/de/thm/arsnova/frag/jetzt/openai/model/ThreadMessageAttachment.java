package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThreadMessageAttachment {

  @JsonProperty("file_id")
  private String fileId;

  private List<Object> tools;

  public ThreadMessageAttachment() {}

  public ThreadMessageAttachment(String fileId, List<Object> tools) {
    this.fileId = fileId;
    this.tools = tools;
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public List<Object> getTools() {
    return tools;
  }

  public void setTools(List<Object> tools) {
    this.tools = tools;
  }

  @Override
  public String toString() {
    return (
      "ThreadMessageAttachment [fileId=" + fileId + ", tools=" + tools + "]"
    );
  }
}
