package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Data
@Jacksonized
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Amount extends Price {

  // optional
  private Breakdown breakdown;
}
