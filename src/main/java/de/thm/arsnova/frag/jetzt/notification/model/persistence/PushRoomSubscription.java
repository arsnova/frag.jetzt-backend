package de.thm.arsnova.frag.jetzt.notification.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.arsnova.frag.jetzt.notification.model.ICommentSubscription;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class PushRoomSubscription
  implements Persistable<UUID>, ICommentSubscription {

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @NonNull
  private UUID roomId;

  @NonNull
  private long ownCommentBits;

  @NonNull
  private long otherCommentBits;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  @Transient
  private List<PushCommentSubscription> commentSubscriptions;

  public PushRoomSubscription() {}

  public PushRoomSubscription(
    UUID accountId,
    UUID roomId,
    long ownCommentBits,
    long otherCommentBits
  ) {
    this.accountId = accountId;
    this.roomId = roomId;
    this.ownCommentBits = ownCommentBits;
    this.otherCommentBits = otherCommentBits;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public long getOwnCommentBits() {
    return ownCommentBits;
  }

  public void setOwnCommentBits(long ownCommentBits) {
    this.ownCommentBits = ownCommentBits;
  }

  public long getOtherCommentBits() {
    return otherCommentBits;
  }

  public void setOtherCommentBits(long otherCommentBits) {
    this.otherCommentBits = otherCommentBits;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<PushCommentSubscription> getCommentSubscriptions() {
    return commentSubscriptions;
  }

  public void setCommentSubscriptions(
    List<PushCommentSubscription> commentSubscriptions
  ) {
    this.commentSubscriptions = commentSubscriptions;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + (int) (ownCommentBits ^ (ownCommentBits >>> 32));
    result =
      prime * result + (int) (otherCommentBits ^ (otherCommentBits >>> 32));
    result =
      prime *
      result +
      ((commentSubscriptions == null) ? 0 : commentSubscriptions.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    PushRoomSubscription other = (PushRoomSubscription) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (ownCommentBits != other.ownCommentBits) return false;
    if (otherCommentBits != other.otherCommentBits) return false;
    if (commentSubscriptions == null) {
      if (other.commentSubscriptions != null) return false;
    } else if (
      !commentSubscriptions.equals(other.commentSubscriptions)
    ) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "PushRoomSubscription [id=" +
      id +
      ", accountId=" +
      accountId +
      ", roomId=" +
      roomId +
      ", ownCommentBits=" +
      ownCommentBits +
      ", otherCommentBits=" +
      otherCommentBits +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", commentSubscriptions=" +
      commentSubscriptions +
      "]"
    );
  }

  @Override
  @JsonIgnore
  public SubscriptionResult hasSubscribed(CommentChange change) {
    if (!change.getRoomId().equals(roomId)) {
      return SubscriptionResult.NONE;
    }
    boolean ownsComment = change.getCommentCreatorId().equals(accountId);
    boolean isInitiator = change.getInitiatorId().equals(accountId);
    if (ownsComment) {
      if (!change.getType().hasInterest(ownCommentBits)) {
        return SubscriptionResult.NONE;
      }
      return isInitiator
        ? SubscriptionResult.OWN_OWN
        : SubscriptionResult.OWN_OTHER;
    }
    if (!change.getType().hasInterest(otherCommentBits)) {
      return SubscriptionResult.NONE;
    }
    return isInitiator
      ? SubscriptionResult.OTHER_OWN
      : SubscriptionResult.OTHER_OTHER;
  }
}
