package de.thm.arsnova.frag.jetzt.notification.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import de.thm.arsnova.frag.jetzt.notification.service.CommentChangeFindQueryService;
import de.thm.arsnova.frag.jetzt.notification.service.CommentChangeService;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("CommentChangeController")
@RequestMapping(CommentChangeController.REQUEST_MAPPING)
public class CommentChangeController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    CommentChangeController.class
  );

  protected static final String REQUEST_MAPPING = "/comment-change";

  private final CommentChangeService service;
  private final CommentChangeFindQueryService findQueryService;

  @Autowired
  public CommentChangeController(
    CommentChangeService service,
    CommentChangeFindQueryService findQueryService
  ) {
    this.service = service;
    this.findQueryService = findQueryService;
  }

  @GetMapping(GET_MAPPING)
  public Mono<CommentChange> get(@PathVariable UUID id) {
    return service.get(id).switchIfEmpty(Mono.error(NotFoundException::new));
  }

  @PostMapping(FIND_MAPPING)
  public Flux<CommentChange> find(
    @RequestBody final FindQuery<CommentChange> findQuery
  ) {
    logger.debug("Resolving find query: {}", findQuery);
    return findQueryService.resolveQuery(findQuery);
  }
}
