package de.thm.arsnova.frag.jetzt.backend;

import de.thm.arsnova.frag.jetzt.backend.model.event.livepoll.LivepollEvent;
import de.thm.arsnova.frag.jetzt.backend.model.event.livepoll.LivepollResult;
import de.thm.arsnova.frag.jetzt.backend.model.event.livepoll.LivepollResultPayload;
import de.thm.arsnova.frag.jetzt.backend.service.LivepollSessionService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class LivepollEventSource {

  private final AmqpTemplate messagingTemplate;
  private final LivepollSessionService sessionService;
  private final HashSet<UUID> cache = new HashSet<>();

  @Autowired
  public LivepollEventSource(
    final AmqpTemplate messagingTemplate,
    final LivepollSessionService sessionService
  ) {
    this.messagingTemplate = messagingTemplate;
    this.sessionService = sessionService;
  }

  // Half a second
  @Scheduled(fixedDelay = 500)
  protected void sendAndUpdate() {
    if (cache.size() < 1) {
      return;
    }
    ArrayList<UUID> workList;
    synchronized (cache) {
      workList = new ArrayList<>(cache);
      cache.clear();
    }
    for (UUID uuid : workList) {
      this.sessionService.getResults(uuid)
        .subscribe(results -> {
          send(new LivepollResult(new LivepollResultPayload(results), uuid));
        });
    }
  }

  public void sendScoreUpdated(UUID livepollId) {
    synchronized (cache) {
      cache.add(livepollId);
    }
  }

  private void send(LivepollEvent<?> event) {
    if (event.getLivepollId() == null) throw new IllegalArgumentException(
      "Event must have livepoll!"
    );
    messagingTemplate.convertAndSend(
      "amq.topic",
      event.getLivepollId() + ".livepoll.stream",
      event
    );
  }
}
