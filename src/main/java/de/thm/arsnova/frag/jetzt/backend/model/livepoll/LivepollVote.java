package de.thm.arsnova.frag.jetzt.backend.model.livepoll;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class LivepollVote implements Persistable<UUID> {

  @Id
  private UUID id;
  private UUID accountId;
  private UUID sessionId;
  private int voteIndex;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public LivepollVote() {}

  public LivepollVote(UUID accountId, UUID sessionId, int voteIndex) {
    this.accountId = accountId;
    this.sessionId = sessionId;
    this.voteIndex = voteIndex;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getSessionId() {
    return sessionId;
  }

  public void setSessionId(UUID sessionId) {
    this.sessionId = sessionId;
  }

  public int getVoteIndex() {
    return voteIndex;
  }

  public void setVoteIndex(int voteIndex) {
    this.voteIndex = voteIndex;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    result = prime * result + voteIndex;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollVote other = (LivepollVote) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (sessionId == null) {
      if (other.sessionId != null) return false;
    } else if (!sessionId.equals(other.sessionId)) return false;
    if (voteIndex != other.voteIndex) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "LivepollVote [id=" +
      id +
      ", accountId=" +
      accountId +
      ", sessionId=" +
      sessionId +
      ", voteIndex=" +
      voteIndex +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
