package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payee {

  // optional, 3-254 characters
  @JsonProperty("email_address")
  private String emailAdress;

  // optional, 13 characters
  @JsonProperty("merchant_id")
  private String merchantId;
}
