package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class RoomAccess implements Persistable<UUID> {

    public enum Role {
        PARTICIPANT,
        EDITING_MODERATOR,
        EXECUTIVE_MODERATOR
    }

    @Id
    private UUID id;
    private UUID roomId;
    private UUID accountId;
    private Role role;
    private Timestamp lastVisit;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Override
    public boolean isNew() {
        return id == null;
    }

    public RoomAccess() {
    }

    public RoomAccess(UUID roomId, UUID accountId, Role role, Timestamp lastVisit) {
        this.roomId = roomId;
        this.accountId = accountId;
        this.role = role;
        this.lastVisit = lastVisit;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Timestamp getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(Timestamp lastVisit) {
        this.lastVisit = lastVisit;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setManagedFields(RoomAccess roomAccess) {
        this.lastVisit = roomAccess.lastVisit;
        this.createdAt = roomAccess.createdAt;
        this.updatedAt = roomAccess.updatedAt;
    }

    @Override
    public String toString() {
        return "RoomAccess{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", accountId=" + accountId +
                ", role=" + role +
                ", lastVisit=" + lastVisit +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomAccess that = (RoomAccess) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(roomId, that.roomId) &&
                Objects.equals(accountId, that.accountId) &&
                role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, accountId, role);
    }
}
