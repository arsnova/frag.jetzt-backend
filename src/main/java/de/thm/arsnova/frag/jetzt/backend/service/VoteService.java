package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.*;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.DownvoteRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.UpvoteRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import de.thm.arsnova.frag.jetzt.notification.service.CommentChangeService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class VoteService {

  private final AuthorizationHelper authorizationHelper;
  private final UpvoteRepository upvoteRepository;
  private final DownvoteRepository downvoteRepository;
  private final AccountService accountService;
  private final CommentService commentService;
  private final RoomRepository roomRepository;

  @Autowired
  public VoteService(
    AuthorizationHelper authorizationHelper,
    UpvoteRepository upvoteRepository,
    DownvoteRepository downvoteRepository,
    AccountService accountService,
    CommentService commentService,
    RoomRepository roomRepository
  ) {
    this.authorizationHelper = authorizationHelper;
    this.upvoteRepository = upvoteRepository;
    this.downvoteRepository = downvoteRepository;
    this.accountService = accountService;
    this.commentService = commentService;
    this.roomRepository = roomRepository;
  }

  public Mono<AbstractVote> get(UUID id) {
    return upvoteRepository
      .findById(id)
      .map(upvote -> (AbstractVote) upvote)
      .switchIfEmpty(downvoteRepository.findById(id));
  }

  public Flux<AbstractVote> get(List<UUID> ids) {
    return upvoteRepository
      .findAllById(ids)
      .map(upvote -> (AbstractVote) upvote)
      .switchIfEmpty(downvoteRepository.findAllById(ids));
  }

  public Mono<AbstractVote> get(UUID commentId, UUID accountId) {
    return upvoteRepository
      .findByCommentIdAndAccountId(commentId, accountId)
      .map(upvote -> (AbstractVote) upvote)
      .switchIfEmpty(
        downvoteRepository.findByCommentIdAndAccountId(commentId, accountId)
      );
  }

  public Mono<AbstractVote> getForCommentAndUser(UUID commentId, UUID userId) {
    return upvoteRepository
      .findByCommentIdAndAccountId(commentId, userId)
      .map(upvote -> (AbstractVote) upvote)
      .switchIfEmpty(
        downvoteRepository.findByCommentIdAndAccountId(commentId, userId)
      );
  }

  public Flux<AbstractVote> getForCommentsAndUser(
    List<UUID> commentIds,
    UUID accountId
  ) {
    return upvoteRepository
      .findAllByAccountIdAndCommentIdIn(accountId, commentIds)
      .map(upvote -> (AbstractVote) upvote)
      .switchIfEmpty(
        downvoteRepository.findAllByAccountIdAndCommentIdIn(
          accountId,
          commentIds
        )
      );
  }

  public Mono<Upvote> createUpvote(AuthenticatedUser user, Upvote upvote) {
    return upvoteRepository
      .findByCommentIdAndAccountId(upvote.getCommentId(), user.getAccountId())
      .switchIfEmpty(
        Mono
          .just(upvote)
          .map(currentUpvote -> {
            currentUpvote.setId(null);
            currentUpvote.setAccountId(user.getAccountId());
            return currentUpvote;
          })
      )
      .flatMap(vote ->
        downvoteRepository
          .deleteByAccountIdAndCommentId(
            user.getAccountId(),
            vote.getCommentId()
          )
          .then(upvoteRepository.save(vote))
      );
  }

  public Mono<Downvote> createDownvote(
    AuthenticatedUser user,
    Downvote downvote
  ) {
    return downvoteRepository
      .findByCommentIdAndAccountId(downvote.getCommentId(), user.getAccountId())
      .switchIfEmpty(
        Mono
          .just(downvote)
          .map(currentDownvote -> {
            currentDownvote.setId(null);
            currentDownvote.setAccountId(user.getAccountId());
            return currentDownvote;
          })
      )
      .flatMap(vote ->
        upvoteRepository
          .deleteByAccountIdAndCommentId(
            user.getAccountId(),
            vote.getCommentId()
          )
          .then(downvoteRepository.save(vote))
      );
  }

  public Mono<Void> deleteUpvote(Upvote upvote) {
    return authorizationHelper
      .getCurrentUser()
      .filter(authenticatedUser ->
        authenticatedUser.getAccountId().equals(upvote.getAccountId())
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(authenticatedUser -> upvoteRepository.delete(upvote));
  }

  public Mono<Void> deleteDownvote(Downvote downvote) {
    return authorizationHelper
      .getCurrentUser()
      .filter(authenticatedUser ->
        authenticatedUser.getAccountId().equals(downvote.getAccountId())
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(authenticatedUser -> downvoteRepository.delete(downvote));
  }

  public Mono<Void> resetVote(UUID commentId, AuthenticatedUser user) {
    return Mono
      .zip(
        upvoteRepository.deleteByAccountIdAndCommentId(
          user.getAccountId(),
          commentId
        ),
        downvoteRepository.deleteByAccountIdAndCommentId(
          user.getAccountId(),
          commentId
        )
      )
      .then();
  }
}
