package de.thm.arsnova.frag.jetzt.notification.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * This object has the following members:
 * <ul>
 *   <li>action: A string identifying a user action to be displayed on the notification.</li>
 *   <li>title: A string containing action text to be shown to the user.</li>
 *   <li>icon: A string containing the URL of an icon to display with the action.</li>
 * </ul>
 * Appropriate responses are built using event.action within the notificationclick event.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebNotificationAction {

  @NonNull
  private String action;

  @NonNull
  private String title;

  @Nullable
  private String icon;

  public WebNotificationAction() {}

  public WebNotificationAction(
    @NonNull String action,
    @NonNull String title,
    @Nullable String icon
  ) {
    this.setAction(action);
    this.setTitle(title);
    this.setIcon(icon);
  }

  /**
   * @see WebNotificationAction
   */
  public String getAction() {
    return action;
  }

  /**
   * @see WebNotificationAction
   */
  public void setAction(String action) {
    if (action == null) throw new IllegalArgumentException(
      "action must not be null"
    );
    this.action = action;
  }

  /**
   * @see WebNotificationAction
   */
  public String getTitle() {
    return title;
  }

  /**
   * @see WebNotificationAction
   */
  public void setTitle(String title) {
    if (title == null) throw new IllegalArgumentException(
      "title must not be null"
    );
    this.title = title;
  }

  /**
   * @see WebNotificationAction
   */
  public String getIcon() {
    return icon;
  }

  /**
   * @see WebNotificationAction
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((action == null) ? 0 : action.hashCode());
    result = prime * result + ((title == null) ? 0 : title.hashCode());
    result = prime * result + ((icon == null) ? 0 : icon.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    WebNotificationAction other = (WebNotificationAction) obj;
    if (action == null) {
      if (other.action != null) return false;
    } else if (!action.equals(other.action)) return false;
    if (title == null) {
      if (other.title != null) return false;
    } else if (!title.equals(other.title)) return false;
    if (icon == null) {
      if (other.icon != null) return false;
    } else if (!icon.equals(other.icon)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "NotificationAction [action=" +
      action +
      ", title=" +
      title +
      ", icon=" +
      icon +
      "]"
    );
  }
}
