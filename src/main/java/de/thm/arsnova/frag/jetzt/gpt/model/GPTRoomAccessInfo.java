package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.quota.service.QuotaManageService.QuotaStatus;
import java.util.List;

public class GPTRoomAccessInfo {

  private final GPTGlobalAccessInfo globalInfo;
  private final boolean keyPresent;
  private final List<QuotaStatus> status;
  private final boolean restricted;

  public GPTRoomAccessInfo(
    GPTGlobalAccessInfo globalInfo,
    boolean keyPresent,
    List<QuotaStatus> status,
    boolean allowUnregistered
  ) {
    this.globalInfo = globalInfo;
    this.keyPresent = keyPresent;
    this.status = status;
    boolean hasGlobal =
      globalInfo.isApiKeyPresent() &&
      globalInfo.isApiEnabled() &&
      !globalInfo.isApiExpired() &&
      globalInfo.isGlobalActive();
    this.restricted =
      globalInfo.isBlocked() ||
      (!globalInfo.isRegistered() && !allowUnregistered) ||
      (!keyPresent && !hasGlobal) ||
      !status
        .stream()
        .anyMatch(e ->
          !e.isDisabled() &&
          e.isInAccessTime() &&
          e.getEntries().values().stream().allMatch(b -> b)
        );
  }

  public GPTGlobalAccessInfo getGlobalInfo() {
    return globalInfo;
  }

  public boolean isRestricted() {
    return restricted;
  }

  public boolean isKeyPresent() {
    return keyPresent;
  }

  public List<QuotaStatus> getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomAccessInfo [globalInfo=" +
      globalInfo +
      ", keyPresent=" +
      keyPresent +
      ", status=" +
      status +
      ", restricted=" +
      restricted +
      "]"
    );
  }
}
