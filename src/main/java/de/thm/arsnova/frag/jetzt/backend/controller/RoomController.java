package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.handler.RoomCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.ModeratorAccessCode;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@RestController("RoomController")
@RequestMapping(RoomController.REQUEST_MAPPING)
public class RoomController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(RoomController.class);

    protected static final String REQUEST_MAPPING = "/room";
    protected static final String IMPORT_MAPPING = DEFAULT_ID_MAPPING + "/import/{userCount}";
    protected static final String MODERATORS_MAPPING = "/moderator";
    protected static final String MODERATORID_MAPPING = "/moderator/{modId}";
    protected static final String MODERATOR_CODE_MAPPING = DEFAULT_ID_MAPPING + "/moderator-code";
    protected static final String MODERATOR_CODE_REFRESH_MAPPING = DEFAULT_ID_MAPPING + "/moderator-refresh";

    private final RoomService service;
    private final RoomAccessService roomAccessService;
    private final RoomFindQueryService findQueryService;
    private final RoomCommandHandler commandHandler;

    @Autowired
    public RoomController(
            RoomService service,
            RoomAccessService roomAccessService,
            RoomFindQueryService findQueryService,
            RoomCommandHandler commandHandler
    ) {
        this.service = service;
        this.roomAccessService = roomAccessService;
        this.findQueryService = findQueryService;
        this.commandHandler = commandHandler;
    }

    @GetMapping(GET_MAPPING)
    public Mono<Room> get(@PathVariable UUID id) {
        return service.get(id)
                .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @GetMapping(DEFAULT_ALIAS_MAPPING)
    public Mono<Room> getByShortId(@PathVariable final String alias) {
        return service.getByShortId(alias)
                .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @PutMapping(PUT_MAPPING)
    public Mono<Room> put(@RequestBody final Room entity) {
        UpdateRoomPayload payload = new UpdateRoomPayload(entity);
        return commandHandler.handle(new UpdateRoom(payload));
    }

    @PatchMapping(PATCH_MAPPING)
    public Mono<Room> patch(@PathVariable final UUID id, @RequestBody final Map<String, Object> changes) {
        PatchRoomPayload payload = new PatchRoomPayload(id, changes);
        return commandHandler.handle(new PatchRoom(payload));
    }

    @PostMapping(POST_MAPPING)
    public Mono<Room> post(@RequestBody final Room entity) {
        return service.create(entity);
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable final UUID id) {
        return service.delete(id);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<Room> find(@RequestBody final FindQuery<Room> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

    @PostMapping(IMPORT_MAPPING)
    public Flux<UUID> createGuestUUIDs(@PathVariable final UUID id, @PathVariable final int userCount) {
        logger.debug("Resolving request of {} guests for import to room: {}", userCount, id);

        ImportRoomPayload payload = new ImportRoomPayload(id, userCount);
        return commandHandler.handle(new ImportRoom(payload));
    }

    @GetMapping(GET_MAPPING + MODERATORS_MAPPING)
    public Flux<RoomAccess> getModerator(@PathVariable UUID id) {
        return roomAccessService.getByRoomId(id)
                .filter(roomAccess -> roomAccess.getRole() == RoomAccess.Role.EXECUTIVE_MODERATOR);
    }

    @PostMapping(MODERATOR_CODE_MAPPING + "/")
    public Mono<RoomAccess> postModeratorByCode(@PathVariable UUID id) {
        return roomAccessService.addModeratorByCode(id);
    }

    @GetMapping(MODERATOR_CODE_MAPPING)
    public Mono<ModeratorAccessCode> getModeratorCode(@PathVariable UUID id) {
        return service.getModeratorCodeRoom(id).map(room -> new ModeratorAccessCode(room.getShortId()));
    }

    @PutMapping(MODERATOR_CODE_REFRESH_MAPPING)
    public Mono<ModeratorAccessCode> putModeratorCodeRefresh(@PathVariable UUID id) {
        RecreateModeratorCodePayload payload = new RecreateModeratorCodePayload(id);
        return commandHandler.handle(new RecreateModeratorCode(payload));
    }

    @PutMapping(PUT_MAPPING + MODERATORID_MAPPING)
    public Mono<RoomAccess> putModerator(@PathVariable UUID id, @PathVariable UUID modId) {
        return roomAccessService.addModerator(id, modId);
    }

    @DeleteMapping(GET_MAPPING + MODERATORID_MAPPING)
    public Mono<Void> deleteModerator(@PathVariable UUID id, @PathVariable UUID modId) {
        return roomAccessService.removeModerator(id, modId).then();
    }
}