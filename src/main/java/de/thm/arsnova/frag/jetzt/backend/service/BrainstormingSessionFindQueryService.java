package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class BrainstormingSessionFindQueryService {

    private static final Logger logger = LoggerFactory.getLogger(BrainstormingSessionFindQueryService.class);

    private final BrainstormingSessionService brainstormingSessionService;

    @Autowired
    public BrainstormingSessionFindQueryService(BrainstormingSessionService brainstormingSessionService) {
        this.brainstormingSessionService = brainstormingSessionService;
    }

    public Flux<BrainstormingSession> resolveQuery(final FindQuery<BrainstormingSession> findQuery) {
        if (findQuery.getProperties().getRoomId() != null) {
            return brainstormingSessionService.findByRoomId(findQuery.getProperties().getRoomId());
        }
        return Flux.empty();
    }

}
