package de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseUnit {

  // optional, 1-256 characters
  @JsonProperty("reference_id")
  private String referenceId;

  // optional, 1-127 characters
  private String description;
  // optional, 1-255 characters
  private String customId;

  // optional, 1-127 characters
  @JsonProperty("invoice_id")
  private String invoiceId;

  // optional, 1-22 characters
  @JsonProperty("soft_descriptor")
  private String softDescriptor;

  // optional
  private List<Item> items;
  // required
  private Amount amount;
  // optional
  private Payee payee;

  // optional
  @JsonProperty("payment_instruction")
  private Object paymentInstruction;

  // optional
  private Object shipping;

  // optional
  @JsonProperty("supplementary_data")
  private Object supplementaryData;
}
