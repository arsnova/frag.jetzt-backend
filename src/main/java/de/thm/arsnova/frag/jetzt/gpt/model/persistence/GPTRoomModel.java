package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class GPTRoomModel implements Persistable<UUID> {

  @Id
  private UUID id;

  @NonNull
  private UUID settingId;

  @NonNull
  private String name;

  private int index;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public GPTRoomModel() {}

  public GPTRoomModel(UUID settingId, String name, int index) {
    this.settingId = settingId;
    this.name = name;
    this.index = index;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getSettingId() {
    return settingId;
  }

  public void setSettingId(UUID settingId) {
    this.settingId = settingId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((settingId == null) ? 0 : settingId.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + index;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomModel other = (GPTRoomModel) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (settingId == null) {
      if (other.settingId != null) return false;
    } else if (!settingId.equals(other.settingId)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (index != other.index) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomModel [id=" +
      id +
      ", settingId=" +
      settingId +
      ", name=" +
      name +
      ", index=" +
      index +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
