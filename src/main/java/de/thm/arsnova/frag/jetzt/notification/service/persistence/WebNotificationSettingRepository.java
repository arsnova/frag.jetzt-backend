package de.thm.arsnova.frag.jetzt.notification.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.WebNotificationSetting;
import reactor.core.publisher.Mono;

@Repository
public interface WebNotificationSettingRepository extends ReactiveCrudRepository<WebNotificationSetting, UUID> {
  Mono<WebNotificationSetting> findByAccountId(UUID accountId);

  Mono<Void> deleteByAccountId(UUID accountId);
}
