package de.thm.arsnova.frag.jetzt.openai.model.persistence;

import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class OpenaiAssistants implements Persistable<UUID> {

  @Id
  private UUID id;

  private String openaiId;
  private UUID roomId;
  private Instant createdAt = Instant.now();
  private Instant updatedAt;

  public OpenaiAssistants(UUID roomId, String openaiId) {
    this.roomId = roomId;
    this.openaiId = openaiId;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getOpenaiId() {
    return openaiId;
  }

  public void setOpenaiId(String openaiId) {
    this.openaiId = openaiId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public Instant getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((openaiId == null) ? 0 : openaiId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    OpenaiAssistants other = (OpenaiAssistants) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (openaiId == null) {
      if (other.openaiId != null) return false;
    } else if (!openaiId.equals(other.openaiId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "OpenaiAssistants [id=" +
      id +
      ", openaiId=" +
      openaiId +
      ", roomId=" +
      roomId +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
