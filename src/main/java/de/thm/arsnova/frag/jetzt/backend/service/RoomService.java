package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.ModeratorAccessCode;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.Tag;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.TagRepository;
import de.thm.arsnova.frag.jetzt.backend.util.RandomUtils;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.*;
import de.thm.arsnova.frag.jetzt.gpt.service.creators.GPTCreateRoomSettingService;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class RoomService {

  private static final Logger logger = LoggerFactory.getLogger(
    RoomService.class
  );

  @Value("${app.room.namelength}")
  private int maxRoomNameLength;

  @Value("${app.room.descriptionlength}")
  private int maxRoomDescriptionLength;

  @Value("${app.room.shortIdLength}")
  private int shortIdLength;

  @Value("${app.room.generatedShortIdLength}")
  private int generatedShortIdLength;

  private final AuthorizationHelper authorizationHelper;
  private final RoomRepository repository;
  private final TagRepository tagRepository;
  private final BrainstormingSessionService sessionService;
  private final LivepollSessionService livepollService;
  private final GPTCreateRoomSettingService createRoomSettingService;

  @Autowired
  public RoomService(
    AuthorizationHelper authorizationHelper,
    RoomRepository repository,
    TagRepository tagRepository,
    BrainstormingSessionService sessionService,
    LivepollSessionService livepollService,
    GPTCreateRoomSettingService createRoomSettingService
  ) {
    this.authorizationHelper = authorizationHelper;
    this.repository = repository;
    this.tagRepository = tagRepository;
    this.sessionService = sessionService;
    this.livepollService = livepollService;
    this.createRoomSettingService = createRoomSettingService;
  }

  public Mono<Room> get(final UUID id) {
    if (id == null) {
      return Mono.error(new BadRequestException("Room id cannot be null"));
    }
    return repository.findById(id).flatMap(this::applyTransientFields);
  }

  public Flux<Room> getByOwnerId(final UUID id) {
    return repository.findByOwnerId(id).flatMap(this::applyTransientFields);
  }

  public Mono<Room> getByShortId(final String shortId) {
    return repository
      .findByShortId(shortId)
      .flatMap(this::applyTransientFields);
  }

  public Mono<Room> getModeratorCodeRoom(final UUID parentRoomId) {
    return repository.findByModeratorRoomReference(parentRoomId);
  }

  public Mono<ModeratorAccessCode> recreateModeratorJoinCode(
    final UUID parentRoomId
  ) {
    return Mono
      .zip(
        authorizationHelper.getCurrentUser(),
        this.repository.findById(parentRoomId)
      )
      .filter(tuple2 ->
        authorizationHelper.checkCreatorOfRoom(tuple2.getT1(), tuple2.getT2())
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(r -> getModeratorCodeRoom(parentRoomId))
      .flatMap(room ->
        this.repository.deleteById(room.getId()).map(r -> parentRoomId)
      )
      .switchIfEmpty(Mono.just(parentRoomId))
      .flatMap(repository::findById)
      .flatMap(this::createModeratorCodeReference)
      .map(room -> new ModeratorAccessCode(room.getShortId()));
  }

  public Mono<Room> create(final Room room) {
    logger.trace("Creating new room: " + room.toString());
    room.setId(null);
    room.setConversationDepth(7);
    return validateRoom(room)
      .flatMap(r -> authorizationHelper.getCurrentUser())
      .map(authenticatedUser -> {
        room.setOwnerId(authenticatedUser.getAccountId());
        return room;
      })
      .flatMap(r -> {
        if (r.getShortId() != null) {
          r.setShortId(r.getShortId().trim());
          return (
              r.getShortId().length() > shortIdLength ||
              r.getShortId().length() == 0
            )
            ? Mono.error(
              new ForbiddenException("ShortId length exceeded or empty")
            )
            : Mono.just(r.getShortId());
        } else return generateShortId();
      }) // Get correct ShortId
      .map(shortId -> {
        room.setShortId(shortId.trim());
        return room;
      })
      .flatMap(repository::save) //Save Room
      .flatMap(created -> {
        if (created.getTags() != null && created.getTags().length > 0) {
          return tagRepository
            .saveAll(getListOfTags(created.getId(), created.getTags()))
            .collectList()
            .map(tagFlux -> created);
        } else return Mono.just(created);
      }) //Save Tags
      .flatMap(r -> Mono.zip(Mono.just(r), createModeratorCodeReference(r))) // Create moderator join code
      .map(Tuple2::getT1)
      .flatMap(created -> {
        return createRoomSettingService
          .createNew(created.getId())
          .thenReturn(created);
      })
      .onErrorResume(throwable -> {
        if (throwable instanceof DataIntegrityViolationException) {
          return Mono.error(new ConflictException("ShortId already exists"));
        } else if (throwable instanceof ForbiddenException) {
          return Mono.error(throwable);
        } else {
          logger.error(throwable.getMessage());
          return Mono.error(InternalServerErrorException::new);
        }
      });
  }

  public Mono<Room> patch(Room entity, final Map<String, Object> changes) {
    return authorizationHelper
      .getCurrentUser()
      .map(user ->
        this.parseRoomChanges(
            entity,
            changes,
            authorizationHelper.checkCreatorOfRoom(user, entity)
          )
      )
      .flatMap(this::validateRoom)
      .flatMap(repository::save)
      .flatMap(updated -> {
        if (!changes.containsKey("tags")) {
          return Mono.just(updated);
        }
        return tagRepository
          .deleteByRoomId(updated.getId())
          .collectList()
          .then(
            tagRepository
              .saveAll(getListOfTags(updated.getId(), updated.getTags()))
              .collectList()
          )
          .thenReturn(updated);
      });
  }

  public Mono<Void> delete(final UUID id) {
    return Mono
      .zip(authorizationHelper.getCurrentUser(), repository.findById(id))
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(tuple2 ->
        authorizationHelper.checkCreatorOfRoom(tuple2.getT1(), tuple2.getT2())
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(authenticatedUser -> repository.deleteById(id));
  }

  public Map<String, Object> calculateChanges(Room entity, Room update) {
    HashMap<String, Object> map = new HashMap<>();
    if (!Objects.equals(entity.getOwnerId(), update.getOwnerId())) {
      map.put("ownerId", update.getOwnerId());
    }
    if (!Objects.equals(entity.getName(), update.getName())) {
      map.put("name", update.getName());
    }
    if (!Objects.equals(entity.getDescription(), update.getDescription())) {
      map.put("description", update.getDescription());
    }
    if (!Objects.equals(entity.isClosed(), update.isClosed())) {
      map.put("closed", update.isClosed());
    }
    if (!Objects.equals(entity.isDirectSend(), update.isDirectSend())) {
      map.put("directSend", update.isDirectSend());
    }
    if (!Objects.equals(entity.getThreshold(), update.getThreshold())) {
      map.put("threshold", update.getThreshold());
    }
    if (!Objects.equals(entity.getBlacklist(), update.getBlacklist())) {
      map.put("blacklist", update.getBlacklist());
    }
    if (
      !Objects.equals(entity.isBlacklistActive(), update.isBlacklistActive())
    ) {
      map.put("blacklistActive", update.isBlacklistActive());
    }
    if (
      !Objects.equals(entity.getProfanityFilter(), update.getProfanityFilter())
    ) {
      map.put("profanityFilter", update.getProfanityFilter().toString());
    }
    if (
      !Objects.equals(entity.isQuestionsBlocked(), update.isQuestionsBlocked())
    ) {
      map.put("questionsBlocked", update.isQuestionsBlocked());
    }
    if (!Arrays.equals(entity.getTags(), update.getTags())) {
      map.put("tags", update.getTags());
    }
    if (
      !Objects.equals(
        entity.getTagCloudSettings(),
        update.getTagCloudSettings()
      )
    ) {
      map.put("tagCloudSettings", update.getTagCloudSettings());
    }
    if (
      !Objects.equals(
        entity.getConversationDepth(),
        update.getConversationDepth()
      )
    ) {
      map.put("conversationDepth", update.getConversationDepth());
    }
    if (
      !Objects.equals(
        entity.isBonusArchiveActive(),
        update.isBonusArchiveActive()
      )
    ) {
      map.put("bonusArchiveActive", update.isBonusArchiveActive());
    }
    if (!Objects.equals(entity.isQuizActive(), update.isQuizActive())) {
      map.put("quizActive", update.isQuizActive());
    }
    if (
      !Objects.equals(
        entity.isBrainstormingActive(),
        update.isBrainstormingActive()
      )
    ) {
      map.put("brainstormingActive", update.isBrainstormingActive());
    }
    if (!Objects.equals(entity.getLanguage(), update.getLanguage())) {
      map.put("language", update.getLanguage());
    }
    return map;
  }

  private Mono<Boolean> isShortIdAvailable(final String shortId) {
    return getByShortId(shortId)
      .flatMap(room -> Mono.just(false))
      .switchIfEmpty(Mono.just(true));
  }

  private Mono<String> generateShortId() {
    final String keyword = RandomUtils.generateNumberToken(
      generatedShortIdLength
    );
    return isShortIdAvailable(keyword)
      .flatMap(available -> {
        if (!available) return generateShortId(); else return Mono.just(
          keyword
        );
      });
  }

  private Room parseRoomChanges(
    Room entity,
    Map<String, Object> changes,
    boolean isOwner
  ) {
    changes.forEach((key, value) -> {
      switch (key) {
        case "ownerId":
          if (!isOwner) {
            throw new ForbiddenException(
              "You are not allowed to change the ownerId"
            );
          }
          entity.setOwnerId((UUID) value);
          break;
        case "name":
          entity.setName((String) value);
          break;
        case "description":
          entity.setDescription((String) value);
          break;
        case "closed":
          entity.setClosed((Boolean) value);
          break;
        case "directSend":
          entity.setDirectSend((Boolean) value);
          break;
        case "threshold":
          entity.setThreshold((Integer) value);
          break;
        case "blacklist":
          entity.setBlacklist((String) value);
          break;
        case "blacklistActive":
          entity.setBlacklistActive((Boolean) value);
          break;
        case "profanityFilter":
          entity.setProfanityFilter(
            Room.ProfanityFilter.valueOf((String) value)
          );
          break;
        case "questionsBlocked":
          entity.setQuestionsBlocked((Boolean) value);
          break;
        case "tags":
          List<String> list = (List<String>) value;
          entity.setTags(list.toArray(new String[0]));
          break;
        case "brainstormingSession":
          throw new BadRequestException(
            "Wrong endpoint for updating brainstormingSession"
          );
        case "tagCloudSettings":
          entity.setTagCloudSettings((String) value);
          break;
        case "conversationDepth":
          entity.setConversationDepth((Integer) value);
          break;
        case "bonusArchiveActive":
          entity.setBonusArchiveActive((Boolean) value);
          break;
        case "quizActive":
          entity.setQuizActive((Boolean) value);
          break;
        case "brainstormingActive":
          entity.setBrainstormingActive((Boolean) value);
          break;
        case "language":
          entity.setLanguage((String) value);
          break;
        case "livepollActive":
          entity.setLivepollActive((Boolean) value);
          break;
        case "keywordExtractionActive":
          entity.setKeywordExtractionActive((Boolean) value);
          break;
        case "radarActive":
          entity.setRadarActive((Boolean) value);
          break;
        case "focusActive":
          entity.setFocusActive((Boolean) value);
          break;
        case "chatGptActive":
          entity.setChatGptActive((Boolean) value);
          break;
        case "mode":
        case "id":
        case "shortId":
        case "moderatorRoomReference":
        case "createdAt":
        case "updatedAt":
        case "lastVisitCreator":
          throw new ForbiddenException(
            "You are not allowed to change the " + key
          );
        default:
          throw new BadRequestException(
            "Invalid ChangeAttribute provided (" + key + ")"
          );
      }
    });
    return entity;
  }

  private Mono<Room> createModeratorCodeReference(Room parentRoom) {
    final Room room = new Room();
    room.setId(null);
    room.setName(parentRoom.getName());
    room.setOwnerId(parentRoom.getOwnerId());
    room.setModeratorRoomReference(parentRoom.getId());
    return Mono
      .zip(generateShortId(), authorizationHelper.getCurrentUser())
      .filter(tuple2 ->
        authorizationHelper.checkCreatorOfRoom(tuple2.getT2(), parentRoom)
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(tuple2 -> {
        room.setShortId(tuple2.getT1());
        return Mono.just(room);
      })
      .flatMap(repository::save);
  }

  private Mono<Room> validateRoom(Room room) {
    return Mono
      .just(room)
      .filter(r ->
        r.getName() != null && r.getName().length() <= maxRoomNameLength
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Maximal Room name length exceeded or not provided"
          )
        )
      )
      .filter(r ->
        r.getDescription() == null ||
        r.getDescription().length() <= maxRoomDescriptionLength
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Maximal Room description length exceeded")
        )
      );
  }

  private List<Tag> getListOfTags(UUID roomId, String[] tags) {
    return Arrays
      .stream(tags)
      .map(s -> new Tag(roomId, s))
      .collect(Collectors.toList());
  }

  private Mono<Room> applyTransientFields(Room room) {
    return Mono
      .zip(
        tagRepository.findByRoomId(room.getId()).collectList(),
        sessionService
          .getActiveSessionByRoomId(room.getId())
          .switchIfEmpty(Mono.just(new BrainstormingSession())),
        livepollService
          .getActiveByRoomId(room.getId())
          .switchIfEmpty(Mono.just(new LivepollSession()))
      )
      .map(tuple -> {
        room.setTags(
          tuple.getT1().stream().map(Tag::getTag).toArray(String[]::new)
        );
        final BrainstormingSession session = tuple.getT2();
        room.setBrainstormingSession(session.isNew() ? null : session);
        final LivepollSession livepoll = tuple.getT3();
        room.setLivepollSession(livepoll.isNew() ? null : livepoll);
        return room;
      });
  }
}
