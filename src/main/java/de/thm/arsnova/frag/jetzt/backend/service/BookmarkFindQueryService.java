package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Bookmark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class BookmarkFindQueryService {

    private static final Logger logger = LoggerFactory.getLogger(BookmarkFindQueryService.class);

    private final BookmarkService service;

    @Autowired
    public BookmarkFindQueryService(BookmarkService service) {
        this.service = service;
    }

    public Flux<Bookmark> resolveQuery(final FindQuery<Bookmark> findQuery) {
        if (findQuery.getProperties().getRoomId() != null) {
            return service.getByRoomIdAndUser(findQuery.getProperties().getRoomId());
        }
        return Flux.empty();
    }
}
