package de.thm.arsnova.frag.jetzt.backend.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatchQuery {
    
    private Map<String, Object> changes;

    @JsonProperty("changes")
    public Map<String, Object> getChanges() {
        return changes;
    }

    @JsonProperty("changes")
    public void setChanges(Map<String, Object> changes) {
        this.changes = changes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((changes == null) ? 0 : changes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PatchQuery other = (PatchQuery) obj;
        if (changes == null) {
            if (other.changes != null)
                return false;
        } else if (!changes.equals(other.changes))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PatchQuery [changes=" + changes + "]";
    }

}
