package de.thm.arsnova.frag.jetzt.backend.model.command;

public class DeleteBrainstorming extends WebSocketCommand<DeleteBrainstormingPayload> {

    public DeleteBrainstorming() {
        super(DeleteBrainstorming.class.getSimpleName());
    }

    public DeleteBrainstorming(DeleteBrainstormingPayload payload) {
        super(DeleteBrainstorming.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstorming that = (CreateBrainstorming) o;
        return this.getPayload().equals(that.getPayload());
    }
}
