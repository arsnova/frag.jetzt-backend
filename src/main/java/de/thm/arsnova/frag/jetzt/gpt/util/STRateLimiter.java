package de.thm.arsnova.frag.jetzt.gpt.util;

import java.time.Duration;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Single Token Rate Limiter
 */
public class STRateLimiter {

  private static class Token {

    private long nextAvailable = 0;
    private Token nexToken = null;
    private Token previousToken = null;

    boolean claim(long millis, long duration) {
      if (millis <= nextAvailable) {
        return false;
      }
      nextAvailable = millis + duration;
      return true;
    }

    long nextTime(long millis) {
      return Math.max(0, millis - nextAvailable);
    }
  }

  private final long timeFrameMillis;
  private Integer maxTokens = null;
  private int tokenCount = 0;
  private Token headToken = null;
  private Token lastToken = null;
  private final int maxQueueCount;
  private int currentQueueCount;

  public STRateLimiter(Duration timeFrame, int queueCount) {
    this.timeFrameMillis = timeFrame.toMillis();
    this.maxQueueCount = queueCount;
  }

  public STRateLimiter(Duration timeFrame, int queueCount, int maxTokens) {
    this.timeFrameMillis = timeFrame.toMillis();
    this.maxQueueCount = queueCount;
    this.maxTokens = maxTokens;
    this.tokenCount = maxTokens;
    headToken = new Token();
    if (maxTokens < 2) {
      lastToken = headToken;
      return;
    }
    lastToken = new Token();
    Token previousToken = headToken;
    int count = maxTokens - 2;
    while (count-- > 0) {
      previousToken.nexToken = new Token();
      previousToken.nexToken.previousToken = previousToken;
      previousToken = previousToken.nexToken;
    }
    previousToken.nexToken = lastToken;
    lastToken.previousToken = previousToken;
  }

  public <T> Mono<T> ensureLimits(Mono<T> action) {
    if (claimToken(false)) {
      return action.onErrorResume(
        WebClientResponseException.class,
        err -> {
          if (err.getStatusCode() == HttpStatus.TOO_MANY_REQUESTS) {
            this.onRateLimitFailure();
          }
          return Mono.error(err);
        }
      );
    }
    return tryQueue(action);
  }

  public <T> Flux<T> ensureLimits(Flux<T> action) {
    if (claimToken(false)) {
      return action.onErrorResume(
        WebClientResponseException.class,
        err -> {
          if (err.getStatusCode() == HttpStatus.TOO_MANY_REQUESTS) {
            this.onRateLimitFailure();
          }
          return Mono.error(err);
        }
      );
    }
    return tryQueue(action);
  }

  private synchronized <T> Flux<T> tryQueue(Flux<T> action) {
    final int border = maxQueueCount < 0
      ? maxTokens
      : Math.min(maxQueueCount, maxTokens);
    if (currentQueueCount >= border) {
      return Flux.error(
        new ResponseStatusException(
          HttpStatus.TOO_MANY_REQUESTS,
          "OpenAI rate limit exceeded (" + maxTokens + "|" + border + ")"
        )
      );
    }
    Token nextToken = headToken;
    int count = currentQueueCount;
    while (count-- > 0) {
      nextToken = nextToken.nexToken;
    }
    currentQueueCount += 1;
    return action
      .delaySubscription(
        Duration.ofMillis(nextToken.nextTime(System.currentTimeMillis()))
      )
      .doOnSubscribe(sub -> decreaseQueueCount())
      .onErrorResume(
        WebClientResponseException.class,
        err -> {
          if (err.getStatusCode() == HttpStatus.TOO_MANY_REQUESTS) {
            this.onRateLimitFailure();
          }
          return Mono.error(err);
        }
      );
  }

  private synchronized <T> Mono<T> tryQueue(Mono<T> action) {
    final int border = maxQueueCount < 0
      ? maxTokens
      : Math.min(maxQueueCount, maxTokens);
    if (currentQueueCount >= border) {
      return Mono.error(
        new ResponseStatusException(
          HttpStatus.TOO_MANY_REQUESTS,
          "OpenAI rate limit exceeded (" + maxTokens + "|" + border + ")"
        )
      );
    }
    Token nextToken = headToken;
    int count = currentQueueCount;
    while (count-- > 0) {
      nextToken = nextToken.nexToken;
    }
    currentQueueCount += 1;
    return action
      .delaySubscription(
        Duration.ofMillis(nextToken.nextTime(System.currentTimeMillis()))
      )
      .doOnSubscribe(sub -> decreaseQueueCount())
      .onErrorResume(
        WebClientResponseException.class,
        err -> {
          if (err.getStatusCode() == HttpStatus.TOO_MANY_REQUESTS) {
            this.onRateLimitFailure();
          }
          return Mono.error(err);
        }
      );
  }

  private synchronized void decreaseQueueCount() {
    currentQueueCount--;
    claimToken(true);
  }

  private synchronized void onRateLimitFailure() {
    if (tokenCount < 2) {
      return;
    }
    tokenCount -= 1;
    maxTokens = tokenCount;
    lastToken.previousToken.nexToken = null;
    lastToken = lastToken.previousToken;
  }

  private synchronized boolean claimToken(boolean inQueue) {
    if (currentQueueCount > 0 && !inQueue) {
      return false;
    }
    long current = System.currentTimeMillis();
    if (headToken == null) {
      headToken = new Token();
      lastToken = headToken;
      headToken.claim(current, this.timeFrameMillis);
      tokenCount += 1;
      return true;
    }
    if (!headToken.claim(current, this.timeFrameMillis)) {
      if (maxTokens == null) {
        lastToken.nexToken = new Token();
        lastToken.nexToken.previousToken = lastToken;
        lastToken = lastToken.nexToken;
        lastToken.claim(current, this.timeFrameMillis);
        tokenCount += 1;
        return true;
      }
      return false;
    }
    Token next = headToken.nexToken;
    if (next == null) {
      return true;
    }
    lastToken.nexToken = headToken;
    headToken.nexToken = null;
    headToken.previousToken = lastToken;
    lastToken = headToken;
    headToken = next;
    next.previousToken = null;
    return true;
  }
}
