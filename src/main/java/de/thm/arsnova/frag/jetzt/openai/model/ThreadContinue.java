package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThreadContinue {

  @JsonProperty("assistant_id")
  private String assistantId;

  @JsonProperty("additional_messages")
  private List<ThreadMessage> additionalMessages;

  private boolean stream = true;

  public ThreadContinue() {}

  public String getAssistantId() {
    return assistantId;
  }

  public void setAssistantId(String assistantId) {
    this.assistantId = assistantId;
  }

  public List<ThreadMessage> getAdditionalMessages() {
    return additionalMessages;
  }

  public void setAdditionalMessages(List<ThreadMessage> additionalMessages) {
    this.additionalMessages = additionalMessages;
  }

  public boolean isStream() {
    return stream;
  }

  @Override
  public String toString() {
    return (
      "ThreadContinue [assistantId=" +
      assistantId +
      ", additionalMessages=" +
      additionalMessages +
      ", stream=" +
      stream +
      "]"
    );
  }
}
