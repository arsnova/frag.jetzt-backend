package de.thm.arsnova.frag.jetzt.quota.model.persistence;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class QuotaEntry implements Persistable<UUID> {

  // MON 05.01.1970 00:00:00
  private static final Instant EPOCH = Instant.ofEpochMilli(345_600_000);

  private static ZonedDateTime getReference(ZonedDateTime date) {
    ZonedDateTime reference = EPOCH
      .atZone(date.getZone())
      .withHour(0)
      .withMinute(0)
      .withSecond(0);
    if (reference.getDayOfWeek().getValue() == 7) {
      reference = reference.plusDays(1);
    }
    return reference;
  }

  public enum ResetStrategy {
    DAILY {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        ZoneId z = date.getZone();
        ZonedDateTime last = lastReset.toLocalDate().atStartOfDay(z);
        ZonedDateTime current = date.toLocalDate().atStartOfDay(z);
        return ChronoUnit.DAYS.between(last, current) / factor > 0;
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        return quota;
      }
    },
    WEEKLY {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        ZonedDateTime reference = getReference(date);
        long start = ChronoUnit.WEEKS.between(reference, date) / factor;
        long current = ChronoUnit.WEEKS.between(reference, lastReset) / factor;
        return start != current;
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        return quota;
      }
    },
    WEEKLY_FLOWING {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        return WEEKLY.isReset(factor, lastReset, date);
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        ZonedDateTime reference = getReference(date);
        long diff = ChronoUnit.WEEKS.between(reference, date) / factor;
        ZonedDateTime start = reference.plusWeeks(diff * factor);
        ZonedDateTime end = start.plusWeeks(factor);
        long diff2 = ChronoUnit.DAYS.between(start, end);
        long diff3 = ChronoUnit.DAYS.between(start, date);
        BigInteger q = BigInteger.valueOf(quota);
        q = q.multiply(BigInteger.valueOf(diff3));
        q = q.divide(BigInteger.valueOf(diff2));
        return q.longValue();
      }
    },
    MONTHLY {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        int current = date.getYear() * 12 + date.getMonth().ordinal();
        int last = lastReset.getYear() * 12 + lastReset.getMonth().ordinal();
        return last / factor != current / factor;
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        return quota;
      }
    },
    MONTHLY_FLOWING {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        return MONTHLY.isReset(factor, lastReset, date);
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        int d = date.getYear() * 12 + date.getMonth().ordinal();
        final int adjusted = d / factor * factor;
        ZonedDateTime start = date.withYear(adjusted / 12);
        start = start.withMonth(adjusted % 12);
        start = start.withDayOfMonth(1);
        ZonedDateTime end = start.plusMonths(factor);
        long diff = ChronoUnit.DAYS.between(start, end);
        long diff2 = ChronoUnit.DAYS.between(start, date);
        BigInteger q = BigInteger.valueOf(quota);
        q = q.multiply(BigInteger.valueOf(diff2));
        q = q.divide(BigInteger.valueOf(diff));
        return q.longValue();
      }
    },
    YEARLY {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        return lastReset.getYear() / factor != date.getYear() / factor;
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        return quota;
      }
    },
    YEARLY_FLOWING {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        return YEARLY.isReset(factor, lastReset, date);
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        int d = date.getYear() / factor;
        ZonedDateTime start = date.withYear(d * factor);
        start = start.withDayOfYear(1);
        ZonedDateTime end = start.plusYears(factor);
        long diff = ChronoUnit.DAYS.between(start, end);
        long diff2 = ChronoUnit.DAYS.between(start, date);
        BigInteger q = BigInteger.valueOf(quota);
        q = q.multiply(BigInteger.valueOf(diff2));
        q = q.divide(BigInteger.valueOf(diff));
        return q.longValue();
      }
    },
    NEVER {
      @Override
      public boolean isReset(
        int factor,
        ZonedDateTime lastReset,
        ZonedDateTime date
      ) {
        return false;
      }

      @Override
      public long getQuota(int factor, ZonedDateTime date, long quota) {
        return quota;
      }
    };

    public abstract boolean isReset(
      int factor,
      ZonedDateTime lastReset,
      ZonedDateTime date
    );

    public abstract long getQuota(int factor, ZonedDateTime date, long quota);
  }

  @Id
  private UUID id;

  @NonNull
  private UUID quotaId;

  @Nullable
  private Timestamp startDate;

  @Nullable
  private Timestamp endDate;

  @NonNull
  private long quota;

  @NonNull
  private long counter;

  @NonNull
  private long resetCounter;

  @NonNull
  private Timestamp lastReset = Timestamp.from(Instant.now());

  @NonNull
  private ResetStrategy resetStrategy;

  @NonNull
  private int resetFactor;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public QuotaEntry() {}

  public QuotaEntry(
    Timestamp startDate,
    Timestamp endDate,
    long quota,
    ResetStrategy resetStrategy,
    int resetFactor
  ) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.quota = quota;
    this.resetStrategy = resetStrategy;
    this.resetFactor = resetFactor;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getQuotaId() {
    return quotaId;
  }

  public void setQuotaId(UUID quotaId) {
    this.quotaId = quotaId;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  public long getQuota() {
    return quota;
  }

  public void setQuota(long quota) {
    this.quota = quota;
  }

  public long getCounter() {
    return counter;
  }

  public void setCounter(long counter) {
    this.counter = counter;
  }

  public long getResetCounter() {
    return resetCounter;
  }

  public void setResetCounter(long resetCounter) {
    this.resetCounter = resetCounter;
  }

  public Timestamp getLastReset() {
    return lastReset;
  }

  public void setLastReset(Timestamp lastReset) {
    this.lastReset = lastReset;
  }

  public ResetStrategy getResetStrategy() {
    return resetStrategy;
  }

  public void setResetStrategy(ResetStrategy resetStrategy) {
    this.resetStrategy = resetStrategy;
  }

  public int getResetFactor() {
    return resetFactor;
  }

  public void setResetFactor(int resetFactor) {
    this.resetFactor = resetFactor;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((quotaId == null) ? 0 : quotaId.hashCode());
    result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
    result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
    result = prime * result + (int) (quota ^ (quota >>> 32));
    result = prime * result + (int) (counter ^ (counter >>> 32));
    result = prime * result + (int) (resetCounter ^ (resetCounter >>> 32));
    result = prime * result + ((lastReset == null) ? 0 : lastReset.hashCode());
    result =
      prime * result + ((resetStrategy == null) ? 0 : resetStrategy.hashCode());
    result = prime * result + resetFactor;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    QuotaEntry other = (QuotaEntry) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (quotaId == null) {
      if (other.quotaId != null) return false;
    } else if (!quotaId.equals(other.quotaId)) return false;
    if (startDate == null) {
      if (other.startDate != null) return false;
    } else if (!startDate.equals(other.startDate)) return false;
    if (endDate == null) {
      if (other.endDate != null) return false;
    } else if (!endDate.equals(other.endDate)) return false;
    if (quota != other.quota) return false;
    if (counter != other.counter) return false;
    if (resetCounter != other.resetCounter) return false;
    if (lastReset == null) {
      if (other.lastReset != null) return false;
    } else if (!lastReset.equals(other.lastReset)) return false;
    if (resetStrategy != other.resetStrategy) return false;
    if (resetFactor != other.resetFactor) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "QuotaEntry [id=" +
      id +
      ", quotaId=" +
      quotaId +
      ", startDate=" +
      startDate +
      ", endDate=" +
      endDate +
      ", quota=" +
      quota +
      ", counter=" +
      counter +
      ", resetCounter=" +
      resetCounter +
      ", lastReset=" +
      lastReset +
      ", resetStrategy=" +
      resetStrategy +
      ", resetFactor=" +
      resetFactor +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
