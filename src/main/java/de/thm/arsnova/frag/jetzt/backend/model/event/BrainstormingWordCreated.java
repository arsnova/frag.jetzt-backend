package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingWordCreated extends WebSocketEvent<BrainstormingWordCreatedPayload> {

    public BrainstormingWordCreated() {
        super(BrainstormingWordCreated.class.getSimpleName());
    }

    public BrainstormingWordCreated(BrainstormingWordCreatedPayload payload, UUID roomId) {
        super(BrainstormingWordCreated.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingWordCreated that = (BrainstormingWordCreated) o;
        return this.getPayload().equals(that.getPayload());
    }

}
