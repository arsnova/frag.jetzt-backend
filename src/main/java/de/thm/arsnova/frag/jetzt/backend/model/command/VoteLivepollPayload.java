package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import java.util.UUID;

public class VoteLivepollPayload implements WebSocketPayload {

  private UUID sessionId;
  private Integer voteIndex;

  public VoteLivepollPayload(UUID sessionId, Integer voteIndex) {
    this.sessionId = sessionId;
    this.voteIndex = voteIndex;
  }

  public VoteLivepollPayload() {}

  public UUID getSessionId() {
    return sessionId;
  }

  public void setSessionId(UUID sessionId) {
    this.sessionId = sessionId;
  }

  public Integer getVoteIndex() {
    return voteIndex;
  }

  public void setVoteIndex(Integer voteIndex) {
    this.voteIndex = voteIndex;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    result = prime * result + ((voteIndex == null) ? 0 : voteIndex.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    VoteLivepollPayload other = (VoteLivepollPayload) obj;
    if (sessionId == null) {
      if (other.sessionId != null) return false;
    } else if (!sessionId.equals(other.sessionId)) return false;
    if (voteIndex == null) {
      if (other.voteIndex != null) return false;
    } else if (!voteIndex.equals(other.voteIndex)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "VoteLivepollPayload [sessionId=" +
      sessionId +
      ", voteIndex=" +
      voteIndex +
      "]"
    );
  }
}
