package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTConversation;
import reactor.core.publisher.Flux;

@Repository
public interface GPTConversationRepository extends ReactiveCrudRepository<GPTConversation, UUID> {
 
    Flux<GPTConversation> findAllByAccountId(UUID accountId);
}
