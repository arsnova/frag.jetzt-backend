package de.thm.arsnova.frag.jetzt.notification.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

public class CommentChangeEventPayload implements WebSocketPayload {

    private UUID id;
    private UUID commentId;
    private UUID roomId;
    private CommentInterestBit type;
    private String previousValueString;
    private String currentValueString;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private UUID initiatorId;
    private CommentChange.UserRole initiatorRole;
    private String commentNumber;
    private UUID commentCreatorId;
    private boolean isAnswer;
    private String roomName;
    private String roomShortId;

    public CommentChangeEventPayload() {

    }

    public CommentChangeEventPayload(CommentChange commentChange) {
        if (commentChange == null) {
            return;
        }
        id = commentChange.getId();
        commentId = commentChange.getCommentId();
        roomId = commentChange.getRoomId();
        type = commentChange.getType();
        previousValueString = commentChange.getPreviousValueString();
        currentValueString = commentChange.getCurrentValueString();
        createdAt = commentChange.getCreatedAt();
        updatedAt = commentChange.getUpdatedAt();
        initiatorId = commentChange.getInitiatorId();
        initiatorRole = commentChange.getInitiatorRole();
        commentNumber = commentChange.getComment().getNumber();
        commentCreatorId = commentChange.getComment().getCreatorId();
        isAnswer = commentChange.getComment().getCommentReference() != null;
        roomName = commentChange.getRoom().getName();
        roomShortId = commentChange.getRoom().getShortId();
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty("commentId")
    public UUID getCommentId() {
        return commentId;
    }

    @JsonProperty("commentId")
    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    @JsonProperty("roomId")
    public UUID getRoomId() {
        return roomId;
    }

    @JsonProperty("roomId")
    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    @JsonProperty("type")
    public CommentInterestBit getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(CommentInterestBit type) {
        this.type = type;
    }

    @JsonProperty("previousValueString")
    public String getPreviousValueString() {
        return previousValueString;
    }

    @JsonProperty("previousValueString")
    public void setPreviousValueString(String previousValueString) {
        this.previousValueString = previousValueString;
    }

    @JsonProperty("currentValueString")
    public String getCurrentValueString() {
        return currentValueString;
    }

    @JsonProperty("currentValueString")
    public void setCurrentValueString(String currentValueString) {
        this.currentValueString = currentValueString;
    }

    @JsonProperty("createdAt")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updatedAt")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("initiatorId")
    public UUID getInitiatorId() {
        return initiatorId;
    }

    @JsonProperty("initiatorId")
    public void setInitiatorId(UUID initiatorId) {
        this.initiatorId = initiatorId;
    }

    @JsonProperty("initiatorRole")
    public CommentChange.UserRole getInitiatorRole() {
        return initiatorRole;
    }

    @JsonProperty("initiatorRole")
    public void setInitiatorRole(CommentChange.UserRole initiatorRole) {
        this.initiatorRole = initiatorRole;
    }

    @JsonProperty("commentNumber")
    public String getCommentNumber() {
        return commentNumber;
    }

    @JsonProperty("commentNumber")
    public void setCommentNumber(String commentNumber) {
        this.commentNumber = commentNumber;
    }

    @JsonProperty("commentCreatorId")
    public UUID getCommentCreatorId() {
        return commentCreatorId;
    }

    @JsonProperty("commentCreatorId")
    public void setCommentCreatorId(UUID commentCreatorId) {
        this.commentCreatorId = commentCreatorId;
    }

    @JsonProperty("isAnswer")
    public boolean isAnswer() {
        return isAnswer;
    }

    @JsonProperty("isAnswer")
    public void setAnswer(boolean answer) {
        isAnswer = answer;
    }

    @JsonProperty("roomName")
    public String getRoomName() {
        return roomName;
    }

    @JsonProperty("roomName")
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @JsonProperty("roomShortId")
    public String getRoomShortId() {
        return roomShortId;
    }

    @JsonProperty("roomShortId")
    public void setRoomShortId(String roomShortId) {
        this.roomShortId = roomShortId;
    }

    @Override
    public String toString() {
        return "CommentChangeEventPayload{" +
                "id=" + id +
                ", commentId=" + commentId +
                ", roomId=" + roomId +
                ", type=" + type +
                ", previousValueString='" + previousValueString + '\'' +
                ", currentValueString='" + currentValueString + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", initiatorId=" + initiatorId +
                ", initiatorRole=" + initiatorRole +
                ", commentNumber='" + commentNumber + '\'' +
                ", commentCreatorId=" + commentCreatorId +
                ", isAnswer=" + isAnswer +
                ", roomName='" + roomName + '\'' +
                ", roomShortId='" + roomShortId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentChangeEventPayload payload = (CommentChangeEventPayload) o;
        return isAnswer == payload.isAnswer &&
                Objects.equals(id, payload.id) &&
                Objects.equals(commentId, payload.commentId) &&
                Objects.equals(roomId, payload.roomId) &&
                type == payload.type &&
                Objects.equals(previousValueString, payload.previousValueString) &&
                Objects.equals(currentValueString, payload.currentValueString) &&
                Objects.equals(createdAt, payload.createdAt) &&
                Objects.equals(updatedAt, payload.updatedAt) &&
                Objects.equals(initiatorId, payload.initiatorId) &&
                initiatorRole == payload.initiatorRole &&
                Objects.equals(commentNumber, payload.commentNumber) &&
                Objects.equals(commentCreatorId, payload.commentCreatorId) &&
                Objects.equals(roomName, payload.roomName) &&
                Objects.equals(roomShortId, payload.roomShortId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, commentId, roomId, type, previousValueString, currentValueString, createdAt, updatedAt, initiatorId, initiatorRole, commentNumber, commentCreatorId, isAnswer, roomName, roomShortId);
    }
}
