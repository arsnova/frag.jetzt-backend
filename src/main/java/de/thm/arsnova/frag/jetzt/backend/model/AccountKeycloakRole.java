package de.thm.arsnova.frag.jetzt.backend.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class AccountKeycloakRole implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private String role;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public AccountKeycloakRole() {}

  public AccountKeycloakRole(UUID accountId, String role) {
    this.accountId = accountId;
    this.role = role;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AccountKeycloakRole other = (AccountKeycloakRole) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (role == null) {
      if (other.role != null) return false;
    } else if (!role.equals(other.role)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "AccountKeycloakRole [id=" +
      id +
      ", accountId=" +
      accountId +
      ", role=" +
      role +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
