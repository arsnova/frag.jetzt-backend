package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;

public class UpdateRoomPayload implements WebSocketPayload {

    private Room room;

    public UpdateRoomPayload() {

    }

    public UpdateRoomPayload(Room room) {
        this.room = room;
    }

    @JsonProperty("room")
    public Room getRoom() {
        return room;
    }

    @JsonProperty("room")
    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "UpdateRoomPayload{" +
                "room=" + room +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateRoomPayload that = (UpdateRoomPayload) o;
        return Objects.equals(room, that.room);
    }

    @Override
    public int hashCode() {
        return Objects.hash(room);
    }
}
