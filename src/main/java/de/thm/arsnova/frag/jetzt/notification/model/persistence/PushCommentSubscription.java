package de.thm.arsnova.frag.jetzt.notification.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.arsnova.frag.jetzt.notification.model.CommentInterestBit;
import de.thm.arsnova.frag.jetzt.notification.model.ICommentSubscription;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class PushCommentSubscription
  implements Persistable<UUID>, ICommentSubscription {

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @NonNull
  private UUID roomId;

  @NonNull
  private UUID commentId;

  @NonNull
  private long interestBits;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public PushCommentSubscription() {}

  public PushCommentSubscription(
    UUID accountId,
    UUID roomId,
    UUID commentId,
    long interestBits
  ) {
    this.accountId = accountId;
    this.roomId = roomId;
    this.commentId = commentId;
    this.interestBits = interestBits;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public UUID getCommentId() {
    return commentId;
  }

  public void setCommentId(UUID commentId) {
    this.commentId = commentId;
  }

  public long getInterestBits() {
    return interestBits;
  }

  public void setInterestBits(long interestBits) {
    this.interestBits = interestBits;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
    result = prime * result + (int) (interestBits ^ (interestBits >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    PushCommentSubscription other = (PushCommentSubscription) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (commentId == null) {
      if (other.commentId != null) return false;
    } else if (!commentId.equals(other.commentId)) return false;
    if (interestBits != other.interestBits) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "PushCommentSubscription [id=" +
      id +
      ", accountId=" +
      accountId +
      ", roomId=" +
      roomId +
      ", commentId=" +
      commentId +
      ", interestBits=" +
      interestBits +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }

  @Override
  @JsonIgnore
  public SubscriptionResult hasSubscribed(CommentChange change) {
    if (!change.getCommentId().equals(commentId)) {
      return SubscriptionResult.NONE;
    }
    if (!change.getType().hasInterest(interestBits)) {
      return SubscriptionResult.NONE;
    }
    boolean ownsComment = change.getCommentCreatorId().equals(accountId);
    boolean isInitiator = change.getInitiatorId().equals(accountId);
    if (ownsComment) {
      return isInitiator
        ? SubscriptionResult.OWN_OWN
        : SubscriptionResult.OWN_OTHER;
    }
    return isInitiator
      ? SubscriptionResult.OTHER_OWN
      : SubscriptionResult.OTHER_OTHER;
  }
}
