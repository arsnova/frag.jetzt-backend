package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.BonusToken;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;


public interface BonusTokenRepository extends ReactiveCrudRepository<BonusToken, UUID> {
    Flux<BonusToken> findByRoomId(UUID roomId);
    Flux<BonusToken> findByAccountId(UUID accountId);
    Mono<Void> deleteByCommentIdAndAccountId(UUID commentId, UUID accountId);
}
