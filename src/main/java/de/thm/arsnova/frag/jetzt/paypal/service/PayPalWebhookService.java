package de.thm.arsnova.frag.jetzt.paypal.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ongres.scram.common.bouncycastle.base64.Base64;
import de.thm.arsnova.frag.jetzt.openai.service.ErrorHandler;
import de.thm.arsnova.frag.jetzt.paypal.config.PayPalConfig;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.CRC32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class PayPalWebhookService {

  private final ObjectMapper mapper;
  private final String webhookId;
  private final HashMap<String, X509Certificate> cachedCertficates = new HashMap<>();
  private final Logger logger = LoggerFactory.getLogger(
    PayPalWebhookService.class.getName()
  );

  public PayPalWebhookService(ObjectMapper mapper, PayPalConfig config) {
    this.mapper = mapper;
    this.webhookId = config.getWebhookId();
  }

  public void receiveWebhookData(Map<String, String> headers, String body) {
    this.verifyData(headers, body)
      .subscribe(
        valid -> {
          if (valid == null || valid == false) {
            logger.error("Received invalid webhook data!");
            return;
          }
          try {
            Map<?, ?> data = mapper.readValue(body, Map.class);
            System.out.println(data);
          } catch (JsonProcessingException e) {
            e.printStackTrace();
          }
        },
        e -> {
          logger.error("Error during validation of webhook data", e);
        }
      );
  }

  private Mono<Boolean> verifyData(Map<String, String> headers, String body) {
    try {
      // Build up the message
      String transmissionId = getHeader(headers, "paypal-transmission-id");
      String timeStamp = getHeader(headers, "paypal-transmission-time");
      long crc = getCRC(body);
      String message =
        transmissionId + "|" + timeStamp + "|" + webhookId + "|" + crc;
      // Verify with signatue and certificate
      String certUrl = getHeader(headers, "paypal-cert-url");
      String signature = getHeader(headers, "paypal-transmission-sig");
      String algorithm = getHeader(headers, "paypal-auth-algo");
      if (
        certUrl == null ||
        !(
          certUrl.startsWith(
            "https://api.paypal.com/v1/notifications/certs/CERT-"
          ) ||
          certUrl.startsWith(
            "https://api.sandbox.paypal.com/v1/notifications/certs/CERT-"
          )
        )
      ) {
        throw new IllegalArgumentException(
          "Invalid certificate URL: " + certUrl
        );
      }
      return getPublicKeyByUrl(certUrl)
        .map(key -> verifySignature(key, message, signature, algorithm));
    } catch (Exception e) {
      logger.error("Error during data verify", e);
      return Mono.just(false);
    }
  }

  private boolean verifySignature(
    PublicKey key,
    String message,
    String messageSignature,
    String signatureAlgorithm
  ) {
    if (!signatureAlgorithm.equals("SHA256withRSA")) {
      logger.error("Invalid signature algorithm");
      return false;
    }
    try {
      byte[] signature = Base64.decode(messageSignature);
      Signature verifier = Signature.getInstance(signatureAlgorithm);
      verifier.initVerify(key);
      verifier.update(message.getBytes(StandardCharsets.UTF_8));
      return verifier.verify(signature);
    } catch (Exception e) {
      logger.error("Error during signature verify", e);
      return false;
    }
  }

  private Mono<PublicKey> getPublicKeyByUrl(String url) {
    X509Certificate current = cachedCertficates.get(url);
    if (current != null) {
      return Mono.just(current.getPublicKey());
    }
    return WebClient
      .create(url)
      .get()
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          String.class,
          "Failed to get certificate",
          response
        )
      )
      .flatMap(certPem -> {
        CertificateFactory certFactory;
        try {
          certFactory = CertificateFactory.getInstance("X.509");
          byte[] certBytes = Base64.decode(
            certPem
              .replace("-----BEGIN CERTIFICATE-----", "")
              .replace("-----END CERTIFICATE-----", "")
              .replaceAll("\\s", "")
          );
          X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(
            new ByteArrayInputStream(certBytes)
          );
          return Mono.just(certificate);
        } catch (CertificateException e) {
          e.printStackTrace();
          return Mono.error(e);
        }
      })
      .map(certificate -> {
        cachedCertficates.put(url, certificate);
        return certificate.getPublicKey();
      });
  }

  private String getHeader(Map<String, String> headers, String key) {
    String header = headers.get(key);
    if (header == null) {
      header = headers.get(key.toUpperCase());
    }
    if (header == null) {
      throw new IllegalArgumentException("Header " + key + " not found");
    }
    return header;
  }

  private long getCRC(String body) {
    CRC32 crc = new CRC32();
    crc.update(body.getBytes(StandardCharsets.UTF_8));
    return crc.getValue();
  }
}
