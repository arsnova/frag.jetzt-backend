package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class BrainstormingVotesResetPayload implements WebSocketPayload {
    private UUID sessionId;

    public BrainstormingVotesResetPayload() {
    }

    public BrainstormingVotesResetPayload(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "BrainstormingVotesResetPayload{" +
                "sessionId=" + sessionId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingVotesResetPayload that = (BrainstormingVotesResetPayload) o;
        return Objects.equals(sessionId, that.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId);
    }
}
