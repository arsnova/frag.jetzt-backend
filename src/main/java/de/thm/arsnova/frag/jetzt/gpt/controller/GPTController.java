package de.thm.arsnova.frag.jetzt.gpt.controller;

import de.thm.arsnova.frag.jetzt.backend.controller.AbstractEntityController;
import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.model.PatchQuery;
import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTConfigRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTGlobalAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTModerationWrapped;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomPreset;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTPromptPreset;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRating;
import de.thm.arsnova.frag.jetzt.gpt.model.requestStatistic.GPTRequestStatisticResult;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.IGPTStreamObject;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTAccessService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTAdminConfigService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTPromptPresetService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTRatingService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTRoomPresetService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTService;
import de.thm.arsnova.frag.jetzt.gpt.service.GPTStatisticService;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTUserRepository;
import de.thm.arsnova.frag.jetzt.gpt.util.GPTModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("GPTController")
@RequestMapping(GPTController.REQUEST_MAPPING)
public class GPTController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    GPTController.class
  );

  protected static final String REQUEST_MAPPING = "/gpt";
  private static final String MODELS = "/models";
  private static final String CONFIG = "/config";
  private static final String CHAT = "/chat";
  private static final String COMPLETION = "/completion";
  private static final String STREAM_COMPLETION = "/stream-completion";
  private static final String STATUS = "/status/{roomId}";
  private static final String PLAIN_STATUS = "/plain-status";
  private static final String ROOM_CONVERSATIONS = "/room-conversations";
  private static final String INTERRUPT_STREAM = "/interrupt-stream";
  private static final String ROOM_PRESET = "/room-preset/{roomId}";
  private static final String CONSENTED = "/gdpr";
  private static final String RATING = "/rating";
  private static final String RATING_TEXTS = "/rating-texts";
  private static final String RATING_INFO = "/rating-info";
  private static final String PROMPT_PRESET = "/prompt-preset";
  private static final String GLOBAL_PROMPT_PRESET = "/global-prompt-preset";
  private static final String PROMPT_PRESETS = "/prompt-presets";
  private static final String GLOBAL_PROMPT_PRESETS = "/global-prompt-presets";

  private final GPTService service;
  private final GPTAccessService accessService;
  private final GPTRoomPresetService presetService;
  private final GPTPromptPresetService promptPresetService;
  private final AuthorizationHelper helper;
  private final GPTRatingService ratingService;
  private final GPTAdminConfigService configService;
  private final GPTUserRepository userRepository;
  private final GPTStatisticService statisticService;

  @Autowired
  public GPTController(
    GPTService service,
    GPTAccessService accessService,
    AuthorizationHelper helper,
    GPTRoomPresetService presetService,
    GPTPromptPresetService promptPresetService,
    GPTRatingService ratingService,
    GPTAdminConfigService configService,
    GPTUserRepository userRepository,
    GPTStatisticService statisticService
  ) {
    this.service = service;
    this.accessService = accessService;
    this.helper = helper;
    this.presetService = presetService;
    this.promptPresetService = promptPresetService;
    this.ratingService = ratingService;
    this.configService = configService;
    this.userRepository = userRepository;
    this.statisticService = statisticService;
  }

  public static class PromptPreset {

    private String act;
    private String prompt;
    private String language;
    private float temperature;
    private float presencePenalty;
    private float frequencyPenalty;
    private float topP;

    public String getAct() {
      return act;
    }

    public void setAct(String act) {
      this.act = act;
    }

    public String getPrompt() {
      return prompt;
    }

    public void setPrompt(String prompt) {
      this.prompt = prompt;
    }

    public String getLanguage() {
      return language;
    }

    public void setLanguage(String language) {
      this.language = language;
    }

    public float getTemperature() {
      return temperature;
    }

    public void setTemperature(float temperature) {
      this.temperature = temperature;
    }

    public float getPresencePenalty() {
      return presencePenalty;
    }

    public void setPresencePenalty(float presencePenalty) {
      this.presencePenalty = presencePenalty;
    }

    public float getFrequencyPenalty() {
      return frequencyPenalty;
    }

    public void setFrequencyPenalty(float frequencyPenalty) {
      this.frequencyPenalty = frequencyPenalty;
    }

    public float getTopP() {
      return topP;
    }

    public void setTopP(float topP) {
      this.topP = topP;
    }
  }

  public static class TrialRequest {

    private String trialCode;

    public String getTrialCode() {
      return trialCode;
    }

    public void setTrialCode(String trialCode) {
      this.trialCode = trialCode;
    }
  }

  public static class UpdateConsent {

    private Boolean consentState;

    public Boolean getConsentState() {
      return consentState;
    }

    public void setConsentState(Boolean consentState) {
      this.consentState = consentState;
    }
  }

  @GetMapping(MODELS)
  public Flux<GPTModel> getModels() {
    return Flux.fromArray(
      GPTModel.getAllowedModels().values().toArray(new GPTModel[0])
    );
  }

  @GetMapping(ROOM_PRESET)
  public Mono<GPTRoomPreset> getPreset(@PathVariable UUID roomId) {
    return presetService.getPreset(roomId);
  }

  @PatchMapping(ROOM_PRESET)
  public Mono<GPTRoomPreset> updatePreset(
    @PathVariable UUID roomId,
    @RequestBody HashMap<String, Object> presetObject
  ) {
    return presetService.setPreset(roomId, presetObject);
  }

  @GetMapping(CONSENTED)
  public Mono<String> getConsentState() {
    return helper
      .getCurrentUser()
      .flatMap(user -> service.getUserByAccount(user.getAccountId()))
      .map(user -> "" + user.getConsented());
  }

  @PostMapping(CONSENTED)
  public Mono<String> upateConsentState(
    @RequestBody UpdateConsent updateConsent
  ) {
    return helper
      .getCurrentUser()
      .flatMap(user -> service.getUserByAccount(user.getAccountId()))
      .flatMap(user -> {
        user.setConsented(updateConsent.getConsentState());
        return userRepository.save(user);
      })
      .map(user -> "" + user.getConsented());
  }

  @GetMapping(CONFIG)
  public Mono<GPTConfigRequest> getConfiguration() {
    logger.trace("Get Configuration request");
    return helper
      .getCurrentUser()
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException(
            "Only Administrators may be request configuration!"
          )
        )
      )
      .flatMap(user -> configService.getConfiguration());
  }

  @PatchMapping(CONFIG)
  public Mono<Void> patchConfiguration(@RequestBody final PatchQuery query) {
    logger.trace("Got Patch configuration request");
    return helper
      .getCurrentUser()
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException(
            "Only Administrators may update configuration!"
          )
        )
      )
      .flatMap(user -> configService.updateConfiguration(query.getChanges()));
  }

  @PostMapping(COMPLETION)
  public Mono<GPTModerationWrapped<GPTTextCompletionResponse>> requestCompletion(
    @RequestBody GPTTextCompletionRequest prompt
  ) {
    logger.trace("Got Completion request");
    return service.requestText(prompt);
  }

  @PostMapping(STREAM_COMPLETION)
  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamCompletion(
    @RequestBody GPTTextCompletionRequest prompt
  ) {
    logger.trace("Got Completion request");
    return service.requestStreamText(prompt);
  }

  @PostMapping(CHAT + COMPLETION)
  public Mono<GPTModerationWrapped<GPTChatCompletionResponse>> requestCompletion(
    @RequestBody GPTChatCompletionRequest prompt
  ) {
    logger.trace("Got Chat Completion request");
    return service.requestChat(prompt);
  }

  @PostMapping(CHAT + STREAM_COMPLETION)
  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamCompletion(
    @RequestBody GPTChatCompletionRequest prompt
  ) {
    logger.trace("Got Chat Completion request");
    return service.requestStreamChat(prompt);
  }

  @PostMapping(INTERRUPT_STREAM)
  public Mono<Boolean> interruptStream() {
    return helper
      .getCurrentUser()
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(user -> service.cancelRequest(user.getAccountId()));
  }

  @GetMapping(STATUS)
  public Mono<GPTRoomAccessInfo> requestStatus(@PathVariable UUID roomId) {
    logger.trace("Got Status request");
    return accessService.getRoomAccessInfo(roomId);
  }

  @GetMapping(PLAIN_STATUS)
  public Mono<GPTGlobalAccessInfo> requestPlainStatus() {
    logger.trace("Got plain Status request");
    return accessService.getGlobalAccessInfo();
  }

  @PostMapping(RATING)
  public Mono<GPTRating> makeRating(@RequestBody GPTRating rating) {
    return ratingService.makeRating(rating.getRating(), rating.getRatingText());
  }

  @GetMapping(RATING)
  public Mono<GPTRating> getRating() {
    return ratingService.getRating();
  }

  @GetMapping(RATING_TEXTS)
  public Mono<List<String>> getRatingTexts() {
    return ratingService.getRatingLists();
  }

  @GetMapping(RATING_INFO)
  public Mono<RatingResult> getInfo() {
    return ratingService.getRatingInfo();
  }

  @GetMapping(ROOM_CONVERSATIONS + GET_MAPPING)
  public Mono<GPTRequestStatisticResult> requestSummary(
    @PathVariable("id") UUID id
  ) {
    logger.trace("Got Summary request");
    return helper
      .getCurrentUser()
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("Only Administrators may get summarys!")
        )
      )
      .flatMap(u -> statisticService.getResultByRoomId(id));
  }

  @PostMapping(PROMPT_PRESET)
  public Mono<GPTPromptPreset> create(@RequestBody PromptPreset preset) {
    GPTPromptPreset p = new GPTPromptPreset(
      null,
      preset.getAct(),
      preset.getPrompt(),
      preset.getLanguage(),
      preset.getTemperature(),
      preset.getPresencePenalty(),
      preset.getFrequencyPenalty(),
      preset.getTopP()
    );
    return promptPresetService.add(false, p);
  }

  @PostMapping(GLOBAL_PROMPT_PRESET)
  public Mono<GPTPromptPreset> createGlobal(@RequestBody PromptPreset preset) {
    GPTPromptPreset p = new GPTPromptPreset(
      null,
      preset.getAct(),
      preset.getPrompt(),
      preset.getLanguage(),
      preset.getTemperature(),
      preset.getPresencePenalty(),
      preset.getFrequencyPenalty(),
      preset.getTopP()
    );
    return promptPresetService.add(true, p);
  }

  @PatchMapping(PROMPT_PRESET + DEFAULT_ID_MAPPING)
  public Mono<GPTPromptPreset> patch(
    @PathVariable("id") UUID id,
    @RequestBody Map<String, Object> changes
  ) {
    return promptPresetService.patch(id, changes);
  }

  @DeleteMapping(PROMPT_PRESET + DEFAULT_ID_MAPPING)
  public Mono<Void> deletePrompt(@PathVariable("id") UUID id) {
    return promptPresetService.delete(id);
  }

  @GetMapping(PROMPT_PRESETS)
  public Mono<List<GPTPromptPreset>> getPresets() {
    return promptPresetService.getList(false);
  }

  @GetMapping(GLOBAL_PROMPT_PRESETS)
  public Mono<List<GPTPromptPreset>> getGlobalPresets() {
    return promptPresetService.getList(true);
  }
}
