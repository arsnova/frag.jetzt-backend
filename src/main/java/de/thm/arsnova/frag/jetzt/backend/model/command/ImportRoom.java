package de.thm.arsnova.frag.jetzt.backend.model.command;

public class ImportRoom extends WebSocketCommand<ImportRoomPayload> {

    public ImportRoom() {
        super(ImportRoom.class.getSimpleName());
    }

    public ImportRoom(ImportRoomPayload payload) {
        super(ImportRoom.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportRoom that = (ImportRoom) o;
        return this.getPayload().equals(that.getPayload());
    }

}

