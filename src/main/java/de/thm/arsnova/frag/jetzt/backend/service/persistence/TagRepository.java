package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Tag;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface TagRepository extends ReactiveCrudRepository<Tag, UUID> {
    Flux<Tag> findByRoomId(UUID roomId);
    Flux<Void> deleteByRoomId(UUID roomId);
}
