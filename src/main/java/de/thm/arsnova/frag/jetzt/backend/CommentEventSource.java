package de.thm.arsnova.frag.jetzt.backend;

import de.thm.arsnova.frag.jetzt.backend.model.event.CommentPatched;
import de.thm.arsnova.frag.jetzt.backend.model.event.CommentPatchedPayload;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class CommentEventSource {
    private final AmqpTemplate messagingTemplate;
    private final CommentService service;

    @Autowired
    public CommentEventSource(
            AmqpTemplate messagingTemplate,
            CommentService service
    ) {
        this.messagingTemplate = messagingTemplate;
        this.service = service;
    }

    public void ScoreChanged(UUID id) {
        service.get(id)
                .subscribe(c -> {
                    int score = c.getScore();
                    int downvotes = c.getDownvotes();
                    int upvotes = c.getUpvotes();

                    Map<String, Object> changeMap = new HashMap<>();
                    changeMap.put("score", score);
                    changeMap.put("upvotes", upvotes);
                    changeMap.put("downvotes", downvotes);

                    CommentPatchedPayload p = new CommentPatchedPayload(id, changeMap);
                    CommentPatched event = new CommentPatched(p, c.getRoomId());

                    messagingTemplate.convertAndSend(
                            "amq.topic",
                            event.getRoomId() + ".comment.stream",
                            event
                    );
                });
    }
}
