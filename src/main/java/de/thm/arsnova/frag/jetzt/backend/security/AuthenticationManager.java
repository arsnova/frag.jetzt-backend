package de.thm.arsnova.frag.jetzt.backend.security;

import de.thm.arsnova.frag.jetzt.backend.service.AccountServiceBase;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomAccessRepository;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

  private static final Logger logger = LoggerFactory.getLogger(
    AuthenticationManager.class
  );

  private final JwtUtil jwtUtil;
  private final AccountServiceBase accountService;
  private final RoomAccessRepository roomAccessRepository;

  @Autowired
  public AuthenticationManager(
    JwtUtil jwtUtil,
    AccountServiceBase accountService,
    RoomAccessRepository roomAccessRepository
  ) {
    this.jwtUtil = jwtUtil;
    this.accountService = accountService;
    this.roomAccessRepository = roomAccessRepository;
  }

  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    return Mono
      .just(authentication)
      // get token from Authentication
      .map(auth -> auth.getCredentials().toString())
      // check if token is still valid
      // this also throws an error when the token can't be read
      .filter(jwtUtil::validateToken)
      // catch error and fall back to an empty Mono for error handling in API
      .onErrorResume(e -> Mono.empty())
      // if no error occured, load the user object
      .flatMap(token -> {
        UUID userId = jwtUtil.getAccountIdFromToken(token);
        Claims claims = jwtUtil.getAllClaimsFromToken(token);
        List<String> roles = claims.get(JwtUtil.CLAIM_KEY, ArrayList.class);
        List<SimpleGrantedAuthority> authorities = roles
          .stream()
          .map(SimpleGrantedAuthority::new)
          .collect(Collectors.toList());
        return accountService
          .get(userId)
          .flatMap(account -> {
            return accountService.refreshLastActive(
              account,
              roomAccessRepository
                .findByAccountId(userId)
                .collectList()
                .map(roomAccesses ->
                  new AuthenticatedUser(
                    token,
                    userId.toString(),
                    claims.get("type", String.class),
                    account.getEmail(),
                    roomAccesses,
                    authorities,
                    account
                  )
                )
            );
          });
      });
  }
}
