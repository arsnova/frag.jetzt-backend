package de.thm.arsnova.frag.jetzt.paypal.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Amount;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Breakdown;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.CreateOrderIntent;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Item;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.Price;
import de.thm.arsnova.frag.jetzt.paypal.model.rest.create_order.PurchaseUnit;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrder {

  // 1..10, required
  @JsonProperty("purchase_units")
  private List<PurchaseUnit> purchaseUnits;

  // required
  private CreateOrderIntent intent = CreateOrderIntent.CAPTURE;

  public void generateAmount() {
    for (PurchaseUnit purchaseUnit : purchaseUnits) {
      List<Item> items = purchaseUnit.getItems();
      if (items == null) {
        continue;
      }
      String code = null;
      BigDecimal total = BigDecimal.ZERO;
      BigDecimal taxTotal = BigDecimal.ZERO;
      for (Item item : items) {
        // price
        Price temp = item.getUnitAmount();
        if (code == null) {
          code = temp.getCurrencyCode();
        } else if (!code.equals(temp.getCurrencyCode())) {
          throw new IllegalArgumentException("Currency code mismatch");
        }
        BigDecimal value = new BigDecimal(temp.getValue());
        if (value.compareTo(BigDecimal.ZERO) < 0) {
          throw new IllegalArgumentException("No Negative value allowed");
        }
        total = total.add(value);
        // tax
        temp = item.getTax();
        if (temp == null) {
          continue;
        }
        if (!code.equals(temp.getCurrencyCode())) {
          throw new IllegalArgumentException("Currency code mismatch");
        }
        value = new BigDecimal(temp.getValue());
        if (value.compareTo(BigDecimal.ZERO) < 0) {
          throw new IllegalArgumentException("No Negative value allowed");
        }
        taxTotal = taxTotal.add(value);
      }
      BigDecimal all = total.add(taxTotal);
      purchaseUnit.setAmount(
        Amount
          .builder()
          .currencyCode(code)
          .value(all.toString())
          .breakdown(
            Breakdown
              .builder()
              .itemTotal(new Price(code, total.toString()))
              .taxTotal(new Price(code, taxTotal.toString()))
              .build()
          )
          .build()
      );
    }
  }
}
