package de.thm.arsnova.frag.jetzt.openai.service;

import de.thm.arsnova.frag.jetzt.openai.model.AuthorizationInfo;
import de.thm.arsnova.frag.jetzt.openai.model.FileObject;
import java.nio.file.Path;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FileUploadService {

  private final WebClient restWebClient = WebClient.create(
    "https://api.openai.com/v1/files"
  );

  public Mono<FileObject> uploadFile(
    AuthorizationInfo info,
    Path p,
    String name
  ) {
    var builder = new MultipartBodyBuilder();
    builder.part("purpose", "assistants");
    builder.part(
      "file",
      new FileSystemResource(p) {
        @Override
        public String getFilename() {
          return name;
        }
      }
    );
    return info
      .apply(restWebClient.post())
      .contentType(MediaType.MULTIPART_FORM_DATA)
      .body(BodyInserters.fromMultipartData(builder.build()))
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          FileObject.class,
          "File upload failed",
          response
        )
      );
  }

  public Mono<FileObject> getFileInfo(AuthorizationInfo info, String id) {
    return info
      .apply(restWebClient.get().uri("/{id}", id))
      .exchangeToMono(response ->
        ErrorHandler.handleError(
          FileObject.class,
          "File info retrieval failed",
          response
        )
      );
  }

  public Flux<DataBuffer> getFileContent(AuthorizationInfo info, String id) {
    return info
      .apply(restWebClient.get().uri("/{id}/content", id))
      .exchangeToFlux(response ->
        ErrorHandler.handleFluxError(
          DataBuffer.class,
          "File content retrieval failed",
          response
        )
      );
  }

  public Mono<Void> deleteFile(AuthorizationInfo info, String id) {
    return info
      .apply(restWebClient.delete().uri("/{id}", id))
      .exchangeToMono(response ->
        ErrorHandler.handleError(Void.class, "File deletion failed", response)
      );
  }
}
