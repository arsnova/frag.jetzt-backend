package de.thm.arsnova.frag.jetzt.backend.model.command;

public class PatchLivepollSession
  extends WebSocketCommand<PatchLivepollSessionPayload> {

  public PatchLivepollSession() {
    super(PatchLivepollSession.class.getSimpleName());
  }

  public PatchLivepollSession(PatchLivepollSessionPayload payload) {
    super(PatchLivepollSession.class.getSimpleName());
    this.payload = payload;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PatchLivepollSession that = (PatchLivepollSession) o;
    return this.getPayload().equals(that.getPayload());
  }
}
