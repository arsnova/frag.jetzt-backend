package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

public class CreateBrainstormingPayload implements WebSocketPayload {

    private UUID roomId;
    private String title;
    private int maxWordLength;
    private int maxWordCount;
    private String language;
    private boolean ratingAllowed;
    private boolean ideasFrozen;
    private Integer ideasTimeDuration;
    private Timestamp ideasEndTimestamp;

    public CreateBrainstormingPayload() {

    }

    public CreateBrainstormingPayload(BrainstormingSession session) {
        this.roomId = session.getRoomId();
        this.title = session.getTitle();
        this.maxWordLength = session.getMaxWordLength();
        this.maxWordCount = session.getMaxWordCount();
        this.language = session.getLanguage();
        this.ratingAllowed = session.isRatingAllowed();
        this.ideasFrozen = session.isIdeasFrozen();
        this.ideasTimeDuration = session.getIdeasTimeDuration();
        this.ideasEndTimestamp = session.getIdeasEndTimestamp();
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxWordLength() {
        return maxWordLength;
    }

    public void setMaxWordLength(int maxWordLength) {
        this.maxWordLength = maxWordLength;
    }

    public int getMaxWordCount() {
        return maxWordCount;
    }

    public void setMaxWordCount(int maxWordCount) {
        this.maxWordCount = maxWordCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isRatingAllowed() {
        return ratingAllowed;
    }

    public void setRatingAllowed(boolean ratingAllowed) {
        this.ratingAllowed = ratingAllowed;
    }

    public boolean isIdeasFrozen() {
        return ideasFrozen;
    }

    public void setIdeasFrozen(boolean ideasFrozen) {
        this.ideasFrozen = ideasFrozen;
    }

    public Integer getIdeasTimeDuration() {
        return ideasTimeDuration;
    }

    public void setIdeasTimeDuration(Integer ideasTimeDuration) {
        this.ideasTimeDuration = ideasTimeDuration;
    }

    public Timestamp getIdeasEndTimestamp() {
        return ideasEndTimestamp;
    }

    public void setIdeasEndTimestamp(Timestamp ideasEndTimestamp) {
        this.ideasEndTimestamp = ideasEndTimestamp;
    }

    @Override
    public String toString() {
        return "CreateBrainstormingPayload{" +
                "roomId=" + roomId +
                ", title='" + title + '\'' +
                ", maxWordLength=" + maxWordLength +
                ", maxWordCount=" + maxWordCount +
                ", language='" + language + '\'' +
                ", ratingAllowed=" + ratingAllowed +
                ", ideasFrozen=" + ideasFrozen +
                ", ideasTimeDuration=" + ideasTimeDuration +
                ", ideasEndTimestamp=" + ideasEndTimestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstormingPayload that = (CreateBrainstormingPayload) o;
        return maxWordLength == that.maxWordLength &&
                maxWordCount == that.maxWordCount &&
                ratingAllowed == that.ratingAllowed &&
                ideasFrozen == that.ideasFrozen &&
                Objects.equals(roomId, that.roomId) &&
                Objects.equals(title, that.title) &&
                Objects.equals(language, that.language) &&
                Objects.equals(ideasTimeDuration, that.ideasTimeDuration) &&
                Objects.equals(ideasEndTimestamp, that.ideasEndTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId, title, maxWordLength, maxWordCount, language, ratingAllowed, ideasFrozen, ideasTimeDuration, ideasEndTimestamp);
    }
}
