package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account.AccountRole;
import de.thm.arsnova.frag.jetzt.backend.model.KeycloakProvider;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.KeycloakProviderRepository;
import de.thm.arsnova.frag.jetzt.backend.util.IpParser;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class KeycloakProviderService {

  private static final Logger logger = LoggerFactory.getLogger(
    KeycloakProviderService.class
  );

  private final KeycloakProviderRepository repository;
  private final AuthorizationHelper helper;

  @Autowired
  public KeycloakProviderService(
    KeycloakProviderRepository repository,
    AuthorizationHelper helper
  ) {
    this.repository = repository;
    this.helper = helper;
  }

  public Flux<KeycloakProvider> getAll() {
    return helper
      .isLoggedIn()
      .flatMap(loggedIn -> {
        if (loggedIn) {
          return helper
            .getCurrentUser()
            .map(user -> !user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD)
            );
        }
        return Mono.just(true);
      })
      .flatMapMany(forbidden ->
        repository
          .findAll()
          .map(e -> {
            if (forbidden) {
              e.setEventPassword(null);
            }
            return e;
          })
      );
  }

  public Mono<KeycloakProvider> getById(UUID id) {
    return helper
      .getCurrentUser()
      .flatMap(user -> {
        boolean forbidden = !user
          .getAccount()
          .hasRole(AccountRole.ADMIN_DASHBOARD);
        return repository
          .findById(id)
          .map(e -> {
            if (forbidden) {
              e.setEventPassword(null);
            }
            return e;
          });
      });
  }

  public Mono<KeycloakProvider> patch(UUID id, Map<String, Object> changes) {
    return checkAccess()
      .flatMap(user -> getById(id))
      .switchIfEmpty(
        Mono.error(new NotFoundException("Provider does not exist"))
      )
      .flatMap(provider -> {
        boolean isDefault = provider.getNameDe().length() < 1;
        return validate(parseChanges(provider, changes), isDefault);
      })
      .flatMap(repository::save);
  }

  public Mono<KeycloakProvider> create(KeycloakProvider provider) {
    provider.setId(null);
    return checkAccess()
      .flatMap(user -> validate(provider, false))
      .flatMap(repository::save);
  }

  public Mono<Void> deleteById(UUID id) {
    return checkAccess()
      .flatMap(user -> getById(id))
      .switchIfEmpty(
        Mono.error(new NotFoundException("Provider does not exist"))
      )
      .flatMap(provider -> repository.deleteById(id));
  }

  private KeycloakProvider parseChanges(
    KeycloakProvider provider,
    Map<String, Object> changes
  ) {
    changes.forEach((key, value) -> {
      switch (key) {
        case "priority":
          provider.setPriority((int) value);
          break;
        case "url":
          provider.setUrl((String) value);
          break;
        case "frontendUrl":
          provider.setFrontendUrl((String) value);
          break;
        case "eventPassword":
          provider.setEventPassword((String) value);
          break;
        case "allowedIps":
          provider.setAllowedIps((String) value);
          break;
        case "realm":
          provider.setRealm((String) value);
          break;
        case "clientId":
          provider.setClientId((String) value);
          break;
        case "nameDe":
          provider.setNameDe((String) value);
          break;
        case "nameEn":
          provider.setNameEn((String) value);
          break;
        case "nameFr":
          provider.setNameFr((String) value);
          break;
        case "descriptionDe":
          provider.setDescriptionDe((String) value);
          break;
        case "descriptionEn":
          provider.setDescriptionEn((String) value);
          break;
        case "descriptionFr":
          provider.setDescriptionFr((String) value);
          break;
        case "id":
        case "createdAt":
        case "updatedAt":
          throw new BadRequestException(
            "Can not change '" + key + "' attribute."
          );
        default:
          throw new BadRequestException("Invalid change attribute!");
      }
    });
    return provider;
  }

  private Mono<KeycloakProvider> validate(
    KeycloakProvider provider,
    boolean isDefault
  ) {
    return Mono
      .just(provider)
      .filter(p -> p.getNameDe().length() < 1 == isDefault)
      .switchIfEmpty(Mono.error(new ForbiddenException("nameDe is not valid.")))
      .filter(p -> p.getNameEn().length() < 1 == isDefault)
      .switchIfEmpty(Mono.error(new ForbiddenException("nameEn is not valid.")))
      .filter(p -> p.getNameFr().length() < 1 == isDefault)
      .switchIfEmpty(Mono.error(new ForbiddenException("nameFr is not valid.")))
      .filter(p -> p.getDescriptionDe().length() < 1 == isDefault)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("descriptionDe is not valid."))
      )
      .filter(p -> p.getDescriptionEn().length() < 1 == isDefault)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("descriptionEn is not valid."))
      )
      .filter(p -> p.getDescriptionFr().length() < 1 == isDefault)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("descriptionFr is not valid."))
      )
      .filter(p -> p.getUrl() != null && p.getUrl().length() > 0)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("url can not be empty."))
      )
      .filter(p -> p.getFrontendUrl() != null && p.getFrontendUrl().length() > 0
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("frontendUrl can not be empty."))
      )
      .filter(p ->
        p.getEventPassword() != null && p.getEventPassword().length() > 0
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("eventPassword can not be empty."))
      )
      .filter(p -> p.getRealm() != null && p.getRealm().length() > 0)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("realm can not be empty."))
      )
      .filter(p -> p.getClientId() != null && p.getClientId().length() > 0)
      .switchIfEmpty(
        Mono.error(new ForbiddenException("clientId can not be empty."))
      )
      .filter(p -> {
        if (p.getAllowedIps() == null) {
          return false;
        }
        try {
          IpParser.parseIpRanges(p.getAllowedIps());
        } catch (UnknownHostException e) {
          e.printStackTrace();
          return false;
        }
        return true;
      })
      .switchIfEmpty(
        Mono.error(new ForbiddenException("allowedIps is null or invalid."))
      );
  }

  private Mono<AuthenticatedUser> checkAccess() {
    return helper
      .getCurrentUser()
      .filter(user -> user.getAccount().hasRole(AccountRole.ADMIN_DASHBOARD))
      .switchIfEmpty(
        Mono.error(new ForbiddenException("You dont have access"))
      );
  }
}
