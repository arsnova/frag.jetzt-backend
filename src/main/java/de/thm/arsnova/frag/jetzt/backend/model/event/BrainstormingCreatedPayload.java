package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;

public class BrainstormingCreatedPayload implements WebSocketPayload {

    private BrainstormingSession session;

    public BrainstormingCreatedPayload() {

    }

    public BrainstormingCreatedPayload(BrainstormingSession session) {
        this.session = session;
    }

    public BrainstormingSession getSession() {
        return session;
    }

    public void setSession(BrainstormingSession session) {
        this.session = session;
    }

    @Override
    public String toString() {
        return "BrainstormingCreatedPayload{" +
                "session=" + session +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCreatedPayload that = (BrainstormingCreatedPayload) o;
        return Objects.equals(session, that.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(session);
    }
}
