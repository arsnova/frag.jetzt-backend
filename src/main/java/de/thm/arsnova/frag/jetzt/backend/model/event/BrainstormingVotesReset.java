package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingVotesReset extends WebSocketEvent<BrainstormingVotesResetPayload> {

    public BrainstormingVotesReset() {
        super(BrainstormingVotesReset.class.getSimpleName());
    }

    public BrainstormingVotesReset(BrainstormingVotesResetPayload payload, UUID roomId) {
        super(BrainstormingVotesReset.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingVotesReset that = (BrainstormingVotesReset) o;
        return this.getPayload().equals(that.getPayload());
    }
}
