package de.thm.arsnova.frag.jetzt.notification.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Table
public class WebNotificationSetting implements Persistable<UUID> {

  public enum Language {
    DE,
    EN,
    FR,
  }

  @Id
  private UUID id;

  @NonNull
  private UUID accountId;

  @NonNull
  private int debounceTimeSeconds;

  @NonNull
  private Language language;

  @NonNull
  private Timestamp createdAt = Timestamp.from(Instant.now());

  @Nullable
  private Timestamp updatedAt;

  public WebNotificationSetting() {}

  public WebNotificationSetting(
    UUID accountId,
    int debounceTimeSeconds,
    Language language
  ) {
    this.accountId = accountId;
    this.debounceTimeSeconds = debounceTimeSeconds;
    this.language = language;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public int getDebounceTimeSeconds() {
    return debounceTimeSeconds;
  }

  public void setDebounceTimeSeconds(int debounceTimeSeconds) {
    this.debounceTimeSeconds = debounceTimeSeconds;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + debounceTimeSeconds;
    result = prime * result + ((language == null) ? 0 : language.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    WebNotificationSetting other = (WebNotificationSetting) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (debounceTimeSeconds != other.debounceTimeSeconds) return false;
    if (language != other.language) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "WebNotificationSetting [id=" +
      id +
      ", accountId=" +
      accountId +
      ", debounceTimeSeconds=" +
      debounceTimeSeconds +
      ", language=" +
      language +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
