package de.thm.arsnova.frag.jetzt.paypal.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OAuthToken {

  @JsonProperty("access_token")
  private String accessToken;

  @JsonProperty("token_type")
  private String tokenType;

  @JsonProperty("app_id")
  private String appId;

  @JsonProperty("expires_in")
  private int expiresIn;

  private String nonce;
  private String scope;

  public OAuthToken() {}

  public OAuthToken(
    String accessToken,
    String tokenType,
    String appId,
    int expiresIn,
    String nonce,
    String scope
  ) {
    this.accessToken = accessToken;
    this.tokenType = tokenType;
    this.appId = appId;
    this.expiresIn = expiresIn;
    this.nonce = nonce;
    this.scope = scope;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public int getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(int expiresIn) {
    this.expiresIn = expiresIn;
  }

  public String getNonce() {
    return nonce;
  }

  public void setNonce(String nonce) {
    this.nonce = nonce;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
      prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
    result = prime * result + ((tokenType == null) ? 0 : tokenType.hashCode());
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + expiresIn;
    result = prime * result + ((nonce == null) ? 0 : nonce.hashCode());
    result = prime * result + ((scope == null) ? 0 : scope.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    OAuthToken other = (OAuthToken) obj;
    if (accessToken == null) {
      if (other.accessToken != null) return false;
    } else if (!accessToken.equals(other.accessToken)) return false;
    if (tokenType == null) {
      if (other.tokenType != null) return false;
    } else if (!tokenType.equals(other.tokenType)) return false;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (expiresIn != other.expiresIn) return false;
    if (nonce == null) {
      if (other.nonce != null) return false;
    } else if (!nonce.equals(other.nonce)) return false;
    if (scope == null) {
      if (other.scope != null) return false;
    } else if (!scope.equals(other.scope)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "OAuthToken [accessToken=" +
      accessToken +
      ", tokenType=" +
      tokenType +
      ", appId=" +
      appId +
      ", expiresIn=" +
      expiresIn +
      ", nonce=" +
      nonce +
      ", scope=" +
      scope +
      "]"
    );
  }
}
