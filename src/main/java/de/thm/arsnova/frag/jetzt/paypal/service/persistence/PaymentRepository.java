package de.thm.arsnova.frag.jetzt.paypal.service.persistence;

import de.thm.arsnova.frag.jetzt.paypal.model.Payment;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.Data;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PaymentRepository
  extends ReactiveCrudRepository<Payment, UUID> {
  @Data
  public static class PaymentResult {

    private UUID accountId;
    private Long count;
  }

  Flux<Payment> findByAccountId(UUID accountId);

  Mono<Long> countByAccountId(UUID accountId);

  @Query(
    "SELECT account_id, COUNT(*) AS count FROM payment GROUP BY account_id"
  )
  Flux<PaymentResult> countByAccountIdGrouped();

  @Query("SELECT SUM(amount) FROM payment WHERE account_id = :accountId")
  Mono<BigDecimal> sumAmountByAccountId(UUID accountId);
}
