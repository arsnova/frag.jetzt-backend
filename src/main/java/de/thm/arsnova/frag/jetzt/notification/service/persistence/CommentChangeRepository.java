package de.thm.arsnova.frag.jetzt.notification.service.persistence;

import de.thm.arsnova.frag.jetzt.notification.model.persistence.CommentChange;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

public interface CommentChangeRepository extends ReactiveCrudRepository<CommentChange, UUID> {
  Flux<CommentChange> findAllByRoomIdAndCreatedAtGreaterThan(
    UUID roomId,
    Timestamp timestamp
  );

  Flux<CommentChange> findAllByCommentIdInAndCreatedAtGreaterThan(
    List<UUID> commentId,
    Timestamp timestamp
  );

  @Transactional
  Flux<Void> deleteByCreatedAtLessThanEqual(Timestamp timestamp);
}
