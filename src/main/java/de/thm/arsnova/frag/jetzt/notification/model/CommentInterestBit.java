package de.thm.arsnova.frag.jetzt.notification.model;

import java.util.ArrayList;
import java.util.List;

public enum CommentInterestBit {
  CREATED,
  DELETED,
  ANSWERED,
  CHANGE_ACK,
  CHANGE_FAVORITE,
  CHANGE_CORRECT,
  CHANGE_TAG,
  CHANGE_SCORE;

  private static final int maxBit;

  static {
    maxBit = values().length - 1;
  }

  public int getBit() {
    return ordinal();
  }

  public boolean hasInterest(long interestBits) {
    return (interestBits & (1L << getBit())) != 0;
  }

  public static CommentInterestBit getByBit(int bit) {
    return values()[bit];
  }

  public static List<CommentInterestBit> toList(long bitset) {
    ArrayList<CommentInterestBit> list = new ArrayList<>();
    for (int i = 0; i <= maxBit; i++) {
      if ((bitset & (1L << i)) != 0) {
        list.add(getByBit(i));
      }
    }
    return list;
  }
}
