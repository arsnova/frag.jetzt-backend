package de.thm.arsnova.frag.jetzt.openai.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.openai.model.persistence.UploadedFile;
import reactor.core.publisher.Flux;

@Repository
public interface UploadFileRepository extends ReactiveCrudRepository<UploadedFile, UUID> {

    Flux<UploadedFile> findAllByFileName(String fileName);
    
}
