package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;

public class GPTConfigRequest {

    private GPTConfig config;

    public GPTConfigRequest(GPTConfig config) {
        this.config = config;
    }

    public GPTConfig getConfig() {
        return config;
    }

    public void setConfig(GPTConfig config) {
        this.config = config;
    }

}
