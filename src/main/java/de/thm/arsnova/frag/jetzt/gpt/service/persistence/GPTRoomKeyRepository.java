package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomKey;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface GPTRoomKeyRepository extends ReactiveCrudRepository<GPTRoomKey, UUID> {
  Flux<GPTRoomKey> findAllBySettingId(UUID settingId);

  Flux<Void> deleteAllBySettingId(UUID settingId);
}
