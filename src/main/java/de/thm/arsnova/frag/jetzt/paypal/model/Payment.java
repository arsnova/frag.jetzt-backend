package de.thm.arsnova.frag.jetzt.paypal.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table
@Builder
public class Payment implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private BigDecimal amount;
  private String currency;
  private String orderId;
  @Builder.Default
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  @Override
  public UUID getId() {
    return id;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }
}
