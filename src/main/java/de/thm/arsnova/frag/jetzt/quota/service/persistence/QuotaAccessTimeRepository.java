package de.thm.arsnova.frag.jetzt.quota.service.persistence;

import de.thm.arsnova.frag.jetzt.quota.model.persistence.QuotaAccessTime;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface QuotaAccessTimeRepository extends ReactiveCrudRepository<QuotaAccessTime, UUID> {

  Flux<QuotaAccessTime> findAllByQuotaId(UUID quotaId);

  Flux<Void> deleteAllByQuotaId(UUID quotaId);
}
