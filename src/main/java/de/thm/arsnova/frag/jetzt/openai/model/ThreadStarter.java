package de.thm.arsnova.frag.jetzt.openai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThreadStarter {

  @JsonProperty("assistant_id")
  private String assistantId;

  private Map<String, Object> thread;
  private boolean stream = true;

  public ThreadStarter() {}

  public String getAssistantId() {
    return assistantId;
  }

  public void setAssistantId(String assistantId) {
    this.assistantId = assistantId;
  }

  public Map<String, Object> getThread() {
    return thread;
  }

  @JsonIgnore
  public void addMessages(List<ThreadMessage> messages) {
    thread = Map.of("messages", messages);
  }

  public boolean isStream() {
    return stream;
  }
}
