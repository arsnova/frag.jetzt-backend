package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingPatched extends WebSocketEvent<BrainstormingPatchedPayload> {

    public BrainstormingPatched() {
        super(BrainstormingPatched.class.getSimpleName());
    }

    public BrainstormingPatched(BrainstormingPatchedPayload payload, UUID roomId) {
        super(BrainstormingPatched.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingPatched that = (BrainstormingPatched) o;
        return this.getPayload().equals(that.getPayload());
    }

}
