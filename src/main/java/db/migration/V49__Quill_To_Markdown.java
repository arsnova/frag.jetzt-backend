package db.migration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.UUID;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

public class V49__Quill_To_Markdown extends BaseJavaMigration {

  @Override
  public void migrate(Context context) throws Exception {
    try (
      Statement statement = context.getConnection().createStatement();
      PreparedStatement prepared = context
        .getConnection()
        .prepareStatement("UPDATE comment SET body = ? WHERE id = ?")
    ) {
      ResultSet set = statement.executeQuery("SELECT id, body FROM comment");
      while (set.next()) {
        final UUID id = set.getObject(1, UUID.class);
        final String body = set.getString(2);
        prepared.setString(
          1,
          QuillToToastUi.getInstance().transformToMarkdown(id, body)
        );
        prepared.setObject(2, id);
        prepared.addBatch();
      }
      prepared.executeBatch();
      context.getConnection().commit();
    } catch (Exception e) {
      context.getConnection().rollback();
      throw new RuntimeException(e);
    }
  }
}
