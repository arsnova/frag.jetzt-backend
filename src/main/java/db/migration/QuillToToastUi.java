package db.migration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class QuillToToastUi {

  private static final QuillToToastUi instance = new QuillToToastUi();

  public static QuillToToastUi getInstance() {
    return instance;
  }

  private Map<String, String> emojiTranslate = new HashMap<>();

  {
    emojiTranslate.put("thumbsup", "👍");
    emojiTranslate.put("thumbsdown", "👎");
    emojiTranslate.put("yum", "😋");
    emojiTranslate.put("wink", "😉");
    emojiTranslate.put("sweat_smile", "😅");
    emojiTranslate.put("grinning", "😀");
    emojiTranslate.put("slightly_smiling_face", "🙂");
    emojiTranslate.put("hugging_face", "🤗");
    emojiTranslate.put("blush", "😊");
    emojiTranslate.put("peach", "🍑");
    emojiTranslate.put("sparkling_heart", "💖");
    emojiTranslate.put("sunny", "☀️");
    emojiTranslate.put("star2", "🌟");
    emojiTranslate.put("sparkles", "✨");
    emojiTranslate.put("sunglasses", "😎");
    emojiTranslate.put("fist", "✊");
    emojiTranslate.put("grin", "😁");
    emojiTranslate.put("banana", "🍌");
    emojiTranslate.put("smile", "😄");
    emojiTranslate.put("joy", "😂");
    emojiTranslate.put("thinking_face", "🤔");
    emojiTranslate.put("herb", "🌿");
    emojiTranslate.put("smirk", "😏");
    emojiTranslate.put("clap", "👏");
    emojiTranslate.put("school_satchel", "🎒");
    emojiTranslate.put("fearful", "😨");
    emojiTranslate.put("cherries", "🍒");
    emojiTranslate.put("eggplant", "🍆");
    emojiTranslate.put("speech", "💬");
    emojiTranslate.put("womans_hat", "👒");
    emojiTranslate.put("kissing_closed_eyes", "😚");
    emojiTranslate.put("point_up_2", "👆");
    emojiTranslate.put("older_man", "👴");
    emojiTranslate.put("man_with_gua_pi_mao", "👲");
    emojiTranslate.put("ok_woman", "🙆‍♀️");
  }

  private ObjectMapper mapper = new ObjectMapper();

  private QuillToToastUi() {}

  String transformToMarkdown(UUID debugId, String quill)
    throws JsonMappingException, JsonProcessingException {
    List<?> data = mapper.readValue(quill, List.class);
    final StringBuilder result = new StringBuilder();
    final StringBuilder currentLine = new StringBuilder();
    for (Object entry : data) {
      if (entry instanceof String stringEntry) {
        if (stringEntry.endsWith("\n")) {
          result.append(currentLine);
          currentLine.setLength(0);
          result.append(stringEntry);
        } else {
          currentLine.append(stringEntry);
        }
      } else if (entry instanceof Map<?, ?> mapEntry) {
        Object insertObject = mapEntry.remove("insert");
        Object attrMap = mapEntry.remove("attributes");
        if (insertObject == null) {
          parseEmbeddedInsert(debugId, null, mapEntry, currentLine);
          mapEntry.clear();
        } else if (insertObject instanceof String insertText) {
          parseStringInsert(debugId, attrMap, insertText, currentLine, result);
        } else if (insertObject instanceof Map<?, ?> insertMap) {
          parseEmbeddedInsert(debugId, attrMap, insertMap, currentLine);
        } else {
          throw new RuntimeException(
            "Could not parse insert element. ID: " + debugId
          );
        }
        if (mapEntry.size() > 0) {
          throw new RuntimeException("Quill was not fully parsed! ID: " + debugId);
        }
      } else {
        throw new RuntimeException("Quill can not be parsed! ID: " + debugId);
      }
    }
    return result.toString().stripTrailing();
  }

  private void parseEmbeddedInsert(
    UUID id,
    Object attrMap,
    Map<?, ?> insertMap,
    StringBuilder currentLine
  ) {
    final int length = currentLine.length();
    final String image = verify(id, insertMap.get("image"), String.class);
    if (image != null) {
      currentLine.append("![](").append(image).append(")");
      return;
    }
    final String newVideo = verify(
      id,
      insertMap.get("dsgvo-video"),
      String.class
    );
    final String oldVideo = verify(id, insertMap.get("video"), String.class);
    final String video = newVideo != null ? newVideo : oldVideo;
    if (video != null) {
      if (length > 0 && !currentLine.substring(length - 1).equals("\n")) {
        currentLine.append("\n");
      }
      currentLine.append("$$dsgvoMedia\n").append(video).append("\n$$\n");
      return;
    }
    final String formula = verify(id, insertMap.get("formula"), String.class);
    if (formula != null) {
      if (length > 0 && !currentLine.substring(length - 1).equals("\n")) {
        currentLine.append("\n");
      }
      currentLine.append("$$katex\n").append(formula).append("\n$$\n");
      return;
    }
    final String emoji = verify(id, insertMap.get("emoji"), String.class);
    final String translatedEmoji = emojiTranslate.get(emoji);
    if (translatedEmoji != null) {
      currentLine.append(translatedEmoji);
      return;
    }
    currentLine.append("❓");
  }

  private static void parseStringInsert(
    UUID id,
    Object attrMap,
    String insertText,
    StringBuilder currentLine,
    StringBuilder result
  ) {
    if (insertText.endsWith("\n")) {
      // End of line(s)
      parseStringInsertEnd(id, attrMap, insertText, currentLine, result);
      return;
    }
    // In-line
    parseStringInsertInline(id, attrMap, insertText, currentLine);
  }

  private static void parseStringInsertEnd(
    UUID id,
    Object attrMap,
    String insertText,
    StringBuilder currentLine,
    StringBuilder result
  ) {
    final int count = (int) insertText.chars().filter(ch -> ch == '\n').count();
    final String list = get(id, attrMap, "list", String.class);
    final Integer indent = get(id, attrMap, "indent", Integer.class);
    final Integer header = get(id, attrMap, "header", Integer.class);
    final Boolean blockquote = get(id, attrMap, "blockquote", Boolean.class);
    final Boolean codeBlock = get(id, attrMap, "code-block", Boolean.class);
    final int indentCount = indent == null ? 0 : indent.intValue();
    for (int line = 0; line < count; line++) {
      if ("ordered".equals(list)) {
        currentLine.insert(0, "1. ");
        for (int i = 0; i < indentCount; i++) {
          currentLine.insert(0, "    ");
        }
      } else if ("bullet".equals(list)) {
        currentLine.insert(0, "- ");
        for (int i = 0; i < indentCount; i++) {
          currentLine.insert(0, "    ");
        }
      }
      if (blockquote != null && blockquote.booleanValue()) {
        currentLine.insert(0, "> ");
      }
      if (codeBlock != null && codeBlock.booleanValue()) {
        currentLine.insert(0, "    ");
      }
      if (header != null) {
        currentLine.insert(0, " ");
        for (int i = 0; i < header.intValue(); i++) {
          currentLine.insert(0, "#");
        }
      }
      currentLine.append(insertText);
      result.append(currentLine);
      currentLine.setLength(0);
    }
  }

  private static void parseStringInsertInline(
    UUID id,
    Object attrMap,
    String insertText,
    StringBuilder currentLine
  ) {
    StringBuilder local = new StringBuilder(insertText);
    final String color = get(id, attrMap, "color", String.class);
    final String link = get(id, attrMap, "link", String.class);
    final Boolean bold = get(id, attrMap, "bold", Boolean.class);
    final Boolean strike = get(id, attrMap, "strike", Boolean.class);
    final Boolean underline = get(id, attrMap, "underline", Boolean.class);
    final Boolean italic = get(id, attrMap, "italic", Boolean.class);
    String spanClass = color != null ? "color:" + color : "";
    if (underline != null && underline.booleanValue()) {
      spanClass =
        spanClass.isEmpty()
          ? "text-decoration:underline"
          : spanClass + ";text-decoration:underline";
    }
    // link or (color and underline)
    if (link != null) {
      local.insert(0, "[").append("](").append(link).append(")");
    } else if (!spanClass.isEmpty()) {
      local.insert(0, "<span style=\"" + spanClass + "\">").append("</span>");
    }
    // bold, strike, italic
    if (bold != null && bold.booleanValue()) {
      local.insert(0, "**").append("**");
    }
    if (strike != null && strike.booleanValue()) {
      local.insert(0, "~~").append("~~");
    }
    if (italic != null && italic.booleanValue()) {
      local.insert(0, "_").append("_");
    }
    currentLine.append(local);
  }

  private static <T> T verify(UUID id, Object o, Class<T> clazz) {
    if (o == null || clazz.isInstance(o)) {
      @SuppressWarnings("unchecked")
      T t = (T) o;
      return t;
    }
    throw new IllegalArgumentException("Value has wrong format! ID: " + id);
  }

  private static <T> T get(UUID id, Object map, String entry, Class<T> clazz) {
    if (map == null) {
      return null;
    } else if (map instanceof Map<?, ?> m) {
      return verify(id, m.get(entry), clazz);
    }
    throw new IllegalArgumentException("Map is not a map! ID " + id);
  }
}
