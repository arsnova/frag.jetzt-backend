DROP TABLE gpt_room_user_description;

DROP TABLE gpt_room_preset_tone;

ALTER TABLE gpt_room_setting
  DROP COLUMN preset_persona_creator,
  DROP COLUMN preset_persona_moderator,
  DROP COLUMN preset_persona_participant,
  DROP COLUMN preset_language,
  DROP COLUMN preset_formal;