ALTER TABLE gpt_room_setting
  DROP COLUMN daily_quota_counter,
  DROP COLUMN monthly_quota_counter,
  DROP COLUMN accumulated_quota_counter,
  DROP COLUMN daily_cost_counter,
  DROP COLUMN monthly_cost_counter,
  DROP COLUMN accumulated_cost_counter;

ALTER TABLE gpt_room_setting
  RENAME COLUMN last_quota_usage TO last_update;

ALTER TABLE gpt_room_setting
  ADD COLUMN daily_room_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN monthly_room_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN accumulated_room_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN daily_participant_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN monthly_participant_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN accumulated_participant_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN daily_moderator_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN monthly_moderator_cost_counter BIGINT NOT NULL DEFAULT 0,
  ADD COLUMN accumulated_moderator_cost_counter BIGINT NOT NULL DEFAULT 0;