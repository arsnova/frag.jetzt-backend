ALTER TABLE comment
ADD COLUMN questioner_name VARCHAR(20) NULL DEFAULT NULL;

DELETE FROM room_access
WHERE id IN (
    SELECT ra.id
    FROM room r INNER JOIN room_access ra ON (r.id = ra.room_id) AND (r.owner_id = ra.account_id)
);