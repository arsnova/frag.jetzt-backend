ALTER TABLE gpt_room_setting
  RENAME COLUMN max_daily_room_quota TO max_daily_room_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_monthly_room_quota TO max_monthly_room_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_accumulated_room_quota TO max_accumulated_room_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_daily_participant_quota TO max_daily_participant_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_monthly_participant_quota TO max_monthly_participant_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_accumulated_participant_quota TO max_accumulated_participant_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_daily_moderator_quota TO max_daily_moderator_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_monthly_moderator_quota TO max_monthly_moderator_cost;
ALTER TABLE gpt_room_setting
  RENAME COLUMN max_accumulated_moderator_quota TO max_accumulated_moderator_cost;

ALTER TABLE gpt_room_user_description
  ADD COLUMN daily_cost_counter INT NOT NULL DEFAULT 0,
  ADD COLUMN monthly_cost_counter INT NOT NULL DEFAULT 0,
  ADD COLUMN accumulated_cost_counter BIGINT NOT NULL DEFAULT 0;

ALTER TABLE gpt_user
  ADD COLUMN consented BOOLEAN NULL;

ALTER TABLE gpt_user
  DROP COLUMN daily_quota,
  DROP COLUMN weekly_quota,
  DROP COLUMN accumulated_quota,
  DROP COLUMN last_quota_usage;

ALTER TABLE frag_jetzt_credentials
  ADD COLUMN password_expiration_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;