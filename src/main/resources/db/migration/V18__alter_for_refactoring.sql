UPDATE comment
    SET number = replace(comment.number, '.', '/');

ALTER TABLE room
    ALTER COLUMN conversation_depth SET DEFAULT 3;

CREATE OR REPLACE FUNCTION increase_comment_number()
    RETURNS trigger AS
$$
DECLARE
previousString TEXT;
BEGIN
    IF (NEW.comment_reference IS NOT NULL) THEN
        SELECT comment.number
            INTO previousString
            FROM comment
            WHERE comment.id = NEW.comment_reference;
        SELECT previousString || '/' || (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+/)*(\d+)', '\1', '')::int), 0) + 1)
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference = NEW.comment_reference);
    ELSE
        SELECT (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+/)*(\d+)', '\1', '')::int), 0) + 1)::text
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference IS NULL);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;