
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE frag_jetzt_credentials (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	email VARCHAR(255) NOT NULL,
	password VARCHAR(100) NOT NULL,
	activation_key VARCHAR(100),
	activation_key_time TIMESTAMP,
	password_reset_key VARCHAR(100),
	password_reset_time TIMESTAMP,

	PRIMARY KEY (id),
	UNIQUE (email)
);

-- If no special credentials are set (frag_jetzt_credentials) the account is a guest
CREATE TABLE account (
	id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	email VARCHAR(255),
	last_login TIMESTAMP,

	PRIMARY KEY (id),
	UNIQUE (email),
	FOREIGN KEY (email) REFERENCES frag_jetzt_credentials(email) ON DELETE CASCADE
);

CREATE TABLE room (
	id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	owner_id UUID NOT NULL,
	short_id TEXT NOT NULL,
	name VARCHAR(100) NOT NULL,
	description TEXT,
	closed BOOLEAN NOT NULL DEFAULT FALSE,
	moderated BOOLEAN NOT NULL DEFAULT TRUE,
	direct_send BOOLEAN NOT NULL,
	threshold INTEGER NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (short_id),
	FOREIGN KEY (owner_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX room_owner_id_idx ON room(owner_id);
CREATE INDEX room_short_id_idx ON room(short_id);

CREATE TABLE tag (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    room_id UUID NOT NULL,
    tag VARCHAR(20),

    PRIMARY KEY (id),
    UNIQUE (room_id, tag),
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TABLE room_access (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	room_id UUID NOT NULL,
	account_id UUID NOT NULL,
	role VARCHAR(20) NOT NULL,
	last_visit TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (room_id, account_id),
	FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
	FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX access_room_id_idx ON room_access(room_id);
CREATE INDEX access_account_id_idx ON room_access(account_id);

CREATE TABLE comment (
	id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	room_id UUID NOT NULL DEFAULT NULL,
	creator_id UUID NOT NULL DEFAULT NULL,
	number INTEGER NOT NULL,
	ack BOOLEAN NOT NULL,
	answer TEXT NULL DEFAULT NULL,
	body TEXT NULL DEFAULT NULL,
	correct INTEGER NOT NULL,
	favorite BOOLEAN NOT NULL,
	read BOOLEAN NOT NULL,
	tag VARCHAR(20) NULL DEFAULT NULL,
	timestamp TIMESTAMP NULL DEFAULT NULL,

	PRIMARY KEY (id),
	UNIQUE (room_id, number),
	FOREIGN KEY (creator_id) REFERENCES account(id) ON DELETE CASCADE,
	FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE INDEX comment_room_id_idx ON comment(room_id);
CREATE INDEX comment_creator_id_idx ON comment(creator_id);

CREATE TABLE bonus_token (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	comment_id UUID NOT NULL,
	room_id UUID NOT NULL,
	account_id UUID NOT NULL,
	token VARCHAR(255) NOT NULL,
	timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id),
	UNIQUE (comment_id, room_id, account_id),
	FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE,
	FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
	FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX token_comment_id_idx ON bonus_token(comment_id);
CREATE INDEX token_room_id_idx ON bonus_token(room_id);
CREATE INDEX token_account_id_idx ON bonus_token(account_id);

CREATE TABLE vote (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
	comment_id UUID NOT NULL,
	account_id UUID NOT NULL,
	vote INTEGER NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (comment_id, account_id),
	FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE,
	FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TABLE motd (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    msg_german TEXT NOT NULL,
    msg_english TEXT NOT NULL,
    start_timestamp TIMESTAMP NOT NULL,
    end_timestamp TIMESTAMP NOT NULL
);

CREATE INDEX vote_comment_id_idx ON vote(comment_id);
CREATE INDEX vote_account_id_idx ON vote(account_id);



CREATE OR REPLACE FUNCTION increase_comment_number()
    RETURNS trigger AS
$$
BEGIN
    SELECT COALESCE(MAX(comment.number), 0) + 1 INTO NEW.number FROM comment WHERE comment.room_id = NEW.room_id;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS ins_comment ON comment;
CREATE TRIGGER ins_comment
    BEFORE INSERT
    ON comment
    FOR EACH ROW
EXECUTE PROCEDURE increase_comment_number();
