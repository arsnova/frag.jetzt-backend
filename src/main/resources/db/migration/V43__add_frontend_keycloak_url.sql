ALTER TABLE keycloak_provider 
  ADD COLUMN frontend_url TEXT NOT NULL DEFAULT '/auth';