---------------------------------
-- E2E Tests for this version: --
--  - open existing room       --
--    - look if chatgpt works  --
--    - change quota           --
--  - create new room          --
--    - look if chatgpt works  --
--    - change quota           --
--  - create new user (guest)  --
--    - repeat steps above     --
--    - add as moderator       --
---------------------------------

-----------------------
-- delete quota data --
-----------------------

DELETE FROM quota;

---------------------------
-- create all gpt users  --
---------------------------

DO $$
DECLARE
  user_cursor CURSOR FOR
    SELECT id FROM account;
  user_ref UUID;
BEGIN
  OPEN user_cursor;

  LOOP
    FETCH user_cursor INTO user_ref;
    EXIT WHEN NOT FOUND;
    INSERT INTO gpt_user(account_id)
      VALUES (user_ref)
      ON CONFLICT (account_id) DO NOTHING;
  END LOOP;

  CLOSE user_cursor;
END $$;

----------------------------------
-- create all gpt room settings --
----------------------------------

DO $$
DECLARE
  room_cursor CURSOR FOR
    SELECT id FROM room;
  room_ref UUID;
BEGIN
  OPEN room_cursor;

  LOOP
    FETCH room_cursor INTO room_ref;
    EXIT WHEN NOT FOUND;
    INSERT INTO gpt_room_setting(room_id, rights_bitset, payment_counter)
      VALUES (room_ref, 127, 0)
      ON CONFLICT (room_id) DO NOTHING;
  END LOOP;

  CLOSE room_cursor;
END $$;

------------------------------
-- default assign for quota --
------------------------------

-- default quota assign for room settings
DO $$
DECLARE
  room_cursor CURSOR FOR
    SELECT room_id FROM gpt_room_setting;
  room_ref UUID;
  part_quota_id UUID;
  mod_quota_id UUID;
  all_quota_id UUID;
BEGIN
  OPEN room_cursor;

  LOOP
    FETCH room_cursor INTO room_ref;
    EXIT WHEN NOT FOUND;
    -- participant quota
    INSERT INTO quota(timezone)
      VALUES ('UTC')
      RETURNING id INTO part_quota_id;
    INSERT INTO quota_entry(quota_id, quota, counter, reset_counter, reset_strategy)
      VALUES (part_quota_id, -1, 0, 0, 'NEVER'),
        (part_quota_id, -1, 0, 0, 'MONTHLY'),
        (part_quota_id, -1, 0, 0, 'MONTHLY_FLOWING'),
        (part_quota_id, -1, 0, 0, 'DAILY');
    -- moderator quota
    INSERT INTO quota(timezone)
      VALUES ('UTC')
      RETURNING id INTO mod_quota_id;
    INSERT INTO quota_entry(quota_id, quota, counter, reset_counter, reset_strategy)
      VALUES (mod_quota_id, -1, 0, 0, 'NEVER'),
        (mod_quota_id, -1, 0, 0, 'MONTHLY'),
        (mod_quota_id, -1, 0, 0, 'MONTHLY_FLOWING'),
        (mod_quota_id, -1, 0, 0, 'DAILY');
    -- room quota
    INSERT INTO quota(timezone)
      VALUES ('UTC')
      RETURNING id INTO all_quota_id;
    INSERT INTO quota_entry(quota_id, quota, counter, reset_counter, reset_strategy)
      VALUES (all_quota_id, -1, 0, 0, 'NEVER'),
        (all_quota_id, -1, 0, 0, 'MONTHLY'),
        (all_quota_id, -1, 0, 0, 'MONTHLY_FLOWING'),
        (all_quota_id, -1, 0, 0, 'DAILY');
    -- update room setting
    UPDATE gpt_room_setting
      SET participant_quota_id = part_quota_id,
          moderator_quota_id = mod_quota_id,
          room_quota_id = all_quota_id
      WHERE room_id = room_ref;
  END LOOP;

  CLOSE room_cursor;
END $$;

--------------------------------
-- Restrict deletion of quota --
--------------------------------

-- in gpt_room_setting

ALTER TABLE gpt_room_setting
  DROP CONSTRAINT gpt_room_setting_participant_quota_id_fkey,
  DROP CONSTRAINT gpt_room_setting_moderator_quota_id_fkey,
  DROP CONSTRAINT gpt_room_setting_room_quota_id_fkey;

ALTER TABLE gpt_room_setting
  ADD CONSTRAINT gpt_room_setting_participant_quota_id_fkey
    FOREIGN KEY (participant_quota_id) REFERENCES quota(id) ON DELETE RESTRICT,
  ADD CONSTRAINT gpt_room_setting_moderator_quota_id_fkey
    FOREIGN KEY (moderator_quota_id) REFERENCES quota(id) ON DELETE RESTRICT,
  ADD CONSTRAINT gpt_room_setting_room_quota_id_fkey
    FOREIGN KEY (room_quota_id) REFERENCES quota(id) ON DELETE RESTRICT;

-- in gpt_voucher

ALTER TABLE gpt_voucher
  DROP CONSTRAINT gpt_voucher_quota_id_fkey;

ALTER TABLE gpt_voucher
  ADD CONSTRAINT gpt_voucher_quota_id_fkey
    FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE RESTRICT;

-- in gpt_api_setting

ALTER TABLE gpt_api_setting
  DROP CONSTRAINT gpt_api_setting_quota_id_fkey;

ALTER TABLE gpt_api_setting
  ADD CONSTRAINT gpt_api_setting_quota_id_fkey
    FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE RESTRICT;