CREATE TABLE uploaded_file (
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    account_id UUID NOT NULL,
    file_name VARCHAR(255) NOT NULL,
    file_info bytea NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_uploaded_file
    BEFORE INSERT OR UPDATE
    ON uploaded_file
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

-- assistants id -> room, threads id -> user, room

CREATE TABLE openai_assistants(
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    openai_id VARCHAR(255) NOT NULL,
    room_id UUID NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_openai_assistants
    BEFORE INSERT OR UPDATE
    ON openai_assistants
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE openai_threads(
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    openai_id VARCHAR(255) NOT NULL,
    account_id UUID NOT NULL,
    room_id UUID NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_openai_threads
    BEFORE INSERT OR UPDATE
    ON openai_threads
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();