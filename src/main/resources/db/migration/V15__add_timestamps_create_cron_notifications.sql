CREATE OR REPLACE FUNCTION trigger_timestamp_create_update_func()
    RETURNS trigger AS
$$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.created_at = NOW();
        NEW.updated_at = NULL;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.created_at <> NEW.created_at) THEN
            NEW.created_at = OLD.created_at;
        END IF;
        NEW.updated_at = NOW();
        RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

ALTER TABLE account
    ADD COLUMN last_active TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE account
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE account
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_account
    BEFORE INSERT OR UPDATE
    ON account
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE bonus_token
    RENAME COLUMN timestamp TO created_at;
ALTER TABLE bonus_token
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_bonus_token
    BEFORE INSERT OR UPDATE
    ON bonus_token
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE bookmark
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE bookmark
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_bookmark
    BEFORE INSERT OR UPDATE
    ON bookmark
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE brainstorming_session
    RENAME COLUMN started TO created_at;
ALTER TABLE brainstorming_session
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_brainstorming_session
    BEFORE INSERT OR UPDATE
    ON brainstorming_session
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE brainstorming_vote
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE brainstorming_vote
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_brainstorming_vote
    BEFORE INSERT OR UPDATE
    ON brainstorming_vote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE brainstorming_word
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE brainstorming_word
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_brainstorming_session
    BEFORE INSERT OR UPDATE
    ON brainstorming_word
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE comment
    RENAME COLUMN timestamp TO created_at;
ALTER TABLE comment
    ALTER COLUMN created_at SET DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE comment
    ALTER COLUMN created_at SET NOT NULL;
ALTER TABLE comment
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_comment
    BEFORE INSERT OR UPDATE
    ON comment
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE downvote
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE downvote
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_downvote
    BEFORE INSERT OR UPDATE
    ON downvote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE frag_jetzt_credentials
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE frag_jetzt_credentials
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_frag_jetzt_credentials
    BEFORE INSERT OR UPDATE
    ON frag_jetzt_credentials
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE motd
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE motd
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_motd
    BEFORE INSERT OR UPDATE
    ON motd
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE room
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE room
    ADD COLUMN updated_at TIMESTAMP NULL;
ALTER TABLE room
    ADD COLUMN last_visit_creator TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
CREATE TRIGGER trigger_timestamp_room
    BEFORE INSERT OR UPDATE
    ON room
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE room_access
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE room_access
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_room_access
    BEFORE INSERT OR UPDATE
    ON room_access
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE tag
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE tag
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_tag
    BEFORE INSERT OR UPDATE
    ON tag
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE upvote
    ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE upvote
    ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_upvote
    BEFORE INSERT OR UPDATE
    ON upvote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE comment_notification (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    account_id UUID NOT NULL,
    room_id UUID NOT NULL,
    -- first 11 bits for hour and minute (24 * 60 = 1440)
    -- next 3 bits for weekday (0-6)
    notification_setting SMALLINT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);
CREATE TRIGGER trigger_timestamp_comment_notification
    BEFORE INSERT OR UPDATE
    ON comment_notification
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();
CREATE OR REPLACE FUNCTION trigger_ensure_unique_days()
    RETURNS trigger AS
$$
DECLARE
    dayCount INTEGER;
BEGIN
    IF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') THEN
        dayCount = NEW.notification_setting >> 11;
        IF (dayCount > 6) THEN
            RAISE EXCEPTION 'Day is invalid! Must be between 0 and 6!';
        END IF;
        SELECT COUNT(*) INTO dayCount FROM comment_notification
            WHERE (room_id = NEW.room_id) AND (account_id = NEW.account_id);
        IF (dayCount > 7) THEN
            RAISE EXCEPTION 'Cannot create more than 7 notifications!';
        END IF;
        RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_ensure_unique_days_comment_notification
    BEFORE INSERT OR UPDATE
    ON comment_notification
    FOR EACH ROW
EXECUTE PROCEDURE trigger_ensure_unique_days();