UPDATE room
SET description = '[' || to_json(regexp_replace(description,
    '((\*|_|~)+(?=[^\*_~\s]))|((?<=[^\*_~\s])(\*|_|~)+)|(^[ \t]*#+ )|(^[ \t]*>(>| )*)|(`+)', '', 'gm') || chr(10)) || ']';