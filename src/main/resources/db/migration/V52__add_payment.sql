CREATE TABLE payment (
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    account_id UUID NOT NULL,
    amount NUMERIC(32, 16) NOT NULL,
    currency VARCHAR(15) NOT NULL,
    order_id VARCHAR(255) NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TRIGGER trigger_timestamp_payment
    BEFORE INSERT OR UPDATE
    ON payment
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();