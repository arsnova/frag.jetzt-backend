ALTER TABLE room
  ADD COLUMN radar_active BOOLEAN NOT NULL DEFAULT TRUE,
  ADD COLUMN focus_active BOOLEAN NOT NULL DEFAULT TRUE,
  ADD COLUMN chat_gpt_active BOOLEAN NOT NULL DEFAULT TRUE,
  ADD COLUMN mode VARCHAR(20) NOT NULL DEFAULT 'ARS';

CREATE TABLE quota (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  timezone VARCHAR(255) NOT NULL, -- See https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
  disabled BOOLEAN NOT NULL DEFAULT FALSE,
  max_request BIGINT NOT NULL DEFAULT -1, -- formatted with 10^-8 $

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id)
);

CREATE TRIGGER trigger_timestamp_quota
    BEFORE INSERT OR UPDATE
    ON quota
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE quota_entry (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  quota_id UUID NOT NULL,
  start_date TIMESTAMP NULL,
  end_date TIMESTAMP NULL,
  quota BIGINT NOT NULL, -- formatted with 10^-8 $
  counter BIGINT NOT NULL, -- formatted with 10^-8 $
  last_reset TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  reset_counter BIGINT NOT NULL,
  reset_strategy VARCHAR(255) NOT NULL,
  reset_factor INTEGER NOT NULL DEFAULT 1,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_quota_entry
    BEFORE INSERT OR UPDATE
    ON quota_entry
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE quota_access_time (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  quota_id UUID NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  recurring_strategy VARCHAR(255) NOT NULL,
  recurring_factor INTEGER NOT NULL DEFAULT 1,
  strategy VARCHAR(255) NOT NULL,
  start_time TIME NOT NULL,
  end_time TIME NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_quota_access_time
    BEFORE INSERT OR UPDATE
    ON quota_access_time
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_voucher (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  code VARCHAR(255) NOT NULL,
  account_id UUID NULL,
  quota_id UUID NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (code),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE SET NULL,
  FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE SET NULL
);

CREATE TRIGGER trigger_timestamp_gpt_voucher
    BEFORE INSERT OR UPDATE
    ON gpt_voucher
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_api_setting (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  quota_id UUID NULL,
  api_key VARCHAR(255) NOT NULL,
  api_organization VARCHAR(255) NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
  FOREIGN KEY (quota_id) REFERENCES quota(id) ON DELETE SET NULL
);

CREATE TRIGGER trigger_timestamp_gpt_api_setting
    BEFORE INSERT OR UPDATE
    ON gpt_api_setting
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

-- save valid old api settings to user keys (except vouchers)
INSERT INTO gpt_api_setting(account_id, api_key, api_organization)
  SELECT r.owner_id, g.api_key, g.api_organization
  FROM gpt_room_setting g INNER JOIN room r ON g.room_id = r.id
  WHERE g.api_key IS NOT NULL AND g.api_key LIKE 'sk-%';


ALTER TABLE gpt_room_setting
  ADD COLUMN default_model VARCHAR(255) NULL,
  ADD COLUMN room_quota_id UUID NULL REFERENCES quota(id) ON DELETE SET NULL,
  ADD COLUMN moderator_quota_id UUID NULL REFERENCES quota(id) ON DELETE SET NULL,
  ADD COLUMN participant_quota_id UUID NULL REFERENCES quota(id) ON DELETE SET NULL;

-- delete old keys and settings

ALTER TABLE gpt_room_setting
  DROP COLUMN api_key,
  DROP COLUMN api_organization,
  DROP COLUMN trial_enabled,
  DROP COLUMN max_daily_room_cost,
  DROP COLUMN max_monthly_room_cost,
  DROP COLUMN max_accumulated_room_cost,
  DROP COLUMN max_daily_participant_cost,
  DROP COLUMN max_monthly_participant_cost,
  DROP COLUMN max_accumulated_participant_cost,
  DROP COLUMN max_daily_moderator_cost,
  DROP COLUMN max_monthly_moderator_cost,
  DROP COLUMN max_accumulated_moderator_cost,
  DROP COLUMN max_monthly_flowing_room_cost,
  DROP COLUMN max_monthly_flowing_participant_cost,
  DROP COLUMN max_monthly_flowing_moderator_cost,
  DROP COLUMN daily_room_cost_counter,
  DROP COLUMN monthly_room_cost_counter,
  DROP COLUMN accumulated_room_cost_counter,
  DROP COLUMN daily_participant_cost_counter,
  DROP COLUMN monthly_participant_cost_counter,
  DROP COLUMN accumulated_participant_cost_counter,
  DROP COLUMN daily_moderator_cost_counter,
  DROP COLUMN monthly_moderator_cost_counter,
  DROP COLUMN accumulated_moderator_cost_counter,
  DROP COLUMN last_update;

-- add models for room

CREATE TABLE gpt_room_model (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  name VARCHAR(255) NOT NULL,
  index INT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_model
    BEFORE INSERT OR UPDATE
    ON gpt_room_model
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

-- add keys for room

CREATE TABLE gpt_room_key (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  api_setting_id UUID NULL,
  voucher_id UUID NULL,
  index INT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (setting_id, index),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE,
  FOREIGN KEY (api_setting_id) REFERENCES gpt_api_setting(id) ON DELETE CASCADE,
  FOREIGN KEY (voucher_id) REFERENCES gpt_voucher(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_key
    BEFORE INSERT OR UPDATE
    ON gpt_room_key
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

DROP TABLE gpt_room_usage_time;