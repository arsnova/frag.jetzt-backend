CREATE TABLE livepoll_session (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  room_id UUID NOT NULL,
  active BOOLEAN NOT NULL,
  template VARCHAR(63) NOT NULL,
  title VARCHAR(255) NULL,
  result_visible BOOLEAN NOT NULL,
  views_visible BOOLEAN NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_livepoll_session
    BEFORE INSERT OR UPDATE
    ON livepoll_session
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();