CREATE ROLE replicator WITH REPLICATION LOGIN PASSWORD 'replication_password';

-- Allow dumping of the database
GRANT CONNECT ON DATABASE fragjetzt TO replicator;
GRANT USAGE ON SCHEMA public TO replicator;
GRANT SELECT ON TABLE public.room, public.account, public.room_access, public.account_keycloak_role, public.flyway_schema_history TO replicator;

CREATE PUBLICATION fragjetzt_primary_publication FOR TABLE room, account, room_access, account_keycloak_role;