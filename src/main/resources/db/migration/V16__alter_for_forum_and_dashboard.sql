ALTER TABLE comment
    ADD comment_reference UUID NULL,
    ADD deleted_at TIMESTAMP NULL,
    ADD comment_depth INT NOT NULL DEFAULT 0;

CREATE OR REPLACE FUNCTION trigger_calculate_comment_depth_func()
    RETURNS trigger AS
$$
DECLARE
    tempDepth INTEGER;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        IF (NEW.comment_reference IS NOT NULL) THEN
            SELECT comment.comment_depth
                INTO tempDepth
                FROM comment
                WHERE id = NEW.comment_reference;
            tempDepth = tempDepth + 1;
            NEW.comment_depth = tempDepth;
        ELSE
            NEW.comment_depth = 0;
        END IF;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.comment_depth <> NEW.comment_depth) THEN
            NEW.comment_depth = OLD.comment_depth;
        END IF;
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trigger_calculate_comment_depth
    BEFORE INSERT OR UPDATE
    ON comment
    FOR EACH ROW
EXECUTE PROCEDURE trigger_calculate_comment_depth_func();

INSERT INTO comment (room_id, creator_id, number, ack, body, correct, favorite, read, keywords_from_questioner, keywords_from_spacy, comment_reference)
    (SELECT comment.room_id, room.owner_id as creator_id, 0 as number, comment.ack, comment.answer as body, 0 as correct,
       FALSE as favorite, FALSE as read, answer_questioner_keywords as keywords_from_questioner,
       answer_fulltext_keywords as keywords_from_spacy, comment.id as comment_reference
    FROM comment INNER JOIN room ON comment.room_id = room.id
    WHERE answer IS NOT NULL);

ALTER TABLE comment
    DROP answer,
    DROP answer_questioner_keywords,
    DROP answer_fulltext_keywords;

ALTER TABLE room
    ADD conversation_depth INT DEFAULT 0;

CREATE TABLE comment_change (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    comment_id UUID NOT NULL,
    room_id UUID NOT NULL,
    type VARCHAR(255) NOT NULL,
    previous_value_string TEXT NULL,
    previous_value_number INT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_comment_change
    BEFORE INSERT OR UPDATE
    ON comment_change
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE OR REPLACE FUNCTION trigger_create_comment_changes_func()
    RETURNS trigger AS
$$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO comment_change (comment_id, room_id, type)
            VALUES (NEW.id, NEW.room_id, 'CREATED');
        IF (NEW.comment_reference IS NOT NULL) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_string)
                VALUES (NEW.comment_reference, NEW.room_id, 'ANSWERED', NEW.id::text);
        END IF;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.deleted_at <> NEW.deleted_at) THEN
            INSERT INTO comment_change (comment_id, room_id, type)
                VALUES (NEW.id, NEW.room_id, 'DELETED');
        END IF;
        IF (OLD.ack <> NEW.ack) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_number)
                VALUES (NEW.id, NEW.room_id, 'CHANGE_ACK', OLD.ack::int);
        END IF;
        IF (OLD.favorite <> NEW.favorite) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_number)
                VALUES (NEW.id, NEW.room_id, 'CHANGE_FAVORITE', OLD.favorite::int);
        END IF;
        IF (OLD.correct <> NEW.correct) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_number)
                VALUES (NEW.id, NEW.room_id, 'CHANGE_CORRECT', OLD.correct);
        END IF;
        IF (OLD.tag <> NEW.tag) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_string)
                VALUES (NEW.id, NEW.room_id, 'CHANGE_TAG', OLD.tag);
        END IF;
        IF (OLD.score <> NEW.score) THEN
            INSERT INTO comment_change (comment_id, room_id, type, previous_value_number)
                VALUES (NEW.id, NEW.room_id, 'CHANGE_SCORE', OLD.score);
        END IF;
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trigger_comment_change_create
    AFTER INSERT OR UPDATE
    ON comment
    FOR EACH ROW
EXECUTE PROCEDURE trigger_create_comment_changes_func();

CREATE TABLE comment_change_subscription (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    comment_id UUID NOT NULL,
    room_id UUID NOT NULL,
    account_id UUID NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (account_id, comment_id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
    FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE
);

CREATE TABLE room_comment_change_subscription (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    room_id UUID NOT NULL,
    account_id UUID NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (account_id, room_id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);