ALTER TABLE room
  ADD COLUMN language VARCHAR(255) NULL DEFAULT NULL;

CREATE TABLE brainstorming_category (
  id UUID NOT NULL DEFAULT uuid_generate_v1 (),
  room_id UUID NOT NULL,
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,

  PRIMARY KEY (id),
  UNIQUE (name, room_id),
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_brainstorming_category
    BEFORE INSERT OR UPDATE
    ON brainstorming_category
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE brainstorming_word
  ADD COLUMN banned BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE brainstorming_word
  ADD COLUMN category_id UUID NULL DEFAULT NULL
  CONSTRAINT brainstorming_word_category_id_fkey REFERENCES brainstorming_category(id)
    ON DELETE SET NULL;
ALTER TABLE brainstorming_word
    ADD COLUMN corrected_word VARCHAR(255) NULL;

DELETE FROM brainstorming_session WHERE TRUE;
ALTER TABLE brainstorming_session
    ADD COLUMN language VARCHAR(255) NOT NULL;
ALTER TABLE brainstorming_session
    ADD COLUMN rating_allowed BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE brainstorming_session
    ADD COLUMN ideas_frozen BOOLEAN NOT NULL DEFAULT TRUE;
ALTER TABLE brainstorming_session
    ADD COLUMN ideas_time_duration INTEGER NULL;
ALTER TABLE brainstorming_session
    ADD COLUMN ideas_end_timestamp TIMESTAMP NULL;
ALTER TABLE brainstorming_session
    DROP CONSTRAINT brainstorming_session_room_id_key;

DELETE FROM comment
    WHERE brainstorming_question = TRUE;
ALTER TABLE comment
    DROP COLUMN brainstorming_question;
ALTER TABLE comment
    ADD COLUMN brainstorming_session_id UUID DEFAULT NULL
    CONSTRAINT comment_brainstorming_session_id_fkey REFERENCES brainstorming_session(id)
    ON DELETE CASCADE;
ALTER TABLE comment
    ADD COLUMN brainstorming_word_id UUID DEFAULT NULL
    CONSTRAINT comment_brainstorming_word_id_fkey REFERENCES brainstorming_word(id)
    ON DELETE CASCADE;