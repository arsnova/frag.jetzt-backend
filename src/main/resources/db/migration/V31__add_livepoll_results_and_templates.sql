ALTER TABLE livepoll_session
  ADD COLUMN paused BOOLEAN NOT NULL DEFAULT FALSE;

CREATE TABLE livepoll_vote (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  session_id UUID NOT NULL,
  vote_index INT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE(account_id, session_id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
  FOREIGN KEY (session_id) REFERENCES livepoll_session(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_livepoll_vote
    BEFORE INSERT OR UPDATE
    ON livepoll_vote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE livepoll_custom_template_entry (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  session_id UUID NOT NULL,
  index INT NOT NULL,
  icon VARCHAR(127) NULL,
  text VARCHAR(255) NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE(index, session_id),
  FOREIGN KEY (session_id) REFERENCES livepoll_session(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_livepoll_custom_template_entry
    BEFORE INSERT OR UPDATE
    ON livepoll_custom_template_entry
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();