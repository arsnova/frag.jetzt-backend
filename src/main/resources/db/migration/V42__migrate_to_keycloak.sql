CREATE TABLE keycloak_provider (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  priority INT NOT NULL DEFAULT 0,
  url TEXT NOT NULL,
  event_password VARCHAR(255) NOT NULL,
  realm VARCHAR(255) NOT NULL,
  client_id VARCHAR(255) NOT NULL,
  name_de TEXT NOT NULL,
  name_en TEXT NOT NULL,
  name_fr TEXT NOT NULL,
  description_de TEXT NOT NULL,
  description_en TEXT NOT NULL,
  description_fr TEXT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id)
);

CREATE TRIGGER trigger_timestamp_keycloak_provider
    BEFORE INSERT OR UPDATE
    ON keycloak_provider
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE account DROP CONSTRAINT account_email_key;
ALTER TABLE account DROP CONSTRAINT account_email_fkey;

ALTER TABLE account
  ADD COLUMN keycloak_id UUID NULL
  CONSTRAINT account_keycloak_id_fkey REFERENCES keycloak_provider(id)
  ON DELETE CASCADE;

ALTER TABLE account
  ADD COLUMN keycloak_user_id UUID NULL;

ALTER TABLE account
  ADD CONSTRAINT account_keycloak_id_keycloak_user_id_key UNIQUE (keycloak_id, keycloak_user_id);

ALTER TABLE account
  ADD CONSTRAINT account_email_keycloak_id_key UNIQUE (email, keycloak_id);

CREATE TABLE account_keycloak_role(
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  role VARCHAR(255) NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
  UNIQUE (account_id, role)
);

-- default keycloak assign
DO $$
DECLARE default_id UUID;
BEGIN
  INSERT INTO keycloak_provider(url, event_password, realm, client_id, name_de, name_en, name_fr, description_de, description_en, description_fr)
    VALUES ('http://fragjetzt-keycloak:8080/auth', 'CHANGE ME', 'fragjetzt', 'frag.jetzt-frontend', '', '', '', '', '', '')
    RETURNING id INTO default_id;
  
  UPDATE account
    SET keycloak_id = default_id
    WHERE email IS NOT NULL;
END $$;

-- drop old data
DROP TABLE frag_jetzt_credentials;
