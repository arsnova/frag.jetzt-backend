ALTER TABLE motd
  ADD PRIMARY KEY (id);

CREATE TABLE motd_message (
  id UUID NOT NULL DEFAULT uuid_generate_v1 (),
  motd_id UUID NOT NULL,
  language VARCHAR(255) NOT NULL,
  message TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,

  PRIMARY KEY (id),
  UNIQUE (motd_id, language),
  FOREIGN KEY (motd_id) REFERENCES motd(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_motd_message
    BEFORE INSERT OR UPDATE
    ON motd_message
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

INSERT INTO motd_message(motd_id, language, message)
  SELECT id, 'de', msg_german FROM motd;
INSERT INTO motd_message(motd_id, language, message)
  SELECT id, 'en', msg_english FROM motd;

ALTER TABLE motd
  DROP COLUMN msg_german,
  DROP COLUMN msg_english;

ALTER TABLE rating
  ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE rating
  ADD COLUMN updated_at TIMESTAMP NULL;
CREATE TRIGGER trigger_timestamp_rating
    BEFORE INSERT OR UPDATE
    ON rating
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();