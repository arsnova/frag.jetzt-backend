ALTER TABLE gpt_room_setting
  ADD COLUMN role_instruction TEXT NULL;

DELETE FROM gpt_request_statistic WHERE TRUE;

ALTER TABLE gpt_request_statistic
  DROP COLUMN prompt_length,
  DROP COLUMN prompt_tokens,
  DROP COLUMN response_code,
  DROP COLUMN completion_length,
  DROP COLUMN completion_tokens,
  DROP COLUMN completion_count;

ALTER TABLE gpt_request_statistic
  ADD COLUMN prompt TEXT NOT NULL,
  ADD COLUMN completion TEXT NOT NULL,
  ADD COLUMN room_id UUID NULL;