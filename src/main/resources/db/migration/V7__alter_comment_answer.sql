UPDATE comment
SET answer = '[' || to_json(regexp_replace(answer,
    '((\*|_|~)+(?=[^\*_~\s]))|((?<=[^\*_~\s])(\*|_|~)+)|(^[ \t]*#+ )|(^[ \t]*>(>| )*)|(`+)', '', 'gm') || chr(10)) || ']';