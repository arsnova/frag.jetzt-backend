ALTER TABLE keycloak_provider 
  ADD COLUMN allowed_ips TEXT NOT NULL DEFAULT '';

UPDATE keycloak_provider
  SET allowed_ips = 'fragjetzt-keycloak'
  WHERE name_de = '';