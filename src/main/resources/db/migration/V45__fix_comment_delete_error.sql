ALTER TABLE comment
  DROP CONSTRAINT comment_reference_constraint,
  ADD CONSTRAINT comment_reference_constraint
   FOREIGN KEY (comment_reference)
   REFERENCES comment(id)
   ON DELETE CASCADE;