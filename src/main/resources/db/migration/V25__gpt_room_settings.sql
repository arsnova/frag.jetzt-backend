-- update live polling to true
UPDATE room SET livepoll_active = TRUE;
ALTER TABLE room ALTER COLUMN livepoll_active SET DEFAULT TRUE;

-- gpt migrations

CREATE TABLE gpt_room_setting (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  room_id UUID NOT NULL,
  api_key VARCHAR(255) NULL,
  api_organization VARCHAR(255) NULL,
  trial_enabled BOOLEAN NOT NULL DEFAULT FALSE,
  max_daily_room_quota INT NULL,
  max_monthly_room_quota INT NULL,
  max_accumulated_room_quota BIGINT NULL,
  max_daily_participant_quota INT NULL,
  max_monthly_participant_quota INT NULL,
  max_accumulated_participant_quota BIGINT NULL,
  max_daily_moderator_quota INT NULL,
  max_monthly_moderator_quota INT NULL,
  max_accumulated_moderator_quota BIGINT NULL,
  -- stats
  daily_quota_counter INT NOT NULL,
  monthly_quota_counter INT NOT NULL,
  accumulated_quota_counter BIGINT NOT NULL,
  daily_cost_counter BIGINT NOT NULL, -- currency is modelled in 1*10^-8 US Dollar $
  monthly_cost_counter BIGINT NOT NULL, -- currency is modelled in 1*10^-8 US Dollar $
  accumulated_cost_counter BIGINT NOT NULL, -- currency is modelled in 1*10^-8 US Dollar $
  last_quota_usage TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  -- rights
  rights_bitset INT NOT NULL,
  -- payment
  payment_counter BIGINT NOT NULL, -- currency is modelled in 1*10^-8 US Dollar $


  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (room_id),
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_setting
    BEFORE INSERT OR UPDATE
    ON gpt_room_setting
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_room_keyword (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  keyword VARCHAR(255) NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_keyword
    BEFORE INSERT OR UPDATE
    ON gpt_room_keyword
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_room_user_description (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  room_id UUID NOT NULL,
  description TEXT NULL,
  -- stats
  daily_quota_counter INT NOT NULL,
  monthly_quota_counter INT NOT NULL,
  accumulated_quota_counter BIGINT NOT NULL,
  last_quota_usage TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_user_description
    BEFORE INSERT OR UPDATE
    ON gpt_room_user_description
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_room_usage_time (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  repeat_duration INT NULL,
  repeat_unit VARCHAR(63) NULL,
  start_date TIMESTAMP NOT NULL,
  end_date TIMESTAMP NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_usage_time
    BEFORE INSERT OR UPDATE
    ON gpt_room_usage_time
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();