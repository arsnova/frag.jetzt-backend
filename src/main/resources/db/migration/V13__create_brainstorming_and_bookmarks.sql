ALTER TABLE comment
    ALTER COLUMN questioner_name TYPE VARCHAR(127),
    ADD COLUMN answer_questioner_keywords TEXT NOT NULL default '[]',
    ADD COLUMN answer_fulltext_keywords TEXT NOT NULL default '[]';

CREATE TABLE bookmark (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    account_id UUID NOT NULL DEFAULT NULL,
    room_id UUID NOT NULL DEFAULT NULL,
    comment_id UUID NOT NULL DEFAULT NULL,

    PRIMARY KEY (id),
    UNIQUE (account_id, comment_id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
    FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE
);

CREATE TABLE brainstorming_session (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    room_id UUID NOT NULL DEFAULT NULL,
    title VARCHAR(255) NOT NULL DEFAULT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    started TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    max_word_length INTEGER NOT NULL DEFAULT 20,
    max_word_count INTEGER NOT NULL DEFAULT 1,

    PRIMARY KEY (id),
    UNIQUE (room_id), -- maximum 1 session per room
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TABLE brainstorming_word (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    session_id UUID NOT NULL DEFAULT NULL,
    word VARCHAR(255) NOT NULL,
    upvotes INTEGER NOT NULL DEFAULT 0,
    downvotes INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),
    UNIQUE (session_id, word),
    FOREIGN KEY (session_id) REFERENCES brainstorming_session(id) ON DELETE CASCADE
);

CREATE TABLE brainstorming_vote (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    account_id UUID NOT NULL DEFAULT NULL,
    word_id UUID NOT NULL DEFAULT NULL,
    is_upvote BOOLEAN NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (account_id, word_id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (word_id) REFERENCES brainstorming_word(id) ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION trigger_brainstorming_vote_func()
    RETURNS trigger AS
$$
DECLARE
    tempUpvotes INTEGER;
    tempDownvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR (TG_OP = 'UPDATE') THEN
        BEGIN
            SELECT upvotes, downvotes INTO tempUpvotes, tempDownvotes FROM brainstorming_word WHERE id = OLD.word_id;
            IF OLD.is_upvote THEN
                tempUpvotes = tempUpvotes - 1;
            ELSE
                tempDownvotes = tempDownvotes - 1;
            END IF;
            UPDATE brainstorming_word
            SET upvotes = tempUpvotes, downvotes = tempDownvotes
            WHERE id = OLD.word_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') THEN
        SELECT upvotes, downvotes INTO tempUpvotes, tempDownvotes FROM brainstorming_word WHERE id = NEW.word_id;
        IF NEW.is_upvote THEN
            tempUpvotes = tempUpvotes + 1;
        ELSE
            tempDownvotes = tempDownvotes + 1;
        END IF;
        UPDATE brainstorming_word
        SET upvotes = tempUpvotes, downvotes = tempDownvotes
        WHERE id = NEW.word_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_brainstorming_vote ON brainstorming_vote;
CREATE TRIGGER trigger_brainstorming_vote
    AFTER INSERT OR UPDATE OR DELETE
    ON brainstorming_vote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_brainstorming_vote_func();
