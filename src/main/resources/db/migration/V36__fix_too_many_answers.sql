ALTER TABLE livepoll_session
  ADD COLUMN answer_count INT NOT NULL DEFAULT 4;

ALTER TABLE livepoll_session
  ALTER COLUMN title TYPE TEXT;