CREATE TABLE gpt_user (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  daily_quota INT NOT NULL DEFAULT 0,
  weekly_quota INT NOT NULL DEFAULT 0,
  accumulated_quota BIGINT NOT NULL DEFAULT 0,
  last_quota_usage TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  blocked BOOLEAN NOT NULL DEFAULT FALSE,
  blocked_end_timestamp TIMESTAMP NULL DEFAULT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (account_id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_user
    BEFORE INSERT OR UPDATE
    ON gpt_user
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_request_statistic (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  requester_id UUID NULL,
  prompt_length INT NOT NULL,
  prompt_tokens INT NOT NULL,
  response_code INT NOT NULL,
  completion_length INT NOT NULL,
  completion_tokens INT NOT NULL,
  completion_count INT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (requester_id) REFERENCES gpt_user(id) ON DELETE SET NULL
);

CREATE TRIGGER trigger_timestamp_gpt_request_statistic
    BEFORE INSERT OR UPDATE
    ON gpt_request_statistic
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();