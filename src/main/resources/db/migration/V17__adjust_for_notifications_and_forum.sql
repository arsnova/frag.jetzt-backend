-- remove comment change trigger
DROP TRIGGER trigger_comment_change_create ON comment;
DROP FUNCTION trigger_create_comment_changes_func;

-- Add initiator_id to comment_change
DELETE FROM comment_change
    WHERE TRUE;
ALTER TABLE comment_change
    ADD initiator_id UUID NOT NULL,
    ADD CONSTRAINT comment_change_initiator_constraint
        FOREIGN KEY (initiator_id) REFERENCES account(id),
    ADD initiator_role VARCHAR(20) NOT NULL,
    ADD current_value_string TEXT NULL,
    DROP previous_value_number;

-- change comment number to type string, e. g. 123.2.1
ALTER TABLE comment
    ALTER COLUMN number TYPE TEXT;

CREATE OR REPLACE FUNCTION increase_comment_number()
    RETURNS trigger AS
$$
DECLARE
    previousString TEXT;
BEGIN
    IF (NEW.comment_reference IS NOT NULL) THEN
        SELECT comment.number
            INTO previousString
            FROM comment
            WHERE comment.id = NEW.comment_reference;
        SELECT previousString || '.' || (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+\.)*(\d+)', '\1', '')::int), 0) + 1)
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference = NEW.comment_reference);
    ELSE
        SELECT (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+\.)*(\d+)', '\1', '')::int), 0) + 1)::text
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference IS NULL);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS ins_comment ON comment;
CREATE TRIGGER ins_comment
    BEFORE INSERT
    ON comment
    FOR EACH ROW
    EXECUTE PROCEDURE increase_comment_number();

-- update comment reference foreign keys
ALTER TABLE comment
    ADD CONSTRAINT comment_reference_constraint
        FOREIGN KEY (comment_reference) REFERENCES comment(id);

-- let rerun triggers, for recalculating comment numbers
CREATE TEMPORARY TABLE comment_migration AS
    (SELECT *
        FROM comment
        WHERE comment_reference IS NOT NULL
        ORDER BY created_at ASC);
DELETE FROM comment
    WHERE comment_reference IS NOT NULL;
INSERT INTO comment
    (SELECT * FROM comment_migration);

