ALTER TABLE gpt_room_setting
  ADD COLUMN max_monthly_flowing_room_cost INT NULL,
  ADD COLUMN max_monthly_flowing_participant_cost INT NULL,
  ADD COLUMN max_monthly_flowing_moderator_cost INT NULL;