ALTER TABLE comment
DROP COLUMN keywords;

ALTER TABLE comment
ADD COLUMN keywords_from_questioner TEXT NOT NULL default '[]';

ALTER TABLE comment
ADD COLUMN keywords_from_spacy TEXT NOT NULL default '[]';

ALTER TABLE comment
ADD COLUMN score INTEGER NOT NULL DEFAULT 0;

ALTER TABLE comment
ADD COLUMN upvotes INTEGER NOT NULL DEFAULT 0;

ALTER TABLE comment
ADD COLUMN downvotes INTEGER NOT NULL DEFAULT 0;

ALTER TABLE room
ADD COLUMN blacklist TEXT;

ALTER TABLE room
ADD COLUMN questions_blocked BOOLEAN NOT NULL default FALSE;

CREATE TABLE upvote (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    comment_id UUID NOT NULL,
    account_id UUID NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (comment_id, account_id),
    FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX upvote_comment_id_idx ON upvote(comment_id);
CREATE INDEX upvote_account_id_idx ON upvote(account_id);

CREATE OR REPLACE FUNCTION trigger_comment_upvotes()
    RETURNS trigger AS
$$
DECLARE
    newScore INTEGER;
    newUpvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        BEGIN
            SELECT score - 1, upvotes - 1 INTO newScore, newUpvotes FROM comment WHERE comment.id = OLD.comment_id;
            UPDATE comment
            SET score = newScore, upvotes = newUpvotes
            WHERE comment.id = OLD.comment_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        SELECT score + 1, upvotes + 1 INTO newScore, newUpvotes FROM comment WHERE comment.id = NEW.comment_id;
        UPDATE comment
        SET score = newScore, upvotes = newUpvotes
        WHERE comment.id = NEW.comment_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_upvote ON upvote;
CREATE TRIGGER trigger_upvote
    AFTER INSERT OR UPDATE OR DELETE
    ON upvote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_comment_upvotes();

INSERT INTO upvote(comment_id, account_id) SELECT vote.comment_id, vote.account_id FROM vote WHERE vote.vote > 0;

CREATE TABLE downvote (
    id UUID NOT NULL DEFAULT uuid_generate_v1 (),
    comment_id UUID NOT NULL,
    account_id UUID NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (comment_id, account_id),
    FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX downvote_comment_id_idx ON downvote(comment_id);
CREATE INDEX downvote_account_id_idx ON downvote(account_id);

CREATE OR REPLACE FUNCTION trigger_comment_downvotes()
    RETURNS trigger AS
$$
DECLARE
    newScore INTEGER;
    newDownvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        BEGIN
            SELECT score + 1, downvotes - 1 INTO newScore, newDownvotes FROM comment WHERE comment.id = OLD.comment_id;
            UPDATE comment
            SET score = newScore, downvotes = newDownvotes
            WHERE comment.id = OLD.comment_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        SELECT score - 1, downvotes + 1 INTO newScore, newDownvotes FROM comment WHERE comment.id = NEW.comment_id;
        UPDATE comment
        SET score = newScore, downvotes = newDownvotes
        WHERE comment.id = NEW.comment_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_downvote ON downvote;
CREATE TRIGGER trigger_downvote
    AFTER INSERT OR UPDATE OR DELETE
    ON downvote
    FOR EACH ROW
EXECUTE PROCEDURE trigger_comment_downvotes();

INSERT INTO downvote(comment_id, account_id) SELECT vote.comment_id, vote.account_id FROM vote WHERE vote.vote < 0;

DROP TABLE vote;