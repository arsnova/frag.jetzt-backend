CREATE TABLE web_subscription (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  endpoint TEXT NOT NULL,
  key TEXT NOT NULL,
  auth TEXT NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_web_subscription
    BEFORE INSERT OR UPDATE
    ON web_subscription
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE web_notification_setting(
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  debounce_time_seconds INTEGER NOT NULL DEFAULT 0,
  language VARCHAR(15) NOT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_web_notification_setting
    BEFORE INSERT OR UPDATE
    ON web_notification_setting
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE push_room_subscription (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  room_id UUID NOT NULL,
  own_comment_bits BIGINT NOT NULL DEFAULT 0,
  other_comment_bits BIGINT NOT NULL DEFAULT 0,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_push_room_subscription
    BEFORE INSERT OR UPDATE
    ON push_room_subscription
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE push_comment_subscription (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  account_id UUID NOT NULL,
  room_id UUID NOT NULL,
  comment_id UUID NOT NULL,
  interest_bits BIGINT NOT NULL DEFAULT 0,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
  FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE CASCADE,
  FOREIGN KEY (comment_id) REFERENCES comment(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_push_comment_subscription
    BEFORE INSERT OR UPDATE
    ON push_comment_subscription
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

DROP TABLE comment_change_subscription;
DROP TABLE room_comment_change_subscription;
DELETE FROM comment_change WHERE TRUE;

ALTER TABLE comment_change
  ADD COLUMN comment_creator_id UUID NOT NULL;