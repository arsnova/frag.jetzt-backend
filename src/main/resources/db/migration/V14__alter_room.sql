ALTER TABLE room
ADD COLUMN moderator_room_reference UUID NULL DEFAULT NULL;

ALTER TABLE room
ADD CONSTRAINT "fk_moderator_code_room" FOREIGN KEY (moderator_room_reference) REFERENCES room(id) ON DELETE CASCADE;

ALTER TABLE room
ADD CONSTRAINT "unique_moderator_code_room" UNIQUE (moderator_room_reference);