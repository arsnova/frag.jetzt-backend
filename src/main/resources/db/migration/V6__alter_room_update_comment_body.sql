ALTER TABLE room
ADD COLUMN tag_cloud_settings TEXT;

-- removes some markdown and converts to json
UPDATE comment
SET body = '[' || to_json(regexp_replace(body,
    '((\*|_|~)+(?=[^\*_~\s]))|((?<=[^\*_~\s])(\*|_|~)+)|(^[ \t]*#+ )|(^[ \t]*>(>| )*)|(`+)', '', 'gm') || chr(10)) || ']';