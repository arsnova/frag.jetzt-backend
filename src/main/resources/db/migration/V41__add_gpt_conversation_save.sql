CREATE TABLE gpt_conversation (
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    account_id UUID NOT NULL,
    room_id UUID NULL,

    model VARCHAR(127) NOT NULL,
    temperature REAL NULL,
    top_p REAL NULL,
    presence_penalty REAL NULL,
    frequency_penalty REAL NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(id) ON DELETE SET NULL
);

CREATE TRIGGER trigger_timestamp_gpt_conversation
    BEFORE INSERT OR UPDATE
    ON gpt_conversation
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_conversation_entry (
    id UUID NOT NULL DEFAULT uuid_generate_v1(),
    conversation_id UUID NOT NULL,
    index INT NOT NULL,

    role VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    name VARCHAR(255) NULL,
    function_call TEXT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL,
    PRIMARY KEY (id),
    UNIQUE (conversation_id, index),
    FOREIGN KEY (conversation_id) REFERENCES gpt_conversation(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_conversation_entry
    BEFORE INSERT OR UPDATE
    ON gpt_conversation_entry
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();