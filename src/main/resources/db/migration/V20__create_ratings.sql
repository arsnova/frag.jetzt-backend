CREATE TABLE rating (
  id UUID NOT NULL DEFAULT uuid_generate_v1 (),
  account_id UUID NOT NULL,
  rating FLOAT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (account_id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);