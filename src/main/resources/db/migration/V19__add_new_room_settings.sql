ALTER TABLE room DISABLE TRIGGER USER;

ALTER TABLE room
    ADD COLUMN quiz_active BOOLEAN DEFAULT TRUE,
    ADD COLUMN brainstorming_active BOOLEAN DEFAULT TRUE;
ALTER TABLE room
  RENAME moderated TO bonus_archive_active;
ALTER TABLE room
  RENAME blacklist_is_active TO blacklist_active;

UPDATE room
  SET bonus_archive_active = TRUE
  WHERE TRUE;

ALTER TABLE room ENABLE TRIGGER USER;