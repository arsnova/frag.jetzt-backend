CREATE TABLE gpt_rating (
  id UUID NOT NULL DEFAULT uuid_generate_v1 (),
  account_id UUID NOT NULL,
  rating FLOAT NOT NULL,
  rating_text TEXT NULL,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (account_id),
  FOREIGN KEY (account_id) REFERENCES account(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_rating
    BEFORE INSERT OR UPDATE
    ON gpt_rating
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

ALTER TABLE comment
  ADD COLUMN approved BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN gpt_writer_state INTEGER NOT NULL DEFAULT 0;