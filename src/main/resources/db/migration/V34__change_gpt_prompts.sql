-- delete everything
DELETE FROM gpt_prompt_preset WHERE TRUE;

ALTER TABLE gpt_prompt_preset
  ADD COLUMN language VARCHAR(127) NOT NULL,
  ADD COLUMN temperature REAL NOT NULL DEFAULT 0.7,
  ADD COLUMN presence_penalty REAL NOT NULL DEFAULT 0,
  ADD COLUMN frequency_penalty REAL NOT NULL DEFAULT 0;

ALTER TABLE comment_change
  DROP CONSTRAINT comment_change_initiator_constraint;

ALTER TABLE comment_change
  ADD CONSTRAINT comment_change_initiator_constraint
    FOREIGN KEY (initiator_id) REFERENCES account(id) ON DELETE NO ACTION;