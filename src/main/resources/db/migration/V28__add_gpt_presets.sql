DROP TABLE gpt_room_keyword;

ALTER TABLE gpt_room_setting
  ADD COLUMN preset_context VARCHAR(510) NOT NULL DEFAULT '',
  ADD COLUMN preset_persona_creator VARCHAR(510) NOT NULL DEFAULT '',
  ADD COLUMN preset_persona_moderator VARCHAR(510) NOT NULL DEFAULT '',
  ADD COLUMN preset_persona_participant VARCHAR(510) NOT NULL DEFAULT '',
  ADD COLUMN preset_language VARCHAR(127) NOT NULL DEFAULT '',
  ADD COLUMN preset_formal BOOLEAN NULL DEFAULT NULL,
  ADD COLUMN preset_length VARCHAR(255) NOT NULL DEFAULT '';


CREATE TABLE gpt_room_preset_topic (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  description VARCHAR(255) NOT NULL,
  active BOOLEAN NOT NULL DEFAULT TRUE,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (setting_id, description),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_preset_topic
    BEFORE INSERT OR UPDATE
    ON gpt_room_preset_topic
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();

CREATE TABLE gpt_room_preset_tone (
  id UUID NOT NULL DEFAULT uuid_generate_v1(),
  setting_id UUID NOT NULL,
  description VARCHAR(127) NOT NULL,
  active BOOLEAN NOT NULL DEFAULT TRUE,

  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL,
  PRIMARY KEY (id),
  UNIQUE (setting_id, description),
  FOREIGN KEY (setting_id) REFERENCES gpt_room_setting(id) ON DELETE CASCADE
);

CREATE TRIGGER trigger_timestamp_gpt_room_preset_tone
    BEFORE INSERT OR UPDATE
    ON gpt_room_preset_tone
    FOR EACH ROW
EXECUTE PROCEDURE trigger_timestamp_create_update_func();